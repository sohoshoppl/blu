<?php


define('PS_DB', 'mysql:host=blu.dkonto.pl.mysql.dhosting.pl;dbname=riep3r_bludkont');
define('PS_DB_LOGIN', 'oong7e_bludkont');
define('PS_DB_PSWD', 'Fa0raegi7Kee');


require dirname(__FILE__) . '/config/config.inc.php';
/*
define('PS_DB', 'mysql:host=localhost;dbname=blue');
define('PS_DB_LOGIN', 'root');
define('PS_DB_PSWD', '');
*/

error_reporting(E_ALL);
ini_set('display_errors', 1);

// ustawienie kodowania znaków na UTF-8
mb_internal_encoding('UTF-8');

$file = fopen('aktualnosci_import.csv', 'r');

$db = new PDO(PS_DB, PS_DB_LOGIN, PS_DB_PSWD);
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$db->exec("set names utf8");


$sql_query_insert_post = "INSERT IGNORE INTO `ps_ybc_blog_post` (`id_category_default`, `is_featured`, `products`, `added_by`, `is_customer`, `modified_by`, `enabled`, `datetime_added`, `datetime_modified`, `datetime_active`, `sort_order`, `click_number`, `likes`) VALUES (:id_category_default, :is_featured, :products, :added_by, :is_customer, :modified_by, :enabled, :datetime_added, :datetime_modified, :datetime_active, :sort_order, :click_number, :likes);";
$sth_query_insert_post = $db->prepare($sql_query_insert_post);

$sql_query_insert_tag = "INSERT IGNORE INTO `ps_ybc_blog_tag` (`id_post`, `id_lang`, `tag`, `click_number`) VALUES (:id_post, :id_lang, :tag, :click_number);";
$sth_query_insert_tag = $db->prepare($sql_query_insert_tag);

$sql_query_insert_post_lang = "INSERT IGNORE INTO `ps_ybc_blog_post_lang` (`id_post`, `id_lang`, `title`, `url_alias`, `meta_title`, `description`, `short_description`, `meta_keywords`, `meta_description`, `thumb`, `image`) VALUES (:id_post, :id_lang, :title, :url_alias, :meta_title, :description, :short_description, :meta_keywords, :meta_description, :thumb, :image)";
$sth_query_insert_post_lang = $db->prepare($sql_query_insert_post_lang);

$sql_query_insert_post_shop = "INSERT IGNORE INTO `ps_ybc_blog_post_shop` (`id_post`, `id_shop`) VALUES (:id_post, :id_shop) ;";
$sth_query_insert_post_shop = $db->prepare($sql_query_insert_post_shop);

$sql_query_insert_post_category = "INSERT IGNORE INTO `ps_ybc_blog_post_category` (`id_post`, `id_category`, `position`) VALUES (:id_post, :id_category, :position)";
$sth_query_insert_post_category = $db->prepare($sql_query_insert_post_category);

$post_sort_order = 18;
$post_category_pos = 0;
$i = 0;

if (!defined('_PS_YBC_BLOG_IMG_DIR_')) {
    define('_PS_YBC_BLOG_IMG_DIR_', _PS_IMG_DIR_ . 'ybc_blog/');
}

while (($line = fgetcsv($file, 0, ';')) !== FALSE) {
    if ($i < 1) {
        $i++;
        continue;
    } else {
        $i++;

        $post_sort_order++;
        $post_category_pos++;
        echo '<pre>';

        $insert = [':id_category_default' => 28, ':is_featured' => 0, ':products' => '', ':added_by' => 1, ':is_customer' => 0, ':modified_by' => 1, ':enabled' => 1, ':datetime_added' => date('Y-m-d H:i:s', strtotime($line[3])), ':datetime_modified' => date('Y-m-d H:i:s', strtotime($line[3])), ':datetime_active' => date('Y-m-d', strtotime($line[3])), ':sort_order' => $post_sort_order, ':click_number' => 0, ':likes' => 0];

        try {
            $sth_query_insert_post->execute($insert);
        } catch (PDOException $e) {
            print_r($e);
            die();
        };
        $id_post = $db->lastInsertId();


        $insert = [':id_post' => $id_post, ':id_shop' => 1];
        try {
            $sth_query_insert_post_shop->execute($insert);
        } catch (PDOException $e) {
            print_r($e);
            die();
        }

        //tagi
        if (!empty($line[4])) {
            $tags = explode(',', $line[4]);
            foreach ($tags as $tag) {
                $insert = [':id_post' => $id_post, ':id_lang' => 1, ':tag' => $tag, ':click_number' => 0];
                try {
                    $sth_query_insert_tag->execute($insert);
                } catch (PDOException $e) {
                    print_r($e);
                    die();
                }
            }
        }
//----------------------------

        $insert = [':id_post' => $id_post, ':id_category' => 28, ':position' => $post_category_pos];
        try {
            $sth_query_insert_post_category->execute($insert);
        } catch (PDOException $e) {
            print_r($e);
            die();
        }

        $url_alias = Tools::link_rewrite($line[5]);

        //kopiowanie zdjęć
        $image_name = $url_alias . '.jpg';

        $target = realpath('./img/tmp') . '/import_post_image.jpg';
        grab_image($line[7], $target);

        //główne
        ImageManager::cut(
            $target,
            _PS_YBC_BLOG_IMG_DIR_ . 'post/' . $image_name,
            Configuration::get('YBC_BLOG_IMAGE_BLOG_WIDTH', null, null, null, 1920),
            Configuration::get('YBC_BLOG_IMAGE_BLOG_HEIGHT', null, null, null, 750),
            'jpg',
            0,
            0,
            1
        );
        //miniaturka
        ImageManager::cut(
            $target,
            _PS_YBC_BLOG_IMG_DIR_ . 'post/thumb/' . $image_name,
            Configuration::get('YBC_BLOG_IMAGE_BLOG_THUMB_WIDTH', null, null, null, 260),
            Configuration::get('YBC_BLOG_IMAGE_BLOG_THUMB_HEIGHT', null, null, null, 180),
            'jpg',
            0,
            0,
            2
        );
        unlink($target);


        $insert = [':id_post' => $id_post, ':id_lang' => 1, ':title' => $line[5], ':url_alias' => $url_alias, ':meta_title' => $line[1], ':description' => $line[8], ':short_description' => $line[2], ':meta_keywords' => ''.$line[4], ':meta_description' => $line[0], ':thumb' => $image_name, ':image' => $image_name];
        try {
            $sth_query_insert_post_lang->execute($insert);
        } catch (PDOException $e) {
            print_r($e);
            die();
        }

    }
}


fclose($file);


function grab_image($image_url, $image_file)
{
    $fp = fopen($image_file, 'w+');

    $ch = curl_init($image_url);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_FILE, $fp);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 1000);
    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0');
    curl_setopt($ch, CURLOPT_VERBOSE, true);
    curl_exec($ch);

    curl_close($ch);
    fclose($fp);
}