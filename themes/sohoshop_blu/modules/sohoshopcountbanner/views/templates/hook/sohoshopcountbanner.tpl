{*
 * SohoshopTopbanner module
 *
 * @author    SohoSHOP
 * @copyright Copyright (c) 2018 SohoSHOP
 * @license   http://SohoSHOP.pl
 *
 * http://SohoSHOP.pl
*}

<div id="sohoshopcountbanner"{if !$display_counter} class="active"{/if}>
{if $display_counter}
<script>
{literal}	var cutoff ={/literal}{$count_cutoff}{literal};{/literal}
{literal}	var date = new Date(cutoff);{/literal}
{literal}	var dateFrom = {/literal}"{$count_from}"{literal};{/literal}
{literal}	var dateTo = {/literal}"{$count_to}"{literal};{/literal}	
</script>
{/if}
	<div class="container">
		<div class="content">
			<span class="text">{$count_content nofilter}</span>
			{if $display_counter}
			<span id="couter" class="couter">
				<div class="timer">
					<div class="time">
						<div class="days">
							<span class="value"></span><span class="name">dni</span>
						</div>
						<div class="hours">
							<span class="value"></span><span class="name">godz</span>
						</div>
						<div class="mins">
							<span class="value"></span><span class="name">min</span>
						</div>
						<div class="secs">
							<span class="value"></span><span class="name">sek</span>
						</div>
					</div>
				</div>
			</span>
			{/if}
		</div>
	</div>
</div>