{**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 *}

{if $page.page_name eq 'index' AND $homeslider.slides}
    <div id="carousel" data-ride="carousel" class="carousel slide" data-interval="{$homeslider.speed}"
         data-wrap="{$homeslider.wrap}" data-pause="{$homeslider.pause}" data-touch="true">
      <div class="carousel-inner-wrapper">
        <ol class="carousel-indicators">
            {foreach from=$homeslider.slides item=slide key=idxSlide name='homeslider'}
                <li data-target="#carousel" data-slide-to="{$idxSlide}"{if $idxSlide == 0} class="active"{/if}></li>
            {/foreach}
        </ol>
        <ul class="carousel-inner" role="listbox">
            {foreach from=$homeslider.slides item=slide name='homeslider'}
                <li class="carousel-item {if $smarty.foreach.homeslider.first}active{/if}" role="option"
                    aria-hidden="{if $smarty.foreach.homeslider.first}false{else}true{/if}">
                    <a href="{$slide.url}">
                        <figure>
                            <img src="{$slide.image_url}" alt="{$slide.legend|escape}">
                            {if $slide.title || $slide.description}
                                <figcaption class="caption">
                                    <h2 class="display-1 text-uppercase">{$slide.title}</h2>
                                    <div class="caption-description">{$slide.description nofilter}</div>
                                </figcaption>
                            {/if}
                        </figure>
                    </a>
                </li>
            {/foreach}
        </ul>
        <div class="direction" aria-label="{l s='Carousel buttons' d='Shop.Theme.Global'}">
            <a class="left carousel-control" href="#carousel" role="button" data-slide="prev"
               aria-label="{l s='Previous' d='Shop.Theme.Global'}">
        <span class="icon-prev hidden-xs" aria-hidden="true">
        <svg xmlns="http://www.w3.org/2000/svg" width="67" height="66" viewBox="0 0 67 66"><rect
                    style="fill:#006cb5;opacity:0.596;" width="67" height="66" rx="33"/><g
                    transform="translate(21.758 23.5)"><path style="fill:none;stroke:#fff;stroke-width:2px;"
                                                             d="M1343.5,2884.57l10.01,10.01-10.01,10.009"
                                                             transform="translate(-1328.753 -2884.57)"/><path
                        style="fill:none;stroke:#fff;stroke-width:2px;" d="M1325.266,2901.153h25.091"
                        transform="translate(-1325.266 -2891.153)"/></g></svg>
        </span>
            </a>
            <a class="right carousel-control" href="#carousel" role="button" data-slide="next"
               aria-label="{l s='Next' d='Shop.Theme.Global'}">
        <span class="icon-next" aria-hidden="true">
        <svg xmlns="http://www.w3.org/2000/svg" width="67" height="66" viewBox="0 0 67 66"><rect
                    style="fill:#006cb5;opacity:0.596;" width="67" height="66" rx="33"/><g
                    transform="translate(21.758 23.5)"><path style="fill:none;stroke:#fff;stroke-width:2px;"
                                                             d="M1343.5,2884.57l10.01,10.01-10.01,10.009"
                                                             transform="translate(-1328.753 -2884.57)"/><path
                        style="fill:none;stroke:#fff;stroke-width:2px;" d="M1325.266,2901.153h25.091"
                        transform="translate(-1325.266 -2891.153)"/></g></svg>
        </span>
            </a>
        </div>
      </div>
    </div>
{/if}
