{*
* 2007-2021 Evol.digital
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web
*
* DISCLAIMER
*
*
*  @author Jerome Aldigier
*  @copyright  2007-2020 Jérôme Aldigier
*  @version  Release: 1.0
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of Jerome Aldigier
*}
<section id="storeLocator">
    {block name='content'}
        <div class="row">
            <div class="col-lg-5 map_header">
                <h1 class="page-heading">
                    {l s='Znajdź najbliższy salon BLU' mod='freestorelocator'}
                </h1>
                <p class="store_desc">{l s='Znajdź najbliższy salon BLU w Twoim mieście. Sprawdź godzinę otwarcia, dostępną ofertę oraz umów wizytę w dowolnym terminie.' mod='freestorelocator'}</p>
                <div>
                    {if $search ==1}
                        <form id="searchForm" type="GET" action="/module/freestorelocator/page">
                            <input name="city" id="searchvalue" type="text"
                                   value="{if isset($city)}{$city|escape:'htmlall':'UTF-8'}{/if}"
                                   placeholder="{l s='Wpisz miasto' mod='freestorelocator'}">
                            <button class='btn btn-primary btn-max-width' type="submit" value="">{l s='Pokaż najbliższe >' mod='freestorelocator'}</button>
                        </form>
                    {/if}
                </div>
            </div>

            <div class="col-lg-7 mapcontainer">
                <div id="{$page.page_name}-storecontainer">

                    {foreach from=$stores item=store name=ind}

                        {if $smarty.foreach.ind.index == 3}
                            {break}
                        {/if}
                    {/foreach}

                </div>
                <div id="map">
                    <!-- Ici s'affichera la carte -->
                </div>

                <div id="closed_at" style="display:none">
                    - {l s='The store closes at ' mod='freestorelocator'}
                </div>
                <div id="closed_info" style="display:none">
                    <div>{l s='CLOSED' mod='freestorelocator'}
                        <div class='closedot'></div>
                    </div>
                </div>

                <div id="opened_info" style="display:none">
                    <div>{l s='OPEN' mod='freestorelocator'}
                        <div class='opendot'></div>
                    </div>
                </div>

            </div>
            <div style="display:none" id="txt_see_more">{l s='See store details' mod='freestorelocator'} </div>


        </div>
        <script type="text/javascript">

            var geolocalized = false;
            var lat = {$lat|escape:'htmlall':'UTF-8'};
            var lon = {$long|escape:'htmlall':'UTF-8'};


            function maPosition(position) {

                lat = position.coords.latitude;
                lon = position.coords.longitude;

            }


            if (navigator.geolocation) {
                //navigator.geolocation.getCurrentPosition(maPosition);
                //initMap();
            }


            var url_map = "";
            {if $darkmode ==1}
            {literal}
            url_map = "https://{s}.tile.openstreetmap.de/tiles/osmde/{z}/{x}/{y}.png"; // Blue
            {/literal}
            {elseif $darkmode ==2}
            {literal}
            url_map = "https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png"; // Grise
            {/literal}
            {elseif $darkmode ==3}
            {literal}
            url_map = "https://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}.jpg";
            {/literal}
            {elseif $darkmode ==4}
            {literal}
            url_map = "https://{s}.tile.opentopomap.org/{z}/{x}/{y}.png"; // Carto
            {/literal}
            {elseif $darkmode ==5}
            {literal}
            url_map = "https://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png"; // Green
            {/literal}
            {elseif $darkmode ==6}
            {literal}
            url_map = "https://stamen-tiles-{s}.a.ssl.fastly.net/watercolor/{z}/{x}/{y}.jpg";//draw
            {/literal}
            {/if}
            {literal}

            setTimeout(function () {
                var txt_see = $('#txt_see_more').html();
                var macarte = null;
                var img_store_dir = '{$img_store_dir}';
                var img_ps_dir = '{$img_ps_dir}';
                var search = 0;
                $('#map').css('width', '100%');
                $("#storecontainer").css('display', 'none');
                {/literal}
                var villes = {
                    {foreach from=$stores item=store}

                    {if $store.display == 1}

                    {literal}
                    {/literal} "{$store.name|escape:'htmlall':'UTF-8'}" {literal}: {
                        "lat":{/literal} {$store.latitude|escape:'htmlall':'UTF-8'},
                        "hours": JSON.parse(decodeHTMLEntities("{$store.hours}")),
                        "lon":{$store.longitude|escape:'htmlall':'UTF-8'} ,
                        "pop": "<div class='container'><div class='row'><div class='col-10'><p><b>{$store.name|escape:'htmlall':'UTF-8'}</b><br />{$store.address1|escape:'htmlall':'UTF-8'} {if $store.address2}<br />{$store.address2|escape:'htmlall':'UTF-8'}{/if}<br />{$store.postcode} {$store.city|escape:'htmlall':'UTF-8'}<br />{if $store.phone}{l s='Phone:' js=0} {$store.phone|escape:'htmlall':'UTF-8'}{/if}<br><a class='btn-store' href='{$store.link} '>" + txt_see + "</a></p></div>        <div class='col-2'><div class='social-bar'> {if ($store.web_link|count_characters) >1 }<a href='{$store.web_link}' class='link' target='_blank'><i class='fa fa-globe' aria-hidden='true'>www</i></i></a>{/if}{if ($store.facebook_link|count_characters) >1 }<a href='{$store.facebook_link}' class='facebook' target='_blank'><i class='fa fa-facebook' aria-hidden='true'></i></a>{/if}{if ($store.insta_link|count_characters) >1 }<a href='{$store.insta_link}' target='_blank' class='instagram'><i class='fa fa-insta' aria-hidden='true'></i></a>{/if}</div></div></div></div>"   {literal} },
                    {/literal}

                    {/if}

                    {/foreach}
                };
                {if $search ==1}
                {literal}
                for (ville in villes) {
                    villes[ville].distance = distance(villes[ville].lat, villes[ville].lon, lat, lon, "K");
                    villes[ville].id = ville;
                    villes[ville].isOpen = isOpen(villes[ville].hours);

                }

                search = 1;
                villesordered = sortProperties(villes, 'distance', true, false);


                {/literal}
                {if $city|count_characters==0 && $zip|count_characters==0}
                {literal}
                for (villeo in villesordered) {
                    console.log(villesordered[villeo][1].hours);
                    $("#storecontainer").append("<div class='store' data-property='" + villesordered[villeo][1].id + "'>" + villesordered[villeo][1].pop + "<div style='color: #777; font-size: 12px;'> A <b>" + Math.round(villesordered[villeo][1].distance) + " km(s) </b>" + "<br>" + villesordered[villeo][1].isOpen + "</div></div>");


                }

                {/literal}
                {else}
                {literal}
                for (villeo in villesordered) {
                    if (villeo <= 10) {
                        $("#storecontainer").append("<div class='store' data-property='" + villesordered[villeo][1].id + "'>" + villesordered[villeo][1].pop + "<div style='color: #777; font-size: 12px;'> A <b>" + Math.round(villesordered[villeo][1].distance) + " km(s) </b>" + "<br>" + villesordered[villeo][1].isOpen + "</div></div>");
                    }

                }
                {/literal}
                {/if}
                {literal}

                //si non mobile
                $("#storecontainer").css('display', 'block');
                $("#storecontainer").css('width', '30%');
                if (window.innerWidth <= 600) {

                    $("#storecontainer").css('clear', 'left');
                    $('#storecontainer').css('width', '100%');
                    $('#storecontainer').css('height', '100%');
                    $('#map').css('width', '100%');
                    $('#module-freestorelocator-page form').css('width', '480px');

                }



                {/literal}



                {/if}
                {literal}


                function decodeHTMLEntities(text) {
                    var entities = [
                        ['amp', '&'],
                        ['apos', '\''],
                        ['#x27', '\''],
                        ['#x2F', '/'],
                        ['#39', '\''],
                        ['#47', '/'],
                        ['lt', '<'],
                        ['gt', '>'],
                        ['nbsp', ' '],
                        ['quot', '"']
                    ];

                    for (var i = 0, max = entities.length; i < max; ++i)
                        text = text.replace(new RegExp('&' + entities[i][0] + ';', 'g'), entities[i][1]);

                    return text;
                }

                function initMap() {

                    //We search zoom level to display 1 store
                    if (search == 1) {
                        var distances = [];
                        for (ville in villes) {

                            distances.push(distance(villes[ville].lat, villes[ville].lon, lat, lon, "K"));

                        }
                        distances = distances.sort(function (a, b) {
                            return a - b;
                        });

                        var zoom = 0;

                        if (distances[0] < 10) {
                            zoom = 12;
                        } else if (distances[0] < 30) {
                            zoom = 11;
                        } else if (distances[0] < 100) {
                            zoom = 9;
                        } else if (distances[0] < 200) {
                            zoom = 8;
                        } else if (distances[0] < 300) {
                            zoom = 8;
                        } else if (distances[0] < 500) {
                            zoom = 7;
                        } else if (distances[0] < 1000) {
                            zoom = 6;
                        } else if (distances[0] < 8000) {
                            zoom = 5;
                        }

                    }


                    {/literal}
                    {if $city|count_characters==0 && $zip|count_characters==0}
                    {literal}
                    zoom =    {/literal}{$zoom|escape:'htmlall':'UTF-8'}{literal};
                    {/literal}
                    {/if}
                    {literal}

                    macarte = L.map('map').setView([lat, lon], zoom);
                    macarte.scrollWheelZoom.disable();


                    L.tileLayer(url_map, {
                        attribution: 'données © <a href="//osm.org/copyright">OpenStreetMap</a>/ODbL - rendu <a href="//openstreetmap.fr">OSM France</a>',
                        minZoom: 1,
                        maxZoom: 20
                    }).addTo(macarte);


                    /* interactive markers*/
                    listmarkers = [];
                    var markersLayer = L.featureGroup().addTo(macarte);
                    /* interactive markers*/


                    for (ville in villes) {
                        var icon = L.divIcon({className: 'my-div-icon'});
                        var marker = L.marker([villes[ville].lat, villes[ville].lon], {icon: icon}).addTo(macarte);
                        /* interactive markers*/
                        marker.property = ville;
                        marker.addTo(markersLayer);
                        listmarkers.push(marker);
                        /* interactive markers*/
                        marker.bindPopup(villes[ville].pop);
                    }

                    macarte.flyTo([lat, lon], zoom + 1, {
                        animate: true,
                        duration: 0.8
                    });


                    jQuery.fn.scrollTo = function (elem, speed) {
                        $(this).animate({
                            scrollTop: $(this).scrollTop() - $(this).offset().top + $(elem).offset().top
                        }, speed == undefined ? 1000 : speed);
                        return this;
                    };

                    /* interactive markers*/

                    markersLayer.on("mouseover", function (event) {
                        var clickedMarker = event.layer;
                        var icon = clickedMarker.options.icon;
                        icon.options.className = "my-div-icon2";
                        clickedMarker.setIcon(icon);


                        $('.store').each(function () {
                            if ($(this).data("property") == clickedMarker.property) {


                                $("#storecontainer").scrollTo($(this), 400);
                                $(this).css('background-color', "#ddd");
                            }
                        })
                    });


                    /* form research  */
                    $('#searchForm').submit(function (e) {
                        e.stopPropagation();
                        e.preventDefault();
                        search();

                        return false;

                    });


                    function search() {

                        if (geolocalized) {

                            macarte.panTo(new L.LatLng(lat, long));
                            setTimeout(function () {
                                macarte.setZoom(10);
                            }, 600);
                            geolocalized = false;

                            for (ville in villes) {
                                villes[ville].distance = distance(villes[ville].lat, villes[ville].lon, lat, long, "K");
                                villes[ville].id = ville;
                                villes[ville].isOpen = isOpen(villes[ville].hours);
                            }


                            $("#storecontainer").html("");
                            villesordered = sortProperties(villes, 'distance', true, false);
                            for (villeo in villesordered) {
                                //console.log(villesordered[villeo][1].hours);
                                $("#storecontainer").append("<div class='store' data-property='" + villesordered[villeo][1].id + "'>" + villesordered[villeo][1].pop + "<div style='color: #777; font-size: 12px;'> A <b>" + Math.round(villesordered[villeo][1].distance) + " km(s) </b>" + "<br>" + villesordered[villeo][1].isOpen + "</div></div>");


                            }


                            $('.store').hover(function (event) {
                                clearTimeout(timer);
                                for (ite in listmarkers) {

                                    if ($(this).data("property") == listmarkers[ite].property) {


                                        var toto = listmarkers[ite];

                                        timer = setTimeout(function () {

                                            setTimeout(function () {
                                                macarte.panTo(new L.LatLng(toto.getLatLng().lat, toto.getLatLng().lng));
                                                setTimeout(function () {
                                                    macarte.setZoom(8);
                                                }, 600);
                                            }, 600);


                                        }, 2000);


                                    }

                                }
                            });

                            $('.store').mouseleave(function () {
                                clearTimeout(timer);
                            });


                        } else {
                            var search = $('#searchvalue').val();
                            var cp_regex = "^(([0-8][0-9])|(9[0-5])|(2[ab]))[0-9]{3}$";
                            var url = '';

                            if (search.match(cp_regex)) {
                                url = '//nominatim.openstreetmap.org/search?format=json&countrycodes=ca&postalcode=' + search;
                                // url ='//nominatim.openstreetmap.org/search?format=json&postalcode='+search;
                            } else {
                                url = '//nominatim.openstreetmap.org/search?format=json&countrycodes=ca&city=' + search;
                                // url ='//nominatim.openstreetmap.org/search?format=json&city='+search;
                            }
                            var longi = lon;
                            var lati = lat;
                            $("#storecontainer").scrollTo($('.store').eq(0), 400);
                            if (search == "") {

                                macarte.panTo(new L.LatLng(lati, longi));
                                setTimeout(function () {
                                    macarte.setZoom(10);
                                }, 600);
                            } else {
                                $.get(location.protocol + url, function (data) {

                                    macarte.panTo(new L.LatLng(lati, longi));
                                    macarte.setZoom(6);
                                    setTimeout(function () {
                                        macarte.panTo(new L.LatLng(data[0].lat, data[0].lon));
                                        setTimeout(function () {
                                            macarte.setZoom(10);
                                        }, 600);
                                    }, 600);

                                    for (ville in villes) {
                                        villes[ville].distance = distance(villes[ville].lat, villes[ville].lon, data[0].lat, data[0].lon, "K");
                                        villes[ville].id = ville;
                                        villes[ville].isOpen = isOpen(villes[ville].hours);
                                    }


                                    $("#storecontainer").html("");
                                    villesordered = sortProperties(villes, 'distance', true, false);
                                    for (villeo in villesordered) {
                                        console.log(villesordered[villeo][1].hours);
                                        $("#storecontainer").append("<div class='store' data-property='" + villesordered[villeo][1].id + "'>" + villesordered[villeo][1].pop + "<div style='color: #777; font-size: 12px;'> A <b>" + Math.round(villesordered[villeo][1].distance) + " km(s) </b>" + "<br>" + villesordered[villeo][1].isOpen + "</div></div>");


                                    }


                                    $('.store').hover(function (event) {
                                        clearTimeout(timer);
                                        for (ite in listmarkers) {

                                            if ($(this).data("property") == listmarkers[ite].property) {


                                                var toto = listmarkers[ite];

                                                timer = setTimeout(function () {

                                                    setTimeout(function () {
                                                        macarte.panTo(new L.LatLng(toto.getLatLng().lat, toto.getLatLng().lng));
                                                        setTimeout(function () {
                                                            macarte.setZoom(8);
                                                        }, 600);
                                                    }, 600);


                                                }, 2000);


                                            }

                                        }
                                    });

                                    $('.store').mouseleave(function () {
                                        clearTimeout(timer);
                                    });


                                });
                            }

                        }


                    }

                    //

                    function maPosition(position) {

                        lat = position.coords.latitude;
                        long = position.coords.longitude;
                        geolocalized = true;
                        search();

                    }


                    if (navigator.geolocation) {
                        setTimeout(function () {
                            navigator.geolocation.getCurrentPosition(maPosition);
                        }, 2500);

                    }

                    var timer;
                    markersLayer.on("mouseout", function (event) {
                        var clickedMarker = event.layer;
                        var icon = clickedMarker.options.icon;
                        icon.options.className = "my-div-icon";
                        clickedMarker.setIcon(icon);
                        $('.store').each(function () {
                            if ($(this).data("property") == clickedMarker.property) {
                                $(this).css('background-color', "#ffffff");
                            }
                        })
                    });

                    $('.store').hover(function (event) {
                        clearTimeout(timer);
                        for (ite in listmarkers) {

                            if ($(this).data("property") == listmarkers[ite].property) {

                                var icon = listmarkers[ite].options.icon;
                                icon.options.className = "my-div-icon2";
                                listmarkers[ite].setIcon(icon);
                                var toto = listmarkers[ite];

                                timer = setTimeout(function () {

                                    setTimeout(function () {
                                        macarte.panTo(new L.LatLng(toto.getLatLng().lat, toto.getLatLng().lng));
                                        setTimeout(function () {
                                            macarte.setZoom(8);
                                        }, 600);
                                    }, 600);


                                }, 2000);


                            }

                        }
                    });

                    $('.store').mouseleave(function () {
                        clearTimeout(timer);
                    });


                    $('.store').mouseleave(function (event) {
                        for (ite in listmarkers) {

                            if ($(this).data("property") == listmarkers[ite].property) {
                                var icon = listmarkers[ite].options.icon;
                                icon.options.className = "my-div-icon";
                                listmarkers[ite].setIcon(icon);
                            }

                        }
                    });
                    /* interactive markers*/


                }

                // Fonction d'initialisation qui s'exécute lorsque le DOM est chargé
                initMap();

            }, 2000); //wait 2s for jQuery to be loaded
            //https://leaflet-extras.github.io/leaflet-providers/preview/


            function isOpen(hours) {

                //  //console.log(hours);

                var dt = new Date();

                if (dt.getDay() == 0)
                    day = 'su';
                if (dt.getDay() == 1)
                    day = 'Mo';
                if (dt.getDay() == 2)
                    day = 'Tu';
                if (dt.getDay() == 3)
                    day = 'We';
                if (dt.getDay() == 4)
                    day = 'Th';
                if (dt.getDay() == 5)
                    day = 'Fr';
                if (dt.getDay() == 6)
                    day = 'Sa';


                open = "";


                if (hours[day] !== undefined) {
                    if (hours[day]['0-'] !== undefined) {
                        if (hours[day]['0-']['from'] !== 'undefined') {

                            var date_test = new Date(dt.getFullYear() + '-' + (parseInt(dt.getMonth()) + 1) + '-' + dt.getDate() + ' ' + hours[day]['0-']['from'].replace(/-/g, "/"));

                            if (typeof date_test.getMonth !== 'function' || hours[day]['0-']['from'] == "") {
                                ////console.log('date non valide');
                                open = "";
                            } else {

                                if (dates.compare(dt, date_test) == 1) {
                                    open = true;

                                } else {
                                    open = false;
                                }
                            }


                        }
                        if (hours[day]['0-']['to'] !== 'undefined' && open == true) {

                            var date_test2 = new Date(dt.getFullYear() + '-' + (parseInt(dt.getMonth()) + 1) + '-' + dt.getDate() + ' ' + hours[day]['0-']['to'].replace(/-/g, "/"));


                            if (typeof date_test.getMonth !== 'function' || hours[day]['0-']['to'] == "") {
                                ////console.log('date non valide');
                                open = "";
                            } else {
                                if (dates.compare(dt, date_test2) == 1) {
                                    open = false;

                                } else {
                                    open = true;
                                    opento = date_test2.getHours();
                                    //console.log('magasin ouvert le matin!')
                                }
                            }
                        }
                    }


                    if (hours[day]['1-'] !== undefined && (open == false || open == "")) {

                        if (hours[day]['1-']['from'] !== 'undefined') {


                            var date_test = new Date(dt.getFullYear() + '-' + (parseInt(dt.getMonth()) + 1) + '-' + dt.getDate() + ' ' + hours[day]['1-']['from'].replace(/-/g, "/"));

                            if (typeof date_test.getMonth !== 'function' || hours[day]['1-']['from'] == "") {
                                ////console.log('date non valide');
                                open = "";
                            } else {

                                if (dates.compare(dt, date_test) == 1) {
                                    open = true;

                                }
                            }

                        }
                        if (hours[day]['1-']['to'] !== 'undefined' && open == true) {


                            var date_test2 = new Date(dt.getFullYear() + '-' + (parseInt(dt.getMonth()) + 1) + '-' + dt.getDate() + ' ' + hours[day]['1-']['to'].replace(/-/g, "/"));


                            if (typeof date_test.getMonth !== 'function' || hours[day]['1-']['to'] == "") {
                                //console.log('date non valide');
                                open = "";
                            } else {
                                if (dates.compare(dt, date_test2) == 1) {
                                    open = false;
                                    //console.log('apres fermeture apres midi');
                                } else {
                                    open = true;
                                    opento = date_test2.getHours() + "h" + date_test2.getMinutes()

                                }
                            }
                        }
                    }


                }

                var openstr = "";

                if (open === true)
                    openstr = $('#opened_info').html() + $('#closed_at').html() + opento;
                else if (open === false)
                    openstr = $('#closed_info').html();


                return (openstr);


            }


            function distance(lat1, lon1, lat2, lon2, unit) {
                if ((lat1 == lat2) && (lon1 == lon2)) {
                    return 0;
                } else {
                    var radlat1 = Math.PI * lat1 / 180;
                    var radlat2 = Math.PI * lat2 / 180;
                    var theta = lon1 - lon2;
                    var radtheta = Math.PI * theta / 180;
                    var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
                    if (dist > 1) {
                        dist = 1;
                    }
                    dist = Math.acos(dist);
                    dist = dist * 180 / Math.PI;
                    dist = dist * 60 * 1.1515;
                    if (unit == "K") {
                        dist = dist * 1.609344
                    }
                    if (unit == "N") {
                        dist = dist * 0.8684
                    }
                    return dist;
                }
            }

            function sortProperties(obj, sortedBy, isNumericSort, reverse) {
                sortedBy = sortedBy || 1; // by default first key
                isNumericSort = isNumericSort || false; // by default text sort
                reverse = reverse || false; // by default no reverse

                var reversed = (reverse) ? -1 : 1;

                var sortable = [];
                for (var key in obj) {
                    if (obj.hasOwnProperty(key)) {
                        sortable.push([key, obj[key]]);
                    }
                }
                if (isNumericSort)
                    sortable.sort(function (a, b) {
                        return reversed * (a[1][sortedBy] - b[1][sortedBy]);
                    });
                else
                    sortable.sort(function (a, b) {
                        var x = a[1][sortedBy].toLowerCase(),
                            y = b[1][sortedBy].toLowerCase();
                        return x < y ? reversed * -1 : x > y ? reversed : 0;
                    });
                return sortable;
            }


            var dates = {
                convert: function (d) {
                    // Converts the date in d to a date-object. The input can be:
                    //   a date object: returned without modification
                    //  an array      : Interpreted as [year,month,day]. NOTE: month is 0-11.
                    //   a number     : Interpreted as number of milliseconds
                    //                  since 1 Jan 1970 (a timestamp)
                    //   a string     : Any format supported by the javascript engine, like
                    //                  "YYYY/MM/DD", "MM/DD/YYYY", "Jan 31 2009" etc.
                    //  an object     : Interpreted as an object with year, month and date
                    //                  attributes.  **NOTE** month is 0-11.
                    return (
                        d.constructor === Date ? d :
                            d.constructor === Array ? new Date(d[0], d[1], d[2]) :
                                d.constructor === Number ? new Date(d) :
                                    d.constructor === String ? new Date(d) :
                                        typeof d === "object" ? new Date(d.year, d.month, d.date) :
                                            NaN
                    );
                },
                compare: function (a, b) {
                    // Compare two dates (could be of any type supported by the convert
                    // function above) and returns:
                    //  -1 : if a < b
                    //   0 : if a = b
                    //   1 : if a > b
                    // NaN : if a or b is an illegal date
                    // NOTE: The code inside isFinite does an assignment (=).
                    return (
                        isFinite(a = this.convert(a).valueOf()) &&
                        isFinite(b = this.convert(b).valueOf()) ?
                            (a > b) - (a < b) :
                            NaN
                    );
                },
                inRange: function (d, start, end) {
                    // Checks if date in d is between dates in start and end.
                    // Returns a boolean or NaN:
                    //    true  : if d is between start and end (inclusive)
                    //    false : if d is before start or after end
                    //    NaN   : if one or more of the dates is illegal.
                    // NOTE: The code inside isFinite does an assignment (=).
                    return (
                        isFinite(d = this.convert(d).valueOf()) &&
                        isFinite(start = this.convert(start).valueOf()) &&
                        isFinite(end = this.convert(end).valueOf()) ?
                            start <= d && d <= end :
                            NaN
                    );
                }
            }


        </script>
    {/literal}
    {/block}
</section>