{**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 *}

<div class="block-contact col-md-3 links wrapper">
  <div class="hidden-sm-down">
    <p class="footer-block-title">{l s='Skontaktuj się z nami' d='Shop.Theme.Global'}</p>
    <div class="stores hidden-lg-down">
      {literal}
          <svg xmlns="http://www.w3.org/2000/svg" width="41.282" height="51.338" viewBox="0 0 41.282 51.338">
              <defs>
                  <style>.a {
                          fill: #006cb5;
                      }</style>
              </defs>
              <g transform="translate(96.765 592.06)">
                  <path class="a"
                        d="M-76.113-583.118l-.009-.5v.5h0a5.343,5.343,0,0,0-5.337,5.337,5.3,5.3,0,0,0,1.562,3.775,5.3,5.3,0,0,0,3.774,1.564h0a5.345,5.345,0,0,0,5.338-5.33,5.3,5.3,0,0,0-1.556-3.776A5.306,5.306,0,0,0-76.113-583.118Zm-.018,8.712h-.01a3.382,3.382,0,0,1-3.367-3.377,3.365,3.365,0,0,1,3.342-3.377l.044.427.007-.427a3.359,3.359,0,0,1,2.38,1,3.354,3.354,0,0,1,.982,2.391A3.382,3.382,0,0,1-76.131-574.406Z"/>
                  <path class="a"
                        d="M-76.123-540.722c11.767,0,20.64-3.758,20.64-8.742,0-3.772-5.265-7-13.421-8.213l-1.549-.231.861-1.308c2.822-4.289,4.675-7.161,5.359-8.3a16.59,16.59,0,0,0,2.608-9.183,15.581,15.581,0,0,0-4.256-10.87,14.094,14.094,0,0,0-10.242-4.486c-7.978,0-14.479,6.86-14.489,15.292a16.557,16.557,0,0,0,2.568,9.185c.491.826,1.808,2.925,5.368,8.372l.853,1.3h-.007l5.67,8.6.814-1.226c.121-.186,1.235-1.862,2.718-4.1l.769-1.157.6.06c8.61.872,13.755,3.868,13.755,6.262,0,3.072-7.962,6.508-18.617,6.508s-18.614-3.436-18.614-6.508c0-2.265,4.607-5.073,12.394-6.1l-.856-2.14-.167.025c-8.14,1.223-13.4,4.446-13.4,8.211C-96.765-544.48-87.891-540.722-76.123-540.722Zm.8-13.621-.837,1.261-.834-1.264c-5.128-7.79-8.449-12.918-9.352-14.44a14.269,14.269,0,0,1-2.246-7.995c0-7.18,5.6-13.038,12.486-13.038a12.088,12.088,0,0,1,8.59,3.616,13.292,13.292,0,0,1,3.867,9.486,14.347,14.347,0,0,1-2.294,8C-67.154-566.684-72.34-558.837-75.321-554.343Z"/>
              </g>
          </svg>
      {/literal}
      <a href="{url entity='module' name='freestorelocator' controller='page'}">
          {l s='Wybierz salon' d='Shop.Theme.Global'}
      </a>
  </div>
      {* <ul>
          <li><a href="tel:{$contact_infos.phone}">{l s='tel.' d='Shop.Theme.Global'} {$contact_infos.phone}</a></li>
          <li><a href="mailto:{$contact_infos.email}">{$contact_infos.email}</a></li>
      </ul>
      <p class="consultants">{$shop.registration_number|nl2br nofilter}</p> *}
  </div>
  <div class="hidden-md-up links">
    <div class="title col-md-6" data-target="#footer_sub_menu_contact" data-toggle="collapse">
      <span class="footer-block-title">{l s='Skontaktuj się z nami' d='Shop.Theme.Global'}</span>
      <span class="float-xs-right">
          <span class="navbar-toggler collapse-icons" style="color:#000">
            <i class="material-icons add">&#xE313;</i>
            <i class="material-icons remove">&#xE316;</i>
          </span>
      </span>
    </div>
    <ul id="footer_sub_menu_contact" class="collapse">
      <li>    
        <div class="stores hidden-lg-down">
          {literal}
              <svg xmlns="http://www.w3.org/2000/svg" width="41.282" height="51.338" viewBox="0 0 41.282 51.338">
                  <defs>
                      <style>.a {
                              fill: #006cb5;
                          }</style>
                  </defs>
                  <g transform="translate(96.765 592.06)">
                      <path class="a"
                            d="M-76.113-583.118l-.009-.5v.5h0a5.343,5.343,0,0,0-5.337,5.337,5.3,5.3,0,0,0,1.562,3.775,5.3,5.3,0,0,0,3.774,1.564h0a5.345,5.345,0,0,0,5.338-5.33,5.3,5.3,0,0,0-1.556-3.776A5.306,5.306,0,0,0-76.113-583.118Zm-.018,8.712h-.01a3.382,3.382,0,0,1-3.367-3.377,3.365,3.365,0,0,1,3.342-3.377l.044.427.007-.427a3.359,3.359,0,0,1,2.38,1,3.354,3.354,0,0,1,.982,2.391A3.382,3.382,0,0,1-76.131-574.406Z"/>
                      <path class="a"
                            d="M-76.123-540.722c11.767,0,20.64-3.758,20.64-8.742,0-3.772-5.265-7-13.421-8.213l-1.549-.231.861-1.308c2.822-4.289,4.675-7.161,5.359-8.3a16.59,16.59,0,0,0,2.608-9.183,15.581,15.581,0,0,0-4.256-10.87,14.094,14.094,0,0,0-10.242-4.486c-7.978,0-14.479,6.86-14.489,15.292a16.557,16.557,0,0,0,2.568,9.185c.491.826,1.808,2.925,5.368,8.372l.853,1.3h-.007l5.67,8.6.814-1.226c.121-.186,1.235-1.862,2.718-4.1l.769-1.157.6.06c8.61.872,13.755,3.868,13.755,6.262,0,3.072-7.962,6.508-18.617,6.508s-18.614-3.436-18.614-6.508c0-2.265,4.607-5.073,12.394-6.1l-.856-2.14-.167.025c-8.14,1.223-13.4,4.446-13.4,8.211C-96.765-544.48-87.891-540.722-76.123-540.722Zm.8-13.621-.837,1.261-.834-1.264c-5.128-7.79-8.449-12.918-9.352-14.44a14.269,14.269,0,0,1-2.246-7.995c0-7.18,5.6-13.038,12.486-13.038a12.088,12.088,0,0,1,8.59,3.616,13.292,13.292,0,0,1,3.867,9.486,14.347,14.347,0,0,1-2.294,8C-67.154-566.684-72.34-558.837-75.321-554.343Z"/>
                  </g>
              </svg>
          {/literal}
          <a href="{url entity='module' name='freestorelocator' controller='page'}">
              {l s='Wybierz salon' d='Shop.Theme.Global'}
          </a>
      </div>
    </li>
      {* <li><a href="tel:{$contact_infos.phone}">{l s='tel.' d='Shop.Theme.Global'} {$contact_infos.phone}</a></li>
      <li><a href="mailto:{$contact_infos.email}">{$contact_infos.email}</a></li>
      <li class="consultants">{$shop.registration_number|nl2br nofilter}</li> *}
    </ul>
  </div>
</div>
