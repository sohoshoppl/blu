{assign var=_counter value=0}
{function name="menu" nodes=[] depth=0 parent=null}
    {if $nodes|count}
      <ul class="top-menu" {if $depth == 0}id="top-menu"{/if} data-depth="{$depth}">
        {foreach from=$nodes item=node key=nodeindex}
          {if $depth === 1 && $nodeindex == 0}
            <li class="category hidden-sm-down">
                <a class="show_all_menu" href="#">
                    {l s='Pokaż wszystko' d='Shop.Theme.Global'}
                </a>
            </li>
          {/if}
          <li class="{$node.type}{if $node.current} current {/if}" id="{$node.page_identifier}">
            {assign var=_counter value=$_counter+1}
            <div class="dekstop-menu_wrapper">
                <a
                  class="{if $depth >= 0}dropdown-item{/if}{if $depth === 1} dropdown-submenu{/if}"
                  href="{$node.url}" data-depth="{$depth}"
                  {if $node.open_in_new_window} target="_blank" {/if}
                >
                  {if $node.children|count}
                    {* Cannot use page identifier as we can have the same page several times *}
                    {assign var=_expand_id value=10|mt_rand:100000}
                    <span class="float-xs-right hidden-md-up">
                      <span data-target="#top_sub_menu_{$_expand_id}" data-toggle="collapse" class="navbar-toggler collapse-icons">
                        <svg class="mobile-icon-menu add" xmlns="http://www.w3.org/2000/svg" width="9.376" height="16.631" viewBox="0 0 9.376 16.631"><defs></defs><path class="a" d="M-313.147-44.61l-7.785,7.785-7.785-7.785" transform="translate(45.141 -312.616) rotate(-90)"/>
                        </svg>
                        <svg class="mobile-icon-menu remove" xmlns="http://www.w3.org/2000/svg" width="9.376" height="16.631" viewBox="0 0 9.376 16.631"><defs></defs><path class="a" d="M-313.147-44.61l-7.785,7.785-7.785-7.785" transform="translate(45.141 -312.616) rotate(-90)"/>
                        </svg>
                      </span>
                    </span>
                    <div class="hidden-sm-down {if $depth !== 0}desktop-collapse-icons{/if}">
                      <svg  data-target="#top_sub_menu_{$_expand_id}" xmlns="http://www.w3.org/2000/svg" width="12.262" height="7.192" viewBox="0 0 12.262 7.192"><defs></defs><g transform="translate(0.53 0.53)"><path  d="M-317.515-44.61l-5.6,5.6-5.6-5.6" transform="translate(328.717 44.61)"/></g></svg>
                    </div>
                  {/if}
                  {$node.label}
                </a>
              {if $node.children|count}
                <div {if $depth === 0} class="popover sub-menu js-sub-menu collapse"{else} class="collapse children-menu"{/if} id="top_sub_menu_{$_expand_id}">
                  {menu nodes=$node.children depth=$node.depth parent=$node}
                </div>
              {/if}
            </div>
          </li>
        {/foreach}
      </ul>
    {/if}
{/function}

<div class="menu js-top-menu position-static hidden-sm-down" id="_desktop_top_menu">
    {menu nodes=$menu.children}
    <div class="clearfix"></div>
</div>
