{**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 *}

{assign var="stores" value=(Store::getStores($language.id))}
<section class="contact-form ">
<div class="flexbox mobile">
  <span class="contact-us-title-mobile">
    Kontakt
  </span>
  </div>

<div id="kontakt">



  <div class="flexbox">
  <span class="contact-us-title">
    Kontakt
  </span>
  </div>
  
  <div class="row contact-banner-table">
    <div class="col-md-1"></div>

    {* lewa strona banera *}
    <div class="col-md-4">
      <div class="left-contact-banner">Dzień dobry,<br> w czym możemy ci pomóc?</div>
      <div class="flexbox mt-5 hover-contact-us-button"><a href="#scrollPin" class="contact-us-button"><span>Skontaktuj się z salonem</span></a></div>
      <div class="flexbox mt-5 contact-hours">
        Nasi konsultanci są dostępni:<br>
        Pon. - Pt. 10:00-18:00 <br>
        Sob. 11.00-14.00, Niedz 11.00-14.00
      </div>
    </div>

    <div class="col-md-3"></div>

    {* prawa strona banera *}
    <div class="col-md-3 parent-inline">
      <div class="parent-right-side">
        <div class="contact-right-side right-side-fixed">
          <a href="#pinContact"><div class="right-contact-banner  mt-2-right">Dane kontaktowe</div></a>
          <a href="#mapPin"><div class="right-contact-banner  mt-2-right">Mapa dojazdu</div></a>
          <a href="#scrollPin" class="contact-form-button-handler"><div class="right-contact-banner  mt-2-right">Formularz kontaktowy</div></a>
          <a href="#"><div class="right-contact-banner  mt-2-right">Pomoc</div></a>
        </div>
      </div>
    </div>

  </div>

</div>

<div class="row">

  

  <div class="col-md-9">
    <div class="flexbox title-salon">
      <span>52 Salony BLU w calej Polsce. Skontaktuj się z najbliższym.</span>
    </div>
    <div class="flexbox">
      <form id="searchForm" action="/module/freestorelocator/page" class="store-map-search top-salon-search" method="GET">
        <input class="store-map-input search-top-map" placeholder="{l s='Wpisz miasto lub kod' mod="freestorelocator"}">
        <button type="submit" id="searchSalon"><i class="material-icons search">&#xE8B6;</i></button>
      </form>
    </div>
    <div class="overflow">
      {foreach from=$stores item=store}
        <div class="{if $store@iteration > 1} next-thumb {/if} salon-thumb">
          <span class="title">{$store.name}</span>
          {* first row *}
          <div class="row mt-1">
            <div class="col-md-1">
              <img src="{$urls.img_url}point-icon.svg"/>
            </div>
            <div class="col-md-3">
              {$store.postcode} {$store.city}<br>{$store.address1}
            </div>
            <div class="col-md-7">
              Godziny otwarcia: <br>Pon. - Pt. 10:00-18:00 <br>Sob. 11.00-14.00, Niedz 11.00-14.00
            </div>
            <a href="{$urls.pages.stores}"><div class="col-md-1 mt-3 enter-arrow"><img src="{$urls.img_url}arrow-right.png"></div></a>
          </div>
          {* second row *}
          <div class="row mt-2">
            <div class="col-md-1">
            
            </div>
            <div class="col-md-3">
              tel. {$store.phone}<br> <a href="{$store.email}">{$store.email}</a>
            </div>
            <div class="col-md-8">
              
            </div>
          </div>
        </div>
      {/foreach}

    </div>

    
    <div class="mobile-overflow">
      <div class="flexbox salon-map">
        <div class="najblizszy-salon">Twój najbliższy salon:</div>
      </div>
        {foreach from=$stores item=store}
          {if $store@iteration > 1}
          {break}
          {/if}
          <div class="flexbox">
            <div class="mobile-title">{$store.name}</div>
          </div>

          <div class="flexbox">
            <div class="mobile mobile-address row">
              <div class="col-xs-3"></div>
              <div class="col-xs-1"><img class="mobile-pin-icon " src="{$urls.img_url}point-icon.svg"/></div>
              <div class="col-xs-8">{$store.postcode} {$store.city} <br> {$store.address1}</div>
            </div>
          </div>

          <div class="flexbox">
            <div class="mobile mobile-tel row">
              <div class="col-xs-4"></div>
                <div class="col-xs-8">
                  tel. {$store.phone}<br><a href="{$store.email}">{$store.email}</a>
              </div>
            </div>
          </div>

          <div class="flexbox">
            <div class="row mobile">
              <div class="col-xs-4"></div>
              <div class="col-xs-8">
                <div class="mobile-working-hours">
                  Godziny otwarcia: <br>Pon. - Pt. 10:00-18:00 <br>Sob. 11.00-14.00, Niedz 11.00-14.00
                </div>
              </div>
            </div>
          </div>

          <div class="flexbox">
            <a href="#" class="mobile-show-more-button">Pokaż więcej</a>
          </div>


        {/foreach}
    </div>

    <div class="map-title" id="mapPin">Czekamy na Ciebie w salonie BLU</div>
     {hook h="displayOnContactPage" mod="freestorelocator"}
   
    {* <div class="row mt-3">
      <div class="col-md-6 find-us">
        <div class="flexbox title">
          Znajdź salon BLU blisko Ciebie. Sprawdź<br> godziny otwarcia i mapę dojazdu.<br> Zapraszamy!
        </div>
        <div class="flexbox">
          <form action="" method="post" class="form-search-salon">
            <input type="hidden" name="action" value="contactController">
            <input type="text" class="search-salon" name="salon-city" placeholder="Wpisz miasto lub kod">
            <button class="find-us-button" type="submit">Pokaż najbliższe ></button>
          </form>
        </div>
      </div>
      <div class="col-md-6">
        <div class="flexbox">
          <div class="find-us-map">123</div>
        </div>
      </div>
    </div> *}

    {if $notifications}
      <div class="col-xs-12 alert {if $notifications.nw_error}alert-danger{else}alert-success{/if}">
        <ul>
          {foreach $notifications.messages as $notif}
            <li>{$notif}</li>
          {/foreach}
        </ul>
      </div>
    {/if}

    <form action="{$urls.pages.contact}" method="post" class="bottom-contact-us-form bottom-contact-form" id="scrollPin">
    <input type="hidden" name="token" value="{$token}" />
    <div class="flexbox title">{l s='Skontaktuj się z nami.' d='Shop.Theme.Actions'} <br>{l s='Nasi doradcy są do Twojej dyspozycji.' d='Shop.Theme.Actions'}</div>
      <div class="row first">
        <div class="col-md-6">
          <input type="text" name="name_surname" placeholder="{l s='Twoje imię i nazwisko*' d='Shop.Theme.Actions'}" class="bottom-input">
        </div>

        <div class="col-md-6 margin-mobile">
          <select name="salon" class="bottom-input" id="salon-list-form">
            {foreach from=$stores item=store}
              <option class="salon-list-form" value="{$store.email}"><span class="salon-list-form">{$store.name}</span></option>
            {/foreach}
          <select>
        </div>
      </div>
      {* second row *}

      <div class="row second">
        <div class="col-md-6 margin-mobile">
          <input type="text" name="email" placeholder="{l s='Twój adres e-mail*' d='Shop.Theme.Actions'}" class="bottom-input">
        </div>

        <div class="col-md-6">
          <input type="text" name="phone_number" placeholder="{l s='Twój numer telefonu*' d='Shop.Theme.Actions'}" class="bottom-input">
        </div>
      </div>
      <textarea name="message" rows="4" cols="50" class="bottom-textarea">
        {l s='Treść' d='Shop.Theme.Actions'}
      </textarea><br>
      <span>
        <sup>*</sup>
        {l s='Twoje dane będziemy przetwarzać zgodnie z naszą' d='Shop.Theme.Actions'}
        <a href="{url entity=cms id=2}">{l s='Polityką prywatności' d='Shop.Theme.Actions'}</a>
      </span>
      <div class="flexbox">
      <button class="contact-us-button-bottom" name="submitMessage" type="submit">{l s='Wyślij >' d='Shop.Theme.Actions'}</button>
      </div>
    </form>
    
    <div class="flexbox footer-items mt-15" id="pinContact">
      <span> BR Konsorcjum Sp. z o.o. „Refleks” Sp. k. </span>
    </div>
    <div class="row footer-grid">
      <div class="col-md-2"></div>
      <div class="flexbox-mobile">
        <div class="col-lg-4 mt-15">
          Składowa 12 <br>15-399 Białystok <br>tel. (85) 745 04 40 <br>tel. (85) 745 04 50 <br><a href="mailto:salonylazienek@blu.com.pl">salonylazienek@blu.com.pl</a>
        </div>
      </div>
      <div class="col-md-2"></div>
      <div class="flexbox-mobile">
        <div class="col-lg-4 mt-15">
          Obsługa logistyczna i magazynowa:<br> Nasienna 15 <br>95-040 Koluszki <br><a href="https://logiq.com.pl">logiq.com.pl</a>
        </div>
      </div>
    </div>
    
  </div>
  {* col md 9 closing^^ *}
  
</div>

{literal}
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script type="text/javascript">

$(document).ready(function () {
  var footerOffset = $('#footer').offset().top - 30;
  var footerHeight = $('#footer').height();
  var fixedRightHeight = $('.contact-right-side').height();
  

  $(window).scroll(function (event) {
    var oy = $(this).scrollTop();
    
    if((oy + fixedRightHeight) - 50 < (footerOffset - footerHeight)){
      $('.contact-right-side').css({
        position: 'fixed',
        top:  'auto'
      });
    } else if((oy + fixedRightHeight) - 50 >= (footerOffset - footerHeight)){
    
      $('.contact-right-side').css({
        position: 'absolute',
        top: (footerOffset - footerHeight - fixedRightHeight + 50)
      });
      
    }
  });
  
});



</script>
{/literal}

{* row closing ^^*}
  {* <form action="{$urls.pages.contact}" method="post" {if $contact.allow_file_upload}enctype="multipart/form-data"{/if}>

    {if $notifications}
      <div class="col-xs-12 alert {if $notifications.nw_error}alert-danger{else}alert-success{/if}">
        <ul>
          {foreach $notifications.messages as $notif}
            <li>{$notif}</li>
          {/foreach}
        </ul>
      </div>
    {/if}

    {if !$notifications || $notifications.nw_error}
      <section class="form-fields">

        <div class="form-group row">
          <div class="col-md-9 col-md-offset-3">
            <h3>{l s='Contact us' d='Shop.Theme.Global'}</h3>
          </div>
        </div>

        <div class="form-group row">
          <label class="col-md-3 form-control-label">{l s='Subject' d='Shop.Forms.Labels'}</label>
          <div class="col-md-6">
            <select name="id_contact" class="form-control form-control-select">
              {foreach from=$contact.contacts item=contact_elt}
                <option value="{$contact_elt.id_contact}">{$contact_elt.name}</option>
              {/foreach}
            </select>
          </div>
        </div>

        <div class="form-group row">
          <label class="col-md-3 form-control-label">{l s='Email address' d='Shop.Forms.Labels'}</label>
          <div class="col-md-6">
            <input
              class="form-control"
              name="from"
              type="email"
              value="{$contact.email}"
              placeholder="{l s='your@email.com' d='Shop.Forms.Help'}"
            >
          </div>
        </div>

        {if $contact.orders}
          <div class="form-group row">
            <label class="col-md-3 form-control-label">{l s='Order reference' d='Shop.Forms.Labels'}</label>
            <div class="col-md-6">
              <select name="id_order" class="form-control form-control-select">
                <option value="">{l s='Select reference' d='Shop.Forms.Help'}</option>
                {foreach from=$contact.orders item=order}
                  <option value="{$order.id_order}">{$order.reference}</option>
                {/foreach}
              </select>
            </div>
            <span class="col-md-3 form-control-comment">
              {l s='optional' d='Shop.Forms.Help'}
            </span>
          </div>
        {/if}

        {if $contact.allow_file_upload}
          <div class="form-group row">
            <label class="col-md-3 form-control-label">{l s='Attachment' d='Shop.Forms.Labels'}</label>
            <div class="col-md-6">
              <input type="file" name="fileUpload" class="filestyle" data-buttonText="{l s='Choose file' d='Shop.Theme.Actions'}">
            </div>
            <span class="col-md-3 form-control-comment">
              {l s='optional' d='Shop.Forms.Help'}
            </span>
          </div>
        {/if}

        <div class="form-group row">
          <label class="col-md-3 form-control-label">{l s='Message' d='Shop.Forms.Labels'}</label>
          <div class="col-md-9">
            <textarea
              class="form-control"
              name="message"
              placeholder="{l s='How can we help?' d='Shop.Forms.Help'}"
              rows="3"
            >{if $contact.message}{$contact.message}{/if}</textarea>
          </div>
        </div>

        {if isset($id_module)}
          <div class="form-group row">
            <div class="offset-md-3">
              {hook h='displayGDPRConsent' id_module=$id_module}
            </div>
          </div>
        {/if}

      </section>

      <footer class="form-footer text-sm-right">
        <style>
          input[name=url] {
            display: none !important;
          }
        </style>
        <input type="text" name="url" value=""/>
        <input type="hidden" name="token" value="{$token}" />
        <input class="btn btn-primary" type="submit" name="submitMessage" value="{l s='Send' d='Shop.Theme.Actions'}">
      </footer>
    {/if}

  </form> *}
</section>
