{**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 *}
<div id="_desktop_cart">
    <div class="blockcart cart-preview" data-refresh-url="{$refresh_url}">
        <div class="header">
            {if $cart.products_count > 0}
            <a rel="nofollow" href="{$cart_url}">
                {/if}
                <svg xmlns="http://www.w3.org/2000/svg" width="17.487" height="20.75" viewBox="0 0 17.487 20.75">
                    <path d="M-112.86-569.139v-1.514a4.483,4.483,0,0,0-4.359-4.7,4.483,4.483,0,0,0-4.359,4.7v1.488h-4.385l1.331,14.564h14.825l1.331-14.538Zm-7.674-1.514a3.455,3.455,0,0,1,3.315-3.654,3.455,3.455,0,0,1,3.315,3.654v1.514h-6.63Zm9.762,15.034h-12.894l-1.148-12.5,15.191.052Z"
                          transform="translate(125.963 575.351)"/>
                </svg>
                <br>
                <span>{l s='Twoje' d='Shop.Theme.Checkout'}<br>{l s='rezerwacje' d='Shop.Theme.Checkout'}</span>
                <span class="counter">{$cart.products_count}</span>
                {if $cart.products_count > 0}
            </a>
            {/if}
        </div>
    </div>
</div>
