{*
* 2007-2019 ETS-Soft
*
* NOTICE OF LICENSE
*
* This file is not open source! Each license that you purchased is only available for 1 wesite only.
* If you want to use this file on more websites (or projects), you need to purchase additional licenses. 
* You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
* 
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs, please contact us for extra customization service at an affordable price
*
*  @author ETS-Soft <etssoft.jsc@gmail.com>
*  @copyright  2007-2019 ETS-Soft
*  @license    Valid for 1 website (or project) for each purchase of license
*  International Registered Trademark & Property of ETS-Soft
*}
{if ((isset($blog_config.YBC_BLOG_SHOW_CATEGORIES_BLOCK) && $blog_config.YBC_BLOG_SHOW_CATEGORIES_BLOCK)||(isset($blog_config.YBC_BLOG_SHOW_SEARCH_BLOCK) && $blog_config.YBC_BLOG_SHOW_SEARCH_BLOCK)||(isset($blog_config.YBC_BLOG_SHOW_LATEST_NEWS_BLOCK) && $blog_config.YBC_BLOG_SHOW_LATEST_NEWS_BLOCK) ||(isset($blog_config.YBC_BLOG_SHOW_POPULAR_POST_BLOCK) && $blog_config.YBC_BLOG_SHOW_POPULAR_POST_BLOCK) || (isset($blog_config.YBC_BLOG_SHOW_FEATURED_BLOCK) && $blog_config.YBC_BLOG_SHOW_FEATURED_BLOCK)||(isset($blog_config.YBC_BLOG_SHOW_TAGS_BLOCK) && $blog_config.YBC_BLOG_SHOW_TAGS_BLOCK)||(isset($blog_config.YBC_BLOG_SHOW_GALLERY_BLOCK) && $blog_config.YBC_BLOG_SHOW_GALLERY_BLOCK)||(isset($blog_config.YBC_BLOG_SHOW_ARCHIVES_BLOCK) && $blog_config.YBC_BLOG_SHOW_ARCHIVES_BLOCK)||(isset($blog_config.YBC_BLOG_SHOW_COMMENT_BLOCK) && $blog_config.YBC_BLOG_SHOW_COMMENT_BLOCK)||(isset($blog_config.YBC_BLOG_SHOW_AUTHOR_BLOCK) && $blog_config.YBC_BLOG_SHOW_AUTHOR_BLOCK)||(isset($blog_config.YBC_BLOG_ENABLE_RSS) && $blog_config.YBC_BLOG_ENABLE_RSS && isset($blog_config.YBC_BLOC_RSS_DISPLAY) && $blog_config.YBC_BLOC_RSS_DISPLAY && in_array('side_bar',$blog_config.YBC_BLOC_RSS_DISPLAY))) && !$blog_config.YBC_BLOG_SIDEBAR_ON_MOBILE }
<div class="ybc-navigation-blog">{if $blog_config.YBC_BLOG_NAVIGATION_TITLE}{$blog_config.YBC_BLOG_NAVIGATION_TITLE|escape:'html':'UTF-8'}{else}{l s='Opcje poradnika' mod='ybc_blog'}{/if}</div>
<div class="ybc-navigation-blog-content">
{/if}
{foreach from=$sidebars_postion item='position'}
    {$sidebars.$position nofilter}
{/foreach}
{if ((isset($blog_config.YBC_BLOG_SHOW_CATEGORIES_BLOCK) && $blog_config.YBC_BLOG_SHOW_CATEGORIES_BLOCK)||(isset($blog_config.YBC_BLOG_SHOW_SEARCH_BLOCK) && $blog_config.YBC_BLOG_SHOW_SEARCH_BLOCK)||(isset($blog_config.YBC_BLOG_SHOW_LATEST_NEWS_BLOCK) && $blog_config.YBC_BLOG_SHOW_LATEST_NEWS_BLOCK) ||(isset($blog_config.YBC_BLOG_SHOW_POPULAR_POST_BLOCK) && $blog_config.YBC_BLOG_SHOW_POPULAR_POST_BLOCK) || (isset($blog_config.YBC_BLOG_SHOW_FEATURED_BLOCK) && $blog_config.YBC_BLOG_SHOW_FEATURED_BLOCK)||(isset($blog_config.YBC_BLOG_SHOW_TAGS_BLOCK) && $blog_config.YBC_BLOG_SHOW_TAGS_BLOCK)||(isset($blog_config.YBC_BLOG_SHOW_GALLERY_BLOCK) && $blog_config.YBC_BLOG_SHOW_GALLERY_BLOCK)||(isset($blog_config.YBC_BLOG_SHOW_ARCHIVES_BLOCK) && $blog_config.YBC_BLOG_SHOW_ARCHIVES_BLOCK)||(isset($blog_config.YBC_BLOG_SHOW_COMMENT_BLOCK) && $blog_config.YBC_BLOG_SHOW_COMMENT_BLOCK)||(isset($blog_config.YBC_BLOG_SHOW_AUTHOR_BLOCK) && $blog_config.YBC_BLOG_SHOW_AUTHOR_BLOCK)||(isset($blog_config.YBC_BLOG_ENABLE_RSS) && $blog_config.YBC_BLOG_ENABLE_RSS && isset($blog_config.YBC_BLOC_RSS_DISPLAY) && $blog_config.YBC_BLOC_RSS_DISPLAY && in_array('side_bar',$blog_config.YBC_BLOC_RSS_DISPLAY))) && !$blog_config.YBC_BLOG_SIDEBAR_ON_MOBILE }
</div>
{/if}
  {*
      {if $display_related_products && $blog_post.products}
                        <div id="ybc-blog-related-products" class="m4_lk_box hidden-md-down">
                            <h4 class="title_blog m4_lk_box_title">
                                {if count($blog_post.products) > 1}{l s='Polecane produkty ' mod='ybc_blog'}
                                {else}{l s='Polecane produkty' mod='ybc_blog'}{/if}
                            </h4>
                            <div class="ybc-blog-related-products-wrapper ybc-blog-related-products-list m4_lk_box_content">
                                {assign var='product_row' value='1'}
                                <ul class="dt-{$product_row|intval} blog-product-list product_list grid row ybc_related_products_type_{if $blog_related_product_type}{$blog_related_product_type|escape:'html':'UTF-8'}{else}default{/if}">
                                    {assign var='product_row' value='1'}
                                    {foreach from=$blog_post.products item='product'}
                                        <li class="ajax_block_product col-xs-12 col-sm-4 col-lg-{12/$product_row|intval}">
                                            <div class="product-container">
                                                <div class="left-block">
                                                    <a class="ybc_item_img{if isset($blog_config.YBC_BLOG_LAZY_LOAD)&& $blog_config.YBC_BLOG_LAZY_LOAD} ybc_item_img_ladyload{/if}" href="{$product.link|escape:'html':'UTF-8'}">
                                                        <img src="{if isset($blog_config.YBC_BLOG_LAZY_LOAD) && $blog_config.YBC_BLOG_LAZY_LOAD}{$image_folder|escape:'html':'UTF-8'}bg-grey.png{else}{$product.img_url|escape:'html':'UTF-8'}{/if}" alt="{$product.name|escape:'html':'UTF-8'}" {if isset($blog_config.YBC_BLOG_LAZY_LOAD)&& $blog_config.YBC_BLOG_LAZY_LOAD} data-original="{$product.img_url|escape:'html':'UTF-8'}" data-src="{$product.img_url|escape:'html':'UTF-8'}" class="lazyload"{/if} />
                                                        {if isset($blog_config.YBC_BLOG_LAZY_LOAD)&& $blog_config.YBC_BLOG_LAZY_LOAD}
                                                            <div class="loader_lady_custom"></div>
                                                        {/if}
                                                    </a>
                                                </div>
                                                <div class="right-block">
                                                    <h5><a href="{$product.link|escape:'html':'UTF-8'}">{$product.name|escape:'html':'UTF-8'}</a></h5>
                                                    {if $show_price}
                                                        <div class="blog-product-extra content_price">
                                                            {if $product.price!=$product.old_price}
                                                                <span class="bp-price-old old-price"><span class="bp-price-old-label">{l s='Old price: ' mod='ybc_blog'}</span><span class="bp-price-old-display">{$product.old_price|escape:'html':'UTF-8'}</span></span>
                                                            {/if}
                                                            <span class="bp-price price product-price"><span class="bp-price-label">{l s='Price:  ' mod='ybc_blog'}</span><span class="bp-price-display">{$product.price|escape:'html':'UTF-8'}</span></span>
                                                            {if $product.price!=$product.old_price}
                                                                <span class="bp-percent price-percent-reduction"><span class="bp-percent-label">{l s='Discount: ' mod='ybc_blog'}</span><span class="bp-percent-display">{$product.discount_percent|escape:'html':'UTF-8'}{l s='%' mod='ybc_blog'}</span></span>
                                                                <span class="bp-save"><span class="bp-save-label">{l s='Save up: ' mod='ybc_blog'}</span><span class="bp-save-display">-{$product.discount_amount|escape:'html':'UTF-8'}</span></span>
                                                            {/if}
                                                        </div>
                                                    {/if}
                                                    {if $product.short_description}
                                                        <div class="blog-product-desc">
                                                            {$product.short_description|strip_tags:'UTF-8'|truncate:80:'...'|escape:'html':'UTF-8'}
                                                        </div>
                                                    {/if}
                                                </div>
                                            </div>
                                        </li>
                                    {/foreach}
                                </ul>
                            </div>
                        </div>
                    {/if}
    *}