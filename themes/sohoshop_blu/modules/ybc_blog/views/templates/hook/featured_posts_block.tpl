{*
* 2007-2019 ETS-Soft
*
* NOTICE OF LICENSE
*
* This file is not open source! Each license that you purchased is only available for 1 wesite only.
* If you want to use this file on more websites (or projects), you need to purchase additional licenses. 
* You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
* 
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs, please contact us for extra customization service at an affordable price
*
*  @author ETS-Soft <etssoft.jsc@gmail.com>
*  @copyright  2007-2019 ETS-Soft
*  @license    Valid for 1 website (or project) for each purchase of license
*  International Registered Trademark & Property of ETS-Soft
*}
{if $posts}
    <div class="block {$blog_config.YBC_BLOG_RTL_CLASS|escape:'html':'UTF-8'} ybc_block_featured {if isset($page) && $page}page_{$page|escape:'html':'UTF-8'}{else}page_blog{/if} {if isset($page) && $page=='home'}{if isset($blog_config.YBC_BLOG_HOME_POST_TYPE) && $blog_config.YBC_BLOG_HOME_POST_TYPE=='default' || count($posts)<=1} ybc_block_default{else} ybc_block_slider{/if}{else}{if isset($blog_config.YBC_BLOG_SIDEBAR_POST_TYPE) && $blog_config.YBC_BLOG_SIDEBAR_POST_TYPE=='default' || count($posts)<=1} ybc_block_default{else} ybc_block_slider{/if}{/if}">
        <h4 class="title_blog title_block">{l s='Najnowsze trendy' mod='ybc_blog'}</h4>
        {assign var='product_row' value=$blog_config.YBC_BLOG_HOME_PER_ROW|intval}
        <div class="block_content swiper-container">
            <ul class="swiper-wrapper {if count($posts)>1}{if isset($page) && $page=='home' && $blog_config.YBC_BLOG_HOME_POST_TYPE!='default'}owl-carousel{elseif (!isset($page)||(isset($page) && $page!='home')) && $blog_config.YBC_BLOG_SIDEBAR_POST_TYPE!='default'}owl-carousel{/if}{/if}">
                {foreach from=$posts item='post'}
                    <li {if $page=='home'}class="swiper-slide col-xs-12 col-sm-4 col-lg-{12/$product_row|intval}"{/if}> 
                        {if $post.thumb}<a class="ybc_item_img" href="{$post.link|escape:'html':'UTF-8'}"><img src="{$post.thumb|escape:'html':'UTF-8'}" alt="{$post.title|escape:'html':'UTF-8'}" title="{$post.title|escape:'html':'UTF-8'}" /></a>{/if}
                        <div class="ybc-blog-latest-post-content">
                            <a class="ybc_title_block" href="{$post.link|escape:'html':'UTF-8'}">{$post.title|escape:'html':'UTF-8'}</a>
                            <div class="ybc-blog-sidear-post-meta">
                                {if isset($post.categories) && $post.categories}
                                    <div class="ybc-blog-categories">
                                        {assign var='ik' value=0}
                                        {assign var='totalCat' value=count($post.categories)}                        
                                        <div class="be-categories">
                                            <span class="be-label">{l s='Posted in' mod='ybc_blog'}: </span>
                                            {foreach from=$post.categories item='cat'}
                                                {assign var='ik' value=$ik+1}                                        
                                                <a href="{$cat.link|escape:'html':'UTF-8'}">{ucfirst($cat.title)|escape:'html':'UTF-8'}</a>{if $ik < $totalCat}<span class="comma">, </span>{/if}
                                            {/foreach}
                                        </div>
                                    </div>
                                {/if}
                                <span class="post-date">{if isset($blog_config.YBC_BLOG_DATE_FORMAT)&&$blog_config.YBC_BLOG_DATE_FORMAT}{date($blog_config.YBC_BLOG_DATE_FORMAT, strtotime($post.datetime_added))|escape:'html':'UTF-8'}{else}{date('F jS Y', strtotime($post.datetime_added))|escape:'html':'UTF-8'}{/if}</span>
                            </div>
                            
                            {if $allowComments || $show_views || $allow_like} 
                                <div class="ybc-blog-latest-toolbar">
                                    {if $show_views}
                                        <span class="ybc-blog-latest-toolbar-views">
                                            {$post.click_number|intval} {if $post.click_number!=1}
                                                <span>{l s='Wyświetleń' mod='ybc_blog'}</span>
                                            {else}
                                                <span>{l s='Wyświetleń' mod='ybc_blog'}</span>
                                            {/if}
                                        </span> 
                                    {/if}    
                                    {if $allow_like}
                                        <div class="m4-fb-buttonwrapper"><div class="fb-like" data-href="{$post.link|escape:'html':'UTF-8'}" data-width="75px" data-layout="button_count" data-action="like" data-size="small" data-share="false"></div></div>
                                    {/if}   
                                    {if $allowComments && $post.comments_num>0}
                                        <span class="ybc-blog-latest-toolbar-comments">{$post.comments_num|intval}
                                        {if $post.comments_num!=1}<span>
                                        {l s='comments' mod='ybc_blog'}</span>{else}
                                        <span>{l s='comment' mod='ybc_blog'}</span>{/if}
                                        </span> 
                                    {/if}                             
                                    {if $allow_like}
                                        <div class="m4-fb-buttonwrapper"><div class="fb-like" data-href="{$post.link|escape:'html':'UTF-8'}" data-width="75px" data-layout="button_count" data-action="like" data-size="small" data-share="false"></div></div>
                                    {/if}
                                    
                                </div>
                            {/if}  

                            <a class="btn btn-primary" href="{$post.link|escape:'html':'UTF-8'}">{if $blog_config.YBC_BLOG_TEXT_READMORE}{$blog_config.YBC_BLOG_TEXT_READMORE|escape:'html':'UTF-8'} 
                            <svg class="arrow-right-icon-blog" xmlns="http://www.w3.org/2000/svg" width="14.262" height="9.192" viewBox="0 0 12.262 7.192">
                                <defs></defs>
                                <g transform="translate(0.53 0.53)">
                                    <path d="M-317.515-44.61l-5.6,5.6-5.6-5.6" transform="translate(328.717 44.61)"></path>
                                </g>
                            </svg>{else}{l s='Więcej' mod='ybc_blog'}{/if}</a>
                            
                        </div>
                    </li>
                {/foreach}
            </ul>
            <!-- If we need scrollbar -->
            <div class="swiper-scrollbar"></div>
            {if $blog_config.YBC_BLOG_DISPLAY_BUTTON_ALL_HOMEPAGE || $page!='home'}
                <div class="blog_view_all_button">
                    <a href="{$featured_link|escape:'html':'UTF-8'}" class="view_all_link">{l s='View all featured posts' mod='ybc_blog'}</a>
                </div>
            {/if}
        </div>
        <div class="clear"></div>
    </div>
    
{/if}