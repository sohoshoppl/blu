{**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 *}
<div id="_desktop_user_info">
    <div class="user-info">
        {if $logged}
            <a
                    class="logout"
                    href="{$logout_url}"
                    rel="nofollow"
            >
                <svg xmlns="http://www.w3.org/2000/svg" width="19.883" height="22.785" viewBox="0 0 19.883 22.785">
                    <g transform="translate(0 0)">
                        <path d="M-165.056-575.486a5.849,5.849,0,0,0-5.849,5.849,5.849,5.849,0,0,0,5.849,5.849,5.849,5.849,0,0,0,5.849-5.849A5.849,5.849,0,0,0-165.056-575.486Zm0,10.528a4.679,4.679,0,0,1-4.679-4.679,4.679,4.679,0,0,1,4.679-4.679,4.679,4.679,0,0,1,4.679,4.679A4.679,4.679,0,0,1-165.056-564.958Z"
                              transform="translate(174.997 575.486)"/>
                        <path d="M-176.844-543.22a8.773,8.773,0,0,1,8.772-8.748,8.772,8.772,0,0,1,8.772,8.748h1.17a9.942,9.942,0,0,0-9.942-9.918,9.942,9.942,0,0,0-9.942,9.918Z"
                              transform="translate(178.013 566.005)"/>
                    </g>
                </svg>
                <br>
                <span>{l s='Wyloguj' d='Shop.Theme.Actions'}</span>
            </a>
            {*
          <a
            class="account"
            href="{$my_account_url}"
            title="{l s='View my customer account' d='Shop.Theme.Customeraccount'}"
            rel="nofollow"
          >
            <i class="material-icons hidden-md-up logged">&#xE7FF;</i>
            <span class="hidden-sm-down">{$customerName}</span>
          </a>*}
        {else}
            <a
                    href="{$my_account_url}"
                    title="{l s='Log in to your customer account' d='Shop.Theme.Customeraccount'}"
                    rel="nofollow"
            >
                <svg xmlns="http://www.w3.org/2000/svg" width="19.883" height="22.785" viewBox="0 0 19.883 22.785">
                    <g transform="translate(0 0)">
                        <path d="M-165.056-575.486a5.849,5.849,0,0,0-5.849,5.849,5.849,5.849,0,0,0,5.849,5.849,5.849,5.849,0,0,0,5.849-5.849A5.849,5.849,0,0,0-165.056-575.486Zm0,10.528a4.679,4.679,0,0,1-4.679-4.679,4.679,4.679,0,0,1,4.679-4.679,4.679,4.679,0,0,1,4.679,4.679A4.679,4.679,0,0,1-165.056-564.958Z"
                              transform="translate(174.997 575.486)"/>
                        <path d="M-176.844-543.22a8.773,8.773,0,0,1,8.772-8.748,8.772,8.772,0,0,1,8.772,8.748h1.17a9.942,9.942,0,0,0-9.942-9.918,9.942,9.942,0,0,0-9.942,9.918Z"
                              transform="translate(178.013 566.005)"/>
                    </g>
                </svg>
                <br>
                <span>{l s='Logowanie' d='Shop.Theme.Actions'}</span>
            </a>
        {/if}
    </div>
</div>
