<section class="new-products clearfix mt-3">
    <h2 class="h2 products-section-title">
        {l s='Nowości których nie możesz przegapić' d='Shop.Theme.Catalog'}
    </h2>
    <div class="swiper-container swiper-new">
        {include file="catalog/_partials/productlist.tpl" products=$products cssClass="row" swiper="1"}
        <div class="swiper-button-prev"></div>
        <div class="swiper-button-next"></div>

        <div class="swiper-scrollbar"></div>
    </div>
</section>
