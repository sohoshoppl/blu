{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}


{assign var="wl_count" value=0}
{if $wishlist_products}

    {foreach from=$wishlist_products item="w_product"}
        {assign var="wl_count" value=$wl_count + $w_product.quantity }
    {/foreach}

{/if}
<div id="_desktop_wishlist-top">
    <div id="wishlist-top" class="float-right ">
        <a class="header-icon"
           href="{url entity='module' name='blockwishlist' controller='mywishlist'}"
           title="{l s='Ulubione' mod='blockwishlist'}">
            <svg xmlns="http://www.w3.org/2000/svg" width="22.435" height="20.747" viewBox="0 0 22.435 20.747">
                <path d="M-70.638-554.6l.044-.017a.575.575,0,0,0,.094-.043l.027-.013c.428-.28,10.651-7.049,10.9-14.411a.709.709,0,0,0,.005-.081v-.2a.592.592,0,0,0,0-.061,5.82,5.82,0,0,0-5.829-5.906,6.209,6.209,0,0,0-5.3,3.015l-.089.149-.09-.149a6.208,6.208,0,0,0-5.295-3.015,5.818,5.818,0,0,0-5.828,5.9.63.63,0,0,0,0,.071v.193a.647.647,0,0,0,0,.073c.251,7.371,10.475,14.141,10.91,14.425h0a.064.064,0,0,1,.021.01.543.543,0,0,0,.091.041l.024.01a.551.551,0,0,0,.159.028h.015A.554.554,0,0,0-70.638-554.6Zm-.145-1.2-.06-.042c-1.665-1.163-9.973-7.275-10.049-13.428a4.8,4.8,0,0,1,4.724-4.947,5.08,5.08,0,0,1,4.853,3.635.578.578,0,0,0,.027.064l.011.027a.216.216,0,0,0,.028.047l.026.038a.3.3,0,0,0,.04.047l.033.032a.243.243,0,0,0,.039.032l.038.025a.283.283,0,0,0,.029.019l.038.016a.609.609,0,0,0,.065.027l.037.007a.493.493,0,0,0,.072.014h.028a.727.727,0,0,0,.08,0,.361.361,0,0,0,.041-.007l.066-.014a.356.356,0,0,0,.039-.016l.036-.015.027-.011.032-.022c.028-.018.043-.027.056-.038l.025-.026a.483.483,0,0,0,.05-.05l.025-.038a.444.444,0,0,0,.034-.051l.017-.039a.545.545,0,0,0,.025-.062,5.085,5.085,0,0,1,4.856-3.642,4.8,4.8,0,0,1,4.724,4.938c-.071,6.149-8.384,12.271-10.049,13.437Z"
                      transform="translate(82 575.323)"/>
            </svg>
            <span class="counter">{$wl_count}</span>
            <br>
            <span class="header-icon-txt">{l s='Ulubione' mod='blockwishlist'}</span>
        </a>
    </div>
</div>
{strip}
    <script>
        static_token = prestashop.static_token;
        wishlistProductsIds ={$wishlist_products|json_encode nofilter};
        loggin_required = '{l s='Musisz być zalogowany aby zarządzać Ulubionymi'  mod='blockwishlist'}';
        added_to_wishlist = '{l s='Produkt został dodany Ulubionych' mod='blockwishlist'}';
        mywishlist_url = '{$link->getModuleLink('blockwishlist', 'mywishlist', array(), true)|escape:'quotes':'UTF-8'}';
        baseDir = '{$urls.base_url}';
        {if $customer.is_logged}
        isLoggedWishlist = true;
        isLogged = true;
        {else}
        isLoggedWishlist = false;
        isLogged = false;
        {/if}
    </script>
{/strip}
