{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{if isset($wishlists)}
    <div class="wishlist">
        <a data-default="{$default}" class="addToWishlist with_default wishlistProd_{$product.id_product|intval} hearticon{if isset($product_in_wishlist) && $product_in_wishlist} active{/if}" href="#" title="{l s='lista życzeń'  mod='blockwishlist'}" rel="{$product.id_product|intval}" onclick="WishlistCart('wishlist_block_list', '{if isset($product_in_wishlist) && $product_in_wishlist}delete{else}add{/if}', '{$product.id_product|intval}', false, 1, '{$default}'); return false;">
            <img src="{$urls.img_url}svg_icons/hart.svg"
            alt="{l s='Dodaj do listy życzeń' d='Shop.Theme.Catalog'}"
            title="{l s='Dodaj do listy życzeń' d='Shop.Theme.Catalog'}">
        </a>
    </div>
{else}
    <div class="wishlist">
        <a class="addToWishlist wishlistProd_{$product.id_product|intval} hearticon{if isset($product_in_wishlist) && $product_in_wishlist} active{/if}" href="#" title="{l s='lista życzeń'  mod='blockwishlist'}" rel="{$product.id_product|intval}" onclick="WishlistCart('wishlist_block_list', '{if isset($product_in_wishlist) && $product_in_wishlist}delete{else}add{/if}', '{$product.id_product|intval}', false, 1);
                return false;">
             <img src="{$urls.img_url}svg_icons/hart.svg"
                alt="{l s='Dodaj do listy życzeń' d='Shop.Theme.Catalog'}"
                title="{l s='Dodaj do listy życzeń' d='Shop.Theme.Catalog'}">
        </a>
    </div>
{/if}