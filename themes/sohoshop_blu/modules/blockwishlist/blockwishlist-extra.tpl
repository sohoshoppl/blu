{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{*
<input type="hidden" id="idCombination" value="{$product.id_product_attribute}" />
<button type="button" class="wishlist{if isset($product_in_wishlist) && $product_in_wishlist} active {/if}" onclick="WishlistCart('wishlist_block_list', '{if isset($product_in_wishlist) && $product_in_wishlist}delete{else}add{/if}', '{$id_product|intval}', $('#idCombination').val(), document.getElementById('quantity_wanted').value); return false;" title="{l s='Add to my wishlist'}">
<i class="far fa-heart"></i> {l s='Do ulubionych' mod='blockwishlist'}
</button>
*}
{*if isset($wishlists) && count($wishlists) > 1}
<div class="buttons_bottom_block no-print">
<div id="wishlist_button">
<select id="idWishlist">
{foreach $wishlists as $wishlist}
3	<option value="{$wishlist.id_wishlist}">{$wishlist.name}</option>
{/foreach}
</select>
<button class="" onclick="WishlistCart('wishlist_block_list', '{if isset($product_in_wishlist) && $product_in_wishlist}delete{else}add{/if}', '{$id_product|intval}', $('#idCombination').val(), document.getElementById('quantity_wanted').value, $('#idWishlist').val()); return false;"  title="{l s='Add to wishlist' mod='blockwishlist'}">
2{l s='Add' mod='blockwishlist'}
</button>
</div>
</div>
{else}
<p class="buttons_bottom_block no-print">
<a id="wishlist_button" href="#" onclick="WishlistCart('wishlist_block_list', '{if isset($product_in_wishlist) && $product_in_wishlist}delete{else}add{/if}', '{$id_product|intval}', $('#idCombination').val(), document.getElementById('quantity_wanted').value); return false;" rel="nofollow"  title="{l s='Add to my wishlist' mod='blockwishlist'}">
<i class="fas fa-edit"></i> {l s='Dodaj do listy zakupowej' mod='blockwishlist'}
</a>
</p>
{/if*}
<!-- blockwishlist-extra -->

{foreach from=$wishlist_products item=wishlist_product}
    {if $wishlist_product.id_product == $id_product }
        {assign var="wishlist_quantity" value=$wishlist_product.quantity}
    {/if}
{/foreach}
<div class="col-lg-6 col-xs-6 mobile-blockwishlist-extra">
    <input type="hidden" id="idCombination" value="{if isset($product.id_product_attribute)}{$product.id_product_attribute}{else}0{/if}"/>
    <img src="{$urls.img_url}svg_icons/hart.svg"
         alt="{l s='Dodaj do listy życzeń' d='Shop.Theme.Catalog'}"
         title="{l s='Dodaj do listy życzeń' d='Shop.Theme.Catalog'}">
    {if is_array($wishlists) && isset($wishlists) && count($wishlists) > 1}
        <a class="wishlist">
            {foreach name=wl from=$wishlists item=wishlist}
                {if $smarty.foreach.wl.first}
                    <span class="wishlist_button_list" title="{l s='Wishlist' mod='blockwishlist'}">
                {if isset($product_in_wishlist) && $product_in_wishlist}{l s='Usuń z ulubionych'  mod='blockwishlist'}{else}{l s='Do ulubionych'  mod='blockwishlist'}{/if}</span>
            <ul class="wishlist_list" style="display: none;">
            {/if}
            <li title="{$wishlist.name}" value="{$wishlist.id_wishlist}"
                onclick="WishlistCart('wishlist_block_list', '{if isset($product_in_wishlist) && $product_in_wishlist}delete{else}add{/if}', '{$id_product|intval}', $('#idCombination').val(), document.getElementById('quantity_wanted').value, '{$wishlist.id_wishlist}');">
                {l s='Dodaj do %s' sprintf=[$wishlist.name] mod='blockwishlist'}
            </li>
            {if $smarty.foreach.wl.last}
            </ul>
        {/if}
        {*
        {foreachelse}
        <a href="#" id="wishlist_button_nopop" onclick="WishlistCart('wishlist_block_list', 'add', '{$id_product|intval}', $('#idCombination').val(), document.getElementById('quantity_wanted').value); return false;" rel="nofollow"  title="{l s='Add to my wishlist' mod='blockwishlist'}">
        <span class="lnr lnr-list"></span></i> {l s='Dodaj do listy zakupowej' mod='blockwishlist'}
        </a>*}
    {/foreach}
</a>
{else}
    <a class="wishlist addToWishlist wishlistProd_{$id_product|intval} {if isset($product_in_wishlist) && $product_in_wishlist}active{/if}"
       href="#" rel="nofollow" data-rel="{$id_product|intval}"
       onclick="WishlistCart('wishlist_block_list', '{if isset($product_in_wishlist) && $product_in_wishlist}delete{else}add{/if}', '{$id_product|intval}', $('#idCombination').val(), document.getElementById('quantity_wanted').value);
               return false;">
{if isset($product_in_wishlist) && $product_in_wishlist}{l s='Usuń z ulubionych'  mod='blockwishlist'}{else}{l s='Dodaj do listy życzeń'  mod='blockwishlist'}{/if}
</a>
{/if}

</div>

<div class="col-lg-6 col-xs-6"><img src="{$urls.img_url}svg_icons/question.svg"
                           alt="{l s='Zapytaj o produkt' d='Shop.Theme.Catalog'}"
                           title="{l s='Zapytaj o produkt' d='Shop.Theme.Catalog'}"><a
                           href="#sohoshop_productask">{l s='Zapytaj o produkt' d='Shop.Theme.Catalog'}</a></div>    


