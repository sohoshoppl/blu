<section class="featured-products clearfix mt-3">
    <h2 class="h2 products-section-title">
        {l s='Wybrane dla Ciebie' d='Shop.Theme.Catalog'}
    </h2>
    <div class="swiper-container swiper-cross">
        {include file="catalog/_partials/productlist.tpl" products=$products cssClass="row" swiper="1"}
        <!-- If we need pagination -->
        <div class="swiper-pagination"></div>

        <!-- If we need navigation buttons -->
        {* <div class="swiper-button-prev"></div>
        <div class="swiper-button-next"></div> *}

        <!-- If we need scrollbar -->
        <div class="swiper-scrollbar"></div>
    </div>
</section>


