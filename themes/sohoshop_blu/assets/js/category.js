$(document).on('click', '#search_filter_toggler_blu', function () {
    $('#search_filters_wrapper').css('transform', 'none');
});

$(document).on('click', '#category #left-column #search_filters_af .facet-title, #search_filter_toggler_blu', function (e) {
    if (e.target !== this) return;
    $('#amazzing_filter').addClass('amazzing_filtershow');
    
});

$(document).on('click', '.af-compact-overlay,#amazzing_filter.block .title_block .material-icons', function (e) {
    $('#amazzing_filter').removeClass('amazzing_filtershow');
});


// $(document).on('click', '.mobile-clear-widget-filter', function (e) {

//     var newUrl ='';
//     var dataLabel = $(this).data('facet-label');
//     var windowUrl = decodeURI(window.location.href);
//     var windowUrlLength = windowUrl.length;
//     var stringIndex = windowUrl.indexOf(dataLabel);
//     var checkSpecial = windowUrl.indexOf('/');

//     var indexs = [];
//     for(var i=0; i<windowUrl.length;i++) {
//         if (windowUrl[i] === "/") indexs.push(i);
//     }
    
//     var stringEndPosition  = indexs.every(index => index < stringIndex);
//     var stringMiddlePosition  = indexs.find(index=>index>stringIndex);   
//     String.prototype.replaceBetween = function(start, end, what) {
//         return this.substring(0, start) + what + this.substring(end);
//     };
//     if ( stringEndPosition ) {
//         newUrl = windowUrl.replaceBetween(stringIndex+dataLabel.length+1, windowUrlLength, "0");
//     } else {
//         newUrl = windowUrl.replaceBetween(stringIndex+dataLabel.length+1, stringMiddlePosition, "0");
//     }

//     if ( stringIndex == -1 ) {
//         return;
//     }
    
//     window.location = newUrl;

// });

$(document).ready(function () {
    var category_page = 1;
    $(document).on('click', '.ajaxLoad-more', function (event) {
        event.preventDefault();
        category_page += 1;
        var templateLoader =
            `<div class="faceted-overlay">
				<div class="overlay__inner">
					<div class="overlay__content"><span class="spinner"></span></div>
				</div>
			</div>`;

        $('body').append(templateLoader);
        var body_id = $('body').attr('id');
        var urlajaxsearach = '';
        if (body_id == 'search') {
            urlajaxsearach = prestashop.urls.current_url + '&page=' + category_page +'&from-xhr';
        } else {
            urlajaxsearach = prestashop.urls.current_url + '?page=' + category_page +'&from-xhr';
        }
        $.ajax({
            url: urlajaxsearach,
            dataType: 'json',
            success: function(result){
                console.log(result);
                let rendered_products = $( '<div></div>' );
                rendered_products.html(result.rendered_products)
                $("#jsProductsFrom").text(result.pagination.items_shown_to);
                $("#js-product-list .products.row").append(rendered_products.find('#js-product-list .products.row').html());
                $('.faceted-overlay').remove();
            },
        });
    })


    //slider z filtrami wygląd pomieszczenie
    var body_class = $('body').attr('class');
    if (body_class.indexOf('category-id-parent-2') !== -1) {
        var sohoshopcategorysliders = [];
        $('#sohoshopcategoryslider article').each(function( index ) {
            sohoshopcategorysliders[index] = new Swiper(".looksSlider-"+index, {
                slidesPerView: "auto",
                spaceBetween: 50,
                loop: true,
                loopFillGroupWithBlank: true,
                pagination: {
                    el: ".swiper-pagination",
                    clickable: true,
                },
                navigation: {
                    nextEl: ".swiper-button-next",
                    prevEl: ".swiper-button-prev",
                },
            });
        });

          var swiperN = new Swiper('.swiper-new', {
            slidesPerView: 1.3,
            loop: true,
            spaceBetween: 20,
            breakpoints: {
                // when window width is >= 992px
                992: {
                    slidesPerView: 4.3,
                    spaceBetween: 20
                },
            },

            scrollbar: {
                el: '.swiper-scrollbar',
                hide: true,
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
        });
    }

});
