//funkcja do ładowania plików js
function script(url) {
    var s = document.createElement('script');
    s.type = 'text/javascript';
    s.async = true;
    s.src = url;
    var x = document.getElementsByTagName('body')[0];
    x.appendChild(s);
}

$(function() {
    var body_id = $('body').attr('id');
    if(body_id == 'index'){
        script(prestashop.urls.js_url+'index.js');
    }
    if(body_id == 'product'){
        script(prestashop.urls.js_url+'product.js');
    }
    if(body_id == 'category' || body_id == 'search' ){
        script(prestashop.urls.js_url+'category.js');
    }

    $('#chooseShopHeaderPopup').on('shown.bs.modal', chooseShopHeaderPopup);

});

$(document).on('click', 'a[href^="#"]:not([role="modal"])', function (event) {
    event.preventDefault();

    $('html, body').animate({
        scrollTop: $($.attr(this, 'href')).offset().top
    }, 500);
});
document.addEventListener('DOMContentLoaded', (event) => {
    const windowscheck = window.innerWidth;
    if (windowscheck > 767) {
        lightGallery(document.getElementById('lightgallery'), {
            counter : false,
            download : false,
            hideBarsDelay :0,
        });
        lightGallery(document.getElementById('lightgallery2'), {
            counter : false,
            download : false,
            hideBarsDelay :0,
        });
    }
    const newspaperbtn = document.querySelector('#header #pappers-top .collapsePappersTop-btn');
    const newspaperoverlay = document.querySelector('#pappers-top .overlay');
    newspaperoverlay.addEventListener("click",function(e){
        newspaperbtn.click();
    });

    const menuItemHover = document.querySelectorAll('#_desktop_top_menu .top-menu[data-depth="0"] > li.category');
    const wrapperToggle = document.querySelector('#wrapper');
    [].forEach.call( menuItemHover, function(menuItem) {
        menuItem.addEventListener("mouseenter", function( event ) {
            if (windowscheck > 767) {
                wrapperToggle.classList.add('overlay');
            }
            
        });
        menuItem.addEventListener("mouseleave", function( event ) {
            if (windowscheck > 767) {
                wrapperToggle.classList.remove('overlay');
            }
        });
     });
     const categoryMenuClose = document.querySelectorAll('.close_category_menu');
     const menuMobileIcon = document.getElementById('menu-icon');
     [].forEach.call( categoryMenuClose, function(categoryMenu) {
        categoryMenu.addEventListener("click", function( event ) {
            menuMobileIcon.click();
        })

     });
     const menuShowAll = document.querySelectorAll('.show_all_menu'); 
     [].forEach.call( menuShowAll, function(menuAll) {
        menuAll.addEventListener("click", function( event ) {
            event.preventDefault();
            const windowLocation =this.closest('.dekstop-menu_wrapper').children[0].getAttribute("href");
            window.location.href = windowLocation;
        })
     });
});

//obsługa markerów/mapowania obrazków
$(document).on('click','body',function(){
    if ($(".marker_content").length){
        $( ".marker_content" ).removeClass('active');
    }
});

//obsługa markerów/mapowania obrazków
$(document).on('click','.marker-icon',function(e){
    $( ".marker_content" ).removeClass('active');
    $( this ).parent().find('.marker_content').toggleClass('active');
    e.stopPropagation();
});

$(document).on('click','.marker .btn-inspire',function(e){
   e.preventDefault();
   console.log($(this).prop('href'));
    window.location.href = $(this).prop('href');
});





function chooseShopHeaderPopup(){
    navigator.geolocation.getCurrentPosition(chooseShopHeaderPopupFindNearest);
}

function chooseShopHeaderPopupFindNearest(pos){
    $.get(prestashop.urls.base_url + 'module/sohoshop_wokolicy/ajax?action=findNearest&lat=' + pos.coords.latitude + '&long=' + pos.coords.longitude, function (data) {
        if (data.code == 'ok') {
            $('#chooseShopHeaderPopupNearestShop').html(
                `<h1>` + data.result[0].store.name + `</h1>
                 <div>
                    <img class="pin" src="/themes/sohoshop_blu/assets/img/point-icon.svg" alt="Najbliższy salon" >
                    <address>
                        ` + data.result[0].store.postcode + ' ' + data.result[0].store.city + `<br>
                        ` + data.result[0].store.address1 + `
                    </address>
                    <button class="btn btn-primary" onclick="chooseSalon(` + data.result[0].store.id_store + `);">
                        ZAREZERWUJ W SALONIE
                    </button>                    
                 </div>`
            );
        }
    });
}

function chooseShopHeaderPopupFindNearestFindShop(e) {
    let query = $('input[name="findShopQuery"]').val();
    if (query.length > 0 ){
        $.get('https://nominatim.openstreetmap.org/search?format=json&countrycodes=pl&q=' + query, function (data) {
            chooseShopHeaderPopupFindNearest({
                coords: {latitude: data[0].lat, longitude: data[0].lon}
            })
        });
    }else{
        $('input[name="findShopQuery"]').get(0).reportValidity();
    }

}

$(document).on('click', '#_mobile_stores_top a', function (event) {
    event.preventDefault();
    $('#chooseShopHeaderPopup').modal('show');
});
