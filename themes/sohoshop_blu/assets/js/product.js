if ($('.miniaturesSlider .thumb-container').length >= 3 ) {
    var swiperI = new Swiper('.swiper-images', {
        direction: 'horizontal',
        slidesPerView: 1,
        loop: false,
        on: {
            init: function(){
                var totalSlides = document.querySelectorAll('.miniaturesSlider .thumb-container').length;
                document.querySelector('.total-slides').innerHTML = totalSlides;
            },
            slideChange:function(swiper){
                document.querySelector('.current-slide').innerHTML = swiper.activeIndex+1;
            },
        },
        scrollbar: {
            el: '.swiper-scrollbar',
        },
        navigation: {
            nextEl: '.custom-button-next',
            prevEl: '.custom-button-prev',
        },
        breakpoints: {
            768: {
                direction: 'vertical',
                slidesPerView: 3,
                loop: true,
                spaceBetween: 20,
            },
        },
    });

} else if ( $('.miniaturesSlider .thumb-container').length < 3) {
    var swiperI = new Swiper('.swiper-images', {
        direction: 'horizontal',
        slidesPerView: 1,
        loop: false,
        on: {
            init: function(){
                var totalSlides = document.querySelectorAll('.miniaturesSlider .thumb-container').length;
                document.querySelector('.total-slides').innerHTML = totalSlides;
            },
            slideChange:function(swiper){
                document.querySelector('.current-slide').innerHTML = swiper.activeIndex+1;
            },
        },
        scrollbar: {
            el: '.swiper-scrollbar',
        },
        navigation: {
            nextEl: '.custom-button-next',
            prevEl: '.custom-button-prev',
        },
        breakpoints: {
            768: {
                direction: 'vertical',
                slidesPerView: "auto",
                loop: true,
                spaceBetween: 20,
            },
        },
    });
}


var swiperC = new Swiper('.swiper-cross', {
    slidesPerView: 1.3,
    loop: true,
    spaceBetween: 20,
    breakpoints: {
        // when window width is >= 992px
        992: {
            slidesPerView: 4.3,
            spaceBetween: 20
        },
    },

    scrollbar: {
        el: '.swiper-scrollbar',
        hide: true,
    },
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
});

var swiperV = new Swiper('.swiper-viwed', {
    slidesPerView: 1.3,
    spaceBetween: 20,
    loop: true,
    breakpoints: {
        767: {
            slidesPerView: 2.3,
            spaceBetween: 20
        },
        // when window width is >= 992px
        992: {
            slidesPerView: 4.3,
            spaceBetween: 20
        },
    },

    scrollbar: {
        el: '.swiper-scrollbar',
        hide: true,
    },
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
});

var swiperB = new Swiper('.swiper-product-blog', {
    slidesPerView: 1.3,
    spaceBetween: 20,
    slidesPerGroup: 1,
    loop: true,
    breakpoints: {
        767: {
            slidesPerView: 2,
            spaceBetween: 14
        },
        // when window width is >= 992px
        992: {
            slidesPerView: 3,
            spaceBetween: 14
        },
    },

    scrollbar: {
        el: '.swiper-scrollbar',
        hide: true,
    },
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
});