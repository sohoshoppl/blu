/* slider */
var swiper = new Swiper('.featured-products .swiper-container', {
    slidesPerView: 1.3,
    loop: true,
    breakpoints: {
        // when window width is >= 992px
        992: {
            slidesPerView: 4.3,
            spaceBetween: 20
        },
    },

    scrollbar: {
        el: '.swiper-scrollbar',
        hide: false,
    },
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
});
if (window.innerWidth < 992) {
    var swiper2 = new Swiper('#soshohomeindexation .swiper-container', {
        slidesPerView: 3.5,
        spaceBetween: 20,
        scrollbar: {
            el: '.swiper-scrollbar',
            hide: false,
        },
    });

    var swiper2 = new Swiper('#soshohomecategories', {
        slidesPerView: 1.5,
        spaceBetween: 20,
        scrollbar: {
            el: '.swiper-scrollbar',
            hide: false,
        },
    });
}

window.addEventListener('resize', function() {
    console.log(window.innerWidth )
    if (window.innerWidth < 992) {
        var swiper2 = new Swiper('#soshohomecategories', {
            slidesPerView: 1.5,
            spaceBetween: 20,
            scrollbar: {
                el: '.swiper-scrollbar',
                hide: false,
            },
        });
    }
});

var swiper3 = new Swiper('#index .ybc_block_featured .swiper-container', {
    slidesPerView: 1.5,
    spaceBetween: 20,
    breakpoints: {
        // when window width is >= 992px
        992: {
            slidesPerView: 3,
            spaceBetween: 20
        },
    },
    scrollbar: {
        el: '.swiper-scrollbar',
        hide: false,
    },

});