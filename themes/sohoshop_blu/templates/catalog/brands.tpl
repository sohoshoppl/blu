{**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 *}
{extends file=$layout}

{block name='content'}
  <section id="main">
  

    <div id="search_banner">
        <img src="{$urls.img_url}brands-banner.jpg" class="img-fluid img-width img-manufacturer">
        <div id="search">
          <div class="strefa_marek"> Strefa Marek </div>
          <div class="description"> 
            Nasz sklep prezentuje ofertę wielu producentów rodzimych i zagranicznych.<br>
            Produkty firm z naszego katalogu producentów charakteryzują się wysoką jakością, są modne, atrakcyjne i dopracowane technologicznie. 
          </div>
          <div id="search_widget" class="search-widget">
            <form class="form-search-brand" method="get" action="">
              <input type="hidden" name="controller" value="search">
              <input class="search-brand" type="text" name="s" placeholder="{l s='Wpisz markę której szukasz' d='Shop.Theme.Catalog'}" aria-label="{l s='Search' d='Shop.Theme.Catalog'}">
              <button class="search" type="submit">
                <i class="material-icons search">&#xE8B6;</i>
                <span class="hidden-xl-down">{l s='Wpisz markę, której szukasz' d='Shop.Theme.Catalog'}</span>
              </button>
            </form>
          </div>
        </div>
      </div>


      <div class="alfabetyczne">
        <span class="alfabetyczne">Marki alfabetycznie:</span> 
        <span class="alfabet">
          {foreach from=$letters item=$letter}
            <a onclick="scrollBrand('{$letter}')">{$letter}</a>
          {/foreach}
        </span>

      </div>
    

    {block name='brand_miniature'}
    {foreach from=$letters item=$letter}
      <h2><div class="brand-listing-padding" id="{$letter}">{$letter}</div></h2>
        <ul class="brand-listing-padding-ul">
          {foreach from=$brands item=brand}
            {if $brand.name|substr:0:1 eq $letter}
              {include file='catalog/_partials/miniatures/brand.tpl' brand=$brand}
            {/if}
          {/foreach}
        </ul>
      {/foreach}
    {/block}
  </section>
{/block}
 {literal}
      <script>
        function scrollBrand(id){
          document.getElementById(id).scrollIntoView()
        }
      </script>
    {/literal}
