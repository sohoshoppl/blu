{*
 * Classic theme doesn't use this subtemplate, feel free to do whatever you need here.
 * This template is generated at each ajax calls.
 * See ProductListingFrontController::getAjaxProductSearchVariables()
 *}
{if isset($category.description)}
<div id="js-product-list-bottom">
    {if $category.description}
        <div id="category-description" class="text-muted">{$category.description nofilter}</div>
    {/if}
</div>
{/if}