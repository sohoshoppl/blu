{**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 *}
<div id="js-product-list-top" class="clearfix">
    <div class="total-products">
        ( {$listing.pagination.total_items} )
    </div>
    <div class="sort-by-row">

        {* {if !empty($listing.rendered_facets)}
            <div class="col-xs-12 hidden-md-up filter-button text-center">
            
                <button id="search_filter_toggler_blu" class="btn btn-primary">
                    {l s='Filtruj wg' d='Shop.Theme.Actions'}
                    <svg class="arrow-right-icon-category" xmlns="http://www.w3.org/2000/svg" width="14" height="9" viewBox="0 0 12.262 7.192">
                            <defs></defs>
                            <g transform="translate(0.53 0.53)">
                                <path d="M-317.515-44.61l-5.6,5.6-5.6-5.6" transform="translate(328.717 44.61)"></path>
                            </g>
                    </svg>
                </button>
            </div>
        {/if} *}
        <button id="search_filter_toggler_blu" class="btn btn-primary hidden-md-up">
            {l s='Filtruj wg' d='Shop.Theme.Actions'}
            <svg class="arrow-right-icon-category" xmlns="http://www.w3.org/2000/svg" width="14" height="9" viewBox="0 0 12.262 7.192">
                    <defs></defs>
                    <g transform="translate(0.53 0.53)">
                        <path d="M-317.515-44.61l-5.6,5.6-5.6-5.6" transform="translate(328.717 44.61)"></path>
                    </g>
            </svg>
        </button>
        {block name='sort_by'}
            {include file='catalog/_partials/sort-orders.tpl' sort_orders=$listing.sort_orders}
        {/block}
    </div>
    {*
    <div class="col-sm-12 hidden-md-up text-sm-center showing">
        {l s='Showing %from%-%to% of %total% item(s)' d='Shop.Theme.Catalog' sprintf=[
        '%from%' => $listing.pagination.items_shown_from ,
        '%to%' => $listing.pagination.items_shown_to,
        '%total%' => $listing.pagination.total_items
        ]}
    </div>
*}
</div>
