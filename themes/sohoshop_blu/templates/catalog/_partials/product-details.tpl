<div class="row-product-details {if $product.show_availability == "unavailable"}unavailable-detail{/if}">
    <div id="product-availability" class="{if $product.show_availability == "unavailable"}col-lg-12{else}col-lg-6{/if}">
        <span>{l s='Dostępność' d='Shop.Theme.Catalog'}:</span>
        <strong>
            {if $product.show_availability}
                {if $product.availability == 'available'}
                    {$product.quantity} {$product.unity}
                {elseif $product.availability == 'last_remaining_items'}
                    <i class="material-icons product-last-items">&#xE002;</i>
                {else}
                    <i class="material-icons product-unavailable">&#xE14B;</i>
                {/if}
                {if $product.availability_message}
                    {$product.availability_message}
                {/if}
            {/if}
        </strong>
    </div>
    <div id="delivery-information" class="col-lg-6 {if $product.show_availability == "unavailable"} hidden{/if}">
        {if $product.additional_delivery_times == 1}
            {if $product.delivery_information}
                <span>{l s='Wysyłka' d='Shop.Theme.Catalog'}:</span>
                <strong>{$product.delivery_information}</strong>
            {/if}
        {elseif $product.additional_delivery_times == 2}
            {if $product.quantity > 0}
                <span>{$product.delivery_in_stock}</span>
                {* Out of stock message should not be displayed if customer can't order the product. *}
            {elseif $product.quantity <= 0 && $product.add_to_cart_url}
                <span>{$product.delivery_out_stock}</span>
            {/if}
        {/if}
    </div>
</div>

{block name='product_out_of_stock'}
    <div class="product-out-of-stock">
        {hook h='actionProductOutOfStock' product=$product}
    </div>
{/block}

<section id="productAdditionalLinks">
    <div class="row">
        {block name='hook_display_reassurance'}
            {hook h='displayReassurance'}
        {/block}
    </div>
    <div class="row">

        <div class="col-lg-6 col-xs-6"><img src="{$urls.img_url}svg_icons/phone.svg"
                                   alt="{l s='Zadzwoń i zamów' d='Shop.Theme.Catalog'}"
                                   title="{l s='Zadzwoń i zamów' d='Shop.Theme.Catalog'}"><a
                                   href="#sohoshop_blockstores">{l s='Zadzwoń i zamów' d='Shop.Theme.Catalog'}</a></div>
        <div class="col-lg-6 col-xs-6 mobile-comparisons-row"><img src="{$urls.img_url}svg_icons/compare.svg"
                                   alt="{l s='Dodaj do porównania' d='Shop.Theme.Catalog'}"
                                   title="{l s='Dodaj do porównania' d='Shop.Theme.Catalog'}"><a class="js-addToCompare"
                                   href="#" data-id_product="{$product.id|intval}">{l s='Dodaj do porównania' d='Shop.Theme.Catalog'}</a></div>                    
    </div>
    <div class="row">
        <div class="col-lg-6 col-xs-6" ></div>
        {if $product.attachments}
        <div class="col-lg-6 col-xs-6 mobile-attachments-row"><img src="{$urls.img_url}svg_icons/download.svg"
                                   alt="{l s='Do pobrania' d='Shop.Theme.Catalog'}"
                                   title="{l s='Do pobrania' d='Shop.Theme.Catalog'}"><a
                                   href="#productAttachments">{l s='Do pobrania' d='Shop.Theme.Catalog'}</a></div>
        {/if}
    </div>
</section>
{block name='product_accessories'}
    {if $accessories}
        <section class="product-accessories clearfix hidden-md-up">
            <div class="sectionTitle">{l s='Inne z serii' d='Shop.Theme.Catalog'}:</div>
            <div class="product-accessories-container">
                <div class="products collapse in" id="product-accessories-collapse" aria-expanded="true">
                    {foreach from=$accessories item="product_accessory" key="position"}
                        <div class="product-accessories-item {if $product_accessory.availability == 'unavailable'}unavailable-mask{/if}">
                        
                            {if $product_accessory.cover}
                                <a href="{$product_accessory.url}" class="thumbnail product-thumbnail">
                                    <img
                                            src="{$product_accessory.cover.bySize.home_default.url}"
                                            alt="{if !empty($product_accessory.cover.legend)}{$product_accessory.cover.legend}{else}{$product_accessory.name}{/if}"
                                            data-full-size-image-url="{$product_accessory.cover.large.url}"
                                            width="110"
                                    />
                                </a>
                            {else}
                                <a href="{$product_accessory.url}" class="thumbnail product-thumbnail">
                                    <img src="{$urls.no_picture_image.bySize.home_default.url}"
                                        alt="{if !empty($product_accessory.cover.legend)}{$product_accessory.cover.legend}{else}{$product_accessory.name|truncate:30:'...'}{/if}"
                                        width="110"/>
                                </a>
                            {/if}
                            <div>
                                <h3 class="h3 product-title">{$product_accessory.name}</h3>
                                <div class="product-subtitle">{hook h='displayProductAdditionalInfo' product=$product_accessory}</div>
                                {if $product_accessory.availability == 'unavailable'}
                                    <a href="{$product_accessory.url}" class="unavaible-msg">{l s='Powiadom mnie o dostępności' d='Shop.Theme.Catalog'}</a>
                                {/if}
                            </div>
                        </div>
                    {/foreach}
                </div>
                <div class="sectionTitle inner">
                    <span class="less-accessories">{l s='Zobacz mniej wariantów' d='Shop.Theme.Catalog'}</span>
                    <span class="more-accessories">{l s='Zobacz więcej wariantów' d='Shop.Theme.Catalog'}</span>
                    <div class="hidden-md-up collapse-mobile" data-target="#product-accessories-collapse" data-toggle="collapse">
                        <span class="collapse-icons">
                            <i class="material-icons add">&#xE313;</i>
                            <i class="material-icons remove">&#xE316;</i>
                        </span>
                    </div>
                </div>
            </div>
        </section>
    {/if}
{/block}
<div class="product_features_wrapper_mobile hidden-md-up">
    {block name='product_features'}
        {if $product.grouped_features}
            <section class="product-features">
                <div class="sectionTitle collapse-title">{l s='Szczegóły techniczne' d='Shop.Theme.Catalog'}:
                    <div class="hidden-md-up collapse-mobile" data-target="#mobile-describe-features" data-toggle="collapse">
                        <span class="collapse-icons">
                            <i class="material-icons add">&#xE313;</i>
                            <i class="material-icons remove">&#xE316;</i>
                        </span>
                    </div>
                </div>
                <div class="collapse" id="mobile-describe-features">
                    <dl class="data-sheet" >
                        {foreach from=$product.grouped_features item=feature}
                            <dt class="name">{$feature.name}:</dt>
                            <dd class="value"><strong>{$feature.value|escape:'htmlall'|nl2br nofilter}</strong></dd>
                                {/foreach}
                    </dl>
                </div>
            </section>
        {/if}
    {/block}

    {* if product have specific references, a table will be added to product details section *}
    {block name='product_specific_references'}
        {if !empty($product.specific_references)}
            <section class="product-features">
                <p class="h6">{l s='Specific References' d='Shop.Theme.Catalog'}</p>
                <dl class="data-sheet">
                    {foreach from=$product.specific_references item=reference key=key}
                        <dt class="name">{$key}</dt>
                        <dd class="value">{$reference}</dd>
                    {/foreach}
                </dl>
            </section>
        {/if}
    {/block}

    {block name='product_condition'}
        {if $product.condition}
            <div class="product-condition">
                <label class="label">{l s='Condition' d='Shop.Theme.Catalog'} </label>
                <link itemprop="itemCondition" href="{$product.condition.schema_url}"/>
                <span>{$product.condition.label}</span>
            </div>
        {/if}
    {/block}      
</div>
