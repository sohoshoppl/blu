{**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 *}
{block name='product_miniature_item'}
    <div class="swiper-slide" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
        {if isset($position)}
            <meta itemprop="position" content="{$position}" />{/if}
        <article class="product-miniature js-product-miniature" data-id-product="{$product.id_product}"
                 data-id-product-attribute="{$product.id_product_attribute}" itemprop="item" itemscope
                 itemtype="http://schema.org/Product">
            <div class="thumbnail-container">
                {hook h='displayProductListFunctionalButtons' product=$product}
                {block name='product_thumbnail'}
                    {if $product.cover}
                        <a href="{$product.url}" class="thumbnail product-thumbnail">
                            <img
                                    src="{$product.cover.bySize.home_default.url}"
                                    alt="{if !empty($product.cover.legend)}{$product.cover.legend}{else}{$product.name|truncate:30:'...'}{/if}"
                                    data-full-size-image-url="{$product.cover.large.url}"
                            />
                            {if !empty($product.images[1])}
                            <img class="imageHover" src="{$product.images[1].bySize.home_default.url}"
                                    alt="{$product.name|truncate:30:'...'}"
                                    data-full-size-image-url="{$product.images[1].large.url}" >
                            {/if}
                        </a>
                    {else}
                        <a href="{$product.url}" class="thumbnail product-thumbnail">
                            <img src="{$urls.no_picture_image.bySize.home_default.url}"/>
                        </a>
                    {/if}
                {/block}

                <div class="product-description">
                    {block name='product_name'}
                        {if $page.page_name == 'index'}
                            <h3 class="h3 product-title" itemprop="name"><a href="{$product.url}" itemprop="url"
                                                                            content="{$product.url}">{$product.name}</a>
                            </h3>
                        {else}
                            <h2 class="h3 product-title" itemprop="name"><a href="{$product.url}" itemprop="url"
                                                                            content="{$product.url}">{$product.name}</a>
                            </h2>
                        {/if}
                    {/block}

                    <div class="product-short-description">{hook h='displayProductAdditionalInfo' product=$product}</div>
                    <div class="product-quantities">
                        <label class="label">{l s='Dostępność:' d='Shop.Theme.Catalog'}</label>
                        <span data-stock="{$product.quantity_all_versions}"
                              data-allow-oosp="{$product.allow_oosp}">{$product.quantity_all_versions}</span>
                    </div>

                    {block name='product_reviews'}
                        {hook h='displayProductListReviews' product=$product}
                    {/block}
                    
                    {block name='product_price_and_shipping'}
                        {if $product.show_price}
                            <div class="product-price-and-shipping">


                                {hook h='displayProductPriceBlock' product=$product type="before_price"}
                                {if $product.has_discount}
                                    <div class="moneySave">{l s='Oszczędzasz' d='Shop.Theme.Catalog'} <strong>{Tools::displayPrice($product.reduction)}</strong></div>
                                {/if}
                                <span class="price"
                                      aria-label="{l s='Price' d='Shop.Theme.Catalog'}">{$product.price}</span>
                                {if $product.has_discount}
                                    {hook h='displayProductPriceBlock' product=$product type="old_price"}
                                    <span class="regular-price"
                                          aria-label="{l s='Regular price' d='Shop.Theme.Catalog'}">{$product.regular_price}</span>
                                    {if $product.discount_type === 'percentage'}
                                        <span class="discount-percentage discount-product">{$product.discount_percentage}</span>
                                    {elseif $product.discount_type === 'amount'}
                                        <span class="discount-amount discount-product">{$product.discount_amount_to_display}</span>
                                    {/if}
                                {/if}
                                <div itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="invisible">
                                    <meta itemprop="priceCurrency" content="{$currency.iso_code}"/>
                                    <meta itemprop="price" content="{$product.price_amount}"/>
                                </div>

                                {hook h='displayProductPriceBlock' product=$product type='unit_price'}

                                {hook h='displayProductPriceBlock' product=$product type='weight'}

                            </div>
                        {/if}
                    {/block}


                </div>

                {include file='catalog/_partials/product-flags.tpl'}
                {assign var="manufacturer_name" value=(Manufacturer::getNameById($product.id_manufacturer))}
                <span class="manufacturer">{$manufacturer_name}</span>

                <div class="highlighted-informations{if !$product.main_variants} no-variants{/if} hidden-sm-down">
                    {*block name='quick_view'}
                        <a class="quick-view" href="#" data-link-action="quickview">
                            <i class="material-icons search">&#xE8B6;</i> {l s='Quick view' d='Shop.Theme.Actions'}
                        </a>
                    {/block*}

                    {block name='product_variants'}
                        {if $product.main_variants}
                            {include file='catalog/_partials/variant-links.tpl' variants=$product.main_variants}
                        {/if}
                    {/block}
                </div>
            </div>
        </article>
    </div>
{/block}
