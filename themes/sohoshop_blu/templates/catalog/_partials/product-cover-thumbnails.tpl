{**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 *}
<div class="images-container">

    {block name='product_images'}
        <div class="miniaturesSlider">
            <div class="swiper-container swiper-images">
                <ul class="product-images swiper-wrapper">

                        {foreach from=$product.images item=image}
                            <li class="thumb-container swiper-slide">
                                <img
                                        class="thumb js-thumb {if $image.id_image == $product.default_image.id_image} selected {/if}"
                                        data-image-medium-src="{$image.bySize.medium_default.url}"
                                        data-image-large-src="{$image.bySize.medium_default.url}"
                                        data-srcset="{$image.bySize.medium_default.url}"

                                        {* data-src="{$image.bySize.medium_default.url}"
                                        data-srcset="{$image.bySize.medium_default.url}" *}
                                        src="{$image.bySize.medium_default.url}"
                                        alt="{$image.legend}"
                                        title="{$image.legend}"
                                        {* width="130" *}
                                        itemprop="image"
                                >
                                {* <div class="swiper-lazy-preloader"></div> *}
                            </li>
                        {/foreach}

                </ul>
                <p class="counter hidden-md-up">
                    <span class="current-slide">1</span>/<span class="total-slides"></span>
                <p>
                <div class="swiper-scrollbar"></div>

            </div>
            {if $product.images|count > 3}
                <div class="custom-button-next"></div>
                <div class="custom-button-prev"></div>
            {/if}
        </div>

    {/block}

    {block name='product_cover'}
        <div class="product-cover hidden-sm-down">
            {if $product.default_image}
                <img class="js-qv-product-cover" src="{$product.default_image.bySize.medium_default.url}"
                     alt="{$product.default_image.legend}" title="{$product.default_image.legend}" style="width:100%;"
                     itemprop="image">
                <div class="layer hidden-sm-down" data-toggle="modal" data-target="#product-modal">
                    <i class="material-icons zoom-in">search</i>
                </div>
            {else}
                <img src="{$urls.no_picture_image.bySize.large_default.url}" style="width:100%;">
            {/if}
        </div>
    {/block}


    {hook h='displayAfterProductThumbs'}
</div>
