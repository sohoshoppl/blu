{**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 *}

{if !empty($swiper) && $swiper eq '1'}
    <div class="swiper-wrapper" itemscope itemtype="http://schema.org/ItemList">
        {foreach from=$products item="product" key="position"}
            {include file="catalog/_partials/miniatures/productSlide.tpl" product=$product position=$position }
        {/foreach}
    </div>
{else}
    <div class="products{if !empty($cssClass)} {$cssClass}{/if}" itemscope itemtype="http://schema.org/ItemList">
        {assign var="blog_thumb" value=""}
        {foreach from=$products item="product" key="position"}
            {if isset($product.ybc_blog_thumb) AND ($position is even)}
                <div class="related_ybc_blog_post">
                    <a href="{$product.url}">
                        <img src="/img/ybc_blog/post/thumb/{$product.ybc_blog_thumb}"
                             alt="{$product.name}"></a>
                    <a class="text-link"
                       href="{$product.url}">{l s='Przejdź do produktu ze zdjęcia >' d='Shop.Theme.Catalog'}</a>
                </div>
            {elseif isset($product.ybc_blog_thumb) AND ($position is odd)}
                {capture name="ybc_blog_thumb" assign=blog_thumb}
                    <div class="related_ybc_blog_post">
                        <a href="{$product.url}"><img src="/img/ybc_blog/post/thumb/{$product.ybc_blog_thumb}"
                                                      alt="{$product.name}"></a>
                        <a class="text-link"
                           href="{$product.url}">
                           {l s='Przejdź do produktu ze zdjęcia ' d='Shop.Theme.Catalog'}
                           <svg class="arrow-right-icon-category" xmlns="http://www.w3.org/2000/svg" width="14" height="9" viewBox="0 0 12.262 7.192">
                                    <defs></defs>
                                    <g transform="translate(0.53 0.53)">
                                        <path d="M-317.515-44.61l-5.6,5.6-5.6-5.6" transform="translate(328.717 44.61)"></path>
                                    </g>
                            </svg>
                        </a>
                    </div>
                {/capture}
            {else}
                {include file="catalog/_partials/miniatures/product.tpl" product=$product position=$position}
                {$blog_thumb nofilter}
                {assign var="blog_thumb" value=""}
            {/if}
        {/foreach}
    </div>
{/if}