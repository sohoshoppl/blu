{**
* Copyright since 2007 PrestaShop SA and Contributors
* PrestaShop is an International Registered Trademark & Property of PrestaShop SA
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License 3.0 (AFL-3.0)
* that is bundled with this package in the file LICENSE.md.
* It is also available through the world-wide-web at this URL:
* https://opensource.org/licenses/AFL-3.0
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to https://devdocs.prestashop.com/ for more information.
*
* @author    PrestaShop SA and Contributors <contact@prestashop.com>
* @copyright Since 2007 PrestaShop SA and Contributors
* @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
*}
{extends file=$layout}

{block name='head_seo' prepend}
    <link rel="canonical" href="{$product.canonical_url}">
{/block}

{block name='head' append}
    <meta property="og:type" content="product">
    <meta property="og:url" content="{$urls.current_url}">
    <meta property="og:title" content="{$page.meta.title}">
    <meta property="og:site_name" content="{$shop.name}">
    <meta property="og:description" content="{$page.meta.description}">
    <meta property="og:image" content="{$product.cover.large.url}">
    {if $product.show_price}
        <meta property="product:pretax_price:amount" content="{$product.price_tax_exc}">
        <meta property="product:pretax_price:currency" content="{$currency.iso_code}">
        <meta property="product:price:amount" content="{$product.price_amount}">
        <meta property="product:price:currency" content="{$currency.iso_code}">
    {/if}
    {if isset($product.weight) && ($product.weight != 0)}
        <meta property="product:weight:value" content="{$product.weight}">
        <meta property="product:weight:units" content="{$product.weight_unit}">
    {/if}
{/block}

{block name='content'}
    <section id="main" itemscope itemtype="https://schema.org/Product">
        <meta itemprop="url" content="{$product.url}">

        <div class="row product-container">
            <div class="product-container-left">
                <div class="hidden-md-up">   
                   {include file='catalog/_partials/product-flags.tpl'}
                </div>
                {block name='page_content_container'}
                    <section class="page-content" id="content">
                        {block name='page_content'}

                            {block name='product_cover_thumbnails'}
                                {include file='catalog/_partials/product-cover-thumbnails.tpl'}
                            {/block}
                            <div class="scroll-box-arrows">
                                <i class="material-icons left">&#xE314;</i>
                                <i class="material-icons right">&#xE315;</i>
                            </div>
                        {/block}
                    </section>
                {/block}
            </div>
            <div class="product-container-right">
                <div class="hidden-sm-down">   
                    {include file='catalog/_partials/product-flags.tpl'}
                </div>
                {if isset($product_manufacturer->id)}
                    <div class="product-manufacturer">
                        {* <label class="label">{l s='Brand' d='Shop.Theme.Catalog'}</label> *}
                        <span>
                            <a href="{$product_brand_url}">
                                <img src="{$manufacturer_image_url}" class="img img-thumbnail manufacturer-logo" alt="{$product_manufacturer->name}">
                            </a>
                        </span>
                    </div>
                {/if}

                {block name='page_header_container'}
                    {block name='page_header'}
                        <h1 class="h1" itemprop="name">{block name='page_title'}{$product.name}{/block}</h1>
                    {/block}
                {/block}

                <div class="product-information">
                    <div class="product-subtitle"
                         itemprop="description">{hook h='displayProductAdditionalInfo' product=$product}</div>
                    <div id="product-reference">{l s='Kod produktu' d='Shop.Theme.Catalog'} : {$product.reference}</div>
                    {hook h='displayProductListReviews' product=$product}
                    <div class="product_prices_block hidden-sm-down">
                        {block name='product_prices'}
                            {include file='catalog/_partials/product-prices.tpl'}
                        {/block}
                    </div>
                    {if $product.is_customizable && count($product.customizations.fields)}
                        {block name='product_customization'}
                            {include file="catalog/_partials/product-customization.tpl" customizations=$product.customizations}
                        {/block}
                    {/if}

                    <div class="product-actions">
                        {block name='product_buy'}
                            <form action="{$urls.pages.cart}" method="post" id="add-to-cart-or-refresh">
                                <input type="hidden" name="token" value="{$static_token}">
                                <input type="hidden" name="id_product" value="{$product.id}"
                                       id="product_page_product_id">
                                <input type="hidden" name="id_customization" value="{$product.id_customization}"
                                       id="product_customization_id">

                                {block name='product_variants'}
                                    {include file='catalog/_partials/product-variants.tpl'}
                                {/block}

                                {block name='product_pack'}
                                    {if $packItems}
                                        <section class="product-pack">
                                            <p class="h4">{l s='This pack contains' d='Shop.Theme.Catalog'}</p>
                                            {foreach from=$packItems item="product_pack"}
                                                {block name='product_miniature'}
                                                    {include file='catalog/_partials/miniatures/pack-product.tpl' product=$product_pack showPackProductsPrice=$product.show_price}
                                                {/block}
                                            {/foreach}
                                        </section>
                                    {/if}
                                {/block}

                                {block name='product_discounts'}
                                    {include file='catalog/_partials/product-discounts.tpl'}
                                {/block}

                                {block name='product_add_to_cart'}
                                    {include file='catalog/_partials/product-add-to-cart.tpl'}
                                {/block}

                                {* Input to refresh product HTML removed, block kept for compatibility with themes *}
                                {block name='product_refresh'}{/block}
                            </form>
                        {/block}

                    </div>
                    
                    {block name='product_details'}
                        {include file='catalog/_partials/product-details.tpl'}
                    {/block}
                    <div id="product-description-short-{$product.id}" class="product-description-short hidden-md-up"
                    itemprop="description">
                        <div class="sectionTitle collapse-title">{l s='Opis produktu' d='Shop.Theme.Catalog'}:
                            <div class="hidden-md-up collapse-mobile" data-target="#mobile-describe-collapse" data-toggle="collapse">
                                <span class="collapse-icons">
                                    <i class="material-icons add">&#xE313;</i>
                                    <i class="material-icons remove">&#xE316;</i>
                                </span>
                            </div>
                        </div>
                        <div class="mobile-describe collapse" id="mobile-describe-collapse">
                            {$product.description_short nofilter}
                            {block name='product_description'}
                                <div class="product-description clearfix hidden-md-up">{$product.description nofilter}</div>
                            {/block}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row product-container-bottom hidden-sm-down">
            <div id="product-description-short-{$product.id}" class="product-description-short"
            itemprop="description">
                <div class="sectionTitle">{l s='Opis produktu' d='Shop.Theme.Catalog'}:</div>
                {$product.description_short nofilter}
            </div>
            <div class="product_features_wrapper">
                    {block name='product_features'}
                        {if $product.grouped_features}
                            <section class="product-features">
                                <div class="sectionTitle collapse-title">{l s='Szczegóły techniczne' d='Shop.Theme.Catalog'}:
                                    <div class="hidden-md-up collapse-mobile" data-target="#mobile-describe-features" data-toggle="collapse">
                                        <span class="collapse-icons">
                                            <i class="material-icons add">&#xE313;</i>
                                            <i class="material-icons remove">&#xE316;</i>
                                        </span>
                                    </div>
                                </div>
                                <div class="collapse" id="mobile-describe-features">
                                    <dl class="data-sheet" >
                                        {foreach from=$product.grouped_features item=feature}
                                            <dt class="name">{$feature.name}:</dt>
                                            <dd class="value"><strong>{$feature.value|escape:'htmlall'|nl2br nofilter}</strong></dd>
                                                {/foreach}
                                    </dl>
                                </div>
                            </section>
                        {/if}
                    {/block}
                    
                    {* if product have specific references, a table will be added to product details section *}
                    {block name='product_specific_references'}
                        {if !empty($product.specific_references)}
                            <section class="product-features">
                                <p class="h6">{l s='Specific References' d='Shop.Theme.Catalog'}</p>
                                <dl class="data-sheet">
                                    {foreach from=$product.specific_references item=reference key=key}
                                        <dt class="name">{$key}</dt>
                                        <dd class="value">{$reference}</dd>
                                    {/foreach}
                                </dl>
                            </section>
                        {/if}
                    {/block}
                    
                    {block name='product_condition'}
                        {if $product.condition}
                            <div class="product-condition">
                                <label class="label">{l s='Condition' d='Shop.Theme.Catalog'} </label>
                                <link itemprop="itemCondition" href="{$product.condition.schema_url}"/>
                                <span>{$product.condition.label}</span>
                            </div>
                        {/if}
                    {/block}             
            </div>            
        </div>
        {block name='product_accessories'}
            {if $accessories}
                <section class="product-accessories clearfix hidden-sm-down">
                    <div class="sectionTitle">{l s='Inne z serii' d='Shop.Theme.Catalog'}:</div>
                    <div class="product-accessories-container">
     
                        <div class="products">
                            {foreach from=$accessories item="product_accessory" key="position"}
                                <div class="product-accessories-item {if $product_accessory.availability == 'unavailable'}unavailable-mask{/if}">
                                
                                    {if $product_accessory.cover}
                                        <a href="{$product_accessory.url}" class="thumbnail product-thumbnail">
                                            <img
                                                    src="{$product_accessory.cover.bySize.home_default.url}"
                                                    alt="{if !empty($product_accessory.cover.legend)}{$product_accessory.cover.legend}{else}{$product_accessory.name}{/if}"
                                                    data-full-size-image-url="{$product_accessory.cover.large.url}"
                                                    width="110"
                                            />
                                        </a>
                                    {else}
                                        <a href="{$product_accessory.url}" class="thumbnail product-thumbnail">
                                            <img src="{$urls.no_picture_image.bySize.home_default.url}"
                                                 alt="{if !empty($product_accessory.cover.legend)}{$product_accessory.cover.legend}{else}{$product_accessory.name|truncate:30:'...'}{/if}"
                                                 width="110"/>
                                        </a>
                                    {/if}
                                    <div>
                                        <h3 class="h3 product-title">{$product_accessory.name}</h3>
                                        <div class="product-subtitle">{hook h='displayProductAdditionalInfo' product=$product_accessory}</div>
                                        {if $product_accessory.availability == 'unavailable'}
                                            <a href="{$product_accessory.url}" class="unavaible-msg">{l s='Powiadom mnie o dostępności' d='Shop.Theme.Catalog'}</a>
                                        {/if}
                                    </div>
                                </div>
                            {/foreach}
                        </div>
                    </div>
                </section>
            {/if}
        {/block}

        {block name='product_description'}
            <div class="product-description clearfix hidden-sm-down">{$product.description nofilter}</div>
        {/block}

        {hook h='displayFooterProduct2' product=$product}


        {block name='product_attachments'}
            {if $product.attachments}
                <section id="productAttachments" class="product-attachments">
                    <div class="sectionTitle collapse-title">{l s='Do pobrania' d='Shop.Theme.Actions'}:
                        <div class="hidden-md-up collapse-mobile" data-target="#attachments-collapse" data-toggle="collapse">
                            <span class="collapse-icons">
                                <i class="material-icons add">&#xE313;</i>
                                <i class="material-icons remove">&#xE316;</i>
                            </span>
                        </div>
                    </div>
                    <ul class="attachments collapse" id="attachments-collapse">
                    {foreach from=$product.attachments item=attachment}
                        <li>
                            <a href="{url entity='attachment' params=['id_attachment' => $attachment.id_attachment]}">
                                <img src="{$urls.img_url}svg_icons/download.svg"
                                alt="{$attachment.name}" height="33">
                                {$attachment.name}
                            </a>
                        </li>
                    {/foreach}
                    </ul>
                </section>
            {/if}
        {/block}

        {widget name="sohoblockreassurance"}

        {foreach from=$product.extraContent item=extra key=extraKey}
            <div class="{$extra.attr.class}" id="extra-{$extraKey}" role="tabpanel"
            {foreach $extra.attr as $key => $val} {$key}="{$val}"{/foreach}>
            {$extra.content nofilter}
            </div>
        {/foreach}


        {block name='product_footer'}
            {hook h='displayFooterProduct' product=$product}
        {/block}

        {block name='product_images_modal'}
            {include file='catalog/_partials/product-images-modal.tpl'}
        {/block}

        {block name='page_footer_container'}
            <footer class="page-footer">
                {block name='page_footer'}
                    <!-- Footer content -->
                {/block}
            </footer>
        {/block}
    </section>
{/block}
