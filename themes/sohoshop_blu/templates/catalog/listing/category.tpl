{**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 *}
{extends file='catalog/listing/product-list.tpl'}

{block name='content'}
    {assign var="showSubPage" value=(Db::getInstance()->getValue("SELECT showSubPage FROM ps_category WHERE id_category =`$category.id`"))}

    {if $showSubPage eq "1" && $smarty.get|sizeof lte 2}
        <section id="main">
            {block name='product_list_header'}
                {include file='catalog/_partials/category-header.tpl' listing=$listing category=$category}
            {/block}
            <div class="clearfix"></div>
            <div class="sub-categories clearfix">
                {foreach from=$subcategories name=subcategories item=subcategorie}
                    <div class="hc_block">
                        <a class="image" href="{$subcategorie.url}">
                            <img src="{$subcategorie.image.bySize.category_default.url}"
                                 title="{$subcategorie.meta_title}" alt="{$subcategorie.meta_title}">
                        </a>
                        <div class="hc_block_title">
                            <h2>
                                <a href="{$subcategorie.url}">{$subcategorie.name}</a>
                            </h2>
                        </div>
                    </div>
                {/foreach}
            </div>

            {hook h='displayCategorySlider'}


            <div class="new_products">
                {hook h='displayNewProducts'}
            </div>
            {hook h="displayFooterCategory"}
            <div class="category-description">{$category.description nofilter}</div>
        </section>
    {else}
        <section id="main">

            {block name='product_list_header'}
                {include file='catalog/_partials/category-header.tpl' listing=$listing category=$category}
            {/block}

            {if $listing.products|count}
                <div>
                    {block name='product_list_top'}
                        {include file='catalog/_partials/products-top.tpl' listing=$listing}
                    {/block}
                </div>
            {/if}
            <div id="left-column">
                {hook h='displayAmazzingFilter'}
                {hook h="displayLeftColumn"}
            </div>

            <section id="products">
                {if $listing.products|count}

                    {block name='product_list_active_filters'}
                        <div id="" class="hidden-sm-down">
                            {$listing.rendered_active_filters nofilter}
                        </div>
                    {/block}
                    <div>
                        {block name='product_list'}
                            {include file='catalog/_partials/products.tpl' listing=$listing}
                        {/block}
                    </div>
                    <div id="js-product-list-bottom">
                        {block name='product_list_bottom'}
                            {include file='catalog/_partials/products-bottom.tpl' listing=$listing}
                        {/block}
                    </div>
                {else}
                    <div id="js-product-list-top"></div>
                    <div id="js-product-list">
                        {include file='errors/not-found.tpl'}
                    </div>
                    <div id="js-product-list-bottom"></div>
                {/if}
            </section>

            {hook h="displayFooterCategory"}

        </section>
    {/if}
{/block}

