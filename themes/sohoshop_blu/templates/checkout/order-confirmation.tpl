{extends file=$layout}

{block name='content'}
    <section id="main">
        <div class="cart-grid row">
            <div class="cart-grid-body col-xs-12">
                <div class="cart-container">
                    <header>
                        <h1 class="h1">{l s='Potwierdzenie' d='Shop.Theme.Checkout'}</h1>
                        <ul class="hidden-sm-down">
                            <li>
                                <span class="reservationStep current">1</span>{l s='Twoje rezerwacje' d='Shop.Theme.Checkout'}
                            </li>
                            <li><span class="reservationStep current">2</span>{l s='Twoje dane' d='Shop.Theme.Checkout'}
                            </li>
                            <li>
                                <span class="reservationStep current">3</span>{l s='Potwierdzenie' d='Shop.Theme.Checkout'}
                            </li>
                        </ul>
                    </header>

                    <section id="content-hook_order_confirmation">
                        <h2><i class="material-icons rtl-no-flip done">&#xE876;</i>{l s='Potwierdzenie rezerwacji Nr' d='Shop.Theme.Checkout'} {$order.details.id}</h2>
                        <p>
                            {l s='Dziękujemy! Przyjęliśmy Twoją rezerwację' d='Shop.Theme.Checkout'}<br>
                            {l s='Wkrótce salon skontaktuje się z Tobą telefonicznie lub mailowo w celu realizacji Twojej rezerwacji' d='Shop.Theme.Checkout'}
                            <br>
                        </p>
                    </section>
                </div>
            </div>
        </div>
    </section>
    {block name='hook_order_confirmation'}
        {$HOOK_ORDER_CONFIRMATION nofilter}
    {/block}

    {block name='page_content_container'}
        <section id="content" class="page-content page-order-confirmation">
            <div class="card-block">
                <div class="row">

                    {block name='order_confirmation_table'}
                        {include
                        file='checkout/_partials/order-confirmation-table.tpl'
                        products=$order.products
                        subtotals=$order.subtotals
                        totals=$order.totals
                        labels=$order.labels
                        add_product_link=false
                        }
                    {/block}



                </div>
            </div>
        </section>
        {block name='hook_payment_return'}
            {if ! empty($HOOK_PAYMENT_RETURN)}
                <section id="content-hook_payment_return" class="card definition-list">
                    <div class="card-block">
                        <div class="row">
                            <div class="col-md-12">
                                {$HOOK_PAYMENT_RETURN nofilter}
                            </div>
                        </div>
                    </div>
                </section>
            {/if}
        {/block}

        {block name='customer_registration_form'}
            {if $customer.is_guest}
                <div id="registration-form" class="card">
                    <div class="card-block">
                        <h4 class="h4">{l s='Save time on your next order, sign up now' d='Shop.Theme.Checkout'}</h4>
                        {render file='customer/_partials/customer-form.tpl' ui=$register_form}
                    </div>
                </div>
            {/if}
        {/block}

        {*

        {block name='hook_order_confirmation_1'}
            {hook h='displayOrderConfirmation1'}
        {/block}

        {block name='hook_order_confirmation_2'}
            <section id="content-hook-order-confirmation-footer">
                {hook h='displayOrderConfirmation2'}
            </section>
        {/block}

        *}
    {/block}


{/block}


