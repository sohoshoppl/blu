{**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 *}
{block name='cart_detailed_actions'}
    <div class="checkout cart-detailed-actions card-block">
        {block name='continue_shopping'}
            <a class="back" href="{$urls.pages.index}">{l s='Continue shopping' d='Shop.Theme.Actions'}</a>
        {/block}
        {if $cart.minimalPurchaseRequired}
            <div class="alert alert-warning" role="alert">
                {$cart.minimalPurchaseRequired}
            </div>
            <button type="button" class="next disabled"
                    disabled>{l s='Zarezerwuj w salonie' d='Shop.Theme.Actions'}</button>
        {elseif empty($cart.products) }
            <button type="button" class="next disabled"
                    disabled>{l s='Zarezerwuj w salonie' d='Shop.Theme.Actions'}</button>
        {else}
            <a href="{url entity='module' name='sohoshop_reservation' controller='reserve'}" class="next">{l s='Zarezerwuj w salonie' d='Shop.Theme.Actions'}</a>
        {/if}
    </div>
    <div class="free-pick-up"><img src="{$urls.img_url}point-icon.svg" alt="{l s='Darmowy odbiór w salonach BLU' d='Shop.Theme.Actions'}" title="Do pobrania">{l s='Darmowy odbiór w salonach BLU' d='Shop.Theme.Actions'}</div>
{/block}
