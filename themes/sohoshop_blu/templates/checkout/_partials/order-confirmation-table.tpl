{**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 *}
<div id="order-items" class="col-md-12">
    <div class="clearfix">
        {block name='order_items_table_head'}
            <h3 class="card-title h3 col-md-6 col-12">{l s='Produkty z zamówienia' d='Shop.Theme.Checkout'}</h3>
            <h3 class="card-title h3 col-md-2 text-md-center _desktop-title">{l s='Cena' d='Shop.Theme.Checkout'}</h3>
            <h3 class="card-title h3 col-md-2 text-md-center _desktop-title">{l s='Ilość' d='Shop.Theme.Checkout'}</h3>
            <h3 class="card-title h3 col-md-2 text-md-center _desktop-title">{l s='Wartość' d='Shop.Theme.Checkout'}</h3>
        {/block}
    </div>

    <div class="order-confirmation-table">

        {block name='order_confirmation_table'}
            {foreach from=$products item=product}
                <div class="order-line row">
                    <div class="col-sm-6 col-xs-3">
                            <span class="image">
                              {if !empty($product.cover)}
                                  <img src="{$product.cover.medium.url}"/>
                                {else}
                                  <img src="{$urls.no_picture_image.bySize.medium_default.url}"/>
                              {/if}
                            </span>
                    </div>
                    <div class="col-sm-6 col-xs-12 qty">
                        <div class="row">
                            <div class="col-xs-4 text-sm-center text-xs-left price">{$product.price}</div>
                            <div class="col-xs-4 text-sm-center">{$product.quantity}</div>
                            <div class="col-xs-4 text-sm-center text-xs-right price">{$product.total}</div>
                        </div>
                    </div>
                </div>
            {/foreach}

                <div class="cart-detailed-totals">
                    <div class="cart-detailed-totals-header">
                        {l s='Łączna wartość produktów' d='Shop.Theme.Catalog'}
                    </div>
                    <div class="cart-summary-line cart-total">
                        <span class="label">{l s='Łączna wartość zamówienia' d='Shop.Theme.Catalog'}:{*{$cart.totals.total.label}&nbsp;{if $configuration.taxes_enabled}{$cart.labels.tax_short}{/if}*}</span>
                        <span class="value">{$totals.total.value}</span>
                    </div>
                </div>

            <div class="cart-detailed-actions text-center">
                <a class="back" href="{$urls.base_url}">{l s='Kontynuuj przeglądanie' d='Shop.Theme.Catalog'}</a>
            </div>

          {*
            <hr>
            <table>
                {foreach $subtotals as $subtotal}
                    {if $subtotal.type !== 'tax' && $subtotal.label !== null}
                        <tr>
                            <td>{$subtotal.label}</td>
                            <td>{if 'discount' == $subtotal.type}-&nbsp;{/if}{$subtotal.value}</td>
                        </tr>
                    {/if}
                {/foreach}

                {if !$configuration.display_prices_tax_incl && $configuration.taxes_enabled}
                    <tr>
                        <td><span class="text-uppercase">{$totals.total.label}&nbsp;{$labels.tax_short}</span></td>
                        <td>{$totals.total.value}</td>
                    </tr>
                    <tr class="total-value font-weight-bold">
                        <td><span class="text-uppercase">{$totals.total_including_tax.label}</span></td>
                        <td>{$totals.total_including_tax.value}</td>
                    </tr>
                {else}
                    <tr class="total-value font-weight-bold">
                        <td>
                            <span class="text-uppercase">{$totals.total.label}&nbsp;{if $configuration.taxes_enabled}{$labels.tax_short}{/if}</span>
                        </td>
                        <td>{$totals.total.value}</td>
                    </tr>
                {/if}
                {if $subtotals.tax.label !== null}
                    <tr class="sub taxes">
                        <td colspan="2"><span
                                    class="label">{l s='%label%:' sprintf=['%label%' => $subtotals.tax.label] d='Shop.Theme.Global'}</span>&nbsp;<span
                                    class="value">{$subtotals.tax.value}</span></td>
                    </tr>
                {/if}
            </table>


            *}
        {/block}

    </div>
</div>
