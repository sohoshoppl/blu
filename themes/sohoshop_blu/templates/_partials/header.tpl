{**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 *}
{block name='header_banner'}
    <div class="header-banner hidden-sm-down">
        {hook h='displayBanner'}
    </div>
{/block}

{block name='header_nav'}
    <nav class="header-nav">
        <div class="container">
            <div class="row nav-row">
                <div class="hidden-sm-down">
                    <div class="col-md-5 col-xs-12">
                        {hook h='displayNav1'}
                    </div>
                    <div class="col-md-7 right-nav">
                        {hook h='displayNav2'}
                    </div>
                </div>
                <div class="hidden-md-up text-sm-center mobile">
                    <div class="float-xs-right" id="_mobile_user_info"></div>
                    <div class="float-xs-right" id="_mobile_cart"></div>
                    <div class="float-xs-right" id="_mobile_wishlist-top"></div>
                    <div class="float-xs-right" id="_mobile_stores_top"></div>
                    <div class="float-xs-right" id="menu-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="28" height="23" viewBox="0 0 28 23">
                            <g transform="translate(0.427 -0.265)">
                                <rect width="28" height="1" transform="translate(-0.427 0.265)"/>
                                <rect width="28" height="1" transform="translate(-0.427 11.265)"/>
                                <rect width="28" height="1" transform="translate(-0.427 22.265)"/>
                            </g>
                        </svg>
                        <br>
                        {l s='Kategorie' d='Shop.Theme.Global'}
                    </div>
                    <div class="top-logo" id="_mobile_logo"></div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </nav>
{/block}

{block name='header_top'}
    <div class="header-top">
        <div class="container">
            <div class="row top-row">
                <div class="col-md-2 hidden-sm-down" id="_desktop_logo">
                    {if $page.page_name == 'index'}
                        <h1>
                            <a href="{$urls.base_url}">
                                <img class="logo img-responsive" src="{$shop.logo}" alt="{$shop.name}">
                            </a>
                        </h1>
                    {else}
                        <a href="{$urls.base_url}">
                            <img class="logo img-responsive" src="{$shop.logo}" alt="{$shop.name}">
                        </a>
                    {/if}
                </div>
                <div class="col-md-10 col-sm-12 position-static">
                    <div class="top-wrapper">
                        <div class="hidden-sm-down" id="_desktop_stores_top">
                            <div class="stores">
                                <svg xmlns="http://www.w3.org/2000/svg" width="41.282" height="51.338"
                                     viewBox="0 0 41.282 51.338">
                                    <defs>
                                        <style>.a {
                                                fill: #006cb5;
                                            }</style>
                                    </defs>
                                    <g transform="translate(96.765 592.06)">
                                        <path d="M-76.113-583.118l-.009-.5v.5h0a5.343,5.343,0,0,0-5.337,5.337,5.3,5.3,0,0,0,1.562,3.775,5.3,5.3,0,0,0,3.774,1.564h0a5.345,5.345,0,0,0,5.338-5.33,5.3,5.3,0,0,0-1.556-3.776A5.306,5.306,0,0,0-76.113-583.118Zm-.018,8.712h-.01a3.382,3.382,0,0,1-3.367-3.377,3.365,3.365,0,0,1,3.342-3.377l.044.427.007-.427a3.359,3.359,0,0,1,2.38,1,3.354,3.354,0,0,1,.982,2.391A3.382,3.382,0,0,1-76.131-574.406Z"></path>
                                        <path d="M-76.123-540.722c11.767,0,20.64-3.758,20.64-8.742,0-3.772-5.265-7-13.421-8.213l-1.549-.231.861-1.308c2.822-4.289,4.675-7.161,5.359-8.3a16.59,16.59,0,0,0,2.608-9.183,15.581,15.581,0,0,0-4.256-10.87,14.094,14.094,0,0,0-10.242-4.486c-7.978,0-14.479,6.86-14.489,15.292a16.557,16.557,0,0,0,2.568,9.185c.491.826,1.808,2.925,5.368,8.372l.853,1.3h-.007l5.67,8.6.814-1.226c.121-.186,1.235-1.862,2.718-4.1l.769-1.157.6.06c8.61.872,13.755,3.868,13.755,6.262,0,3.072-7.962,6.508-18.617,6.508s-18.614-3.436-18.614-6.508c0-2.265,4.607-5.073,12.394-6.1l-.856-2.14-.167.025c-8.14,1.223-13.4,4.446-13.4,8.211C-96.765-544.48-87.891-540.722-76.123-540.722Zm.8-13.621-.837,1.261-.834-1.264c-5.128-7.79-8.449-12.918-9.352-14.44a14.269,14.269,0,0,1-2.246-7.995c0-7.18,5.6-13.038,12.486-13.038a12.088,12.088,0,0,1,8.59,3.616,13.292,13.292,0,0,1,3.867,9.486,14.347,14.347,0,0,1-2.294,8C-67.154-566.684-72.34-558.837-75.321-554.343Z"></path>
                                    </g>
                                </svg>
                                <a href="/salony">
                                    {l s='Wybierz salon' d='Shop.Theme.Global'}
                                </a>
                                <div class="modal fade" id="chooseShopHeaderPopup" tabindex="-1"
                                     aria-labelledby="chooseShopHeaderPopupLabel"
                                     aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <label class="modal-title" id="chooseShopHeaderPopupLabel">
                                                    {l s='Wybierz salon w którym' d='Shop.Theme.Global'}<br>
                                                    {l s='dokonasz rezerwacji produktu' d='Shop.Theme.Global'}
                                                </label>
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                    <span aria-hidden="true"><i class="material-icons">close</i></span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="label">{l s='Twój najbliższy salon' d='Shop.Theme.Global'}:</div>
                                                <div id="chooseShopHeaderPopupNearestShop">
                                                    {assign var="stores" value=(Store::getStores($language.id))}
                                                    <h1>{$stores[0].name}</h1>
                                                    <div>
                                                        <img class="pin" src="/themes/sohoshop_blu/assets/img/point-icon.svg" alt="Najbliższy salon" >
                                                        <address>
                                                            {$stores[0].postcode} {$stores[0].city} <br>
                                                            {$stores[0].address1}
                                                        </address>
                                                        <button class="btn btn-primary" data-dismiss="modal" onclick="chooseSalon({$stores[0].id_store});">
                                                            {l s='ZAREZERWUJ W SALONIE' d='Shop.Theme.Global'}
                                                        </button>
                                                    </div>
                                                </div>
                                                <div id="chooseShopHeaderPopupFindShopForm">
                                                    <div class="label">{l s='Znajdź inny salon' d='Shop.Theme.Global'}:</div>
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <input id="chooseShopHeaderPopupFindShopForm-findShopQuery" class="form-control" name="findShopQuery" required
                                                                   type="text" value=""
                                                                   placeholder="Wpisz miasto lub kod pocztowy">
                                                        </div>
                                                        <div class="col-lg-12 ">
                                                            <button type="button" class="btn btn-primary"
                                                                    onclick="chooseShopHeaderPopupFindNearestFindShop();">
                                                                {l s='POKAŻ NAJBLIŻSZE' d='Shop.Theme.Global'}
                                                                <svg class="arrow-right" xmlns="http://www.w3.org/2000/svg" width="14.262" height="9.192" viewBox="0 0 12.262 7.192">
                                                                    <defs></defs>
                                                                    <g transform="translate(0.53 0.53)">
                                                                        <path d="M-317.515-44.61l-5.6,5.6-5.6-5.6" transform="translate(328.717 44.61)"></path>
                                                                    </g>
                                                                </svg>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {hook h='displayTop'}
                    </div>
                    {hook h='displayNavFullWidth'}

                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="mobile_top_menu_overlay close_category_menu"></div>
            <div id="mobile_top_menu_wrapper" class="row hidden-md-up" style="display:none;">
                <div class="mobile_top_menu_header">
                    <svg class="close_category_menu" xmlns="http://www.w3.org/2000/svg" width="20.673" height="20.673" viewBox="0 0 20.673 20.673"><defs></defs><g transform="translate(-556.872 -526.47)"><path class="a" d="M3778.4,5611l19.612,19.612" transform="translate(-3221 -5084)"/><path class="a" d="M0,0,19.612,19.612" transform="translate(557.402 546.612) rotate(-90)"/></g></svg>
                </div>
                <div class="js-top-menu mobile" id="_mobile_top_menu"></div>
                <div class="js-top-menu-bottom">
                    <div id="_mobile_currency_selector"></div>
                    <div id="_mobile_language_selector"></div>
                    <div id="_mobile_contact_link"></div>
                </div>
            </div>
      </div>

    </div>
  </div>
  

{/block}
