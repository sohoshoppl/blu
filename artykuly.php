<?php


define('PS_DB', 'mysql:host=blu.dkonto.pl.mysql.dhosting.pl;dbname=riep3r_bludkont');
define('PS_DB_LOGIN', 'oong7e_bludkont');
define('PS_DB_PSWD', 'Fa0raegi7Kee');


require dirname(__FILE__) . '/config/config.inc.php';
/*
define('PS_DB', 'mysql:host=localhost;dbname=blue');
define('PS_DB_LOGIN', 'root');
define('PS_DB_PSWD', '');
*/

error_reporting(E_ALL);
ini_set('display_errors', 1);

// ustawienie kodowania znaków na UTF-8
mb_internal_encoding('UTF-8');

$db = new PDO(PS_DB, PS_DB_LOGIN, PS_DB_PSWD);
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$db->exec("set names utf8");


$sql_query_insert_post = "INSERT IGNORE INTO `ps_ybc_blog_post` (`id_category_default`, `is_featured`, `products`, `added_by`, `is_customer`, `modified_by`, `enabled`, `datetime_added`, `datetime_modified`, `datetime_active`, `sort_order`, `click_number`, `likes`) VALUES (:id_category_default, :is_featured, :products, :added_by, :is_customer, :modified_by, :enabled, :datetime_added, :datetime_modified, :datetime_active, :sort_order, :click_number, :likes);";
$sth_query_insert_post = $db->prepare($sql_query_insert_post);

$sql_query_insert_tag = "INSERT IGNORE INTO `ps_ybc_blog_tag` (`id_post`, `id_lang`, `tag`, `click_number`) VALUES (:id_post, :id_lang, :tag, :click_number);";
$sth_query_insert_tag = $db->prepare($sql_query_insert_tag);

$sql_query_insert_post_lang = "INSERT IGNORE INTO `ps_ybc_blog_post_lang` (`id_post`, `id_lang`, `title`, `url_alias`, `meta_title`, `description`, `short_description`, `meta_keywords`, `meta_description`, `thumb`, `image`) VALUES (:id_post, :id_lang, :title, :url_alias, :meta_title, :description, :short_description, :meta_keywords, :meta_description, :thumb, :image)";
$sth_query_insert_post_lang = $db->prepare($sql_query_insert_post_lang);

$sql_query_insert_post_shop = "INSERT IGNORE INTO `ps_ybc_blog_post_shop` (`id_post`, `id_shop`) VALUES (:id_post, :id_shop) ;";
$sth_query_insert_post_shop = $db->prepare($sql_query_insert_post_shop);

$sql_query_insert_category = "INSERT IGNORE INTO `ps_ybc_blog_category` ( `id_parent`, `added_by`, `modified_by`, `enabled`, `datetime_added`, `datetime_modified`, `sort_order`) VALUES (:id_parent, :added_by, :modified_by, :enabled, :datetime_added, :datetime_modified, :sort_order);";
$sth_query_insert_category = $db->prepare($sql_query_insert_category);

$sql_query_insert_category_lang = "INSERT IGNORE INTO `ps_ybc_blog_category_lang` (`id_category`, `id_lang`, `title`,`url_alias`) VALUES (:id_category, :id_lang, :title,  :url_alias)";
$sth_query_insert_category_lang = $db->prepare($sql_query_insert_category_lang);

$sql_query_insert_category_shop = "INSERT IGNORE INTO `ps_ybc_blog_category_shop` (`id_category`, `id_shop`) VALUES (:id_category, :id_shop) ;";
$sth_query_insert_category_shop = $db->prepare($sql_query_insert_category_shop);


$sql_query_get_id_category = "SELECT `id_category` FROM  `ps_ybc_blog_category_lang` WHERE id_lang = 1 AND UPPER(title) LIKE UPPER(:title);";
$sth_get_id_category = $db->prepare($sql_query_get_id_category);

$sql_query_insert_post_category = "INSERT IGNORE INTO `ps_ybc_blog_post_category` (`id_post`, `id_category`, `position`) VALUES (:id_post, :id_category, :position)";
$sth_query_insert_post_category = $db->prepare($sql_query_insert_post_category);


$post_sort_order = 18;
$post_category_pos = 0;
$category_sort_order = 10;
$i = 0;

if (!defined('_PS_YBC_BLOG_IMG_DIR_')) {
    define('_PS_YBC_BLOG_IMG_DIR_', _PS_IMG_DIR_ . 'ybc_blog/');
}


$file = fopen('artykuly_import.csv', 'r');
while (($line = fgetcsv($file, 0, ';')) !== FALSE) {
    if ($i < 1) {
        $i++;
        continue;
    } else {
        $i++;

        $catoegories_ids = [8];
        //sprawdzanie kategorii i ewentualne dodanie
        $categories = explode(',', $line[9]);
        $id_parent_cat = 8;
        foreach ($categories as $category) {
            $cat_name = mb_convert_case($category, MB_CASE_TITLE, 'UTF-8');
            $insert = [':title' => $category];
            $sth_get_id_category->execute($insert);
            $res = $sth_get_id_category->fetch(PDO::FETCH_ASSOC);


            //trzeba założyć nowe kategorie
            if (!$res) {
                $insert = [':id_parent' => $id_parent_cat,  ':added_by' => 1, ':modified_by' => 1, ':enabled' => 1,  ':datetime_added' => date('Y-m-d H:i:s', strtotime($line[3])), ':datetime_modified' => date('Y-m-d H:i:s', strtotime($line[3])), ':sort_order' => $category_sort_order];
                try {
                    $sth_query_insert_category->execute($insert);
                } catch (PDOException $e) {
                    print_r($e);
                    die();
                };
                $id_added_category = $db->lastInsertId();
                $catoegories_ids[] = $id_added_category;
                $id_parent_cat = $id_added_category;

                $url_alias_category = Tools::link_rewrite($cat_name);
                $insert = [':id_category' => $id_added_category, ':id_lang'=>1, ':title' => $cat_name, ':url_alias' => $url_alias_category];
                try {
                    $sth_query_insert_category_lang->execute($insert);
                } catch (PDOException $e) {
                    print_r($e);
                    die();
                };

                $insert = [':id_category' => $id_added_category, 'id_shop' => 1];
                try {
                    $sth_query_insert_category_shop->execute($insert);
                } catch (PDOException $e) {
                    print_r($e);
                    die();
                };

            } else {
                $catoegories_ids[] = $res['id_category'];
                $id_parent_cat = $res['id_category'];
            }
        }

        $post_sort_order++;
        $post_category_pos++;
        $category_sort_order++;
        echo '<pre>';

        $insert = [':id_category_default' => 8, ':is_featured' => 0, ':products' => '', ':added_by' => 1, ':is_customer' => 0, ':modified_by' => 1, ':enabled' => 1, ':datetime_added' => date('Y-m-d H:i:s', strtotime($line[3])), ':datetime_modified' => date('Y-m-d H:i:s', strtotime($line[3])), ':datetime_active' => date('Y-m-d', strtotime($line[3])), ':sort_order' => $post_sort_order, ':click_number' => 0, ':likes' => 0];

        try {
            $sth_query_insert_post->execute($insert);
        } catch (PDOException $e) {
            print_r($e);
            die();
        };
        $id_post = $db->lastInsertId();


        $insert = [':id_post' => $id_post, ':id_shop' => 1];
        try {
            $sth_query_insert_post_shop->execute($insert);
        } catch (PDOException $e) {
            print_r($e);
            die();
        }

        //tagi
        if (!empty($line[4])) {
            $tags = explode(',', $line[4]);
            foreach ($tags as $tag) {
                $insert = [':id_post' => $id_post, ':id_lang' => 1, ':tag' => $tag, ':click_number' => 0];
                try {
                    $sth_query_insert_tag->execute($insert);
                } catch (PDOException $e) {
                    print_r($e);
                    die();
                }
            }
        }
//----------------------------


        foreach ($catoegories_ids as $id_category) {
            $insert = [':id_post' => $id_post, ':id_category' => $id_category, ':position' => $post_category_pos];
            try {
                $sth_query_insert_post_category->execute($insert);
            } catch (PDOException $e) {
                print_r($e);
                die();
            }
        }


        $url_alias = Tools::link_rewrite($line[5]);

        //kopiowanie zdjęć
        $image_name = $url_alias . '.jpg';

        $target = realpath('./img/tmp') . '/import_post_image.jpg';
        grab_image($line[7], $target);

        //główne
        ImageManager::cut(
            $target,
            _PS_YBC_BLOG_IMG_DIR_ . 'post/' . $image_name,
            Configuration::get('YBC_BLOG_IMAGE_BLOG_WIDTH', null, null, null, 1920),
            Configuration::get('YBC_BLOG_IMAGE_BLOG_HEIGHT', null, null, null, 750),
            'jpg',
            0,
            0,
            1
        );
        //miniaturka
        ImageManager::cut(
            $target,
            _PS_YBC_BLOG_IMG_DIR_ . 'post/thumb/' . $image_name,
            Configuration::get('YBC_BLOG_IMAGE_BLOG_THUMB_WIDTH', null, null, null, 260),
            Configuration::get('YBC_BLOG_IMAGE_BLOG_THUMB_HEIGHT', null, null, null, 180),
            'jpg',
            0,
            0,
            2
        );
        unlink($target);


        $insert = [':id_post' => $id_post, ':id_lang' => 1, ':title' => $line[5], ':url_alias' => $url_alias, ':meta_title' => $line[1], ':description' => $line[8], ':short_description' => $line[2], ':meta_keywords' => '' . $line[4], ':meta_description' => $line[0], ':thumb' => $image_name, ':image' => $image_name];
        try {
            $sth_query_insert_post_lang->execute($insert);
        } catch (PDOException $e) {
            print_r($e);
            die();
        }

    }

}


fclose($file);


$sql = "SELECT b.* FROM `ps_ybc_blog_post` as a JOIN `ps_ybc_blog_post_category` as b ON a.id_post = b.id_post AND b.id_category = 28 ORDER BY `datetime_active` DESC";
$sth = $db->prepare( $sql);
$sth->execute();
$result = $sth->fetchAll(PDO::FETCH_ASSOC);
foreach ($result as $key => $row) {
    $sql2 = "UPDATE `ps_ybc_blog_post_category` SET `position` = " . ($key + 1) . " WHERE `id_post` = " . $row['id_post'] . " AND `id_category` =" . $row['id_category'];
    $sth = $db->prepare($sql2);
    $sth->execute();
}

$sql = "SELECT b.* FROM `ps_ybc_blog_post` as a JOIN `ps_ybc_blog_post_category` as b ON a.id_post = b.id_post AND b.id_category = 8 ORDER BY `datetime_active` DESC";
$sth = $db->prepare( $sql);
$sth->execute();
$result = $sth->fetchAll(PDO::FETCH_ASSOC);
foreach ($result as $key => $row) {
    $sql2 = "UPDATE `ps_ybc_blog_post_category` SET `position` = " . ($key + 1) . " WHERE `id_post` = " . $row['id_post'] . " AND `id_category` =" . $row['id_category'];
    $sth = $db->prepare($sql2);
    $sth->execute();
}

function grab_image($image_url, $image_file)
{
    $fp = fopen($image_file, 'w+');

    $ch = curl_init($image_url);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_FILE, $fp);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 1000);
    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0');
    curl_setopt($ch, CURLOPT_VERBOSE, true);
    curl_exec($ch);

    curl_close($ch);
    fclose($fp);
}
