/* global $ */
$(document).ready(function () {
    var $searchWidget = $('#search_widget');
    var $searchBox    = $searchWidget.find('input[type=text]');
    var searchURL     = $searchWidget.attr('data-search-controller-url');

    function text_truncate(str) {
        var ending = '...';
        if (str.length > 42) {
          return str.substring(0, 40 - ending.length) + ending;
        } else {
          return str;
        }
    };

    function removeTags(str) {
        if ((str===null) || (str===''))
            return false;
        else
            str = str.toString();
        // Regular expression to identify HTML tags in 
        // the input string. Replacing the identified 
        // HTML tag with a null string.
        return str.replace( /(<([^>]+)>)/ig, '');
    }
    var categorySearch = [];
    var categorySearchName = [];
    var shopurlSearch = $searchBox.data('shopurl');

    $.widget('prestashop.psBlockSearchAutocomplete', $.ui.autocomplete, {
        _renderItem: function (ul, product) {
            var categorynameurl = product.category_name.toString().replaceAll(' ', "-").replaceAll('(', "").replaceAll(')', "");
            var categorylink = shopurlSearch+product.id_category_default+'-'+categorynameurl;
            var categoryname = product.category_name;
            if ( !categorySearch.includes(categorylink) ) {
                if (product.category_name) {
                    categorySearch.push(categorylink);
                    categorySearchName.push(categoryname);
                }
            }
            var describe = removeTags(product.description_short);
            var describeClearn = text_truncate(describe);
            if ( describeClearn ) {
                describeClearn= describeClearn;
            } else {
                describeClearn= '';
            }
            var li = $('<li><a href="'+product.url+'"><img src="'+product.cover.bySize.small_default.url+'"/><div class="ui-search-wrapper"><span class="title-search-product">'+product.name+'</span><span class="describe-search">'+describeClearn+'<span/></div><a></li>');

            return li.appendTo(ul);
        }
    });

    function setCookie(cName, cValue, expDays) {
            let date = new Date();
            date.setTime(date.getTime() + (expDays * 24 * 60 * 60 * 1000));
            const expires = "expires=" + date.toUTCString();
            document.cookie = cName + "=" + cValue + "; " + expires + "; path=/";
    }

    function getCookie(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for(var i=0;i < ca.length;i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1,c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
        }
        return null;
    }

    

    if (document.cookie.indexOf('ui-cookie-history=') != -1 ) {
        
        var data_complete = getCookie('ui-cookie-history');
        var datajson = JSON.parse(data_complete);
        $searchBox.autocomplete({
            source: datajson,
            minLength: 0,
            scroll: true,
            // select: function(event, ui) {
            //     event.preventDefault();
            //     console.log( this, $(this) , ' history', ui.item);
            //         // var url = ui.item.url;
            //         // window.location.href = url;   
            // },
            open: function() {
                
                var header_historytext = $searchBox.data('historytext');
                var header_clearntext = $searchBox.data('clearntext');
                var header_content = $('<li class="header-search-list"><div class="ui-search-header"><span class="header-search">'+header_historytext+'</span><span class="ui-search-delete-cookie">'+header_clearntext+'<span/></li>');
                  

                  var elheaders = $('#ui-id-1.ui-autocomplete').find(".header-search-list");
                  if (elheaders.length < 1 ) {
                    $('ul.ui-autocomplete').prepend(header_content);
                  }
                new Scrollbot("#ui-id-1",9).setStyle({
                    "background":"#D8D8D8",
                    "z-index":"2"
                },{
                    "background":"#EFEFEF"
                });

            }    
            }).focus(function() {
                    $(this).autocomplete("search", "");
            }).data("ui-autocomplete")._renderItem = function (ul, product) {
                ul.addClass('cookie_history_ul'); 
                var describe = removeTags(product.description_short);
                var describeClearn = text_truncate(describe);
                if (describeClearn != 'false') {
                    describeClearn= describeClearn;
                } else {
                    describeClearn= '';
                }
                var li = $('<li><a href="'+product.url+'"><img src="'+product.image+'"/><div class="ui-search-wrapper"><span class="title-search-product">'+product.name+'</span><span class="describe-search">'+describeClearn+'<span/></div><a></li>');
                return li.appendTo(ul);
            };
    }

    function CategorySearchList(categorySearch,categorySearchName) {
        var list = "";
        Array.prototype.forEach.call(categorySearchName  ,function(name,index){    
            list+="<a href="+categorySearch[index].toLowerCase()+" >"+name+"</a>";
         });
         return list;
    }
    $searchBox.psBlockSearchAutocomplete({
        source: function (query, response) {
            $.post(searchURL, {
                s: query.term,
                resultsPerPage: 10
            }, null, 'json')
            .then(function (resp) {
                var cookieContent = resp.products;
                var objj = [];

                $(cookieContent).each(function( index ) {
                    var this_ = $(this)[0];
                    var descriptionshort = removeTags(this_.description_short)
                    objj.push(
                        {
                            name:this_.name,
                            image:this_.cover.bySize.small_default.url,
                            description_short:descriptionshort,
                            url:this_.url,
                        }
                    );
                });
                var dataCookie = JSON.stringify(objj);
                setCookie("ui-cookie-history",dataCookie,10);
                response(resp.products);    
            })
            .fail(response);
        },
        // select: function (event, ui) {
        //     console.log(ui, ' not history');
        //     var url = ui.item.url; 
        //     window.location.href = url;
        // },
        open: function() {

            // categories list
            var header_productstext = $searchBox.data('productstext');
            var header_categorystext = $searchBox.data('categorystext');
            var header_categorysresults = $searchBox.data('categorysresults');
            var header_blogresultsUrl = $searchBox.data('search-blog')+encodeURI($searchBox.val());
            var checkUls = $('.cookie_history_ul').length;
            var listHrefs= CategorySearchList(categorySearch,categorySearchName);

            var category_container = $('<div class="category-search-container"><div class="header-search-list"><div class="ui-search-header"><span class="header-search">'+header_categorystext+'</span></div></div><div class="category-search-list">'+listHrefs+'</div></div>');

            var header_content = $('<li class="header-search-list"><div class="ui-search-header"><span class="header-search">'+header_productstext+'</span></div></li>');
            

            // results in categories
            var searchvalue = $searchBox.val();
            var outlet_href = shopurlSearch+'126-outlet?q='+searchvalue;
            var promotion_href = shopurlSearch+'promocje?q='+searchvalue;

            var categorysresults_listHrefs= "<a href="+header_blogresultsUrl+">Inspiracje</a><a href="+header_blogresultsUrl+">Porady</a><a href="+outlet_href+">Outlet</a><a href="+promotion_href+">promocje</a>";
            var categorysresults_container = $('<div class="categoryresult-search-container"><div class="header-search-list"><div class="ui-search-header"><span class="header-search">'+header_categorysresults+'</span></div></div><div class="category-search-list">'+categorysresults_listHrefs+'</div></div>');


            if (checkUls == 1) { 
                
                var elheaders = $('#ui-id-2.ui-autocomplete').find(".header-search-list");
                if (elheaders.length < 1 ) {
                    $('#ui-id-2.ui-autocomplete').prepend(header_content);
                }
                
                new Scrollbot("#ui-id-2",9).setStyle({
                    "background":"#D8D8D8",
                    "z-index":"2"
                },{
                    "background":"#EFEFEF"
                });
                categorysresults_container.appendTo(category_container);
                $('#ui-id-2.ui-autocomplete').prepend(category_container);
            } else {
                var elheaders = $('#ui-id-2.ui-autocomplete').find(".header-search-list");
                if (elheaders.length < 1 ) {
                    $('#ui-id-1.ui-autocomplete').prepend(header_content);
                }
                new Scrollbot("#ui-id-1",9).setStyle({
                    "background":"#D8D8D8",
                    "z-index":"2"
                },{
                    "background":"#EFEFEF"
                });
                categorysresults_container.appendTo(category_container);
                $('#ui-id-1.ui-autocomplete').prepend(category_container);
            }
            $('.delete-search-icon').toggleClass("hidden-index");
        },
    })
    $( document ).on( "click", ".ui-search-delete-cookie", function() {
        var listUi = $(this).closest( ".ui-autocomplete" );
        listUi[0].innerHTML = '';
        listUi.css('display','none');
        document.cookie = "ui-cookie-history= ; expires = Thu, 01 Jan 1970 00:00:00 GMT";
        if (!$('.delete-search-icon').hasClass('hidden-index')) {
            $('.delete-search-icon').addClass('hidden-index');
        }
    });

    $('.delete-search-icon').on( "click", function () {
        $searchBox.val('');
        $(this).toggleClass("hidden-index");
    });
});
