<?php
/**
 * 2007-2020 ETS-Soft
 *
 * NOTICE OF LICENSE
 *
 * This file is not open source! Each license that you purchased is only available for 1 wesite only.
 * If you want to use this file on more websites (or projects), you need to purchase additional licenses. 
 * You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
 * 
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please contact us for extra customization service at an affordable price
 *
 *  @author ETS-Soft <etssoft.jsc@gmail.com>
 *  @copyright  2007-2020 ETS-Soft
 *  @license    Valid for 1 website (or project) for each purchase of license
 *  International Registered Trademark & Property of ETS-Soft
 */

if (!defined('_PS_VERSION_'))
	exit;
class Ybc_blog_paggination_class {
	public $total = 0;
	public $page = 1;
	public $limit = 20;
	public $num_links = 10;
	public $url = '';
	public $text = '{end} z {total} artykułów';
	public $text_first = '<span>|&lt;</span>';
	public $text_last = '<span>&gt;|</span>';
	public $text_next = '<span>&gt;</span>';
	public $text_prev = '<span>&lt;</span>';
	public $style_links = 'links';
	public $style_results = 'results';
    public $alias;
    public $friendly;
    public function __construct()
    {
        $this->alias = Configuration::get('YBC_BLOG_ALIAS');
        $this->friendly = (int)Configuration::get('YBC_BLOG_FRIENDLY_URL') && (int)Configuration::get('PS_REWRITING_SETTINGS') ? true : false;        
    }
	public function render() {
	    
		$total = $this->total;
		if($total<=1)
            return false;
		if ($this->page < 1) {
			$page = 1;
		} else {
			$page = $this->page;
		}
		
		if (!(int)$this->limit) {
			$limit = 10;
		} else {
			$limit = $this->limit;
		}
		
		$num_links = $this->num_links;
		$num_pages = ceil($total / $limit);
		
		$output = [];
		
		if ($page > 1) {
			$output += [
				"pagination_frist" => $this->replacePage(1),
				"pagination_frist_text" => $this->text_first,
				"pagination_prev" => $this->replacePage($page-1),
				"pagination_prev_text" => $this->text_prev,
			];
			// $output .= ' <a class = "frist" href="' . $this->replacePage(1) . '">' . $this->text_first . '</a> <a class = "prev" href="' . $this->replacePage($page-1) . '">' . $this->text_prev . '</a> ';
    	}

		if ($num_pages > 1) {
			if ($num_pages <= $num_links) {
				$start = 1;
				$end = $num_pages;
			} else {
				$start = $page - floor($num_links / 2);
				$end = $page + floor($num_links / 2);
			
				if ($start < 1) {
					$end += abs($start) + 1;
					$start = 1;
				}
						
				if ($end > $num_pages) {
					$start -= ($end - $num_pages);
					$end = $num_pages;
				}
			}

			if ($start > 1) {
				$output += ["more" => '...'];
			}

			for ($i = $start; $i <= $end; $i++) {
				if ($page == $i) {
					// $output .= ' <b>' . $i . '</b> ';
					$output += ['number' => $i];
				} else {
					// $output .= ' <a href="' . $this->replacePage($i) . '">' . $i . '</a> ';
					$output += ['pagination' => [
						'number' => $i,
						'link' => $this->replacePage($i),
						]
					];
				}	
			}
							
			if ($end < $num_pages) {
				$output += ["more" => '...'];
			}
		}
		
   		if ($page < $num_pages) {
			// $output .= ' <a class = "next" href="' . $this->replacePage($page+1) . '">' . $this->text_next . '</a> <a class = "last" href="' . $this->replacePage($num_pages) . '">' . $this->text_last . '</a> ';
			$output += [
				"pagination_next" => $this->replacePage($page+1),
				"pagination_next_text" => $this->text_next,
				"pagination_last" => $this->replacePage($num_pages),
				"pagination_last_text" => $this->text_last,
			];
		}
		
		$find = array(
			'{start}',
			'{end}',
			'{total}',
			'{pages}'
		);
		
		$replace = array(
			($total) ? (($page - 1) * $limit) + 1 : 0,
			((($page - 1) * $limit) > ($total - $limit)) ? $total : ((($page - 1) * $limit) + $limit),
			$total, 
			$num_pages
		);
		if($num_pages==1)
            $this->text= 'Showing {start} to {end} of {total} ({pages} Page)';

		// $paginationArrayContent = [
		// 	"onpage" => ((($page - 1) * $limit) > ($total - $limit)) ? $total : ((($page - 1) * $limit) + $limit),
		// 	"total" => $total,
		// 	"num_pages" => $num_pages,

		// ];
		$output += [
			"onpage" => ((($page - 1) * $limit) > ($total - $limit)) ? $total : ((($page - 1) * $limit) + $limit),
			"total" => $total,
			"num_pages" => $num_pages,

		];
		// dump($paginationArrayContent);

		return $output;
	}
    public function replacePage($page)
    {
        if($page > 1)
            return str_replace('_page_', $page, $this->url);
        elseif($this->friendly && $this->alias && Tools::getValue('controller') != 'AdminModules')
            return str_replace('/_page_', '', $this->url);
        else
            return str_replace('_page_', $page, $this->url);            
    }
}
?>