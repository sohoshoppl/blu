$(document).ready(function () {
 
    initMarkers();
 
    var img_width = $('#head_image #image img').width();
    var img_height = $('#head_image #image img').height();

    $(document).on('click', '#head_image #image img', function (e) {
        e.preventDefault();
        var img_width = $('#head_image #image img').width();
        var img_height = $('#head_image #image img').height();
        // ukrywa wszsytkei inputy na obrazku
        $('#head_image .marker-input').hide();

        // obliczanie procentowej pozycji markera
        var x_pos = ((100 * e.offsetX) / img_width);
        var y_pos = ((100 * e.offsetY - 2000) / img_height);


        addMarkerProduct(x_pos.toFixed(4), y_pos.toFixed(4), Date.now());
        //console.log(x_pos.toFixed(4) + ' - ' + y_pos.toFixed(4) + ' marker: ' + markers);
    });

    // wyswietlanie daneych markera
    $(document).on('click', '#head_image #image .marker', function (e) {
        e.preventDefault();

        // ukrywa wszsytkie inputy na obrazku
        $('#head_image .marker-input').hide();
        $(this).find('.marker-input').show();
    });

    // usuwanie markera
    $(document).on('click', '#head_image .drop_marker', function (e) {
        e.preventDefault();

        $(this).parent().parent().remove();
    });

    $(document).on('click', '#mapping_edit .save_mapping', function (e) {
        e.preventDefault();

        var active_markers = {};
        var el_item = 0;
        $('#mapping_edit #head_image #image .marker').each(function (index, value) {
            var marker_product = $(this).find('input[name="marker_product"]').val();
            var marker_query = $(this).find('input[name="marker_query"]');
            // jesli wyszukano produkt to dodajemy marker do listy
            if (marker_product > 0)
            {
                // konstrukcja markera -> id productu:left:right
                active_markers[el_item] = {
                    'id_product': marker_product,
                    'left': marker_query.data('left'),
                    'top': marker_query.data('top')
                };

                el_item++;
            }
        });

        var requestData = {
            'active_markers': JSON.stringify(active_markers),
            'action': 'saveMapping',
            'id_mapping': id_mapping
        };

        $.post(prestashop.urls.base_url + 'modules/sohoshopbuticzanka/ajax.php', requestData).then(function (data) {
            var dataArr = $.parseJSON(data);
            $.fancybox.open([
                {
                    type: 'inline',
                    autoScale: true,
                    minHeight: 30,
                    content: '<p class="fancybox-error">' + dataArr.text + '</p>'
                }
            ], {
                padding: 0
            });
        });
    });

    $(document).on('click', '.delete-mapping', function (e) {
        e.preventDefault();

        var requestData = {
            'id_mapping': $(this).data('mapping'),
            'action': 'deleteMapping'
        };

        $.post(prestashop.urls.base_url + 'modules/sohoshopbuticzanka/ajax.php', requestData).then(function (data) {
            var dataArr = $.parseJSON(data);
            $.fancybox.open([
                {
                    type: 'inline',
                    autoScale: true,
                    minHeight: 30,
                    content: '<p class="fancybox-error">' + dataArr.text + '</p>'
                }
            ], {
                padding: 0
            });

            $('#item-' + dataArr.id).remove();
        });
    });

});

function addMarkerProduct(xpos, ypos, item, id_product=0)
{
    //console.log('addMarkerProduct: '+xpos+' x ' +ypos);
    var marker_content = '';
    marker_content += '<div style="left:' + xpos + '%;top:' + ypos + '%;" class="imgmc marker" id="marker-' + item + '">';
        marker_content += '<div class="imgmc marker-input">';
            marker_content += '<input id="marker-' + item + '" placeholder="Wyszukaj produkt" class="imgmc form-control marker_pop" type="text" name="marker_query" />';
            marker_content += '<input class="js-marker-product" id="marker-product-' + item + '" type="hidden" name="marker-product-' + item + '" value="'+id_product+'" />';
            marker_content += '<input id="marker-left-' + item + '" type="hidden" name="marker-left-' + item + '" value="'+xpos+'" />';
            marker_content += '<input id="marker-top-' + item + '" type="hidden" name="marker-top-' + item + '" value="'+ypos+'" />';
            marker_content += '<input type="hidden" name="marker_ids[]" value="' + item + '" />';
            marker_content += '<a href="#drop_marker" class="imgmc drop_marker">Usuń</a>';
        marker_content += '</div>';
    marker_content += '</div>';

    $('#head_image #image').append(marker_content);
    

    $('.marker_pop').autocomplete(ybc_blog_ajax_url, {
        minChars: 1,
        autoFill: true,
        max: 20,
        matchContains: true,
        mustMatch: false,
        scroll: false,
        cacheLength: 0,
        formatItem: function (item) {
            return item[1] + ' - ' + item[0];
        },

    }).result(function (event, data, formatted) {
        $('.marker_pop').val('');
        $(this).val(data[0]);
        $(this).parent().find('.js-marker-product').val(data[1]);
        $('#head_image .marker-input').hide();        
        
    });
    $('.marker_pop').setOptions({
        extraParams: {excludeIds: ybcGetAccessoriesIds()}
    }) 

}

function initMarkers(){
   var imageMarkers = JSON.parse(imageMarkersJSON);
        
    var i;
    for (i = 0; i < imageMarkers.length; i++) {
        addMarkerProduct(imageMarkers[i].pos_left, imageMarkers[i].pos_top, imageMarkers[i].id_marker, imageMarkers[i].id_product)
    }  
    $('#head_image .marker-input').hide();    
}