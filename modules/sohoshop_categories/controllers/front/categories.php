<?php

class Sohoshop_categoriesCategoriesModuleFrontController extends ModuleFrontController{

    public function initContent(){
        parent::initcontent();
        $this->context->smarty->assign('categories', Category::getHomeCategories($this->context->language->id));
        $this->setTemplate('module:sohoshop_categories/views/templates/front/categories.tpl');
    }

    public function getBreadcrumbLinks(){
        $breadcrumb = parent::getBreadcrumbLinks();

        $breadcrumb['links'][] = array(
            'title' => $this->trans('Kategorie', array(), 'Shop.Notifications.Success'),
            'url' => '',
        );


        return $breadcrumb;

    }
}