{extends file=$layout}

{block name="content"}

<div id="categories-all">
    <div class="title">{$page.meta.title}</div>


    {foreach from=$categories item=category}
        
            
            <div class="main-category-title">
                {$category['name']} {assign var=child_cats value=Category::getChildren($category['id_category'], $language['id'])}
            </div>
           
                {assign var=open_row value=''}
            <div class="row">
                {foreach from=$child_cats item=child}
                {if $child@iteration % 5 == 0 || $open_row == true}
                    <div class="row">
                    {assign var=open_row value=false}
                {/if}
                {assign var=category_items value=Category::getNestedCategories($child['id_category'])}
                        <div class="col-md-3">
                            <div class="image">
                                <img class="category-image" src="{$link->getCatImageLink(
                                    $category_items[$child['id_category']].name, 
                                    $category_items[$child['id_category']].id_category
                                )}">
                            </div>
                            <div class="child-title">{$child['name']}</div>
                            <div class="child-text">
                            
                                {$category_items[$child['id_category']].description|strip_tags|truncate:300}
                            </div>
                            {assign var=child_children value=Category::getChildren($child['id_category'], $language['id'])}
                         
                            {foreach from=$child_children item=child_child}
                                <a href="{$link->getCategoryLink(
                                    $category_items[$child['id_category']].name, 
                                    $category_items[$child['id_category']].id_category
                                )}">
                                <div class="child-child-title">{$child_child['name']}</div>
                                </a>
                            {/foreach}
                        </div>
                {if $child@iteration % 4 == 0}
                    {assign var=open_row value=true}
                    </div>
                {/if}
                {assign var=last_iteration value=$child@iteration}
                {/foreach}
            {if $last_iteration % 4 != 0}
                    </div>
            {/if}
        
    {/foreach}


    <div class="seo">
        <div class="title-seo">Lorem ipsum seo</div>

        <div class="text"> W ofercie sklepu znajdują się modele płytek o przeróżnych barwach, dlatego przed rozpoczęciem poszukiwań musimy wiedzieć, 
        czy zależy nam np. na beżowych ścianach i podłodze, czy może interesują nas jedynie białe płytki. 
        Inną opcją, która wciąż zyskuje na popularności, są modele imitujące drewno. </div>
        
        <div class="text">Równie ważne będą wymiary płytek – dostępne są zarówno malutkie egzemplarze, 
        dzięki którym stworzymy mozaikę lub wykorzystamy je jako detal w aranżacji, jak i duże płytki, 
        których wystarczy raptem kilka, by pokryć całą ścianę.</div>
        
        <div class="text">Kwestią do rozważenia będzie także sposób wykończenia powierzchni produktu. Niektóre modele mają wysoki połysk, 
        podczas gdy inne są absolutnie matowe. Decyzja w tej materii w dużej mierze wpłynie na to, 
        jak pomieszczenie będzie się ostatecznie prezentować.</div>
 
        <div class="text">Trzeba pamiętać, że każda z oferowanych kolekcji została opracowana z myślą o konkretnym schemacie i jest to ważny czynnik, 
        wpływający na końcowy efekt. Układanie płytek w jodełkę przyniesie całkiem inny – niejednokrotnie bardzo ciekawy – 
        rezultat niż klasyczny wzór.</div>
    </div>
</div>
{/block}