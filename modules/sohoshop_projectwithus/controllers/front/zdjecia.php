<?php

class sohoshop_projectwithusZdjeciaModuleFrontController extends ModuleFrontController{
    public function initContent(){
        parent::initContent();

        $sql = "
            SELECT g.`salon`,g.`id_gallery`,gl.`title`,gl.`description`,gl.`image` 
            FROM `"._DB_PREFIX_."ybc_blog_gallery` as g
            JOIN `"._DB_PREFIX_."ybc_blog_gallery_lang` as gl ON g.id_gallery = gl.id_gallery AND gl.id_lang = ".$this->context->language->id."
            WHERE g.`enabled` = 1 AND g.id_gallery = ".(int)Tools::getValue('id_image', 0);

        $res = Db::getInstance()->getRow($sql);
        if($res){
            $salon = new Store($res['salon']);
            $this->context->smarty->assign('image_data', $res);
            $this->context->smarty->assign('salon', $salon);
            $this->context->smarty->assign('store_link', $this->context->link->getStorePageLink($res['salon']));

        }else{
            Tools::redirect('index.php?controller=404');
        }




        $this->setTemplate('module:sohoshop_projectwithus/views/templates/front/zdjecia.tpl');
    }

    public function setMedia(){
        parent::setMedia();
        $this->registerStylesheet(
            "front-controller-sohonewspaper",
            'modules/sohonewspaper/views/css/single.css',
            [
                'priority' => 999
            ]
        );
    }

    public function getBreadcrumbLinks(){
        $breadcrumb = parent::getBreadcrumbLinks();
        $breadcrumb['links'][] = array(
            'title' => $this->trans('Zdjęcia', array(), 'Shop.Notifications.Success'),
            'url' => '',
        );

        return $breadcrumb;
    }

}