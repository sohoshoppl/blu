<?php

class Sohoshop_projectwithusDimensionModuleFrontController extends ModuleFrontController{

    public function initContent(){
        parent::initContent();
        $blog_class = new Ybc_blog();
        $blog = $blog_class->getPostsByIdCategory(6);
        if(!empty($blog)){
            foreach($blog as $item){
                
                $all_posts[] = array(
                    'url' => $blog_class->getLink('blog', array('id_post' => $item['id_post'])),
                    'id_post' => $item['id_post']
                );
            }
            $this->context->smarty->assign('blog_dimension', $blog);
            $this->context->smarty->assign('dimension_all_posts', $all_posts);
        }

        
        
        $this->setTemplate('module:sohoshop_projectwithus/views/templates/front/dimension.tpl');
    }

    public function getBreadcrumbLinks(){
        $breadcrumb = parent::getBreadcrumbLinks();

        $breadcrumb['links'][] = array(
            'title' => $this->trans('Projektuj z nami', array(), 'Shop.Notifications.Success'),
            'url' => '',
        );

        $breadcrumb['links'][] = array(
            'title' => $this->trans('Jak Wymiarować', array(), 'Shop.Notifications.Success'),
            'url' => '',
        );


        return $breadcrumb;

    }
}