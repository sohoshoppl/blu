<?php

class Sohoshop_projectwithusProjectModuleFrontController extends ModuleFrontController{

    public function initContent(){
        parent::initContent();
        $blog_class = new Ybc_blog();
        $blog_inspire = $blog_class->getPostsByIdCategory(1);
        $blog_help = $blog_class->getPostsByIdCategory(8);
        $blog_inspire_url = $this->getBlogUrl($blog_inspire, $blog_class);
        $blog_help_url = $this->getBlogUrl($blog_help, $blog_class); 
        if(!empty($blog_inspire) && !empty($blog_inspire_url)){
            $this->context->smarty->assign('blog_inspire', $blog_inspire);
            $this->context->smarty->assign('blog_inspire_url', $blog_inspire_url);
        }

        if(!empty($blog_help) && !empty($blog_help_url)){
            $this->context->smarty->assign('blog_help', $blog_help);
            $this->context->smarty->assign('blog_help_url', $blog_help_url);
        }
        
        $this->setTemplate('module:sohoshop_projectwithus/views/templates/front/project.tpl');

    }

    public function getBreadcrumbLinks(){
        $breadcrumb = parent::getBreadcrumbLinks();
        $breadcrumb['links'][] = array(
            'title' => $this->trans('Projektuj z nami', array(), 'Shop.Notifications.Success'),
            'url' => '',
        );

        return $breadcrumb;
    }

    protected function getBlogUrl($blog, $blog_class){
        
        $output_array = array();
        if(!empty($blog)){
            foreach($blog as $item){
                
                $output_array[] = array(
                    'url' => $blog_class->getLink('blog', array('id_post' => $item['id_post'])),
                    'id_post' => $item['id_post']
                );
            }
        }
        return $output_array;
    }



}