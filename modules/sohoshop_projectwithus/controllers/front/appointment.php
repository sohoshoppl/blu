<?php

class sohoshop_projectwithusAppointmentModuleFrontController extends ModuleFrontController{
    public function initContent(){
        parent::initContent();


        $this->setTemplate('module:sohoshop_projectwithus/views/templates/front/appointment.tpl');
    }

    public function setMedia(){
        parent::setMedia();
        $this->path = __PS_BASE_URI__.'modules/sohoshop_projectwithus/';
        $this->addJS($this->path . 'views/js/leaflet.js');
        $this->addJS($this->path . 'views/js/leaflet-provider.js');
        $this->addCSS($this->path . 'views/css/leaflet.css');
        $this->addCSS($this->path . 'views/css/front.css');
    }

    public function getBreadcrumbLinks(){
        $breadcrumb = parent::getBreadcrumbLinks();
        $breadcrumb['links'][] = array(
            'title' => $this->trans('Salony', array(), 'Shop.Notifications.Success'),
            'url' => '',
        );

        return $breadcrumb;
    }
}