{extends file=$layout}

{block name="content"}

    <div id="dimension">
        <div class="row banner">
            <div class="col-md-4 left-banner">
                <div class="flexbox">
                    <div class="left-header"> Wymiarowanie </div>
                </div>
                <div class="flexbox">
                    <div class="left-content">
                        Zmierz swoje pomieszczenie - to wcale nie jest<br>
                        trudne. Ważne, by zrobić to dokładnie - <br>
                        właściwe pomiary są podstawą całego <br>
                        planowania. Poświęć na to tyle czasu, aby mieć <br>
                        pewność, że wszystkie wymiary są prawidłowe.
                    </div>
                </div>
            </div>

            <div class="col-md-8 right-banner">
                <img src="{$urls.img_url}dimensions-banner.png" class="banner-img">
            </div>
        </div>
        <div class="flexbox">
            <div class="under-banner-header"> 10 kroków <br> prawidłowego wymiarowania </div>
        </div>

        {* row *}

        <div class="row steps">
            <div class="col-md-3"></div>
            <div class="col-md-3">
                <div class="number-bg">
                    <div class="circle"> 
                        <span>1.</span>
                    </div> 
                </div>
                <div class="spet-text">Przygotuj ogólne pomiary wysokości<br> ścian i pomieszczeń.</div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-3">
                <div class="number-bg">
                    <div class="circle"> 
                        <span>2.</span>
                    </div> 
                </div>
                <div class="spet-text">Podaj wymiary okien i drzwi<br> (odległość od podłogi do parapetu,<br> 
                wysokość i szerokość okna i drzwi,</div>
            </div>
            <div class="col-md-2"></div>
        </div>
        <div class="flexbox">
            <img class="steps-img" src="{$urls.img_url}wymiarowanie-1.png">
        </div>

        {* row *}

        <div class="row steps">
            <div class="col-md-3"></div>
            <div class="col-md-3">
                <div class="number-bg">
                    <div class="circle"> 
                        <span>3.</span>
                    </div> 
                </div>
                <div class="spet-text">Podaj wymiary wnęk w ścianach<br> (wysokość i szerokość wnęki,<br> odległość od ścian).</div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-3">
                <div class="number-bg">
                    <div class="circle"> 
                        <span>4.</span>
                    </div> 
                </div>
                <div class="spet-text">
                    Zmierz skosy sufitów (wysokość <br> 
                    ścianki kolankowej, głębokość skosu - <br>
                    ile zostaje prostego sufitu, wymiary<br>
                    okien połaciowych).</div>
            </div>
            <div class="col-md-2"></div>
        </div>
        <div class="flexbox">
            <img class="steps-img" src="{$urls.img_url}wymiarowanie-2.png">
        </div>

        {* row *}

        <div class="row steps">
            <div class="col-md-3"></div>
            <div class="col-md-3">
                <div class="number-bg">
                    <div class="circle"> 
                        <span>5.</span>
                    </div> 
                </div>
                <div class="spet-text"> Wskaż pion kanalizacyjny i<br> wentylacyjny.</div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-3">
                <div class="number-bg">
                    <div class="circle"> 
                        <span>6.</span>
                    </div> 
                </div>
                <div class="spet-text">Wskaż dostępne przyłącza<br> elektryczne.</div>
            </div>
            <div class="col-md-2"></div>
        </div>
        <div class="flexbox">
            <img class="steps-img" src="{$urls.img_url}wymiarowanie-3.png">
        </div>


        {* row *}

        <div class="row steps">
            <div class="col-md-3"></div>
            <div class="col-md-3">
                <div class="number-bg">
                    <div class="circle">
                        <span> 7.</span>
                    </div> 
                </div>
                <div class="spet-text">
                    Podaj wymiary elementów <br>
                    nietypowych takich jak kominy, <br>
                    wielkość wyposażenia oraz miejsca <br>
                    ich montażu.</div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-3">
                <div class="number-bg">
                    <div class="circle"> 
                        <span>8.</span>
                    </div> 
                </div>
                <div class="spet-text">
                    Poinformuj nas o stanie wykończeń <br>
                    ścian (czy obecnie jest surowy tynk, <br>
                    czy płytki będą do wymiany, <br>
                    demontażu itp).</div>
            </div>
            <div class="col-md-2"></div>
        </div>

        {* row *}

         <div class="row steps">
            <div class="col-md-3"></div>
            <div class="col-md-3">
                <div class="number-bg">
                    <div class="circle"> 
                        <span>9.</span>
                    </div> 
                </div>
                <div class="spet-text">Załącz zdjęcia obecnego wyglądu <br> pomieszczenia.</div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-3">
                <div class="number-bg">
                    <div class="circle"> 
                        <span>10.</span>
                    </div> 
                </div>
                <div class="spet-text">
                    Wskeż kierunek działań <br>
                    projektowych, nazwę kolekcji płytek, <br>
                    cerarmiki etc., <br>
                    załącz jeśli posiadasz zdjęcia <br>
                    inspiracyjne.</div>
            </div>
            <div class="col-md-2"></div>
        </div>
        {* end rows *}
        
        {if !empty($dimension_all_posts) && !empty($blog_dimension)}
        <div class="blog-header"><div class="flexbox"> Przydatne artykuły</div> </div>
        
        <div class="blog-grid-dim row">
            {foreach from=$blog_dimension item=post}
                <div class="col-md-4 dim-grid-item">
                    
                    <img class="blog-image-dim" src="{$urls.img_ps_url}ybc_blog/post/{$post.image}">
                    
                    <div class="grid-title-dim">
                        {$post.title}
                    </div>
                    
                    {foreach from=$dimension_all_posts item=url}
                        {if $url.id_post == $post.id_post}
                            <a href="{$url.url}">
                                <button class="btn btn-primary">Więcej > </button>
                            </a>
                        {/if}
                    {/foreach}
                </div>
            {/foreach}
        </div>
        {/if}
    </div>

{/block}