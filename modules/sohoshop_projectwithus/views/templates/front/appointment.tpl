{extends file=$layout}

{block name='content'}
{assign var='stores' value=Store::getStores($language.id)}
{$stores|@var_dump}
    <div id="appointment">
        <div class="banner">
            <div class="flexbox">
                <div class="banner-title">Umów się z projektantem w salonie</div>
            </div>
            <div class="flexbox">
                <div class="banner-desc">
                    Nasi projektanci czekają na Ciebie w dogodnej dla Ciebie lokalizacji. Możesz też wybrać kontakt przez telefon.
                </div>
            </div>
            <div class="flexbox">
                <form class="banner-form" id="searchFormAppointment" action="" method="GET">
                    <div class="form-flexbox-container">
                        <input id="desiredCity" class="banner-salon-search" type="text" name="city" placeholder="Wpisz miasto lub kod pocztowy">
                        <button type="submit" class="btn btn-primary">Pokaż salony ></button>
                    </div>
                </form>
            </div>
        </div>

        <div class="store-map">
            <div class="row">
                <div class="col-md-6">
                    <div class="flexbox-total">
                        <div class="map-title">Wybierz salon<br>najbliżej Twojego domu.</div>
                    </div>
                </div>
                <div class="col-md-6 map-container">
                    <div id="umowsiemap">
                        {* mapa *}
                    </div>
                    <div id="x-icon-exit">&#10006;</div> 
                    <div id="overlay-shop-info">
                               
                    </div>
                </div>
            </div>
        </div>

        <div class="form">
        
            <form action="{$urls.pages.contact}" method="post" class="bottom-contact-us-form">
            <input type="hidden" name="token" value="{$token}" />
            <div class="flexbox title">{l s='Skontaktuj się z projektantem' d='Shop.Theme.Actions'} <br>{l s='w wybranym salonie.' d='Shop.Theme.Actions'}</div>
            <div class="row first">
                <div class="col-md-6">
                    <input type="text" name="name_surname" placeholder="{l s='Twoje imię i nazwisko*' d='Shop.Theme.Actions'}" class="bottom-input">
                </div>

                <div class="col-md-6 margin-mobile">
                <select name="salon" class="bottom-input">
                    {foreach from=$stores item=store}
                        <option value="{$store.email}">{$store.city}</option>
                    {/foreach}
                <select>
                </div>
                
            </div>
            {* second row *}

            <div class="row second">
                
                <div class="col-md-6 margin-mobile">
                    <input type="text" name="email" placeholder="{l s='Twój adres e-mail*' d='Shop.Theme.Actions'}" class="bottom-input">
                </div>

                <div class="col-md-6">
                    <input type="text" name="phone_number" placeholder="{l s='Twój numer telefonu*' d='Shop.Theme.Actions'}" class="bottom-input">
                </div>

            </div>
            <textarea name="message" rows="4" cols="50" class="bottom-textarea">
                {l s='Treść' d='Shop.Theme.Actions'}
            </textarea><br>
            <span>{l s='Twoje dane będziemy przetwarzać zgodnie z naszą Polityką prywatności' d='Shop.Theme.Actions'}</span>
            <div class="flexbox">
            <button class="contact-us-button-bottom" name="submitMessage" type="submit">{l s='Wyślij >' d='Shop.Theme.Actions'}</button>
            </div>
            </form>
        </div>
    </div>

    {literal}
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <script type="text/javascript">
        
        $( document ).ready(function() {
            function renderMap(){
            umowSiemap = L.map('umowsiemap').setView([51.815, 19.072], 7);
            L.tileLayer("https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png").addTo(umowSiemap);
            }

            var stores = {
                {/literal}
            {foreach from=$stores item='mapStore'}
                "{$mapStore.name|escape:'htmlall':'UTF-8'}"{literal} :{
                    "lat":{/literal}{$mapStore.latitude|escape:'htmlall':'UTF-8'}{literal},
                    "lon":{/literal}{$mapStore.longitude|escape:'htmlall':'UTF-8'}{literal},
                    "city":{/literal}"{$mapStore.city|escape:'htmlall':'UTF-8'}"{literal},
                    "id":{/literal}{$mapStore.id|escape:'htmlall':'UTF-8'}{literal},
                    "postcode":{/literal}"{$mapStore.postcode|escape:'htmlall':'UTF-8'}"{literal},
                    "email":{/literal}"{$mapStore.email|escape:'htmlall':'UTF-8'}"{literal},
                    "address":{/literal}"{$mapStore.address1|escape:'htmlall':'UTF-8'}"{literal},
                    "name":{/literal}"{$mapStore.name|escape:'htmlall':'UTF-8'}"{literal},
                    "phone":{/literal}"{$mapStore.phone|escape:'htmlall':'UTF-8'}"{literal},
                    "hours":{/literal}"{$mapStore.hours|escape:'htmlall':'UTF-8'}"{literal},
                },
            {/literal}
            {/foreach}
            {literal}
            };
            console.log(stores);
            function renderMarks(){
                var markersLayer = L.featureGroup().addTo(umowSiemap);
                let icon = L.divIcon({
                    iconSize:null,
                    html: '<div class="appointment-store-icon"></div>'
                });
                for(store in stores){
                    var marker = L.marker([stores[store].lat, stores[store].lon], {icon: icon}).addTo(umowSiemap).on('click', showOverlayShop);
                        marker.property = stores[store];
                        marker.addTo(markersLayer);
                }
                console.log(markersLayer);
            }

            function searchOnMap(){
                let desiredCity= $('#desiredCity').val();
                for(store in stores){   
                    if(stores[store].city === desiredCity){
                        var desiredStore = stores[store];
                    }
                }
                
                umowSiemap.flyTo([desiredStore.lat, desiredStore.lon], 7, {
                    animate: true,
                    duration: 0.8
                });
            }

            renderMap();
            renderMarks();



            $('#searchFormAppointment').submit(function (e) {
                e.stopPropagation();
                e.preventDefault();
                searchOnMap();
                return false;

            });

            function showOverlayShop(e){
                
                $('#overlay-shop-info').html(
                    '<div class="flexbox">'+
                        '<div class="overlay-shop-name">'+e.target.property.name+'</div>'+
                    '</div>'+
                    '<div class="flexbox">'+
                        '<div class="row address-container">'+
                            '<div class="col-md-2 svg-icon"></div>'+
                            '<div class="col-md-10 data-container">'+
                                '<div class="city-postal">'+e.target.property.postcode+' '+e.target.property.city+'</div>'+
                                '<div class="address">'+e.target.property.address+'</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                    '<div class="flexbox">'+
                        '<div class="row phone-container">'+
                            '<div class="col-md-2 svg-icon"></div>'+
                            '<div class="col-md-10 data-container">'+
                                '<div class="phone-number">tel. '+e.target.property.phone+'</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                    '<div class="flexbox">'+
                        '<div class="row contact-container">'+
                            '<div class="col-md-2 svg-icon"></div>'+
                            '<div class="col-md-10 data-container">'+
                                '<div class="email-address"><a href=mailto:'+e.target.property.email+'>'+e.target.property.email+'</a></div>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                    '<div class="flexbox-flex-end">'+
                        '<a href="#" class="btn btn-primary">Skontaktuj się z projektantem</a>'+
                    '</div>'
                    
                );
                
                 $('#overlay-shop-info').fadeIn();
                 $('#x-icon-exit').fadeIn();
            }


            $('#x-icon-exit').click(function(){
                $('#x-icon-exit').fadeOut();
                $('#overlay-shop-info').fadeOut();               
            });
        });
        
            
        </script>
    {/literal}

{/block}
