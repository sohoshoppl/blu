{extends file=$layout}

{block name='content'}

    <section id="main" class="soho-single-newspaper">
        <h1 class="header-single-newspaper">{$image_data.title}</h1>
        <div class="header-single-newspaper-img newspaper-img-container">
            <img src="{$urls.img_ps_url}ybc_blog/gallery/{$image_data.image}">

        </div>

    </section>
        <div id="zdjecie-salon">
            <div id="salon" >

                <h1>{$salon->name[$language.id]}</h1>
                <address class="row">
                    <div class="col-xs-1">
                        <img class="pin" src="/themes/sohoshop_blu/assets/img/point-icon.svg"
                             alt="Najbliższy salon">
                    </div>
                    <div class="col-xs-11">
                        {$salon->address1[$language.id]}<br>
                        {$salon->postcode} {$salon->city}<br><br>
                        tel. {$salon->phone}<br>
                        <a href="mailto:{$salon->email}">{$salon->email}</a>
                    </div>
                    <div class="col-xs-12">
                    <a href="{$store_link}" class="btn btn-primary">
                        {l s='Pokaż więcej' mod='freestorelocator'}
                    </a>
                    </div>
                </address>
            </div>
        </div>

{/block}
