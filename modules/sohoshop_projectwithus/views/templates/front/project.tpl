{extends file=$layout}
{block name=content}
{debug}
        <div id="projectwithus">
            <div class="row diff-size project-header">
                <div class="col-md-5 left-side">
                    <div class="flexbox">
                        <div class="title">{$page.meta.title}</div>
                    </div>
                    <div class="flexbox">
                        <div class="desc">Chcesz zaprojektować szybko i sprawnie swoją<br> łazienkę lub inne pomieszczenie?</div>
                    </div>
                    <div class="flexbox">
                        <div class="bot-desc">Ponad 50 projektantów w całej Polsce czeka,<br> aby pomóc Ci zaprojektować wymarzony dom<br> lub mieszkanie.</div>
                    </div>
                </div>
                <div class="col-md-3">
                    <img class="header-image" src="{$urls.img_url}projectwithushead1.png">
                </div>
                <div class="col-md-3">
                    <img class="header-image" src="{$urls.img_url}projectwithushead2.png">
                </div>
                <div class="col-md-3">
                    <img class="header-image" src="{$urls.img_url}projectwithushead3.png">
                </div>
            </div>

            <div class="content-grey">
                <div class="flexbox">
                    <div class="grey-title">Jak zacząć?</div>
                </div>

                <div class="row grey-grid">
                    <div class="col-md-3">
                        <div class="flexbox-center">
                            <span class="number">1.</span>  
                            <div class="grey-img-wrapper">
                                <img class="grey-svg" src="{$urls.img_url}pomysl.svg">
                            </div>
                        </div>
                        <div class="flexbox">
                            <div class="desc">
                                Zacznij od naszych Inspiracji.<br>
                                Zajrzyj do naszego działu z gotowymi<br>
                                projektami. Każdy z nich zawiera<br>
                                szczegółowe informacje o użytych <br>
                                produktach, wybierz te, które Ci się <br>
                                podobają.</div>
                            </div>
                        
                    </div>
                    {* row 2 *}
                    <div class="col-md-3">
                        <div class="flexbox-center">
                            <span class="number">2.</span>  
                            <div class="grey-img-wrapper">
                                <img class="grey-svg" src="{$urls.img_url}miara.svg">
                            </div>
                        </div>
                        <div class="flexbox">
                            <div class="desc mb">Zmierz swoje pomieszczenie</div>
                        </div>
                        <div class="flexbox">
                            <div class="desc"> Ogranicza Cię tylko przestrzeń dlatego<br> starannie ją zwymiaruj.</div>
                        </div>
                        
                    </div>
                    {* row 3 *}
                    <div class="col-md-3">
                        <div class="flexbox-center">
                            <span class="number">3.</span> 
                            <div class="grey-img-wrapper">
                                <img class="grey-svg" src="{$urls.img_url}pomiary.svg">
                            </div>
                        </div>
                        <div class="flexbox">
                            <div class="desc mb">Umów się z projektantem.</div>
                        </div>
                        <div class="flexbox">
                            <div class="desc">Możesz odwiedzić nasz salon lub<br> zadzwonić do projektanta.</div>
                        </div>
                        
                    </div>
                    {* row 4 *}
                    <div class="col-md-3">
                        <div class="flexbox-center">
                            <span class="number">4.</span> 
                            <div class="grey-img-wrapper">
                                <img class="grey-svg" src="{$urls.img_url}pomiary-komp.svg">
                            </div>
                        </div>
                        <div class="flexbox">
                            <div class="desc">
                                Skorzystaj z naszych Porad.<br>
                                Podpowiemy Ci jak wybrać produkty, <br>
                                kolorystykę, jak zagospodarować <br>
                                przestrzeń.</div>
                            </div>
                        
                    </div>

                </div>
                {* ^end row *}

                <div class="row grey-grid margin-top-buttons">
                    <div class="col-md-3"><div class="flexbox"><a class="btn btn-primary" href="#">Zainspiruj się ></a></div></div>
                    <div class="col-md-3"><div class="flexbox"><a class="btn btn-primary" href="#">Jak wymiarować ></a></div></div>
                    <div class="col-md-3"><div class="flexbox"><a class="btn btn-primary" href="#">Umów się ></a></div></div>
                    <div class="col-md-3"><div class="flexbox"><a class="btn btn-primary" href="#">Zobacz porady ></a></div></div>
                </div>
            </div>

            <div class="own-project">
                <div class="flexbox title-own">Pamiętaj</div>
                <div class="flex-middle">
                    <div class="left">
                        <img src="{$urls.img_url}pieniadze.svg">
                        <div class="parent">
                            <div class="text-top">Usługę projektowania możesz <br>otrzymać gratis do zakupów.</div>
                            <div class="text-bottom">O szczegóły zapytaj w lokalnym salonie.</div>
                        </div>
                    </div>
                    <div class="right">
                        <img src="{$urls.img_url}projektant.svg">
                        <div class="parent">
                            <div class="text-top">Możesz nas odwiedzić lub<br>konsultować projekt online.</div>
                            <div class="text-bottom">O szczegóły zapytaj w lokalnym salonie.</div>
                        </div>
                    </div>
                </div>

                <div class="app-project">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="flexbox"><span class="title">Zaprojektuj<br>Samodzielnie</span></div>
                            <div class="flexbox">
                                <div class="desc-app">
                                    Stwórz projekt samodzielnie w domu <br>
                                    - to prostsze niż myślisz. <br>
                                    Nasza aplikacja jest dostępna online, bez <br>
                                    konieczności instalacji oprogramowania.
                                </div>
                            </div>
                            <div class="flexbox">
                                <a href="#" class="btn btn-primary">Wypróbuj aplikację</a>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <img src="{$urls.img_url}imaczaprojektuj.png"
                        </div>
                    </div>
                </div>
            </div>

            <div class="inspire-posts">
                <div class="flexbox"><span class="title-top">Najnowsze inspiracje</span></div>
                <div class="flexbox"><span class="title-bottom">Potrzebujesz pomysłów? Poznaj nasze Inspiracje!</span></div>
                <div class="flexbox">
                    <a href="#" class="btn btn-primary change-content">
                        <span class="hovered">WIĘCEJ ></span>
                        <span class="non-hovered">Więcej Inspiracji ></span>
                    </a>
                </div>
                <div class="row inspire-row-grid">
                    {foreach from=$blog_inspire item='inspire'}
                        {if $inspire@iteration > 2}
                            {break}
                        {/if}
                        <div class="col-md-6">
                            <img class="inspire-image" src="{$urls.img_ps_url}ybc_blog/post/{$inspire.image}">
                            <div class="title">{$inspire.title}</div>
                            <div class="desc">{$inspire.description|truncate:300 nofilter}</div>
                            {foreach from=$blog_inspire_url item='url'}
                                {if $url.id_post == $inspire.id_post}
                                    <a href="{$url.url}" class="btn btn-primary">WIĘCEJ > </a>
                                {/if}
                            {/foreach}
                        </div>
                    {/foreach}
                </div>
                
            </div>


            <div class="news-help">
                <div class="flexbox"><span class="title-top">Najnowsze porady</span></div>
                <div class="flexbox"><span class="title-bottom">Nie wiesz jak zacząć remont? Potrzebujesz fachowej porady?</span></div>
                <div class="flexbox">
                    <a href="#" class="btn btn-primary change-content">
                        <span class="hovered">WIĘCEJ ></span>
                        <span class="non-hovered">Więcej porad ></span>
                    </a>
                </div>
                <div class="row news-help-grid">
                {foreach from=$blog_help item='help'}
                    {if $help@iteration > 3}
                        {break}
                    {/if}
                        <div class="col-md-4">
                            <img class="help-image" src="{$urls.img_ps_url}ybc_blog/post/{$help.image}">
                            <div class="title">{$help.title}</div>
                            <div class="desc">{$help.description|truncate:160 nofilter}</div>
                            {foreach from=$blog_help_url item='url'}
                                {if $url.id_post == $help.id_post}
                                    <a href="{$url.url}" class="btn btn-primary">WIĘCEJ > </a>
                                {/if}
                            {/foreach}
                        </div>
                    {/foreach}
                </div>
            </div>


            <div class="stores-project">
                {assign var='stores' value=(Store::getStores($language.id))}
                <div class="store-title">Dowiedz się więcej o usługach projektowych w najbliższym salonie</div>
                    <div class="overflow">
                        {foreach from=$stores item=store}
                            <div class="{if $store@iteration > 1} next-thumb {/if} salon-thumb">
                            <span class="title">{$store.name}</span>
                            {* first row *}
                            <div class="row mt-1">
                                <div class="col-md-1">
                                <img src="{$urls.img_url}point-icon.svg"/>
                                </div>
                                <div class="col-md-3">
                                {$store.postcode} {$store.city}<br>{$store.address1}
                                </div>
                                <div class="col-md-7">
                                Godziny otwarcia: <br>Pon. - Pt. 10:00-18:00 <br>Sob. 11.00-14.00, Niedz 11.00-14.00
                                </div>
                                <div class="col-md-1 mt-3 enter-arrow"><img src="{$urls.img_url}arrow-right.png"></div>
                            </div>
                            {* second row *}
                            <div class="row mt-2">
                                <div class="col-md-1">
                                
                                </div>
                                <div class="col-md-3">
                                    tel. {$store.phone}<br> 
                                    <a class="email-store" href="{$store.email}">{$store.email}</a>
                                </div>
                                <div class="col-md-8">
                                
                                </div>
                            </div>
                            </div>
                        {/foreach}

                    </div>
                <div class="flexbox custom-width">
                    <a href="#"><button class="btn btn-primary">Znajdź inne salony > </button></a>
                </div>
            </div>
        <div>
{/block}