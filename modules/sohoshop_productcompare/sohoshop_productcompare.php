<?php
/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

require_once(dirname(__FILE__) . '/classes/Compare.php');

class Sohoshop_Productcompare extends Module
{
    private $_html = '';
    private $_postErrors = [];

    public $checkName;
    public $address;
    public $extra_mail_vars;

    public function __construct()
    {
        $this->name = 'sohoshop_productcompare';
        $this->version = '1.0.0';
        $this->author = 'SohoShop.pl';
        $this->controllers = ['compare'];
        $this->bootstrap = true;
        parent::__construct();

        $this->displayName = $this->trans('SohoSHop - porównywarka produktów', [], 'Modules.Productcompare.Admin');
        $this->description = $this->trans('SohoSHop - porównywarka produktów', [], 'Modules.Productcompare.Admin');
        $this->ps_versions_compliancy = ['min' => '1.7.1.0', 'max' => _PS_VERSION_];

    }

    public function install()
    {
        require_once(dirname(__FILE__) . '/install_sql.php');

        return parent::install()
            && $this->registerHook('actionFrontControllerSetMedia')
        ;
    }

    public function uninstall()
    {
        return parent::uninstall();
    }

    public function hookActionFrontControllerSetMedia($params)
    {
        $this->context->controller->registerStylesheet(
            $this->name,
            'modules/' . $this->name . '/views/' . $this->name . '.css',
            [
                'media' => 'all',
                'priority' => 2000,
            ]
        );

        $this->context->controller->registerJavascript(
            $this->name,
            'modules/' . $this->name . '/views/' . $this->name . '.js',
            [
                'priority' => 2000,
            ]
        );
    }

}
