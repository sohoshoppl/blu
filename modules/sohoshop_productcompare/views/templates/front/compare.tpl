{extends file=$layout}
<!-- tłumaczenia
{l s="Produkt został dodany do porównania" d="Shop.Notifications.Success"}
{l s="Przejdź i porównaj" d="Shop.Notifications.Success"}
{l s="Porównywarka" d="Shop.Notifications.Success"}
{l s="Produkt nie mógł być dodany do porównania. Osiągnięto limit %d produktów" sprintf=[0] d="Shop.Notifications.Warning"}



 -->

{block name='content'}
    {if $products}
        <pre style='display: none'>{$products[1]|@print_r}</pre>
        <section id="main" class="background-white pad-16">
        {block name='product_list_header'}
            <div class="section-title row">
                <h1 class="h1">{l s='Porównanie produktów'}</h1>
            </div>
        {/block}
        <div class="row border-bottom">
            <div class="compare-product -remove col-md-6">{l s="Usuń z porównania"}</div>
            {foreach name=products from=$products item=product}
                <div class="compare-product -remove col-md-3 text-xs-center" data-id_product="{$product.id_product}">
                    <button type="button" class="btn-default js-remove-from-compare"
                            title="{l s='Usuń'  d='Shop.Theme.Actions'}" data-id_product="{$product.id_product}"><i
                                class="material-icons">cancel</i></button>
                </div>
            {/foreach}
        </div>
        <div class="row border-bottom">
            <div class="compare-product -image col-md-6"></div>
            {foreach name=products from=$products item=product}
                <div class="compare-product -image col-md-3 text-xs-center" data-id_product="{$product.id_product}"><img
                            src="{$product.cover.medium.url}" alt="{$product.name}"/></div>
                {{/foreach}}
        </div>
        <div class="row border-bottom">
            <div class="compare-product -name col-md-6">{l s="Nazwa"}</div>
            {foreach name=products from=$products item=product}
                <div class="compare-product -name col-md-3 text-xs-center"
                     data-id_product="{$product.id_product}">{$product.name}</div>
            {/foreach}
        </div>
        <div class="row border-bottom">
            <div class="compare-product -details-price col-md-6">{l s="Cena"}</div>
            {foreach name=products from=$products item=product}
                <div class="compare-product -details-price js-price col-md-3 text-xs-center"
                     data-id_product="{$product.id_product}">
                    <span>{Tools::displayPrice($product.price)}</span>
                </div>
            {/foreach}
        </div>
        <div class="row border-bottom">
            <div class="compare-product -reviews col-md-6">{l s="Ocena"}</div>
            {foreach name=products from=$products item=product}
                <div class="compare-product -reviews col-md-3 text-xs-center" data-id_product="{$product.id_product}">
                </div>
            {/foreach}
        </div>
        <div class="row border-bottom">
            <div class="compare-product -details-price-quantity js-quantity col-md-6">{l s="Dostępność productu"}</div>
            {foreach name=products from=$products item=product}
                <div class="compare-product -details-price-quantity js-quantity col-md-3 text-xs-center"
                     data-id_product="{$product.id_product}">{if $product.quantity > 0}{l s="Dostępny"}{else}{l s="Chwilowo niedostępny"}{/if}    </div>
            {/foreach}
        </div>
        <div class="row border-bottom">
            <div class="compare-product-sub --subtitle s col-xs-12">{l s="Produkt"}</div>
        </div>
        <div class="row border-bottom">

            <div class="compare-product --reference col-md-6">{l s="Kod produktu"}</div>
            {foreach name=products from=$products item=product}
                {if $product.reference}
                    <div class="compare-product --reference col-md-3 text-xs-center"
                         data-id_product="{$product.id_product}">{$product.reference}</div>
                {else}
                    <div class="compare-product --reference col-md-3 text-xs-center"
                         data-id_product="{$product.id_product}">-
                    </div>
                {/if}
            {/foreach}

        </div>
        <div class="row padding-zero">
            <div class="compare-product --warranty col-md-6">{l s="Gwarancja"}</div>
            {foreach name=products from=$products item=product}
                {if $product.features}
                    {foreach from=$product.features item=feature}
                        {if $feature.name == Gwarancja}
                            <div class="compare-product --warranty col-md-3 text-xs-center"
                                 data-id_product="{$product.id_product}">{$feature.value}</div>
                        {/if}
                    {/foreach}
                {else}
                    <div class="compare-product --warranty col-md-3 text-xs-center"
                         data-id_product="{$product.id_product}">-
                    </div>
                {/if}
            {/foreach}
        </div>
        <div class="row border-bottom padding-zero">

            <div class="compare-product --warranty col-md-6">{l s="Producent"}</div>
            {foreach name=products from=$products item=product}
                {if $product.features}
                    {foreach from=$product.features item=feature}
                        {if $feature.name == Sprzedawca}
                            <div class="compare-product --warranty col-md-3 text-xs-center"
                                 data-id_product="{$product.id_product}">{$feature.value}</div>
                        {/if}
                    {/foreach}
                {else}
                    <div class="compare-product --warranty col-md-3 text-xs-center"
                         data-id_product="{$product.id_product}">-
                    </div>
                {/if}
            {/foreach}
        </div>
        <div class="row border-bottom">
            <div class="compare-product-sub --subtitle col-xs-12">{l s="Informacje podstawowe"}</div>
        </div>
        {foreach name=products from=$products item=product}
            {if isset($product.attributes)}
                {foreach name=productAttributes from=$product.attributes item=attribute}
                    {assign var=nextGroup value=$feature.name}
                    <div class="padding-zero row">
                        {if $feature.name != $nextGroup}
                            <div class="compare-product -attr col-md-6">{$attribute.group}</div>
                            {assign var=currentAttr value=$attribute.group}
                            {foreach name=products from=$products item=product}
                                {foreach name=productAttributes from=$product.attributes item=attribute}
                                    {if $currentAttr == $attribute.group}
                                        <div class="compare-product -attr col-md-3 text-xs-center"
                                             data-id_product="{$product.id_product}">{$attribute.name}</div>
                                    {else}
                                        <div class="compare-product -attr col-md-3"
                                             data-id_product="{$product.id_product}">-
                                        </div>
                                    {/if}
                                {/foreach}
                            {/foreach}
                            {assign var=nextGroup value=$feature.name}
                        {/if}
                    </div>
                {/foreach}
            {/if}
        {/foreach}
        <div class="row">
            <div class="compare-product-sub --subtitle border-bottom col-xs-12">{l s="Informacje techniczne"}</div>
        </div>
        {assign var=count value=0}
        {assign var=usedgroup  value=[]}
        {assign var=featuresList value=[]}
        {foreach from=$products item=product}

            {foreach from=$product.features item=feature}
                {if $feature.name != Gwarancja && $feature.name != Sprzedawca }
                    {if $feature.name|in_array:$featuresList}
                    {else}
                        {append var=featuresList value=$feature }
                    {/if}
                {/if}
            {/foreach}
        {/foreach}


        {foreach from=$featuresList item=feature}
            <div class="border-bottom row">
                <div class="compare-product  col-md-6">{$feature.name}</div>
                {assign var=prodcount value=0}
                {foreach name=products from=$products item=product}
                    {foreach from=$product.features item=product_feature}
                        {if $feature.id_feature == $product_feature.id_feature}
                            <div class="compare-product -attr col-md-3 text-xs-center"
                                 data-id_product="{$product_feature.id_product}">{$product_feature.value}</div>
                        {/if}
                    {/foreach}
                {/foreach}
            </div>
        {/foreach}


        </div>
    {else}
        <div class="section-title ">
            <article>
                <div class="alert alert-warning">{l s='Nie ma zaznaczonych produktów do porównania'} </div>
            </article>
            <a href="{$urls.base_url}" title="{$shop.name}" class="btn btn-primary">{l s='kontynuuj przeglądanie'}</a>
        </div>
    {/if}


    </section>
{/block}
