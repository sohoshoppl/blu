//porównywarka produktów

$(document).on('click', '.js-addToCompare', function (e) {
    e.preventDefault();
    var idProduct = $(this).attr('data-id_product');
    var jqxhr = $.ajax({
        type: "POST",
        url: prestashop.urls.shop_domain_url + '/index.php?fc=module&module=sohoshop_productcompare&controller=compare',
        data: 'ajax=1&id_product=' + $(this).data('id_product') + '&action=add',
        dataType: "json",
        success: function (respond) {
            if (respond.error) {
                var html = `
                 <div id="js-addToCompareSuccess" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="alert alert-success" role="alert">` + respond.error + `</div>
                        </div>
                    </div>
                 </div>`;
            } else if (respond.success) {
                var html = `
                 <div id="js-addToCompareSuccess" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="alert alert-success" role="alert">` + respond.success + `</div>
                        </div>
                    </div>
                 </div>`;
            }
            $('#notifications .container').html(html);
            $('#js-addToCompareSuccess').modal('show');
        }
    });
});

$(document).on('click', '.js_optionToCompare', function (e) {
    e.preventDefault();
    var optionType = $(this).data('optionType');
    if (!$(this).hasClass('active')) {
        switch (optionType) {
            case 'price':
                $('.js-price').show();
                break;
            case 'quantity':
                $('.js-quantity').show();
                break;
            case 'manufacture':
                break;
            case 'attributeGroup':
                var id_group = $(this).data('id_attribute_group');
                var group_name = $(this).text();

                $('.js-details').each(function () {
                    var htmlToAppend = '<div class="js-attribute-' + id_group + '"><dt>' + group_name + '</dt>';
                    htmlToAppend += typeof $(this).find('.js-productAttributesGroup-' + id_group).html() !== 'undefined' ? $(this).find('.js-productAttributesGroup-' + id_group).html() : '<dd> - </dd>';
                    htmlToAppend += '</div>';
                    $(this).append(htmlToAppend);
                });

                break;
            case 'feature':
                break;
        }
        $(this).addClass('active');
    } else {
        switch (optionType) {
            case 'price':
                $('.js-price').hide();
                break;
            case 'quantity':
                $('.js-quantity').hide();
                break;
            case 'manufacture':
                break;
            case 'attributeGroup':
                var id_group = $(this).data('id_attribute_group');
                $(".js-attribute-" + id_group).remove();
                break;
            case 'feature':
                break;
        }
        $(this).removeClass('active');
    }
});


$(document).on('click', '.js-remove-from-compare', function (e) {
    e.preventDefault();
    var idProduct = parseInt($(this).data('id_product'));
    $.ajax({
        url: prestashop.urls.shop_domain_url + '/index.php?fc=module&module=sohoshop_productcompare&controller=compare&ajax=1&action=remove&id_product=' + idProduct,
        async: false,
        cache: false
    });
    var productId = $(this).attr('data-id_product');
    productId = productId.toString();
    $(".compare-product[data-id_product='" + productId + "']").each(function () {
        $(this).fadeOut(600);
    })

    $(this).fadeOut(600);

});
