<?php
 /* @author    SohoSHOP
 * @copyright Copyright (c) 2019 SohoSHOP
 * @license   http://SohoSHOP.pl
 *
 * http://SohoSHOP.pl
 */
class Compare extends ObjectModel
{
    public $id_compare;

    public $id_customer;
    
    public $id_product;

    public $date_add;

    public $date_upd;

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'compare',
        'primary' => 'id_compare',
        'fields' => array(
            'id_compare' =>    array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
            'id_customer' =>    array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
            'id_product' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
            'date_add' => array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
            'date_upd' => array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
            
        ),
    );

    /**
     * Get all compare products of the customer
     * @param int $id_customer
     * @return array
     */
    public static function getCustomerCompares($id_customer)
    {
        return Db::getInstance()->executeS('SELECT `id_product` FROM `'._DB_PREFIX_.'compare` WHERE `id_customer` = '.(int)($id_customer));
    }


    /**
     * Get the number of compare products of the customer
     * @param int $id_customer
     * @return int
     */
    public static function getNumberProducts($id_customer)
    {
        return (int)(Db::getInstance()->getValue('
			SELECT count(`id_compare`)
			FROM `'._DB_PREFIX_.'compare`
			WHERE `id_customer` = '.(int)($id_customer)));
    }


    /**
     * Remove a compare product for the customer
     * @param int $id_customer
     * @param int $id_product
     * @return bool
     */
    public static function removeCompareProduct($id_customer, $id_product)
    {
        return Db::getInstance()->execute('
		DELETE cp FROM `'._DB_PREFIX_.'compare` cp, `'._DB_PREFIX_.'compare` c
		WHERE cp.`id_compare`=c.`id_compare`
		AND cp.`id_product` = '.(int)$id_product.'
		AND c.`id_customer` = '.(int)$id_customer);
    }

    /**
     * Get the id_compare by id_customer
     * @param int $id_customer
     * @return int $id_compare
     */
    public static function getIdCompareByIdCustomer($id_customer)
    {
        return (int)Db::getInstance()->getValue('
		SELECT `id_compare`
		FROM `'._DB_PREFIX_.'compare`
		WHERE `id_customer`= '.(int)$id_customer);
    }
}
