<?php

/* @author    SohoSHOP
 * @copyright Copyright (c) 2019 SohoSHOP
 * @license   http://SohoSHOP.pl
 *
 * http://SohoSHOP.pl
 */

use PrestaShop\PrestaShop\Adapter\Image\ImageRetriever;

class Sohoshop_ProductcompareCompareModuleFrontController extends ModuleFrontController
{

    const PS_COMPARATOR_MAX_ITEM = '2';

    /**
     * Display ajax content (this function is called instead of classic display, in ajax mode)
     */
    public function init()
    {
        if ($this->ajax) {
            if ($this->context->customer->isLogged()) {
                if (Tools::getValue('action') == 'add') {
                    if (Compare::getNumberProducts($this->context->customer->id) < self::PS_COMPARATOR_MAX_ITEM) {
                        $compare = new Compare();
                        $compare->id_customer = $this->context->customer->id;
                        $compare->id_product = (int)Tools::getValue('id_product');
                        $compare->add();
                        $return['success'] = $this->trans('Produkt został dodany do porównania', array(), 'Shop.Notifications.Success');
                        $return['success'] .= ' <a href="' . $this->context->link->getPageLink('module-sohoshop_productcompare-compare') . '">' . $this->trans('Przejdź i porównaj', array(), 'Shop.Notifications.Success') . '</a>';
                    } else {
                        $return['error'] = $this->trans('Produkt nie mógł być dodany do porównania. Osiągnięto limit %d produktów', array((int)self::PS_COMPARATOR_MAX_ITEM), 'Shop.Notifications.Warning');
                        $return['error'] .= ' <a href="' . $this->context->link->getPageLink('module-sohoshop_productcompare-compare') . '">' . $this->trans('Przejdź i porównaj', array(), 'Shop.Notifications.Success') . '</a>';

                    }
                } elseif (Tools::getValue('action') == 'remove') {
                    if (isset($this->context->customer->id)) {
                        Compare::removeCompareProduct($this->context->customer->id, (int)Tools::getValue('id_product'));
                        $return['success'] = $this->trans('Produkt został usuniety', array(), 'Shop.Notifications.Success');
                        $return['success'] .= ' <a href="' . $this->context->link->getPageLink('module-sohoshop_productcompare-compare') . '">' . $this->trans('Przejdź i porównaj', array(), 'Shop.Notifications.Success') . '</a>';
                    } else {
                        $this->ajaxDie('0');
                    }

                } else {
                    $this->ajaxDie('0');
                }

            } else {
                $return['error'] = $this->trans('Zaloguj się, aby korzystać z porównywarki produktów', array(), 'Shop.Notifications.Warning');
            }
            exit(Tools::jsonEncode($return));
        }
        parent::init();
    }

    /**
     * Assign template vars related to page content
     * @see FrontController::initContent()
     */
    public function initContent()
    {

        parent::initContent();
        //pobrane same id produktów
        $products = Compare::getCustomerCompares($this->context->customer->id);
        $productsForTemptale = array();

        if ($products) {
            //uzupełnienie produktów danymi
            $assembler = new ProductAssembler($this->context);
            $imageRetriever = new ImageRetriever($this->context->link);
            foreach ($products as $id_product) {
                $product = $assembler->assembleProduct($id_product);
                $product['images'] = $imageRetriever->getProductImages($product, $this->context->language);
                if (isset($product['id_product_attribute'])) {
                    foreach ($product['images'] as $image) {
                        if (isset($image['cover']) && null !== $image['cover']) {
                            $product['cover'] = $image;

                            break;
                        }
                    }
                }

                if (!isset($product['cover'])) {
                    if (count($product['images']) > 0) {
                        $product['cover'] = array_values($product['images'])[0];
                    } else {
                        $product['cover'] = null;
                    }
                }


                $productsForTemptale[] = $product;


            }
        }


        $this->context->smarty->assign('features', Feature::getFeatures($this->context->language->id));
        $this->context->smarty->assign('attributeGroups', AttributeGroup::getAttributesGroups($this->context->language->id));
        $this->context->smarty->assign('products', $productsForTemptale);
        $this->context->smarty->assign('nbProducts', count($products));


        $this->setTemplate('module:'.$this->module->name.'/views/templates/front/compare.tpl');

    }

    public function getBreadcrumbLinks()
    {
        $breadcrumb = parent::getBreadcrumbLinks();

        $breadcrumb['links'][] = array(
            'title' => $this->trans('Porównywarka', array(), 'Shop.Notifications.Success'),
            'url' => '',
        );


        return $breadcrumb;
    }
}
