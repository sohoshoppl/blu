<?php
require_once(_PS_ROOT_DIR_ . '/modules/sohoshopProductMultipleDescriptions/classes/SohoshopProductAdditionalDescriptions.php');
class Sohoshop_inspiracjeInspiracjeModuleFrontController extends ModuleFrontController{

    public function initContent(){
        parent::initcontent();

        $ybc_blogModule = Module::getInstanceByName('ybc_blog');
        $id_category = 1;
        if(!Configuration::get('YBC_BLOG_POST_SORT_BY'))
            $sort = 'p.datetime_active DESC, ';
        else
        {
            if(Configuration::get('YBC_BLOG_POST_SORT_BY')=='sort_order')
                $sort = 'pc.position ASC, ';
            else
                $sort = 'p.'.Configuration::get('YBC_BLOG_POST_SORT_BY').' DESC, ';
        }
        $filter = ' AND pc.id_category="'.(int)$id_category.'" AND p.enabled=1';
        $start = 0;
        $limit = 5;
        $postsSlider =  $ybc_blogModule->getPostsWithFilter($filter, $sort, $start, $limit);

        $limit = 30;
        $page = Tools::getValue('page',1);
        if($page > 1){
            $start = $limit * ($page -1);
        }else{
            $start = 0;
        }

        $tagFliters = Tools::getValue('filter',false);
        if($tagFliters){
            $filter .= " AND p.id_post IN (SELECT id_post FROM `"._DB_PREFIX_."ybc_blog_tag` WHERE 1 AND ( 0 ";
            $t =  str_replace('_',' ',trim(Tools::strtolower($tagFliters)));
            $t =  str_replace('+',' ',trim(Tools::strtolower($t)));
            //foreach ($tags as $t){
                $filter .= " OR lower(tag) Like '%".$t."%'";
            //}

            $filter .= ") AND id_lang = ".$this->context->language->id.")";
        }

        $order = Tools::getValue('order', false);

        if ($order) {
            $orderParts = explode('.', $order);
            $sort = $orderParts[0] . '.' . $orderParts[1] . ' ' . $orderParts[2] . ',';
        }

        $posts =  $ybc_blogModule->getPostsWithFilter($filter, $sort, $start, $limit);
        $totalRecords = (int)$ybc_blogModule->countPostsWithFilter($filter);

        if(Tools::getValue('ajaxPage')){
            ob_end_clean();
            header('Content-Type: application/json');
            die(json_encode([
                'posts' => $posts,
                'limit' => $limit,
                'totalRecords' => $totalRecords
            ]));
        }
        $tags = $ybc_blogModule->getTags((int)Configuration::get('YBC_BLOG_TAGS_NUMBER') > 0 ? (int)Configuration::get('YBC_BLOG_TAGS_NUMBER') : 20);

        $sub_categogires = $ybc_blogModule->getCategoriesWithFilter(' AND c.enabled=1',false,false,false,(int)$id_category);
        if($sub_categogires)
        {
            foreach($sub_categogires as &$sub)
            {
                $sub['link'] = $ybc_blogModule->getLink('blog',array('id_category'=>$sub['id_category']));
            }
        }
        $this->context->smarty->assign('sub_categogires',$sub_categogires);
        $this->context->smarty->assign('tags', $tags);
        $this->context->smarty->assign('p', $page);
        $this->context->smarty->assign('posts', $posts);
        $this->context->smarty->assign('postsSlider', $postsSlider);
        $this->context->smarty->assign('totalRecords', $totalRecords);
        $this->context->smarty->assign('total', count($posts) + $start);
        $this->setTemplate('module:sohoshop_inspiracje/views/templates/front/inspiracje.tpl');
    }

    public function getBreadcrumbLinks(){
        $breadcrumb = parent::getBreadcrumbLinks();

        $breadcrumb['links'][] = array(
            'title' => $this->trans('Zainspiruj się', array(), 'Shop.Notifications.Success'),
            'url' => '',
        );


        return $breadcrumb;

    }

    public function setMedia()
    {   parent::setMedia();
        $this->registerJavascript('inspiracje-js', $this->module->getPathUri().'/views/js/front.js', ['position' => 'bottom', 'priority' => 2000]);
        $this->registerStylesheet('inspiracje-css', $this->module->getPathUri().'/views/css/front.css', ['media' => 'all', 'priority' => 2000]);


    }

    }