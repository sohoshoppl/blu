{extends file="page.tpl"}
{block name="content"}
    <h1>{l s='Zainspiruj się' d='Modules.Sohoshopinspiracje.Shop'}</h1>
    <div id="inspiracje-slider">
        <div class="swiper-container sliderInspiracje">
            <div class="swiper-wrapper">
                {foreach from=$postsSlider item=$pSlider name=mainSliders}
                    <div class="swiper-slide">
                        <div class="image-box">
                            <a href="{$pSlider.view_url}">
                                <img src="{$urls.img_ps_url}ybc_blog/post/{$pSlider.image}">
                            </a>
                            {assign var=imageMarkers value=(Ybc_blog::getPostImageMarkers($pSlider.id_post|intval , true))}
                            {if !empty($imageMarkers)}
                                {foreach name=markers from=$imageMarkers item=marker}
                                    {if $marker.productObj->id}
                                        <div class="marker" style="left:{$marker.pos_left}%;top:{$marker.pos_top}%">

                                            <img class="marker-icon" src="{$urls.img_url}product_marker.svg">
                                            <div class="marker_content clearfix {if $marker.pos_left > 50} right_marker_content {else} left_marker_content{/if}  {if $marker.pos_left < 60 &&  $marker.pos_left > 40 } centered_marker_content {/if}">
                                                <div class="marker-inner">
                                                    <img src="{if isset($marker.productImages[0].id_image)}{$urls.base_url}{$marker.productImages[0].id_image}-home_default/{$marker.productObj->link_rewrite[$language.id]}.jpg{else}/img/404.gif{/if}">
                                                    <div class="right-box">
                                                        <h3>{$marker.productObj->name[$language.id]}</h3>
                                                        {assign var=subtitle value=(SohoshopProductAdditionalDescriptions::getProductAdditionalDescriptions($marker.productObj->id))}
                                                        {assign var="manufacturer_name" value=(Manufacturer::getNameById($marker.productObj->id_manufacturer))}
                                                        <div class="product-subtitle">{$subtitle.description_1}</div>
                                                        <div class="product-manufacturer">{$manufacturer_name}</div>
                                                        <a class="btn btn-primary btn-inspire"
                                                           href="{url entity=product id=$marker.productObj->id}">
                                                            {l s='przejdź do produktu' d='Modules.Sohoshopinspiracje.Shop'}
                                                            <svg class="blog_arrow" xmlns="http://www.w3.org/2000/svg"
                                                                 width="16" height="9.5" viewBox="0 0 12.262 7.192">
                                                                <defs></defs>
                                                                <g transform="translate(0.53 0.53)">
                                                                    <path d="M-317.515-44.61l-5.6,5.6-5.6-5.6"
                                                                          transform="translate(328.717 44.61)"></path>
                                                                </g>
                                                            </svg>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    {/if}
                                {/foreach}
                            {/if}
                        </div>
                        <div class="title">{$pSlider.title}</div>
                        <a class="more-button" href="{$pSlider.view_url}">
                            <button class="btn btn-primary btn-inspire">{l s='więcej' d='Modules.Sohoshopinspiracje.Shop'}
                                <svg class="blog_arrow" xmlns="http://www.w3.org/2000/svg" width="16" height="9.5"
                                     viewBox="0 0 12.262 7.192">
                                    <defs></defs>
                                    <g transform="translate(0.53 0.53)">
                                        <path d="M-317.515-44.61l-5.6,5.6-5.6-5.6"
                                              transform="translate(328.717 44.61)"></path>
                                    </g>
                                </svg>
                            </button>
                        </a>

                    </div>
                {/foreach}
            </div>
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
            <!-- If we need scrollbar -->
            <div class="swiper-scrollbar"></div>
        </div>
    </div>
    <div id="inspiracje-find">
        <p class="inspiracje-find-header">{l s='Znajdź według wyglądu:' d='Modules.Sohoshopinspiracje.Shop'}</p>
        <div class="swiper-container sliderInspiracjeFind">
            <div class="swiper-wrapper">
                {foreach from=$sub_categogires item=sub_category name=sub_categories}
                    <div class="swiper-slide">
                        <a href="{$sub_category.link}">
                            <img height="250" src="{$urls.img_ps_url}ybc_blog/category/thumb/{$sub_category.thumb}">
                            <label>
                                {$sub_category.title}
                            </label>
                        </a>
                    </div>
                {/foreach}
            </div>
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
            <!-- If we need scrollbar -->
            <div class="swiper-scrollbar"></div>
        </div>
    </div>
    <div id="inspiracje-tags" class="clearfix">
        <label>{l s='Wybierz po tagach:' d='Modules.Sohoshopinspiracje.Shop'}</label>
        <div>
            {foreach from=$tags item=$tag name=tags}
                <button onclick="location.href='{url entity='module' name='sohoshop_inspiracje' controller='inspiracje' params=['filter' => $tag.tag|lower]}'"
                        type="button">{$tag.tag}</button>
            {/foreach}
        </div>
    </div>
    <div id="inspiracje-filters">
        <div class="inspiracje-dropdown-wrapper">
            <label>{l s='Filtruj wg' d='Modules.Sohoshopinspiracje.Shop'}:</label>
            <div class="dropdown">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    {l s='Wygląd i materiał' d='Modules.Sohoshopinspiracje.Shop'}
                </button>

                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item"
                       href="{url entity='module' name='sohoshop_inspiracje' controller='inspiracje' params=['filter' => 'kamień']}">{l s='Kamień' d='Modules.Sohoshopinspiracje.Shop'}</a>
                    <a class="dropdown-item"
                       href="{url entity='module' name='sohoshop_inspiracje' controller='inspiracje' params=['filter' => 'drewno']}">{l s='Drewno' d='Modules.Sohoshopinspiracje.Shop'}</a>
                    <a class="dropdown-item"
                       href="{url entity='module' name='sohoshop_inspiracje' controller='inspiracje' params=['filter' => 'cegła']}">{l s='Cegła' d='Modules.Sohoshopinspiracje.Shop'}</a>
                    <a class="dropdown-item"
                       href="{url entity='module' name='sohoshop_inspiracje' controller='inspiracje' params=['filter' => 'patchwork']}">{l s='Patchwork' d='Modules.Sohoshopinspiracje.Shop'}</a>
                    <a class="dropdown-item"
                       href="{url entity='module' name='sohoshop_inspiracje' controller='inspiracje' params=['filter' => 'gres']}">{l s='Gres' d='Modules.Sohoshopinspiracje.Shop'}</a>
                    <a class="dropdown-item"
                       href="{url entity='module' name='sohoshop_inspiracje' controller='inspiracje' params=['filter' => 'lastryko']}">{l s='Lastryko' d='Modules.Sohoshopinspiracje.Shop'}</a>
                    <a class="dropdown-item"
                       href="{url entity='module' name='sohoshop_inspiracje' controller='inspiracje' params=['filter' => 'szkło']}">{l s='Szkło' d='Modules.Sohoshopinspiracje.Shop'}</a>
                    <a class="dropdown-item"
                       href="{url entity='module' name='sohoshop_inspiracje' controller='inspiracje' params=['filter' => 'metal']}">{l s='Metal' d='Modules.Sohoshopinspiracje.Shop'}</a>
                </div>
            </div>
            <div class="dropdown">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    {l s='Marki' d='Modules.Sohoshopinspiracje.Shop'}
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item"
                       href="{url entity='module' name='sohoshop_inspiracje' controller='inspiracje' params=['filter' => 'tubądzin']}">{l s='Tubądzin' d='Modules.Sohoshopinspiracje.Shop'}</a>
                    <a class="dropdown-item"
                       href="{url entity='module' name='sohoshop_inspiracje' controller='inspiracje' params=['filter' => 'opoczno']}">{l s='Opoczno' d='Modules.Sohoshopinspiracje.Shop'}</a>
                    <a class="dropdown-item"
                       href="{url entity='module' name='sohoshop_inspiracje' controller='inspiracje' params=['filter' => 'azario']}">{l s='Azario' d='Modules.Sohoshopinspiracje.Shop'}</a>
                    <a class="dropdown-item"
                       href="{url entity='module' name='sohoshop_inspiracje' controller='inspiracje' params=['filter' => 'deante']}">{l s='Deante' d='Modules.Sohoshopinspiracje.Shop'}</a>
                    <a class="dropdown-item"
                       href="{url entity='module' name='sohoshop_inspiracje' controller='inspiracje' params=['filter' => 'nowa gala']}">{l s='Nowa Gala' d='Modules.Sohoshopinspiracje.Shop'}</a>
                    <a class="dropdown-item"
                       href="{url entity='module' name='sohoshop_inspiracje' controller='inspiracje' params=['filter' => 'huppe']}">{l s='Huppe' d='Modules.Sohoshopinspiracje.Shop'}</a>
                    <a class="dropdown-item"
                       href="{url entity='module' name='sohoshop_inspiracje' controller='inspiracje' params=['filter' => 'aquaform']}">{l s='Aquaform' d='Modules.Sohoshopinspiracje.Shop'}</a>
                    <a class="dropdown-item"
                       href="{url entity='module' name='sohoshop_inspiracje' controller='inspiracje' params=['filter' => 'roca']}">{l s='Roca' d='Modules.Sohoshopinspiracje.Shop'}</a>
                    <a class="dropdown-item"
                       href="{url entity='module' name='sohoshop_inspiracje' controller='inspiracje' params=['filter' => 'koło']}">{l s='Koło' d='Modules.Sohoshopinspiracje.Shop'}</a>
                    <a class="dropdown-item"
                       href="{url entity='module' name='sohoshop_inspiracje' controller='inspiracje' params=['filter' => 'geberit']}">{l s='Geberit' d='Modules.Sohoshopinspiracje.Shop'}</a>
                    <a class="dropdown-item"
                       href="{url entity='module' name='sohoshop_inspiracje' controller='inspiracje' params=['filter' => 'kludi']}">{l s='Kludi' d='Modules.Sohoshopinspiracje.Shop'}</a>
                    <a class="dropdown-item"
                       href="{url entity='module' name='sohoshop_inspiracje' controller='inspiracje' params=['filter' => 'elita']}">{l s='Elita' d='Modules.Sohoshopinspiracje.Shop'}</a>
                    <a class="dropdown-item"
                       href="{url entity='module' name='sohoshop_inspiracje' controller='inspiracje' params=['filter' => 'terma']}">{l s='Terma' d='Modules.Sohoshopinspiracje.Shop'}</a>
                    <a class="dropdown-item"
                       href="{url entity='module' name='sohoshop_inspiracje' controller='inspiracje' params=['filter' => 'hansgrohe']}">{l s='Hansgrohe' d='Modules.Sohoshopinspiracje.Shop'}</a>
                    <a class="dropdown-item"
                       href="{url entity='module' name='sohoshop_inspiracje' controller='inspiracje' params=['filter' => 'paradyż']}">{l s='Paradyż' d='Modules.Sohoshopinspiracje.Shop'}</a>
                    <a class="dropdown-item"
                       href="{url entity='module' name='sohoshop_inspiracje' controller='inspiracje' params=['filter' => 'omnires']}">{l s='Omnires' d='Modules.Sohoshopinspiracje.Shop'}</a>
                    <a class="dropdown-item"
                       href="{url entity='module' name='sohoshop_inspiracje' controller='inspiracje' params=['filter' => 'new trendy']}">{l s='New Trendy' d='Modules.Sohoshopinspiracje.Shop'}</a>
                    <a class="dropdown-item"
                       href="{url entity='module' name='sohoshop_inspiracje' controller='inspiracje' params=['filter' => 'cerrad']}">{l s='Cerrad' d='Modules.Sohoshopinspiracje.Shop'}</a>
                    <a class="dropdown-item"
                       href="{url entity='module' name='sohoshop_inspiracje' controller='inspiracje' params=['filter' => 'defra']}">{l s='Defra' d='Modules.Sohoshopinspiracje.Shop'}</a>
                    <a class="dropdown-item"
                       href="{url entity='module' name='sohoshop_inspiracje' controller='inspiracje' params=['filter' => 'excellent']}">{l s='Excellent' d='Modules.Sohoshopinspiracje.Shop'}</a>
                    <a class="dropdown-item"
                       href="{url entity='module' name='sohoshop_inspiracje' controller='inspiracje' params=['filter' => 'radaway']}">{l s='Radaway' d='Modules.Sohoshopinspiracje.Shop'}</a>
                    <a class="dropdown-item"
                       href="{url entity='module' name='sohoshop_inspiracje' controller='inspiracje' params=['filter' => 'Ravak']}">{l s='Ravak' d='Modules.Sohoshopinspiracje.Shop'}</a>
                    <a class="dropdown-item"
                       href="{url entity='module' name='sohoshop_inspiracje' controller='inspiracje' params=['filter' => 'oristo']}">{l s='Oristo' d='Modules.Sohoshopinspiracje.Shop'}</a>
                    <a class="dropdown-item"
                       href="{url entity='module' name='sohoshop_inspiracje' controller='inspiracje' params=['filter' => 'laufen']}">{l s='Laufen' d='Modules.Sohoshopinspiracje.Shop'}</a>
                    <a class="dropdown-item"
                       href="{url entity='module' name='sohoshop_inspiracje' controller='inspiracje' params=['filter' => 'Kludi']}">{l s='Kludi' d='Modules.Sohoshopinspiracje.Shop'}</a>
                    <a class="dropdown-item"
                       href="{url entity='module' name='sohoshop_inspiracje' controller='inspiracje' params=['filter' => 'sanplast']}">{l s='Sanplast' d='Modules.Sohoshopinspiracje.Shop'}</a>
                    <a class="dropdown-item"
                       href="{url entity='module' name='sohoshop_inspiracje' controller='inspiracje' params=['filter' => 'cersanit']}">{l s='Cersanit' d='Modules.Sohoshopinspiracje.Shop'}</a>
                    <a class="dropdown-item"
                       href="{url entity='module' name='sohoshop_inspiracje' controller='inspiracje' params=['filter' => 'oras']}">{l s='Oras' d='Modules.Sohoshopinspiracje.Shop'}</a>
                    <a class="dropdown-item"
                       href="{url entity='module' name='sohoshop_inspiracje' controller='inspiracje' params=['filter' => 'geberit']}">{l s='Geberit' d='Modules.Sohoshopinspiracje.Shop'}</a>
                    <a class="dropdown-item"
                       href="{url entity='module' name='sohoshop_inspiracje' controller='inspiracje' params=['filter' => 'domino']}">{l s='Domino' d='Modules.Sohoshopinspiracje.Shop'}</a>
                    <a class="dropdown-item"
                       href="{url entity='module' name='sohoshop_inspiracje' controller='inspiracje' params=['filter' => 'iÖ']}">{l s='IÖ' d='Modules.Sohoshopinspiracje.Shop'}</a>
                    <a class="dropdown-item"
                       href="{url entity='module' name='sohoshop_inspiracje' controller='inspiracje' params=['filter' => 'vijo']}">{l s='Vijo' d='Modules.Sohoshopinspiracje.Shop'}</a>

                </div>
            </div>
            <div class="dropdown">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    {l s='Pomieszczenia' d='Modules.Sohoshopinspiracje.Shop'}
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item"
                       href="{url entity='module' name='sohoshop_inspiracje' controller='inspiracje' params=['filter' => 'łazienka']}">{l s='Łazienka' d='Modules.Sohoshopinspiracje.Shop'}</a>
                    <a class="dropdown-item"
                       href="{url entity='module' name='sohoshop_inspiracje' controller='inspiracje' params=['filter' => 'kuchnia']}">{l s='Kuchnia' d='Modules.Sohoshopinspiracje.Shop'}</a>
                    <a class="dropdown-item"
                       href="{url entity='module' name='sohoshop_inspiracje' controller='inspiracje' params=['filter' => 'salon']}">{l s='Salon' d='Modules.Sohoshopinspiracje.Shop'}</a>
                    <a class="dropdown-item"
                       href="{url entity='module' name='sohoshop_inspiracje' controller='inspiracje' params=['filter' => 'korytarz']}">{l s='Korytarz' d='Modules.Sohoshopinspiracje.Shop'}</a>
                    <a class="dropdown-item"
                       href="{url entity='module' name='sohoshop_inspiracje' controller='inspiracje' params=['filter' => 'ogród']}">{l s='Ogród' d='Modules.Sohoshopinspiracje.Shop'}</a>
                    <a class="dropdown-item"
                       href="{url entity='module' name='sohoshop_inspiracje' controller='inspiracje' params=['filter' => 'taras']}">{l s='Taras' d='Modules.Sohoshopinspiracje.Shop'}</a>
                    <a class="dropdown-item"
                       href="{url entity='module' name='sohoshop_inspiracje' controller='inspiracje' params=['filter' => 'balkon']}">{l s='Balkon' d='Modules.Sohoshopinspiracje.Shop'}</a>
                    <a class="dropdown-item"
                       href="{url entity='module' name='sohoshop_inspiracje' controller='inspiracje' params=['filter' => 'garaż']}">{l s='Garaż' d='Modules.Sohoshopinspiracje.Shop'}</a>
                    <a class="dropdown-item"
                       href="{url entity='module' name='sohoshop_inspiracje' controller='inspiracje' params=['filter' => 'salon kąpielowy']}">{l s='Salon Kąpielowy' d='Modules.Sohoshopinspiracje.Shop'}</a>
                    <a class="dropdown-item"
                       href="{url entity='module' name='sohoshop_inspiracje' controller='inspiracje' params=['filter' => 'sypialnia']}">{l s='Sypialnia' d='Modules.Sohoshopinspiracje.Shop'}</a>
                </div>
            </div>
        </div>
        <div class="sort-by-row">
            <span class="sort-by">{l s='Sortuj wg:' d='Modules.Sohoshopinspiracje.Shop'}</span>
            <div class="products-sort-order dropdown">
                <button class="btn-unstyle select-title" rel="nofollow" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                    {l s='Domyślne' d='Modules.Sohoshopinspiracje.Shop'}
                    <svg class="blog_arrow" xmlns="http://www.w3.org/2000/svg" width="16" height="9.5"
                         viewBox="0 0 12.262 7.192">
                        <defs></defs>
                        <g transform="translate(0.53 0.53)">
                            <path d="M-317.515-44.61l-5.6,5.6-5.6-5.6" transform="translate(328.717 44.61)"></path>
                        </g>
                    </svg>
                </button>
                <div class="dropdown-menu">
                    <a rel="nofollow" href="/inspiracje?order=p.datetime_active.desc"
                       class="select-list {if isset($smarty.get.order)  &&  $smarty.get.order|strpos:"datetime_active"} current {/if}">
                        {l s='Najnowsze' d='Modules.Sohoshopinspiracje.Shop'}
                    </a>
                    <a rel="nofollow" href="/inspiracje?order=p.id_post.asc"
                       class="select-list {if isset($smarty.get.order)  &&   $smarty.get.order|strpos:"id_post"} current {/if}">
                        {l s='Najstarsze' d='Modules.Sohoshopinspiracje.Shop'}
                    </a>
                    <a rel="nofollow" href="/inspiracje?order=p.click_number.desc"
                       class="select-list {if isset($smarty.get.order)  &&   $smarty.get.order|strpos:"click_number"} current {/if}">
                        {l s='Najpopularniejsze' d='Modules.Sohoshopinspiracje.Shop'}
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="inspiracje-posts" class="row">
        <ul>
            {foreach from=$posts item=$post name=posts}
                <li class="col-xs-12 col-sm-6 col-lg-4">
                    <a class="ybc_item_img" href="{$post.view_url}"><img
                                src="{$urls.img_ps_url}ybc_blog/post/thumb/{$post.thumb}" alt="{$post.title}"
                                title="Trend dwa"></a>
                    <div class="ybc-blog-latest-post-content">
                        <a class="ybc_title_block" href="{$post.view_url}">{$post.title}</a>
                        <a class="btn btn-primary" href="{$post.view_url}">
                            {l s='więcej' d='Modules.Sohoshopinspiracje.Shop'}
                            <svg class="blog_arrow" xmlns="http://www.w3.org/2000/svg" width="16" height="9.5"
                                 viewBox="0 0 12.262 7.192">
                                <defs></defs>
                                <g transform="translate(0.53 0.53)">
                                    <path d="M-317.515-44.61l-5.6,5.6-5.6-5.6"
                                          transform="translate(328.717 44.61)"></path>
                                </g>
                            </svg>
                        </a>
                    </div>
                </li>
            {/foreach}

        </ul>
        <div class="stores-footer">
            <p class="stores-count-text">
                <span id="stores-count-text-total"> {$total} </span>
                z {$totalRecords} {l s='artykułów' d='Modules.Sohoshopinspiracje.Shop'}
            </p>
            <button class="stores-button pagination js-getMoreInspiracje" data-page="{$p}">
                {l s='Pokaż więcej' d='Modules.Sohoshopinspiracje.Shop'}
                <svg class="fs-locator-search-ico" xmlns="http://www.w3.org/2000/svg" width="14.262" height="9.192"
                     viewBox="0 0 12.262 7.192">
                    <defs></defs>
                    <g transform="translate(0.53 0.53)">
                        <path d="M-317.515-44.61l-5.6,5.6-5.6-5.6" transform="translate(328.717 44.61)"></path>
                    </g>
                </svg>
            </button>
        </div>
    </div>
    <div id="store-page-describe">
        <h3 class="store-describe-header">{l s='Umywalki SEO' d='Modules.Sohoshopinspiracje.Shop'}</h3>
        <p>
            {l s='Dobra umywalka do łazienki powinna być trwała i łatwa w utrzymaniu czystości. W IKEA znajdziesz wiele modeli umywalek, które możesz dopasować do niemal każdego wnętrza. Na wszystkie umywalki ceramiczne oraz wykonane z kruszonego marmuru (w tym umywalki nablatowe) oferujemy 10 lat bezpłatnej gwarancji.' d='Modules.Sohoshopinspiracje.Shop'}

        </p>
        <p>
            <b> {l s='Umywalki łazienkowe' d='Modules.Sohoshopinspiracje.Shop'}</b><br>
            {l s='Warto zadbać, by umywalka łazienkowa była dopasowana do metrażu pomieszczenia. Umywalka z szafką podumywalkową to sprytne rozwiązania dla właścicieli niewielkich łazienek. Dzięki nim zyskasz mnóstwo miejsca do przechowywania. Umywalka podwójna lepiej sprawdzi się w przestronnych łazienkach.' d='Modules.Sohoshopinspiracje.Shop'}

        </p>

        <p>
            <b>{l s='Umywalka do łazienki' d='Modules.Sohoshopinspiracje.Shop'}</b><br>
            {l s='Umywalki łazienkowe wytwarzane z solidnych materiałów posłużą ci przez wiele lat. W IKEA znajdziesz wiele modeli do wyboru, które można wykorzystać w każdej łazience – nawet tej niewielkiej. Umywalka mała, wcale nie musi oznaczać umywalkę niepraktyczną. Jeśli zdecydujesz się na umywalkę nablatową, możesz wykorzystać blat wokół niej jako miejsce na kosmetyki i akcesoria łazienkowe.' d='Modules.Sohoshopinspiracje.Shop'}

        </p>
        <p>
            {l s='Warto zadbać, by umywalka łazienkowa była dopasowana do metrażu pomieszczenia. Umywalka z szafką podumywalkową to sprytne rozwiązania dla właścicieli niewielkich łazienek. Dzięki nim zyskasz mnóstwo miejsca do przechowywania. Umywalka podwójna lepiej sprawdzi się w przestronnych łazienkach. Umywalki łazienkowe wytwarzane z solidnych materiałów posłużą ci przez wiele lat. W IKEA znajdziesz wiele modeli do wyboru, które można wykorzystać w każdej łazience – nawet tej niewielkiej. Umywalka mała, wcale nie musi oznaczać umywalkę niepraktyczną. Jeśli zdecydujesz się na umywalkę nablatową, możesz wykorzystać blat wokół niej jako miejsce na kosmetyki i akcesoria łazienkowe.' d='Modules.Sohoshopinspiracje.Shop'}

        </p>
    </div>
    <!-- szablon posta na potrzeby js -->
    <script>
        var postTemplateInspiracje = `<li class="col-xs-12 col-sm-6 col-lg-4">
        <a class="ybc_item_img" href="%view_url%"><img
                    src="{$urls.img_ps_url}ybc_blog/post/thumb/%thumb%" alt="%title%"
                    title="Trend dwa"></a>
        <div class="ybc-blog-latest-post-content">
            <a class="ybc_title_block" href="%view_url%">%title%</a>
            <a class="btn btn-primary" href="%view_url%">
                {l s='więcej' d='Modules.Sohoshopinspiracje.Shop'}
                <svg class="blog_arrow" xmlns="http://www.w3.org/2000/svg" width="16" height="9.5"
                     viewBox="0 0 12.262 7.192">
                    <defs></defs>
                    <g transform="translate(0.53 0.53)">
                        <path d="M-317.515-44.61l-5.6,5.6-5.6-5.6"
                              transform="translate(328.717 44.61)"></path>
                    </g>
                </svg>
            </a>
        </div>
    </li>`
    </script>
{/block}