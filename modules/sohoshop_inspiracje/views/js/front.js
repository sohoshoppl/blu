$(document).ready(function () {
    var swiperI = new Swiper(".sliderInspiracje", {
        slidesPerView: 1,
        centeredSlides: false,
        spaceBetween: 10,
        loop: false,
        scrollbar: {
            el: ".swiper-scrollbar",
        },
        pagination: {
            el: ".swiper-pagination",
            clickable: true,
        },
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
        },
        breakpoints: {
            // when window width is >= 992px
            992: {
                slidesPerView: 1.5,
                centeredSlides: true,
                spaceBetween: 10,
                loop: true,
            },
        },
    });

    var swiperIF = new Swiper(".sliderInspiracjeFind", {
        slidesPerView: 3.5,
        spaceBetween: 20,
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
        },
        scrollbar: {
            el: ".swiper-scrollbar",
        },
        breakpoints: {
            // when window width is >= 992px
            992: {
                slidesPerView: 4.3,
                spaceBetween: 20,
            },
        },
    });
});


$(document).on('click','.js-getMoreInspiracje', function(e){
    let thiz = $(this);
    let page = thiz.data('page');
    page = page + 1;
    thiz.data('page',page);
    $.get( window.location.href, { ajaxPage: 1, page: page } ).done(function( data ) {
        console.log(data);
        let total = data.limit * page;
        $('#stores-count-text-total').text(total);
        for (let i = 0; i < data.posts.length; i++) {
            post = postTemplateInspiracje.replaceAll('%title%', data.posts[i].title)
            post = post.replaceAll('%view_url%', data.posts[i].view_url)
            post = post.replaceAll('%thumb%', data.posts[i].thumb)


            $('#inspiracje-posts ul').append(post);
        }

        if(total >= data.totalRecords){
            thiz.fadeOut();
        }

    });

});