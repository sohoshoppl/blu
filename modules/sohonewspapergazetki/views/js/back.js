$( document ).ready(function() {

    var galleryItems = $('.delete-gallery');
    var dataUrl = window.location.origin+'/modules/sohonewspapergazetki/ajax.php';


    galleryItems.on('click', function() {
        var myObj = {};
        myObj["token"] = $(this).data( "token" );
        myObj["id_sohonewspapergazetki"] = $(this).data( "id_sohonewspapergazetki" );
        deleteHTml = $(this).parent();
        $.ajax({
            type: 'POST',
            url: dataUrl,
            data: myObj,
            success : function (data) {
                deleteHTml.remove();
            },
            error : function (data){
                console.log(data);
            }

        });

    });


    var imageClick = $('.sohonewspapergazetki img');
    imageClick.on('click', function(e) {
        e.preventDefault();
        if (e.target.localName == "img") {
            var img_width = $(this).width();
            var img_height = $(this).height();
            // ukrywa wszsytkei inputy na obrazku
            $('.sohonewspapergazetki .marker-input').hide();

            // obliczanie procentowej pozycji markera
            var x_pos = ((100 * e.offsetX) / img_width);
            var y_pos = ((100 * e.offsetY - 2000) / img_height);
            var fullPath = $(this).attr('src');
            var filename = fullPath.replace(/^.*[\\\/]/, '')


            addMarkerProduct(x_pos.toFixed(4), y_pos.toFixed(4), Date.now(), 0, filename);
        }

    });

    /**
     *
     * @param xpos - marker position top
     * @param ypos - marker position left
     * @param item - id marker
     * @param id_product - produkt przypisany do markera
     * @param img_src - img identyfikator, po tym na froncie rozpoznajemy na którym zdjeciu nałożyć ten marker
     */

    function addMarkerProduct(xpos, ypos, item, id_product=0, img_src) {
            var marker_content = '';
            marker_content += '<div style="left:' + xpos + '%;top:' + ypos + '%;" class="imgmc marker" id="marker-' + item + '">';
                marker_content += '<div class="imgmc marker-input">';
                    marker_content += '<input id="marker-' + item + '" placeholder="Wyszukaj produkt" class="imgmc form-control marker_pop" type="text" name="marker_query" autocomplete="off"/>';
                    marker_content += '<input class="js-marker-product" id="marker-product-' + item + '" type="hidden" name="marker-product-' + item + '" value="'+id_product+'" />';
                    marker_content += '<input id="marker-left-' + item + '" type="hidden" name="marker-left-' + item + '" value="'+xpos+'" />';
                    marker_content += '<input id="marker-top-' + item + '" type="hidden" name="marker-top-' + item + '" value="'+ypos+'" />';
                    marker_content += '<input id="marker-img-' + item + '" type="hidden" name="marker-img-' + item + '" value="'+img_src+'" />';
                    marker_content += '<input type="hidden" name="marker_ids[]" value="' + item + '" />';
                    marker_content += '<a href="#drop_marker" class="imgmc drop_marker">Usuń</a>';
                marker_content += '</div>';
            marker_content += '</div>';

            //https://stackoverflow.com/a/24740738/2769097
            $('img[src$="'+img_src+'"]').parent().append(marker_content);

            $('.marker_pop').autocomplete(autocomplete_gazetki_url, {
                minChars: 1,
                autoFill: true,
                max: 20,
                matchContains: true,
                mustMatch: false,
                scroll: false,
                cacheLength: 0,
                formatItem: function (item) {
                    return item[1] + ' - ' + item[0];
                },

            }).result(function (event, data, formatted) {
                $('.marker_pop').val('');
                $(this).val(data[0]);
                $(this).parent().find('.js-marker-product').val(data[1]);
                $('#head_image .marker-input').hide();

            });

        }

    // usuwanie markera
    $(document).on('click', 'form.sohonewspapergazetki .drop_marker', function (e) {
        e.preventDefault();

        $(this).parent().parent().remove();
    });


    // wyswietlanie daneych markera
    $(document).on('click', '.marker', function (e) {
        e.preventDefault();

        $('.marker-input').hide();
        $(this).find('.marker-input').show();
    });

    initMarkers();

    function initMarkers(){

        if(typeof imageMarkersJSON !== 'undefined'){
            var i;
            for (i = 0; i < imageMarkersJSON.length; i++) {
                addMarkerProduct(imageMarkersJSON[i].pos_left, imageMarkersJSON[i].pos_top, imageMarkersJSON[i].id_marker, imageMarkersJSON[i].id_product, imageMarkersJSON[i].img_src)
            }
            $('.marker-input').hide();
        }


    }
});

