{extends file="page.tpl"}
{block name="content"}
    <header id="porady-header">
        <h1>{l s='Porady' d='Modules.Sohoshopinspiracje.Shop'}</h1>
        <div class="sneakPeak">
            <p>{l s='Przygotowaliśmy dla Ciebie serię porad dotyczących aranżacji wnętrz Twojego domu' d='Modules.Sohoshopinspiracje.Shop'}
                .</p>
            <p>{l s='Znajdziesz tutaj szereg podpowiedzi technicznych oraz informacje pomagające podjąć' d='Modules.Sohoshopinspiracje.Shop'}</p>
            <p>{l s='devyzję o zakupie i porady eksploatacyjne' d='Modules.Sohoshopinspiracje.Shop'}.</p>
        </div>
    </header>
    {hook h='BlogSearchBlock' linkcurrent=$moduleLink}
    <div id="porady-find">
        <div class="swiper-container sliderPoradyFind">
            <div class="swiper-wrapper">
                {foreach from=$sub_categogires item=sub_category name=sub_categories}
                    <div class="swiper-slide">
                        <a href="{$sub_category.link}">
                            <img height="250" src="{$urls.img_ps_url}ybc_blog/category/thumb/{$sub_category.thumb}">
                            <label>
                                {$sub_category.title}
                            </label>
                        </a>
                    </div>
                {/foreach}
            </div>
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
            <!-- If we need scrollbar -->
            <div class="swiper-scrollbar"></div>
        </div>

    </div>
    {*
    <div id="search_widget" class="search-widget" data-search-controller-url="//blu.dkonto.pl/szukaj">
        <form method="get" action="//blu.dkonto.pl/szukaj">
            <input type="hidden" name="controller" value="search">
            <span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span><input type="text" name="s" value="" placeholder="Szukaj produktów..." aria-label="Szukaj" data-historytext="Twoja historia wyszukiwania" data-clearntext="wyczyść" data-productstext="Produkty:" data-categorystext="Kategorie:" data-categorysresults="Pokaż wyniki w:" data-resultstext="Pokaż wyniki w:" data-shopurl="https://blu.dkonto.pl/" class="ui-autocomplete-input" autocomplete="off">
            <span class="material-icons delete-search-icon hidden-index">
			close
		</span>
            <button type="submit">
                <i class="material-icons search"></i>
                <span class="hidden-xl-down">Szukaj</span>
            </button>
        </form>
    </div>
    *}
    <div id="porady-tags" class="clearfix">
        <label>{l s='Wybierz po tagach:' d='Modules.Sohoshopinspiracje.Shop'}</label>
        <div>
            {foreach from=$tags item=$tag name=tags}
                <button onclick="location.href='{url entity='module' name='sohoshop_inspiracje' controller='inspiracje' params=['filter' => $tag.tag|lower]}'"
                        type="button">{$tag.tag}</button>
            {/foreach}
        </div>
    </div>
    <div id="porady-posts" class="row">
        <ul>
            {foreach from=$posts item=$post name=posts}
                {if $smarty.foreach.posts.first}
                    <li class="col-xs-12 first-post">
                        <article>
                            <a class="ybc_item_img" href="{$post.view_url}">
                                <img src="{$urls.img_ps_url}ybc_blog/post/{$post.image}" alt="{$post.title}">
                            </a>
                            <div class="ybc-blog-latest-post-content">
                                <a class="ybc_title_block" href="{$post.view_url}">{$post.title}</a>
                                <a class="ybc_shortdesc_block" href="{$post.view_url}">
                                    <span>
                                     {$post.short_description|truncate:500:'...'|escape:'htmlall':'UTF-8' nofilter}
                                    </span>
                                </a>
                                <br>
                                <a class="btn btn-primary"
                                   href="{$post.view_url}">{l s='WIĘCEJ' d='Modules.Sohoshopinspiracje.Shop'} 
                                   <svg class="blog_arrow" xmlns="http://www.w3.org/2000/svg" width="16" height="9.5" viewBox="0 0 12.262 7.192">
                                        <defs></defs>
                                        <g transform="translate(0.53 0.53)">
                                            <path d="M-317.515-44.61l-5.6,5.6-5.6-5.6" transform="translate(328.717 44.61)"></path>
                                        </g>
                                    </svg>
                                </a>
                            </div>
                        </article>
                    </li>
                {elseif $smarty.foreach.posts.index % 7 == 0}
                    <li class="col-xs-12 beetwen-post">
                        <article>
                            <a class="ybc_item_img" href="{$post.view_url}">
                                <img src="{$urls.img_ps_url}ybc_blog/post/{$post.image}" alt="{$post.title}">
                            </a>
                            <div class="ybc-blog-latest-post-content">
                                <a class="ybc_title_block" href="{$post.view_url}">{$post.title}</a>
                                <a class="ybc_shortdesc_block" href="{$post.view_url}">{$post.short_description|truncate:230:'...'|escape:'htmlall':'UTF-8' nofilter}</a>
                                <br>
                                <a class="btn btn-primary"
                                   href="{$post.view_url}">{l s='WIĘCEJ' d='Modules.Sohoshopinspiracje.Shop'} 
                                   <svg class="blog_arrow" xmlns="http://www.w3.org/2000/svg" width="16" height="9.5" viewBox="0 0 12.262 7.192">
                                        <defs></defs>
                                        <g transform="translate(0.53 0.53)">
                                            <path d="M-317.515-44.61l-5.6,5.6-5.6-5.6" transform="translate(328.717 44.61)"></path>
                                        </g>
                                    </svg>
                                   </a>
                            </div>
                        </article>
                    </li>
                {else}
                    <li class="col-xs-12 col-sm-6 col-lg-4">
                        <a class="ybc_item_img" href="{$post.view_url}"><img
                                    src="{$urls.img_ps_url}ybc_blog/post/thumb/{$post.thumb}" alt="{$post.title}"
                                    title="Trend dwa"></a>
                        <div class="ybc-blog-latest-post-content">
                            <a class="ybc_title_block" href="{$post.view_url}">{$post.title}</a>
                            <a class="ybc_shortdesc_block" href="{$post.view_url}">{$post.short_description|truncate:230:'...'|escape:'htmlall':'UTF-8' nofilter}</a>
                            <br>
                            <a class="btn btn-primary"
                               href="{$post.view_url}">{l s='WIĘCEJ' d='Modules.Sohoshopinspiracje.Shop'} 
                               <svg class="blog_arrow" xmlns="http://www.w3.org/2000/svg" width="16" height="9.5" viewBox="0 0 12.262 7.192">
                                    <defs></defs>
                                    <g transform="translate(0.53 0.53)">
                                        <path d="M-317.515-44.61l-5.6,5.6-5.6-5.6" transform="translate(328.717 44.61)"></path>
                                    </g>
                                </svg>
                               </a>
                        </div>
                    </li>
                {/if}

            {/foreach}

        </ul>
        <div class="stores-footer">
            <p class="stores-count-text">
                <span id="stores-count-text-total">{$total}</span> z {$totalRecords} {l s='artykułów' d='Modules.Sohoshopinspiracje.Shop'}
            </p>
            <button class="stores-button pagination js-getMorePorady" data-page="{$p}"" >
                Pokaż więcej
                <svg class="fs-locator-search-ico" xmlns="http://www.w3.org/2000/svg" width="14.262" height="9.192"
                     viewBox="0 0 12.262 7.192">
                    <defs></defs>
                    <g transform="translate(0.53 0.53)">
                        <path d="M-317.515-44.61l-5.6,5.6-5.6-5.6" transform="translate(328.717 44.61)"></path>
                    </g>
                </svg>
            </button>
        </div>
    </div>
    <div id="store-page-describe">
        <h3 class="store-describe-header">{l s='Umywalki SEO' d='Modules.Sohoshopinspiracje.Shop'}</h3>
        <p>
            {l s='Dobra umywalka do łazienki powinna być trwała i łatwa w utrzymaniu czystości. W IKEA znajdziesz wiele modeli
            umywalek, które możesz dopasować do niemal każdego wnętrza. Na wszystkie umywalki ceramiczne oraz wykonane z
            kruszonego marmuru (w tym umywalki nablatowe) oferujemy 10 lat bezpłatnej gwarancji.' d='Modules.Sohoshopinspiracje.Shop'}
        </p>
        <p>
            <span>{l s='Umywalki łazienkowe' d='Modules.Sohoshopinspiracje.Shop'}</span><br>
            {l s='Warto zadbać, by umywalka łazienkowa była dopasowana do metrażu pomieszczenia. Umywalka z szafką
            podumywalkową to sprytne rozwiązania dla właścicieli niewielkich łazienek. Dzięki nim zyskasz mnóstwo
            miejsca do przechowywania. Umywalka podwójna lepiej sprawdzi się w przestronnych łazienkach.' d='Modules.Sohoshopinspiracje.Shop'}
            
        </p>

        <p>
            <span>{l s='Umywalka do łazienki' d='Modules.Sohoshopinspiracje.Shop'}</span><br>
            {l s='Umywalki łazienkowe wytwarzane z solidnych materiałów posłużą ci przez wiele lat. W IKEA znajdziesz wiele
            modeli do wyboru, które można wykorzystać w każdej łazience – nawet tej niewielkiej. Umywalka mała, wcale
            nie musi oznaczać umywalkę niepraktyczną. Jeśli zdecydujesz się na umywalkę nablatową, możesz wykorzystać
            blat wokół niej jako miejsce na kosmetyki i akcesoria łazienkowe.' d='Modules.Sohoshopinspiracje.Shop'}
            
        </p>
        <p>
            {l s='Warto zadbać, by umywalka łazienkowa była dopasowana do metrażu pomieszczenia. Umywalka z szafką
            podumywalkową to sprytne rozwiązania dla właścicieli niewielkich łazienek. Dzięki nim zyskasz mnóstwo
            miejsca do przechowywania. Umywalka podwójna lepiej sprawdzi się w przestronnych łazienkach. Umywalki
            łazienkowe wytwarzane z solidnych materiałów posłużą ci przez wiele lat. W IKEA znajdziesz wiele modeli do
            wyboru, które można wykorzystać w każdej łazience – nawet tej niewielkiej. Umywalka mała, wcale nie musi
            oznaczać umywalkę niepraktyczną. Jeśli zdecydujesz się na umywalkę nablatową, możesz wykorzystać blat wokół
            niej jako miejsce na kosmetyki i akcesoria łazienkowe.' d='Modules.Sohoshopinspiracje.Shop'}
        </p>
    </div>

    <!-- szablon posta na potrzeby js -->
    <script>
        var postTemplatePorady = `                    <li class="col-xs-12 col-sm-6 col-lg-4">
                        <a class="ybc_item_img" href="%view_url%"><img
                                    src="{$urls.img_ps_url}ybc_blog/post/thumb/%thumb%" alt="%title%"
                                    title="Trend dwa"></a>
                        <div class="ybc-blog-latest-post-content">
                            <a class="ybc_title_block" href="%view_url%">%title%</a>
                            <a class="ybc_shortdesc_block" href="%view_url}">%short_description%</a>
                            <br>
                            <a class="btn btn-primary"
                               href="%view_url%">{l s='WIĘCEJ' d='Modules.Sohoshopinspiracje.Shop'}
                               <svg class="blog_arrow" xmlns="http://www.w3.org/2000/svg" width="16" height="9.5" viewBox="0 0 12.262 7.192">
                                    <defs></defs>
                                    <g transform="translate(0.53 0.53)">
                                        <path d="M-317.515-44.61l-5.6,5.6-5.6-5.6" transform="translate(328.717 44.61)"></path>
                                    </g>
                                </svg>
                               </a>
                        </div>
                    </li>`
    </script>
{/block}