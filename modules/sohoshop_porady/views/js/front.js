var swiperIF = new Swiper(".sliderPoradyFind", {
    slidesPerView: 3.5,
    spaceBetween: 20,
    navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
    },
    scrollbar: {
        el: ".swiper-scrollbar",
    },
    breakpoints: {
        // when window width is >= 992px
        992: {
            slidesPerView: 4.3,
            spaceBetween: 20,
        },
    },
});

$(document).on('click','.js-getMorePorady', function(e){
    let thiz = $(this);
    let page = thiz.data('page');
    page = page + 1;
    thiz.data('page',page);
    $.get( "/porady", { ajaxPage: 1, page: page } ).done(function( data ) {
        console.log(data);
        let total = data.limit * page;
        $('#stores-count-text-total').text(total);
        for (let i = 0; i < data.posts.length; i++) {
            post = postTemplatePorady.replaceAll('%title%', data.posts[i].title)
            post = post.replaceAll('%view_url%', data.posts[i].view_url)
            post = post.replaceAll('%thumb%', data.posts[i].thumb)
            post = post.replaceAll('%short_description%', data.posts[i].short_description)



            $('#porady-posts ul').append(post);
        }

        if(total >= data.totalRecords){
            thiz.fadeOut();
        }

    });

});