<?php
if (!defined('_PS_VERSION_')) {
    exit;
}

function upgrade_module_1_1_0($object)
{
    $object->registerHook('displayBackOfficeCategory');
    $object->registerHook('actionCategoryUpdate');

    if (Db::getInstance()->ExecuteS('SHOW COLUMNS FROM ' . _DB_PREFIX_ . 'category LIKE "showSubPage"') == false) {
        Db::getInstance()->execute("ALTER TABLE  `". _DB_PREFIX_ ."category` ADD  `showSubPage` TINYINT(1) UNSIGNED NOT NULL DEFAULT  '0' ;");
    }

    return true;
}