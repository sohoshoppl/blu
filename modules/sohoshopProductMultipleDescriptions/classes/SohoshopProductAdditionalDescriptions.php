<?php
/*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class SohoshopProductAdditionalDescriptions extends ObjectModel
{
    /** @var int $id_product Product ID */
    public $id_product;

    /** @var string product additional description 1 */
    public $description_1;

    /** @var string  product additional description 1 */
    public $description_2;

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'sohoshopProductAdditionalDescriptions',
        'primary' => 'id_sohoshopProductAdditionalDescriptions',
        'fields' => array(
            'id_product' => array('type' => self::TYPE_INT, 'validate' => 'isInt', 'required' => true),
            'description_1' => array('type' => self::TYPE_HTML, 'validate' => 'isCleanHtml'),
            'description_2' => array('type' => self::TYPE_HTML, 'validate' => 'isCleanHtml'),
        ),
    );


    public static function delProductAdditionalDescriptions($id_product)
    {
        return Db::getInstance()->delete(self::$definition['table'], 'id_product = ' . (int)$id_product);
    }

    public static function getProductAdditionalDescriptions($id_product)
    {
        $sql = "SELECT `description_1`,`description_2` FROM `"._DB_PREFIX_.self::$definition['table']."` WHERE `id_product` =" . (int)$id_product;
        return Db::getInstance()->getRow($sql);
    }
}
