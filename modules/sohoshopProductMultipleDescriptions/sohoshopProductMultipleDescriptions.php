<?php

/**
 * 2007-2020 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2020 PrestaShop SA
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */
if (!defined('_PS_VERSION_')) {
    exit;
}

class SohoshopProductMultipleDescriptions extends Module
{

    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'sohoshopProductMultipleDescriptions';
        $this->tab = 'administration';
        $this->version = '1.1.0';
        $this->author = 'SohoSHOP.pl - MarcinJ';
        $this->need_instance = 0;

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Sohoshop - Product Multiple Descriptions');
        $this->description = $this->l('Pozwala na tworzenie wiele opisów produktu.');

        $this->confirmUninstall = $this->l('');

        $this->ps_versions_compliancy = array('min' => '1.7', 'max' => _PS_VERSION_);
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {

        include(dirname(__FILE__) . '/sql/install.php');

        return parent::install() &&
            $this->registerHook('backOfficeHeader') &&
            $this->registerHook('actionAdminProductsControllerSaveAfter') &&
            $this->registerHook('actionAdminProductsControllerDeleteAfter') &&
            $this->registerHook('displayProductAdditionalInfo') &&
            $this->registerHook('displayFooterProduct') &&
            $this->registerHook('displayAdminProductsMainStepLeftColumnMiddle');
            $this->registerHook('displayBackOfficeCategory');
            $this->registerHook('actionCategoryUpdate');


    }

    public function uninstall()
    {

        include(dirname(__FILE__) . '/sql/uninstall.php');

        return parent::uninstall();
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        /**
         * If values have been submitted in the form, process.
         */
        if (((bool)Tools::isSubmit('submitSohoshopProductMultipleDescriptionsModule')) == true) {
            $this->postProcess();
        }

        $this->context->smarty->assign('module_dir', $this->_path);

        $output = $this->context->smarty->fetch($this->local_path . 'views/templates/admin/configure.tpl');

        return $output;
    }

    /**
     * Add the CSS & JavaScript files you want to be loaded in the BO.
     */
    public function hookBackOfficeHeader($params)
    {
        if (Tools::getValue('module_name') == $this->name || Tools::getValue('id_product', false)) {
            $this->context->controller->addJS($this->_path . 'views/js/back.js');
            $this->context->controller->addCSS($this->_path . 'views/css/back.css');
        }
    }

    public function hookActionAdminProductsControllerSaveAfter()
    {
        require_once(dirname(__FILE__) . '/classes/SohoshopProductAdditionalDescriptions.php');

        $res = true;
        $description_1 = Tools::getValue('form_additional_descriptions_1', '');
        $description_2 = Tools::getValue('form_additional_descriptions_2', '');
        $name_1 = Tools::getValue('form_additional_name_1', '');
        $name_2 = Tools::getValue('form_additional_name_2', '');

        $id_product = Tools::getValue('id_product', 0);
        if ($id_product != 0 && (($description_1 != '') || ($description_2 != '') || ($name_1 != '') || ($name_2 != ''))) {
            $additionalProductDescriptions = new SohoshopProductAdditionalDescriptions();
            $additionalProductDescriptions->id_product = $id_product;
            $additionalProductDescriptions->description_1 = $description_1;
            $additionalProductDescriptions->description_2 = $description_2;
            $additionalProductDescriptions->name_1 = $name_1;
            $additionalProductDescriptions->name_2 = $name_2;
            $res = $res && SohoshopProductAdditionalDescriptions::delProductAdditionalDescriptions($id_product);
            $res = $res && $additionalProductDescriptions->add();
        }
        return $res;
    }

    public function hookActionAdminProductsControllerDeleteAfter()
    {
        require_once(dirname(__FILE__) . '/classes/SohoshopProductAdditionalDescriptions.php');

        return SohoshopProductAdditionalDescriptions::delProductAdditionalDescriptions(Tools::getValue('id_product'));
    }

    public function hookDisplayAdminProductsMainStepLeftColumnMiddle($params)
    {
        require_once(dirname(__FILE__) . '/classes/SohoshopProductAdditionalDescriptions.php');

        $this->context->smarty->assign('additionalDescriptions', SohoshopProductAdditionalDescriptions::getProductAdditionalDescriptions($params['id_product']));

        return $this->context->smarty->fetch($this->local_path . 'views/templates/hook/adminProductsMainStepLeftColumnMiddle.tpl');


    }

    public function hookdisplayProductAdditionalInfo($params)
    {
        require_once(dirname(__FILE__) . '/classes/SohoshopProductAdditionalDescriptions.php');
        $additionalDescriptions = SohoshopProductAdditionalDescriptions::getProductAdditionalDescriptions($params['product']['id_product']);
        if (isset($additionalDescriptions['description_1'])) {
            return '<p>' . $additionalDescriptions['description_1'] . '</p>';
        }
    }

    public function hookdisplayFooterProduct($params)
    {
        require_once(dirname(__FILE__) . '/classes/SohoshopProductAdditionalDescriptions.php');
        $additionalDescriptions = SohoshopProductAdditionalDescriptions::getProductAdditionalDescriptions($params['product']['id_product']);
        if (isset($additionalDescriptions['description_2']) && trim($additionalDescriptions['description_2'])) {
            return '<section id="SohoshopProductMultipleDescriptions"><div class="sohoshopProductMultipleDescriptionsContainer">' . $additionalDescriptions['description_2'] . '</div></section>';
        }
    }

    public function hookDisplayBackOfficeCategory($params){
        $id_category = Tools::getValue('id_category', 0);
        $sql = 'SELECT showSubPage FROM `'._DB_PREFIX_.'category` WHERE `id_category` =' .(int)$id_category;
        $showSubPage  =  Db::getInstance()->getValue($sql);

        $html = '
            <div class="form-group row">
                <label class="form-control-label ">Wyświetl kafle z podkategoriami</label>
                <div class="col-sm">
                    <div class="input-group">
                        <span class="ps-switch">
                            <input id="showSubPage_0" class="ps-switch" name="showSubPage" value="0" type="radio">
                            <label for="showSubPage_0">Nie</label>
                            <input id="showSubPage_1" class="ps-switch" name="showSubPage" value="1" '.($showSubPage? ' checked ' : '' ).'  type="radio">
                            <label for="showSubPage_1">Tak</label>
                            <span class="slide-button"></span>
                        </span>
                    </div>
                </div>
            </div>        
        ';


        return $html;
         //print_r($params);
         //die();
    }

    public function hookActionCategoryUpdate()
    {
        $showSubPage = Tools::getValue('showSubPage', 0);
        $id_category = Tools::getValue('id_category', 0);

        if ($id_category != 0){
            $sql = 'UPDATE `'._DB_PREFIX_.'category` SET `showSubPage` = '.(int)$showSubPage.' WHERE `id_category` = '.(int)$id_category;
            Db::getInstance()->execute($sql);
        }
        return true;
    }

}
