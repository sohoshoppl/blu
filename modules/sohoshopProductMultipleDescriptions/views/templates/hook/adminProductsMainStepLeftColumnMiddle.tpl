<section id="sohoshopProductMultipleDescriptions">
    <div class="form-row">
        <div class="form-group col-12">
            <label for="form_additional_name_1">Podtytuł</label>
            <input type="text" id="form_additional_name_1" name="form_additional_descriptions_1" class="form-control"
                   value="{if isset($additionalDescriptions.description_1)} {$additionalDescriptions.description_1}{/if}">
        </div>
    </div>
    <hr>
    <div class="form-row">
        <div class="form-group col-12">
            <label for="form_additional_name_2">HTML BOX</label>
            <textarea type="text" id="form_additional_name_2" name="form_additional_descriptions_2" class="autoload_rte">
                {if isset($additionalDescriptions.description_2)} {$additionalDescriptions.description_2}{/if}
            </textarea>
        </div>
    </div>
    <hr>
</section>
<style type="text/css">
    #sohoshopProductMultipleDescriptions .panel {
        border: 1px solid #bbcdd2;
    }
</style>