<?php
/*
 * SohoshopIndexation module
 *
 * @author    SohoSHOP
 * @copyright Copyright (c) 2020 SohoSHOP
 * @license   http://SohoSHOP.pl
 *
 * http://SohoSHOP.pl
*/

require_once(dirname(__FILE__).'/../../config/config.inc.php');
require_once(dirname(__FILE__).'/../../init.php');

$sohoshopindexation = Module::getInstanceByName('sohoshopindexation');
echo $sohoshopindexation->ajaxFilterValues();