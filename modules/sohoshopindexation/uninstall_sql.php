<?php
/**
 * SohoshopIndexation module
 *
 * @author    SohoSHOP
 * @copyright Copyright (c) 2020 SohoSHOP
 * @license   http://SohoSHOP.pl
 *
 * http://SohoSHOP.pl
 */

$sql = array();
$sql[] = 'DROP TABLE IF EXISTS  `' . _DB_PREFIX_ . 'sohoshopindexation_indexation`';
