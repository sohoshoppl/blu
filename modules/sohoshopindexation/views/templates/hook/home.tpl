{*
* 2015
*
* NOTICE OF LICENSE
*
*  @author Sosho.pl <biuro@sosho.pl>
*  @copyright  2014 Sosho.pl
*}
<div id="soshohomeindexation"">
    {foreach from=$selectedCategories item=selectedCategory name=selectedCategories}
        <section>
            <header>
                <h2 class="categoryName">{l s='Płytki' mod='sohoshopindexation'}</h2>
                <div class="sneakPeak">{l s='Kupuj według:' mod='sohoshopindexation'}</div>
            </header>

            <div class="clearfix swiper-container">
                <div class="swiper-wrapper">
                    {foreach from=$items item=item name=items}
                        <div class="hc_block swiper-slide">
                            <a class="image" href="{if $item.seo_redirect neq ""}{$item.category_url}{$item.seo_redirect}{else}{$item.seo_url}{/if}">
                                <img src="{$urls.img_ps_url}/sohoshopindexation/{$item.id_sohoshopindexation_indexation}.jpg"
                                    title="{$item.name}"
                                    alt="{$item.name}"
                                    width="304"
                                    height="295"
                                />
                            </a>
                            <div class="hc_block_title">
                                <h2>
                                    <a href="{if $item.seo_redirect neq ""}{$item.category_url}{$item.seo_redirect}{else}{$item.seo_url}{/if}">{$item.name}</a>
                                </h2>
                            </div>
                        </div>
                    {/foreach}
                </div>
                <div class="swiper-scrollbar"></div>
            </div>
            <a href="{$link->getCategoryLink($selectedCategory)}" class="btn">
                {l s='zobacz całą kolekcję' mod='sohoshopindexation'}
                <svg class="arrow-right" xmlns="http://www.w3.org/2000/svg" width="14.262" height="9.192" viewBox="0 0 12.262 7.192">
                                    <defs></defs>
                                    <g transform="translate(0.53 0.53)">
                                        <path d="M-317.515-44.61l-5.6,5.6-5.6-5.6" transform="translate(328.717 44.61)"></path>
                                    </g>
                </svg>
            </a>
        </section>
    {/foreach}


</div>