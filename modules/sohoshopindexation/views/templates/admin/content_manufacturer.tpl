{*
* SohoshopIndexation module
 *
 * @author    SohoSHOP
 * @copyright Copyright (c) 2020 SohoSHOP
 * @license   http://sohoshop.pl
 *
 * http://sohoshop.pl
*}

<div class="fixed-width-xxl search_input">
	<div class="input-group">
		<input class="fixed-width-xxl" type="text" />
		<span class="input-group-addon"><i class="icon-search"></i></span>
	</div>
</div>
<select class="form-control" name="filter[{$type}][values][]" multiple>
{foreach from=$values key=k item=val}
	{if $val.nbr > 0}<option value="{$k}"{if isset($filter_values) && $k|in_array:$filter_values.values} selected="selected"{/if}>{$val.name}</option>{/if}
{/foreach}
</select>