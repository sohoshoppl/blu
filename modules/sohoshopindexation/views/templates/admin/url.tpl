{*
* SohoshopIndexation module
 *
 * @author    SohoSHOP
 * @copyright Copyright (c) 2020 SohoSHOP
 * @license   http://sohoshop.pl
 *
 * http://sohoshop.pl
*}

{if isset($category_link)}
    <div class="col-lg-9">
        <input readonly type="text" id="seo_url" name="seo_url"
               value="{$category_link}{if $serializefilters != ''}_{$serializefilters}{/if}"/>

        <span class="switch prestashop-switch fixed-width-lg">
		<input class="url_input" type="radio" data-id="category_link_default" name="category_link_default"
               id="category_link_default_on" value="1"{if $seo_redirect != ''} checked="checked"{/if} />
		<label for="category_link_default_on">{l s='Własny' mod='sohoshopindexation'}</label>
		<input class="url_input" type="radio" data-id="category_link_default" name="category_link_default"
               id="category_link_default_off" value="0"{if !$seo_redirect} checked="checked"{/if} />
		<label for="category_link_default_off">{l s='Domyślny' mod='sohoshopindexation'}</label>
		<a class="slide-button btn"></a>
	</span>
        <div class="seo_redirect_block"{if !$seo_redirect} style="display:none"{/if}>
            <br/>
            <div class="input-group">
                <span class="input-group-addon">{$category_link}</span>
                <input type="text" id="seo_redirect" name="seo_redirect" value="{$seo_redirect}"/>
            </div>
        </div>
    </div>
{/if}