{*
* SohoshopIndexation module
 *
 * @author    SohoSHOP
 * @copyright Copyright (c) 2020 SohoSHOP
 * @license   http://sohoshop.pl
 *
 * http://sohoshop.pl
*}

<div class="col-lg-9{if $forurl} forurl{/if}">
	<span class="switch prestashop-switch fixed-width-lg">
		<input class="filter_input" type="radio" data-id="filter_{$filter.type}{if isset($filter.id_key) && $filter.id_key}_{$filter.id_key}{/if}" data-type="{$filter.type}" data-value="{if isset($filter.id_key) && $filter.id_key}{$filter.id_key}{/if}" name="filter[{$filter.type}{if isset($filter.id_key) && $filter.id_key}_{$filter.id_key}{/if}][active]" id="filter_{$filter.type}{if isset($filter.id_key) && $filter.id_key}_{$filter.id_key}{/if}_on" value="1"{if isset($filter_values)} checked="checked"{/if} />
		<label for="filter_{$filter.type}{if isset($filter.id_key) && $filter.id_key}_{$filter.id_key}{/if}_on">{l s='Tak' mod='sohoshopindexation'}</label>
		<input class="filter_input" type="radio" data-id="filter_{$filter.type}{if isset($filter.id_key) && $filter.id_key}_{$filter.id_key}{/if}" data-type="{$filter.type}" data-value="{if isset($filter.id_key) && $filter.id_key}{$filter.id_key}{/if}" name="filter[{$filter.type}{if isset($filter.id_key) && $filter.id_key}_{$filter.id_key}{/if}][active]" id="filter_{$filter.type}{if isset($filter.id_key) && $filter.id_key}_{$filter.id_key}{/if}_off" value="0"{if !isset($filter_values)} checked="checked"{/if} />
		<label for="filter_{$filter.type}{if isset($filter.id_key) && $filter.id_key}_{$filter.id_key}{/if}_off">{l s='Nie' mod='sohoshopindexation'}</label>
		<a class="slide-button btn"></a>
	</span>
	<div id="filter_{$filter.type}{if isset($filter.id_key) && $filter.id_key}_{$filter.id_key}{/if}" class="filter_content"{if !isset($filter_values)} style="display:none"{/if}>{if isset($filter_form[$filter_id])}{$filter_form[$filter_id] nofilter}{/if}</div>
</div>