{*
* SohoshopIndexation module
 *
 * @author    SohoSHOP
 * @copyright Copyright (c) 2020 SohoSHOP
 * @license   http://sohoshop.pl
 *
 * http://sohoshop.pl
*}

<div class="input-group fixed-width-xxl">
	<div class="input-group">
		<span class="input-group-addon">{l s='od' mod='sohoshopindexation'}</span>
		<input name="filter[{$type}][values][from]" class="fixed-width-xxl" type="text"{if isset($filter_values)} value="{$filter_values.values.from}"{/if} />
	</div>
</div>
<div class="input-group fixed-width-xxl">
	<div class="input-group">
		<span class="input-group-addon">{l s='do' mod='sohoshopindexation'}</span>
		<input name="filter[{$type}][values][to]" class="fixed-width-xxl" type="text"{if isset($filter_values)} value="{$filter_values.values.to}"{/if} />
	</div>
</div>