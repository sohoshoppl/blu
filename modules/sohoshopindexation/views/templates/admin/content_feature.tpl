{*
* SohoshopIndexation module
 *
 * @author    SohoSHOP
 * @copyright Copyright (c) 2020 SohoSHOP
 * @license   http://sohoshop.pl
 *
 * http://sohoshop.pl
*}

<div class="fixed-width-xxl search_input">
	<div class="input-group">
		<input class="fixed-width-xxl" type="text" />
		<span class="input-group-addon"><i class="icon-search"></i></span>
	</div>
</div>
{*<input name="filter[{$type}{if $value}_{$value}{/if}][names]" value="{foreach from=$values item=value}{if isset($filter_values) && $value.id_feature_value|in_array:$filter_values.values}{$value.value},{/if}{/foreach}" />*}
<select class="form-control" name="filter[{$type}{if $value}_{$value}{/if}][values][]" multiple>
{foreach from=$values key=k item=val}
	{if $val.nbr > 0}<option value="{$k}"{if isset($filter_values) && $k|in_array:$filter_values.values} selected="selected"{/if}>{$val.name}</option>{/if}
{/foreach}
</select>
