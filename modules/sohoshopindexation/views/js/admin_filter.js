/*
 * SohoshopIndexation module
 *
 * @author    SohoSHOP
 * @copyright Copyright (c) 2020 SohoSHOP
 * @license   http://sohoshop.pl
 *
 * http://sohoshop.pl
*/

$(document).ready(function(){
	
	// pobieranie wartosci dla filtrow
	$(document).on('change', '.filter_input', function(e){        
		// jesli wlaczamy filtr
        /*if ($(this).val() == 1) {
			var requestData = {
				'action': 'getFilterValues',
				'value': $(this).data('value'),
				'type': $(this).data('type'),
				'id': $(this).data('id'),
				'id_category_layered': module_id_category,
			};
			
			$.post(baseUrlHttps + '/modules/sohoshopindexation/ajax.php', requestData).then(function (data) {
				var dataArr = $.parseJSON(data);
				if (dataArr) {
					$('#' + dataArr.values.id).html(dataArr.content);
				}
			});
		}
		// jesli filtr jest wylaczany
		else {
			$(this).parent().parent().find('.filter_content').html('');
		}*/
		
		$(this).parent().parent().find('.filter_content').toggle();
    });
	
	// wyszukiwarka
	$(document).on('keyup', '.search_input input', function(e){
		var valThis = $(this).val().toLowerCase();
		
		$(this).parent().parent().parent().find('select > option').each(function(){
			var text = $(this).text().toLowerCase();
			(text.indexOf(valThis) >= 0) ? $(this).show() : $(this).hide();
		});
	});
	
	// odblokowanie edycji adresu
	$(document).on('change', '.url_input', function(e){
        $(this).parent().parent().find('.seo_redirect_block').toggle();
    });
    
    $(document).on('keyup', '#sohoshopindexation_indexation_form #name', function (e) {
        var valThis = $(this).val();
        $('#sohoshopindexation_indexation_form #meta_title').val(valThis);
        $('#sohoshopindexation_indexation_form #seo_title').val(valThis);
    });      
	
});