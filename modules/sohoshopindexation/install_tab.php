<?php
/**
 * SohoshopIndexation module
 *
 * @author    SohoSHOP
 * @copyright Copyright (c) 2020 SohoSHOP
 * @license   http://SohoSHOP.pl
 *
 * http://SohoSHOP.pl
 */
 
$tabs[] = array(
	'level'  =>  0,
	'tab_parent_class'   =>  '',
	'class_name'     =>  self::TAB_MODULE.'Indexation',
	'name'      =>  $this->trans('Strony', array(), 'Modules.SohoshopIndexation.Admin'),
);
?>