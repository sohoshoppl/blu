<?php
/**
 * SohoshopIndexation module
 *
 * @author    SohoSHOP
 * @copyright Copyright (c) 2020 SohoSHOP
 * @license   http://SohoSHOP.pl
 *
 * http://SohoSHOP.pl
 */

$sql = array();

$sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'sohoshopindexation_indexation` (
  `id_sohoshopindexation_indexation` INT(11) unsigned NOT NULL auto_increment,
  `id_shop` INT(11) NOT NULL,
  `name` VARCHAR(255) character set utf8 NOT NULL,
  `id_category` INT(11) NOT NULL,
  `filters` TEXT character set utf8 NOT NULL,
  `meta_title` VARCHAR(255) character set utf8 NOT NULL,
  `meta_description` TEXT character set utf8 NOT NULL,
  `seo_title` VARCHAR(255) character set utf8 NOT NULL,
  `seo_description` TEXT character set utf8 NOT NULL,
  `seo_url` TEXT character set utf8 NOT NULL,
  `seo_redirect` TEXT character set utf8 NOT NULL,
  `category_url` VARCHAR(500) character set utf8 NOT NULL,
  `active` INT(11) NOT NULL,
  `date_add` DATETIME NOT NULL,
  `date_upd` DATETIME NOT NULL,
  PRIMARY KEY (`id_sohoshopindexation_indexation`)
) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8';
