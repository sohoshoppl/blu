<?php
/**
 * SohoshopIndexation module
 *
 * @author    SohoSHOP
 * @copyright Copyright (c) 2020 SohoSHOP
 * @license   http://SohoSHOP.pl
 *
 * http://SohoSHOP.pl
 */
 
if (!defined('_PS_VERSION_')) {
    exit;
}

class SohoshopIndexationIndexation extends ObjectModel
{
    public $id_sohoshopindexation_indexation;
    public $id_shop;
    public $name;
    public $id_category;
    public $filters;
    public $meta_title;
    public $meta_description;
    public $seo_title;
    public $seo_description;
    public $seo_url;
    public $seo_redirect;
    public $category_url;
    public $active;
    public $date_add;
    public $date_upd;

    public static $definition = array(
        'table' => 'sohoshopindexation_indexation',
        'primary' => 'id_sohoshopindexation_indexation',
        'fields' => array(
            'id_sohoshopindexation_indexation'     	=> array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
            'id_shop'     							=> array('type' => self::TYPE_INT),
            'name' 									=> array('type' => self::TYPE_STRING, 'size' => 255, 'required' => true),
            'id_category' 							=> array('type' => self::TYPE_INT, 'required' => true),
            'filters' 						        => array('type' => self::TYPE_STRING),
            'meta_title' 						    => array('type' => self::TYPE_STRING, 'size' => 255),
            'meta_description' 						=> array('type' => self::TYPE_STRING),
            'seo_title' 						    => array('type' => self::TYPE_STRING, 'size' => 255),
            'seo_description' 						=> array('type' => self::TYPE_HTML, 'validate' => 'isCleanHtml'),
            'seo_url' 							    => array('type' => self::TYPE_STRING),
            'seo_redirect' 						    => array('type' => self::TYPE_STRING),
            'category_url' 						    => array('type' => self::TYPE_STRING),
            'active' 						    	=> array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'required' => true),
            'date_add' 								=> array('type' => self::TYPE_DATE),
            'date_upd' 								=> array('type' => self::TYPE_DATE),
        ),
    );

    public function __construct($id = null)
    {
        parent::__construct($id);
        $this->image_dir = _PS_IMG_DIR_ . 'sohoshopindexation/';
        $this->id_image = ($this->id && file_exists($this->image_dir . (int) $this->id . '.jpg')) ? (int) $this->id : false;
    }

	public static function getFiltersByCategory($id_category)
	{
		$results = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
			SELECT `id_sohoshopindexation_indexation`, `filters`
			FROM `'._DB_PREFIX_.'sohoshopindexation_indexation`
			WHERE `id_category` = '.(int)$id_category.'
			AND `active` = 1
		');
		
		return $results;
	}
	
	public static function checkCategoryURL($url)
	{
		$results = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('
			SELECT `seo_url`
			FROM `'._DB_PREFIX_.'sohoshopindexation_indexation`
			WHERE CONCAT(`category_url`,`seo_redirect`) = "'.$url.'"
			AND `active` = 1
		');
		
		return $results;
	}

}