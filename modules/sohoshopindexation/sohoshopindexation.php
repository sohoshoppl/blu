<?php
/**
 * SohoshopIndexation module
 *
 * @author    SohoSHOP
 * @copyright Copyright (c) 2020 SohoSHOP
 * @license   http://SohoSHOP.pl
 *
 * http://SohoSHOP.pl
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

require_once(dirname(__FILE__) . '/classes/SohoshopIndexationIndexation.php');
include_once(_PS_MODULE_DIR_ . 'ps_facetedsearch/ps_facetedsearch.php');

class SohoshopIndexation extends Module
{
    const MODULE_NAME = 'sohoshopindexation';
    const TAB_SOHOSHOP = 'sohoshopindexation';
    const NAME_TAB_SOHOSHOP = 'Sohoshop Indexation';
    const TAB_MODULE = 'AdminSohoshopIndexation';
    const NAME_TAB = 'Sohoshop Indexation';
    protected $_html = '';
    protected $_postErrors = array();
    protected $tabs;

    public function __construct()
    {
        $this->name = self::MODULE_NAME;
        $this->tab = 'sohoshopindexation';
        $this->version = '1.0.0';
        $this->author = 'SOHOshop.pl';
        $this->module_dir = dirname(__FILE__) . '/';
        $this->module_dir_images = '/modules/' . $this->name . '/images/';
        $this->need_upgrade = true;
        $this->secure_key = Tools::encrypt($this->name);
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->trans('Sohoshop Indexation', array(), 'Modules.SohoshopIndexation.Admin');
        $this->description = $this->trans('Indeksacja filtrów produktowych z wykorzystaniem filtrów kategorii modułu ps_facetedsearch.', array(), 'Modules.SohoshopIndexation.Admin');

        $shop_domain = Tools::getCurrentUrlProtocolPrefix() . Tools::getHttpHost();
        $url_modules = $shop_domain . __PS_BASE_URI__ . 'modules/';
        $physic_path_modules = realpath(_PS_ROOT_DIR_ . '/modules') . '/';
        $this->module_path = $url_modules . $this->name . '/';
        $this->module_path_physic = $physic_path_modules . $this->name . '/';
    }

    public function install()
    {
        if (
            !$this->createImgFolder() ||
            !parent::install() ||
            !$this->registerHook('displayHome') ||
            !$this->registerHook('header')
        )
            return false;

        $sql = array();
        require_once(dirname(__FILE__) . '/install_sql.php');
        foreach ($sql as $sq) {
            if (!Db::getInstance()->Execute($sq)) {
                return false;
            }
        }

        $this->installTabs();

        return true;
    }

    public function installTabs()
    {
        $langs = Language::getLanguages();
        $tab_lastid = array();

        $smarttab = new Tab();
        $smarttab->class_name = self::TAB_MODULE;
        $smarttab->module = '';
        $smarttab->id_parent = 0;
        foreach ($langs as $l) {
            $smarttab->name[$l['id_lang']] = $this->displayName;
        }
        $smarttab->save();

        $tab_id = $smarttab->id;

        $tabs[] = array(
            'level' => 0,
            'tab_parent_class' => '',
            'class_name' => self::TAB_MODULE . 'Indexation',
            'name' => $this->trans('Strony', array(), 'Modules.SohoshopIndexation.Admin'),
        );
        $this->tabs = $tabs;

        foreach ($this->tabs as $tab) {
            $newtab = new Tab();
            $newtab->class_name = $tab['class_name'];

            if (!$tab['level']) {
                $newtab->id_parent = $tab_id;
                $newtab->icon = 'extension';
            } else {
                $newtab->id_parent = $tab_lastid[$tab['tab_parent_class']];
            }

            $newtab->module = $this->name;

            foreach ($langs as $l) {
                $newtab->name[$l['id_lang']] = $this->l($tab['name']);
            }

            $newtab->save();

            if (!$tab['level']) {
                $tab_lastid[$tab['class_name']] = $newtab->id;
            }
        }

        return true;
    }

    public function uninstall()
    {
        $files = glob(_PS_IMG_DIR_ . 'sohoshopindexation/*'); // get all file names
        foreach ($files as $file) { // iterate files
            if (is_file($file)) {
                unlink($file); // delete file
            }
        }
        rmdir(_PS_IMG_DIR_ . 'sohoshopindexation');
        if (!parent::uninstall()
        )
            return false;

        $tabs[] = array(
            'level' => 0,
            'tab_parent_class' => '',
            'class_name' => self::TAB_MODULE . 'Indexation',
            'name' => $this->trans('Strony', array(), 'Modules.SohoshopIndexation.Admin'),
        );
        $this->tabs = $tabs;

        foreach ($this->tabs as $tab) {
            $this->uninstallTabs($tab['class_name']);
        }

        $sql = array();
        //require_once(dirname(__FILE__) . '/uninstall_sql.php');
        foreach ($sql as $s) :
            if (!Db::getInstance()->execute($s))
                return false;
        endforeach;

        return true;
    }

    public function uninstallTabs($class_name)
    {
        $id_tab = Tab::getIdFromClassName($class_name);

        if ($id_tab) {
            $tab = new Tab((int)$id_tab);
            $id_parent = $tab->id_parent;
            $parent_tab = new Tab((int)$id_parent);

            $tab->delete();

            if (Tab::getNbTabs($id_parent) <= 0 && $parent_tab->class_name == self::TAB_SOHOSHOP) {
                $tab_parent = new Tab((int)$id_parent);

                $tab_parent->delete();
            }
        }
    }

    public function getContent()
    {
        $this->_html .= '';

        if (Tools::isSubmit('settings')) {
            Configuration::updateValue('sohoshopindexation_homecategories', Tools::getValue('sohoshopindexation_homecategories'));
            $this->_html .= $this->displayConfirmation($this->l('Zapisano ustawienia dla modułu'));
        }


        $this->_html .= $this->getContentHeader();
        $this->_html .= $this->renderForm();

        return $this->_html;
    }

    private function renderForm()
    {
        $fields_form[] = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Ustawienia'),
                    'icon' => 'icon-cogs'
                ),
                'input' => array(
                    array(
                        'type' => 'text',
                        'label' => $this->l('Kategorie na stronie głównej'),
                        'name' => 'sohoshopindexation_homecategories',
                        'desc' => $this->l('ID kategori, oddzielonych przecinkiem')
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Zapisz ustawienia'),
                    'name' => $this->l('settings')
                )
            ),
        );

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $this->fields_form = array();

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false) . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        return $helper->generateForm($fields_form);
    }

    public function getConfigFieldsValues()
    {
        return array(
            'sohoshopindexation_homecategories' => Tools::getValue('sohoshopindexation_homecategories', Configuration::get('sohoshopindexation_homecategories'))
        );
    }

    private function getContentHeader()
    {
        $this->context->smarty->assign(array(
            'module_display_name' => $this->displayName,
            'module_dir' => $this->module_dir_images
        ));

        return $this->context->smarty->fetch($this->module_dir . 'views/templates/admin/content_header.tpl');
    }

    public function ajaxFilterValues()
    {
        global $smarty, $cookie;

        $id_lang = Configuration::get('PS_LANG_DEFAULT', null, null, Context::getContext()->shop->id);
        $get_value = (int)Tools::getValue('value');
        $get_type = Tools::getValue('type');
        $get_id = Tools::getValue('id');
        $data = array('id' => $get_id);
        $idShop = Context::getContext()->shop->id;

        $ps_facetedsearch = Module::getInstanceByName('ps_facetedsearch');

        echo '<pre>';
        print_r($ps_facetedsearch->getFilterBlock([], false));
        echo '</pre>';
        die();

        switch ($get_type) {
            case 'id_feature':
                $this->context->smarty->assign(array(
                    'values' => FeatureValue::getFeatureValuesWithLang($id_lang, $get_value),
                    'type' => $get_type,
                    'value' => $get_value,
                    'id' => $get_id,
                ));
                $content = $this->context->smarty->fetch('module:' . $this->name . '/views/templates/admin/content_feature.tpl');
                break;
            case 'id_attribute_group':
                $this->context->smarty->assign(array(
                    'values' => AttributeGroup::getAttributes($id_lang, $get_value),
                    'type' => $get_type,
                    'value' => $get_value,
                    'id' => $get_id,
                ));
                $content = $this->context->smarty->fetch('module:' . $this->name . '/views/templates/admin/content_attribute.tpl');
                break;
            case 'manufacturer':
                $this->context->smarty->assign(array(
                    'values' => Manufacturer::getLiteManufacturersList($id_lang),
                    'type' => $get_type,
                    'value' => $get_value,
                    'id' => $get_id,
                ));
                $content = $this->context->smarty->fetch('module:' . $this->name . '/views/templates/admin/content_manufacturer.tpl');
                break;
            case 'price':
                $this->context->smarty->assign(array(
                    'type' => $get_type,
                    'value' => $get_value,
                    'id' => $get_id,
                ));
                $content = $this->context->smarty->fetch('module:' . $this->name . '/views/templates/admin/content_price.tpl');
                break;
        }

        return Tools::jsonEncode(array('values' => $data, 'content' => $content));
    }


    public function getFiltersByCategory($id_category, $get_id = false)
    {
        $filters = array();
        foreach (SohoshopIndexationIndexation::getFiltersByCategory($id_category) as $filter) {
            if ($get_id)
                $filters[$filter['id_sohoshopindexation_indexation']] = Tools::jsonDecode($filter['filters'], true);
            else
                $filters[] = Tools::jsonDecode($filter['filters'], true);
        }
        return $filters;
    }

    public function getFilterObj($id_sohoshopindexation_indexation)
    {
        return new SohoshopIndexationIndexation((int)$id_sohoshopindexation_indexation);
    }

    public function setUrlFromFilters($filters, $id_lang)
    {
        $output = array();

        if (isset($filters) && $filters) {
            foreach ($filters as $type => $filter) {
                if ($type == 'price') {
                    foreach ($filter['values'] as $key => $val) {
                        $output['Cena-zł'][$val] = $val;
                    }
                } elseif ($type == 'manufacturer') {
                    foreach ($filter['values'] as $key => $val) {
                        $name = Manufacturer::getNameById($val);
                        $output['Marka'][$name] = $name;
                    }
                } elseif (strpos($type, 'id_feature') !== false) {
                    $idItem = (int)str_replace('id_feature_', '', $type);
                    $headName = $this->getFeatureName($idItem, $id_lang);

                    foreach ($filter['values'] as $key => $val) {
                        $name = '';
                        $valueLangs = FeatureValue::getFeatureValueLang($val);
                        foreach ($valueLangs as $valueLang) {
                            if ((int)$valueLang['id_lang'] == $id_lang)
                                $name = $valueLang['value'];
                        }
                        $output[$headName][$name] = $name;
                    }
                } elseif (strpos($type, 'id_attribute_group') !== false) {
                    $idItem = (int)str_replace('id_attribute_group_', '', $type);
                    $headName = $this->getAttributeName($idItem, $id_lang);

                    foreach ($filter['values'] as $key => $val) {
                        $name = $this->getAttributeValueName($val, $id_lang);
                        $output[$headName][$name] = $name;
                    }
                }
            }
        }

        return $output;
    }

    public function getFeatureName($id_value, $id_lang)
    {
        return Db::getInstance()->getValue('
			SELECT `name`
			FROM ' . _DB_PREFIX_ . 'feature_lang 
			WHERE id_feature = ' . (int)$id_value . '
			AND `id_lang` = ' . (int)$id_lang . '
        ');
    }

    public function getAttributeName($id_value, $id_lang)
    {
        return Db::getInstance()->getValue('
			SELECT `public_name`
			FROM ' . _DB_PREFIX_ . 'attribute_group_lang 
			WHERE id_attribute_group = ' . (int)$id_value . '
			AND `id_lang` = ' . (int)$id_lang . '
        ');
    }

    public function getAttributeValueName($id_value, $id_lang)
    {
        return Db::getInstance()->getValue('
			SELECT `name`
			FROM ' . _DB_PREFIX_ . 'attribute_lang 
			WHERE id_attribute = ' . (int)$id_value . '
			AND `id_lang` = ' . (int)$id_lang . '
        ');
    }

    public function getFilterBlock($id_parent)
    {
        global $cookie;
        static $cache = null;

        $context = Context::getContext();
        $selected_filters = array();

        $id_lang = $context->language->id;
        $currency = $context->currency;
        $id_shop = (int)$context->shop->id;
        $alias = 'product_shop';

        if (is_array($cache))
            return $cache;

        $home_category = Configuration::get('PS_HOME_CATEGORY');
        if ($id_parent == $home_category)
            return;

        $parent = new Category((int)$id_parent, $id_lang);

        /* Get the filters for the current category */
        $filters = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
			SELECT type, id_value, filter_show_limit, filter_type FROM ' . _DB_PREFIX_ . 'layered_category
			WHERE id_category = ' . (int)$id_parent . '
				AND id_shop = ' . $id_shop . '
			GROUP BY `type`, id_value ORDER BY position ASC'
        );

        /* Create the table which contains all the id_product in a cat or a tree */

        Db::getInstance()->execute('DROP TEMPORARY TABLE IF EXISTS ' . _DB_PREFIX_ . 'cat_restriction', false);
        Db::getInstance()->execute('CREATE TEMPORARY TABLE ' . _DB_PREFIX_ . 'cat_restriction ENGINE=MEMORY
													SELECT DISTINCT cp.id_product, p.id_manufacturer, product_shop.condition, p.weight FROM ' . _DB_PREFIX_ . 'category c
													STRAIGHT_JOIN ' . _DB_PREFIX_ . 'category_product cp ON (c.id_category = cp.id_category AND
													' . (Configuration::get('PS_LAYERED_FULL_TREE') ? 'c.nleft >= ' . (int)$parent->nleft . '
													AND c.nright <= ' . (int)$parent->nright : 'c.id_category = ' . (int)$id_parent) . '
													AND c.active = 1)
													STRAIGHT_JOIN ' . _DB_PREFIX_ . 'product_shop product_shop ON (product_shop.id_product = cp.id_product
													AND product_shop.id_shop = ' . (int)$context->shop->id . ')
													STRAIGHT_JOIN ' . _DB_PREFIX_ . 'product p ON (p.id_product=cp.id_product)
													WHERE product_shop.`active` = 1 AND product_shop.`visibility` IN ("both", "catalog")', false);


        Db::getInstance()->execute('ALTER TABLE ' . _DB_PREFIX_ . 'cat_restriction ADD PRIMARY KEY (id_product),
													ADD KEY `id_manufacturer` (`id_manufacturer`,`id_product`) USING BTREE,
													ADD KEY `condition` (`condition`,`id_product`) USING BTREE,
													ADD KEY `weight` (`weight`,`id_product`) USING BTREE', false);

        // Remove all empty selected filters
        foreach ($selected_filters as $key => $value)
            switch ($key) {
                case 'price':
                case 'weight':
                    if ($value[0] === '' && $value[1] === '')
                        unset($selected_filters[$key]);
                    break;
                default:
                    if ($value == '')
                        unset($selected_filters[$key]);
                    break;
            }

        $filter_blocks = array();
        foreach ($filters as $filter) {
            $sql_query = array('select' => '', 'from' => '', 'join' => '', 'where' => '', 'group' => '', 'second_query' => '');
            switch ($filter['type']) {
                case 'price':
                    $sql_query['select'] = 'SELECT p.`id_product`, psi.price_min, psi.price_max ';
                    // price slider is not filter dependent
                    $sql_query['from'] = '
					FROM ' . _DB_PREFIX_ . 'cat_restriction p';
                    $sql_query['join'] = 'INNER JOIN `' . _DB_PREFIX_ . 'layered_price_index` psi
								ON (psi.id_product = p.id_product AND psi.id_currency = ' . (int)$context->currency->id . ' AND psi.id_shop=' . (int)$context->shop->id . ')';
                    $sql_query['where'] = 'WHERE 1';
                    break;
                case 'weight':
                    $sql_query['select'] = 'SELECT p.`id_product`, p.`weight` ';
                    // price slider is not filter dependent
                    $sql_query['from'] = '
					FROM ' . _DB_PREFIX_ . 'cat_restriction p';
                    $sql_query['where'] = 'WHERE 1';
                    break;
                case 'condition':
                    $sql_query['select'] = 'SELECT p.`id_product`, product_shop.`condition` ';
                    $sql_query['from'] = '
					FROM ' . _DB_PREFIX_ . 'cat_restriction p';
                    $sql_query['where'] = 'WHERE 1';
                    $sql_query['from'] .= Shop::addSqlAssociation('product', 'p');
                    break;
                case 'quantity':
                    $sql_query['select'] = 'SELECT p.`id_product`, sa.`quantity`, sa.`out_of_stock` ';

                    $sql_query['from'] = '
					FROM ' . _DB_PREFIX_ . 'cat_restriction p';

                    $sql_query['join'] .= 'LEFT JOIN `' . _DB_PREFIX_ . 'stock_available` sa
						ON (sa.id_product = p.id_product AND sa.id_product_attribute=0 ' . StockAvailable::addSqlShopRestriction(null, null, 'sa') . ') ';
                    $sql_query['where'] = 'WHERE 1';
                    break;

                case 'manufacturer':
                    $sql_query['select'] = 'SELECT COUNT(DISTINCT p.id_product) nbr, m.id_manufacturer, m.name ';
                    $sql_query['from'] = '
					FROM ' . _DB_PREFIX_ . 'cat_restriction p
					INNER JOIN ' . _DB_PREFIX_ . 'manufacturer m ON (m.id_manufacturer = p.id_manufacturer) ';
                    $sql_query['where'] = 'WHERE 1';
                    $sql_query['group'] = ' GROUP BY p.id_manufacturer ORDER BY m.name';

                    if (!Configuration::get('PS_LAYERED_HIDE_0_VALUES')) {
                        $sql_query['second_query'] = '
							SELECT m.name, 0 nbr, m.id_manufacturer

							FROM ' . _DB_PREFIX_ . 'cat_restriction p
							INNER JOIN ' . _DB_PREFIX_ . 'manufacturer m ON (m.id_manufacturer = p.id_manufacturer)
							WHERE 1
							GROUP BY p.id_manufacturer ORDER BY m.name';
                    }

                    break;
                case 'id_attribute_group':// attribute group
                    $sql_query['select'] = '
					SELECT COUNT(DISTINCT lpa.id_product) nbr, lpa.id_attribute_group,
					a.color, al.name attribute_name, agl.public_name attribute_group_name , lpa.id_attribute, ag.is_color_group,
					liagl.url_name name_url_name, liagl.meta_title name_meta_title, lial.url_name value_url_name, lial.meta_title value_meta_title';
                    $sql_query['from'] = '
					FROM ' . _DB_PREFIX_ . 'layered_product_attribute lpa
					INNER JOIN ' . _DB_PREFIX_ . 'attribute a
					ON a.id_attribute = lpa.id_attribute
					INNER JOIN ' . _DB_PREFIX_ . 'attribute_lang al
					ON al.id_attribute = a.id_attribute
					AND al.id_lang = ' . (int)$id_lang . '
					INNER JOIN ' . _DB_PREFIX_ . 'cat_restriction p
					ON p.id_product = lpa.id_product
					INNER JOIN ' . _DB_PREFIX_ . 'attribute_group ag
					ON ag.id_attribute_group = lpa.id_attribute_group
					INNER JOIN ' . _DB_PREFIX_ . 'attribute_group_lang agl
					ON agl.id_attribute_group = lpa.id_attribute_group
					AND agl.id_lang = ' . (int)$id_lang . '
					LEFT JOIN ' . _DB_PREFIX_ . 'layered_indexable_attribute_group_lang_value liagl
					ON (liagl.id_attribute_group = lpa.id_attribute_group AND liagl.id_lang = ' . (int)$id_lang . ')
					LEFT JOIN ' . _DB_PREFIX_ . 'layered_indexable_attribute_lang_value lial
					ON (lial.id_attribute = lpa.id_attribute AND lial.id_lang = ' . (int)$id_lang . ') ';

                    $sql_query['where'] = 'WHERE lpa.id_attribute_group = ' . (int)$filter['id_value'];
                    $sql_query['where'] .= ' AND lpa.`id_shop` = ' . (int)$context->shop->id;
                    $sql_query['group'] = '
					GROUP BY lpa.id_attribute
					ORDER BY ag.`position` ASC, a.`position` ASC';

                    if (!Configuration::get('PS_LAYERED_HIDE_0_VALUES')) {
                        $sql_query['second_query'] = '
							SELECT 0 nbr, lpa.id_attribute_group,
								a.color, al.name attribute_name, agl.public_name attribute_group_name , lpa.id_attribute, ag.is_color_group,
								liagl.url_name name_url_name, liagl.meta_title name_meta_title, lial.url_name value_url_name, lial.meta_title value_meta_title
							FROM ' . _DB_PREFIX_ . 'layered_product_attribute lpa' .
                            Shop::addSqlAssociation('product', 'lpa') . '
							INNER JOIN ' . _DB_PREFIX_ . 'attribute a
								ON a.id_attribute = lpa.id_attribute
							INNER JOIN ' . _DB_PREFIX_ . 'attribute_lang al
								ON al.id_attribute = a.id_attribute AND al.id_lang = ' . (int)$id_lang . '
							INNER JOIN ' . _DB_PREFIX_ . 'product as p
								ON p.id_product = lpa.id_product
							INNER JOIN ' . _DB_PREFIX_ . 'attribute_group ag
								ON ag.id_attribute_group = lpa.id_attribute_group
							INNER JOIN ' . _DB_PREFIX_ . 'attribute_group_lang agl
								ON agl.id_attribute_group = lpa.id_attribute_group
							AND agl.id_lang = ' . (int)$id_lang . '
							LEFT JOIN ' . _DB_PREFIX_ . 'layered_indexable_attribute_group_lang_value liagl
								ON (liagl.id_attribute_group = lpa.id_attribute_group AND liagl.id_lang = ' . (int)$id_lang . ')
							LEFT JOIN ' . _DB_PREFIX_ . 'layered_indexable_attribute_lang_value lial
								ON (lial.id_attribute = lpa.id_attribute AND lial.id_lang = ' . (int)$id_lang . ')
							WHERE lpa.id_attribute_group = ' . (int)$filter['id_value'] . '
							AND lpa.`id_shop` = ' . (int)$context->shop->id . '
							GROUP BY lpa.id_attribute
							ORDER BY id_attribute_group, id_attribute';
                    }
                    break;

                case 'id_feature':
                    $sql_query['select'] = 'SELECT fl.name feature_name, fp.id_feature, fv.id_feature_value, fvl.value,
					COUNT(DISTINCT p.id_product) nbr,
					lifl.url_name name_url_name, lifl.meta_title name_meta_title, lifvl.url_name value_url_name, lifvl.meta_title value_meta_title ';
                    $sql_query['from'] = '
					FROM ' . _DB_PREFIX_ . 'feature_product fp
					INNER JOIN ' . _DB_PREFIX_ . 'cat_restriction p
					ON p.id_product = fp.id_product
					LEFT JOIN ' . _DB_PREFIX_ . 'feature_lang fl ON (fl.id_feature = fp.id_feature AND fl.id_lang = ' . $id_lang . ')
					INNER JOIN ' . _DB_PREFIX_ . 'feature_value fv ON (fv.id_feature_value = fp.id_feature_value AND (fv.custom IS NULL OR fv.custom = 0))
					LEFT JOIN ' . _DB_PREFIX_ . 'feature_value_lang fvl ON (fvl.id_feature_value = fp.id_feature_value AND fvl.id_lang = ' . $id_lang . ')
					LEFT JOIN ' . _DB_PREFIX_ . 'layered_indexable_feature_lang_value lifl
					ON (lifl.id_feature = fp.id_feature AND lifl.id_lang = ' . $id_lang . ')
					LEFT JOIN ' . _DB_PREFIX_ . 'layered_indexable_feature_value_lang_value lifvl
					ON (lifvl.id_feature_value = fp.id_feature_value AND lifvl.id_lang = ' . $id_lang . ') ';
                    $sql_query['where'] = 'WHERE fp.id_feature = ' . (int)$filter['id_value'];
                    $sql_query['group'] = 'GROUP BY fv.id_feature_value ';

                    if (!Configuration::get('PS_LAYERED_HIDE_0_VALUES')) {
                        $sql_query['second_query'] = '
							SELECT fl.name feature_name, fp.id_feature, fv.id_feature_value, fvl.value,
							0 nbr,
							lifl.url_name name_url_name, lifl.meta_title name_meta_title, lifvl.url_name value_url_name, lifvl.meta_title value_meta_title

							FROM ' . _DB_PREFIX_ . 'feature_product fp' .
                            Shop::addSqlAssociation('product', 'fp') . '
							INNER JOIN ' . _DB_PREFIX_ . 'product p ON (p.id_product = fp.id_product)
							LEFT JOIN ' . _DB_PREFIX_ . 'feature_lang fl ON (fl.id_feature = fp.id_feature AND fl.id_lang = ' . (int)$id_lang . ')
							INNER JOIN ' . _DB_PREFIX_ . 'feature_value fv ON (fv.id_feature_value = fp.id_feature_value AND (fv.custom IS NULL OR fv.custom = 0))
							LEFT JOIN ' . _DB_PREFIX_ . 'feature_value_lang fvl ON (fvl.id_feature_value = fp.id_feature_value AND fvl.id_lang = ' . (int)$id_lang . ')
							LEFT JOIN ' . _DB_PREFIX_ . 'layered_indexable_feature_lang_value lifl
								ON (lifl.id_feature = fp.id_feature AND lifl.id_lang = ' . (int)$id_lang . ')
							LEFT JOIN ' . _DB_PREFIX_ . 'layered_indexable_feature_value_lang_value lifvl
								ON (lifvl.id_feature_value = fp.id_feature_value AND lifvl.id_lang = ' . (int)$id_lang . ')
							WHERE fp.id_feature = ' . (int)$filter['id_value'] . '
							GROUP BY fv.id_feature_value';
                    }

                    break;
            }

            foreach ($filters as $filter_tmp) {
                $method_name = 'get' . ucfirst($filter_tmp['type']) . 'FilterSubQuery';
                if (method_exists('BlockLayered', $method_name) &&
                    ($filter['type'] != 'price' && $filter['type'] != 'weight' && $filter['type'] != $filter_tmp['type'] || $filter['type'] == $filter_tmp['type'])) {
                    if ($filter['type'] == $filter_tmp['type'] && $filter['id_value'] == $filter_tmp['id_value'])
                        $sub_query_filter = self::$method_name(array(), true);
                    else {
                        if (!is_null($filter_tmp['id_value']))
                            $selected_filters_cleaned = $this->cleanFilterByIdValue(@$selected_filters[$filter_tmp['type']], $filter_tmp['id_value']);
                        else
                            $selected_filters_cleaned = @$selected_filters[$filter_tmp['type']];
                        $sub_query_filter = self::$method_name($selected_filters_cleaned, $filter['type'] == $filter_tmp['type']);
                    }
                    foreach ($sub_query_filter as $key => $value)
                        $sql_query[$key] .= $value;
                }
            }

            $products = false;
            if (!empty($sql_query['from'])) {
                $products = Db::getInstance()->executeS($sql_query['select'] . "\n" . $sql_query['from'] . "\n" . $sql_query['join'] . "\n" . $sql_query['where'] . "\n" . $sql_query['group'], true, false);
            }

            // price & weight have slidebar, so it's ok to not complete recompute the product list
            if (!empty($selected_filters['price']) && $filter['type'] != 'price' && $filter['type'] != 'weight') {
                $products = self::filterProductsByPrice(@$selected_filters['price'], $products);
            }

            if (!empty($sql_query['second_query'])) {
                $res = Db::getInstance()->executeS($sql_query['second_query']);
                if ($res)
                    $products = array_merge($products, $res);
            }

            switch ($filter['type']) {
                case 'condition':
                    $condition_array = array(
                        'new' => array('name' => $this->l('New'), 'nbr' => 0),
                        'used' => array('name' => $this->l('Used'), 'nbr' => 0),
                        'refurbished' => array('name' => $this->l('Refurbished'),
                            'nbr' => 0)
                    );
                    if (isset($products) && $products)
                        foreach ($products as $product)
                            if (isset($selected_filters['condition']) && in_array($product['condition'], $selected_filters['condition']))
                                $condition_array[$product['condition']]['checked'] = true;
                    foreach ($condition_array as $key => $condition)
                        if (isset($selected_filters['condition']) && in_array($key, $selected_filters['condition']))
                            $condition_array[$key]['checked'] = true;
                    if (isset($products) && $products)
                        foreach ($products as $product)
                            if (isset($condition_array[$product['condition']]))
                                $condition_array[$product['condition']]['nbr']++;
                    $filter_blocks[] = array(
                        'type_lite' => 'condition',
                        'type' => 'condition',
                        'id_key' => 0,
                        'name' => $this->l('Condition'),
                        'values' => $condition_array,
                        'filter_show_limit' => $filter['filter_show_limit'],
                        'filter_type' => $filter['filter_type']
                    );
                    break;

                case 'quantity':
                    $quantity_array = array(
                        0 => array('name' => $this->l('Not available'), 'nbr' => 0),
                        1 => array('name' => $this->l('In stock'), 'nbr' => 0)
                    );
                    foreach ($quantity_array as $key => $quantity)
                        if (isset($selected_filters['quantity']) && in_array($key, $selected_filters['quantity']))
                            $quantity_array[$key]['checked'] = true;
                    if (isset($products) && $products)
                        foreach ($products as $product) {
                            //If oosp move all not available quantity to available quantity
                            if ((int)$product['quantity'] > 0 || Product::isAvailableWhenOutOfStock($product['out_of_stock']))
                                $quantity_array[1]['nbr']++;
                            else
                                $quantity_array[0]['nbr']++;
                        }

                    $filter_blocks[] = array(
                        'type_lite' => 'quantity',
                        'type' => 'quantity',
                        'id_key' => 0,
                        'name' => $this->l('Availability'),
                        'values' => $quantity_array,
                        'filter_show_limit' => $filter['filter_show_limit'],
                        'filter_type' => $filter['filter_type']
                    );

                    break;

                case 'manufacturer':
                    if (isset($products) && $products) {
                        $manufaturers_array = array();
                        foreach ($products as $manufacturer) {
                            if (!isset($manufaturers_array[$manufacturer['id_manufacturer']]))
                                $manufaturers_array[$manufacturer['id_manufacturer']] = array('name' => $manufacturer['name'], 'nbr' => $manufacturer['nbr']);
                            if (isset($selected_filters['manufacturer']) && in_array((int)$manufacturer['id_manufacturer'], $selected_filters['manufacturer']))
                                $manufaturers_array[$manufacturer['id_manufacturer']]['checked'] = true;
                        }
                        $filter_blocks[] = array(
                            'type_lite' => 'manufacturer',
                            'type' => 'manufacturer',
                            'id_key' => 0,
                            'name' => $this->l('Manufacturer'),
                            'values' => $manufaturers_array,
                            'filter_show_limit' => $filter['filter_show_limit'],
                            'filter_type' => $filter['filter_type']
                        );
                    }
                    break;

                case 'id_attribute_group':
                    $attributes_array = array();
                    if (isset($products) && $products) {
                        foreach ($products as $attributes) {
                            if (!isset($attributes_array[$attributes['id_attribute_group']]))
                                $attributes_array[$attributes['id_attribute_group']] = array(
                                    'type_lite' => 'id_attribute_group',
                                    'type' => 'id_attribute_group',
                                    'id_key' => (int)$attributes['id_attribute_group'],
                                    'name' => $attributes['attribute_group_name'],
                                    'is_color_group' => (bool)$attributes['is_color_group'],
                                    'values' => array(),
                                    'url_name' => $attributes['name_url_name'],
                                    'meta_title' => $attributes['name_meta_title'],
                                    'filter_show_limit' => $filter['filter_show_limit'],
                                    'filter_type' => $filter['filter_type']
                                );

                            if (!isset($attributes_array[$attributes['id_attribute_group']]['values'][$attributes['id_attribute']]))
                                $attributes_array[$attributes['id_attribute_group']]['values'][$attributes['id_attribute']] = array(
                                    'color' => $attributes['color'],
                                    'name' => $attributes['attribute_name'],
                                    'nbr' => (int)$attributes['nbr'],
                                    'url_name' => $attributes['value_url_name'],
                                    'meta_title' => $attributes['value_meta_title']
                                );

                            if (isset($selected_filters['id_attribute_group'][$attributes['id_attribute']]))
                                $attributes_array[$attributes['id_attribute_group']]['values'][$attributes['id_attribute']]['checked'] = true;
                        }

                        $filter_blocks = array_merge($filter_blocks, $attributes_array);
                    }
                    break;
                case 'id_feature':
                    $feature_array = array();
                    if (isset($products) && $products) {
                        foreach ($products as $feature) {
                            if (!isset($feature_array[$feature['id_feature']]))
                                $feature_array[$feature['id_feature']] = array(
                                    'type_lite' => 'id_feature',
                                    'type' => 'id_feature',
                                    'id_key' => (int)$feature['id_feature'],
                                    'values' => array(),
                                    'name' => $feature['feature_name'],
                                    'url_name' => $feature['name_url_name'],
                                    'meta_title' => $feature['name_meta_title'],
                                    'filter_show_limit' => $filter['filter_show_limit'],
                                    'filter_type' => $filter['filter_type']
                                );

                            if (!isset($feature_array[$feature['id_feature']]['values'][$feature['id_feature_value']]))
                                $feature_array[$feature['id_feature']]['values'][$feature['id_feature_value']] = array(
                                    'nbr' => (int)$feature['nbr'],
                                    'name' => $feature['value'],
                                    'url_name' => $feature['value_url_name'],
                                    'meta_title' => $feature['value_meta_title']
                                );

                            if (isset($selected_filters['id_feature'][$feature['id_feature_value']]))
                                $feature_array[$feature['id_feature']]['values'][$feature['id_feature_value']]['checked'] = true;
                        }

                        //Natural sort
                        foreach ($feature_array as $key => $value) {
                            $temp = array();
                            foreach ($feature_array[$key]['values'] as $keyint => $valueint)
                                $temp[$keyint] = $valueint['name'];

                            natcasesort($temp);
                            $temp2 = array();

                            foreach ($temp as $keytemp => $valuetemp)
                                $temp2[$keytemp] = $feature_array[$key]['values'][$keytemp];

                            $feature_array[$key]['values'] = $temp2;
                        }

                        $filter_blocks = array_merge($filter_blocks, $feature_array);
                    }
                    break;

                case 'category':
                    $tmp_array = array();
                    if (isset($products) && $products) {
                        $categories_with_products_count = 0;
                        foreach ($products as $category) {
                            $tmp_array[$category['id_category']] = array(
                                'name' => $category['name'],
                                'nbr' => (int)$category['count_products']
                            );

                            if ((int)$category['count_products'])
                                $categories_with_products_count++;

                            if (isset($selected_filters['category']) && in_array($category['id_category'], $selected_filters['category']))
                                $tmp_array[$category['id_category']]['checked'] = true;
                        }
                        if ($categories_with_products_count || !Configuration::get('PS_LAYERED_HIDE_0_VALUES'))
                            $filter_blocks[] = array(
                                'type_lite' => 'category',
                                'type' => 'category',
                                'id_key' => 0, 'name' => $this->l('Categories'),
                                'values' => $tmp_array,
                                'filter_show_limit' => $filter['filter_show_limit'],
                                'filter_type' => $filter['filter_type']
                            );
                    }
                    break;
            }
        }

        // All non indexable attribute and feature
        $non_indexable = array();

        // Get all non indexable attribute groups
        foreach (Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
		SELECT public_name
		FROM `' . _DB_PREFIX_ . 'attribute_group_lang` agl
		LEFT JOIN `' . _DB_PREFIX_ . 'layered_indexable_attribute_group` liag
		ON liag.id_attribute_group = agl.id_attribute_group
		WHERE indexable IS NULL OR indexable = 0
		AND id_lang = ' . (int)$id_lang) as $attribute)
            $non_indexable[] = Tools::link_rewrite($attribute['public_name']);

        // Get all non indexable features
        foreach (Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
		SELECT name
		FROM `' . _DB_PREFIX_ . 'feature_lang` fl
		LEFT JOIN  `' . _DB_PREFIX_ . 'layered_indexable_feature` lif
		ON lif.id_feature = fl.id_feature
		WHERE indexable IS NULL OR indexable = 0
		AND id_lang = ' . (int)$id_lang) as $attribute)
            $non_indexable[] = Tools::link_rewrite($attribute['name']);

        //generate SEO link
        $param_selected = '';
        $param_product_url = '';
        $option_checked_array = array();
        $param_group_selected_array = array();
        $title_values = array();
        $meta_values = array();

        //get filters checked by group

        foreach ($filter_blocks as $type_filter) {
            $filter_name = (!empty($type_filter['url_name']) ? $type_filter['url_name'] : $type_filter['name']);
            $filter_meta = (!empty($type_filter['meta_title']) ? $type_filter['meta_title'] : $type_filter['name']);
            $attr_key = $type_filter['type'] . '_' . $type_filter['id_key'];

            $param_group_selected = '';
            $lower_filter = strtolower($type_filter['type']);
            $filter_name_rewritten = Tools::link_rewrite($filter_name);

            if (($lower_filter == 'price' || $lower_filter == 'weight')
                && (float)$type_filter['values'][0] > (float)$type_filter['min']
                && (float)$type_filter['values'][1] > (float)$type_filter['max']) {
                $param_group_selected .= $this->getAnchor() . str_replace($this->getAnchor(), '_', $type_filter['values'][0])
                    . $this->getAnchor() . str_replace($this->getAnchor(), '_', $type_filter['values'][1]);
                $param_group_selected_array[$filter_name_rewritten][] = $filter_name_rewritten;

                if (!isset($title_values[$filter_meta]))
                    $title_values[$filter_meta] = array();
                $title_values[$filter_meta][] = $filter_meta;
                if (!isset($meta_values[$attr_key]))
                    $meta_values[$attr_key] = array('title' => $filter_meta, 'values' => array());
                $meta_values[$attr_key]['values'][] = $filter_meta;
            } else {
                foreach ($type_filter['values'] as $key => $value) {
                    if (is_array($value) && array_key_exists('checked', $value)) {
                        $value_name = !empty($value['url_name']) ? $value['url_name'] : $value['name'];
                        $value_meta = !empty($value['meta_title']) ? $value['meta_title'] : $value['name'];
                        $param_group_selected .= $this->getAnchor() . str_replace($this->getAnchor(), '_', Tools::link_rewrite($value_name));
                        $param_group_selected_array[$filter_name_rewritten][] = Tools::link_rewrite($value_name);

                        if (!isset($title_values[$filter_meta]))
                            $title_values[$filter_meta] = array();
                        $title_values[$filter_meta][] = $value_name;
                        if (!isset($meta_values[$attr_key]))
                            $meta_values[$attr_key] = array('title' => $filter_meta, 'values' => array());
                        $meta_values[$attr_key]['values'][] = $value_meta;
                    } else
                        $param_group_selected_array[$filter_name_rewritten][] = array();
                }
            }

            if (!empty($param_group_selected)) {
                $param_selected .= '/' . str_replace($this->getAnchor(), '_', $filter_name_rewritten) . $param_group_selected;
                $option_checked_array[$filter_name_rewritten] = $param_group_selected;
            }
            // select only attribute and group attribute to display an unique product combination link
            if (!empty($param_group_selected) && $type_filter['type'] == 'id_attribute_group')
                $param_product_url .= '/' . str_replace($this->getAnchor(), '_', $filter_name_rewritten) . $param_group_selected;

        }

        $blacklist = array('weight', 'price');

        if (!Configuration::get('PS_LAYERED_FILTER_INDEX_CDT'))
            $blacklist[] = 'condition';
        if (!Configuration::get('PS_LAYERED_FILTER_INDEX_QTY'))
            $blacklist[] = 'quantity';
        if (!Configuration::get('PS_LAYERED_FILTER_INDEX_MNF'))
            $blacklist[] = 'manufacturer';
        if (!Configuration::get('PS_LAYERED_FILTER_INDEX_CAT'))
            $blacklist[] = 'category';

        $global_nofollow = false;
        $categorie_link = Context::getContext()->link->getCategoryLink($parent, null, null);

        foreach ($filter_blocks as &$type_filter) {
            $filter_name = (!empty($type_filter['url_name']) ? $type_filter['url_name'] : $type_filter['name']);
            $filter_link_rewrite = Tools::link_rewrite($filter_name);
        }

        $n_filters = 0;
        $price_array = [];
        $weight_array = [];
        if (isset($selected_filters['price']))
            if ($price_array['min'] == $selected_filters['price'][0] && $price_array['max'] == $selected_filters['price'][1])
                unset($selected_filters['price']);
        if (isset($selected_filters['weight']))
            if ($weight_array['min'] == $selected_filters['weight'][0] && $weight_array['max'] == $selected_filters['weight'][1])
                unset($selected_filters['weight']);

        foreach ($selected_filters as $filters)
            $n_filters += count($filters);

        $cache = array(
            'layered_show_qties' => (int)Configuration::get('PS_LAYERED_SHOW_QTIES'),
            'id_category_layered' => (int)$id_parent,
            'selected_filters' => $selected_filters,
            'n_filters' => (int)$n_filters,
            'nbr_filterBlocks' => count($filter_blocks),
            'filters' => $filter_blocks,
            'title_values' => $title_values,
            'meta_values' => $meta_values,
            'current_friendly_url' => $param_selected,
            'param_product_url' => $param_product_url,
            'no_follow' => (!empty($param_selected) || $global_nofollow)
        );

        return $cache;
    }

    public function createImgFolder()
    {
        $dir = _PS_IMG_DIR_ . 'sohoshopindexation/';
        $source_index = _PS_PROD_IMG_DIR_ . 'index.php';

        if (!file_exists($dir)) {
            $success = @mkdir($dir, 0775, true);
            if (($success)
                && !file_exists($dir . 'index.php')
                && file_exists($source_index)) {
                return @copy($source_index, $dir . 'index.php');
            }
        }

        return true;
    }

    public function hookdisplayHome($params)
    {
        $selectedCategories = Configuration::get('sohoshopindexation_homecategories');
        $items = [];
        if ($selectedCategories) {
            $res = Db::getInstance()->executeS('
			SELECT a.*,b.name as category
			FROM `' . _DB_PREFIX_ . 'sohoshopindexation_indexation` as a
			JOIN `' . _DB_PREFIX_ . 'category_lang` as b ON (a.id_category = b.id_category AND b.id_lang =' . (int)$this->context->language->id . ')
			WHERE a.`id_category` IN (' . $selectedCategories . ')');

            foreach ($res as $row) {
                if (file_exists(_PS_IMG_DIR_ . 'sohoshopindexation/' . $row['id_sohoshopindexation_indexation'] . '.jpg')) {
                    $items[] = $row;
                }
            }
        }

        if (!empty($items)) {
            $this->context->smarty->assign(array(
                'items' => $items,
                'selectedCategories' => explode(',', $selectedCategories)
            ));
            return $this->fetch('module:' . $this->name . '/views/templates/hook/home.tpl');
        }
    }
}