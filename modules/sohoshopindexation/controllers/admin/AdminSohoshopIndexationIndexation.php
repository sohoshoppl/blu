<?php

/**
 * SohoshopIndexation module
 *
 * @author    SohoSHOP
 * @copyright Copyright (c) 2020 SohoSHOP
 * @license   http://sohoshop.pl
 *
 * http://sohoshop.pl
 */
if (!defined('_PS_VERSION_')) {
    exit;
}

use PrestaShop\PrestaShop\Core\Product\Search\URLFragmentSerializer;
use PrestaShop\Module\FacetedSearch\Product;
use PrestaShop\Module\FacetedSearch\Filters;
use PrestaShop\Module\FacetedSearch\URLSerializer;
use PrestaShop\PrestaShop\Core\Product\Search\Facet;
use PrestaShop\PrestaShop\Core\Product\Search\FacetCollection;
use PrestaShop\PrestaShop\Core\Product\Search\FacetsRendererInterface;
use PrestaShop\PrestaShop\Core\Product\Search\ProductSearchContext;
use PrestaShop\PrestaShop\Core\Product\Search\ProductSearchProviderInterface;
use PrestaShop\PrestaShop\Core\Product\Search\ProductSearchQuery;
use PrestaShop\PrestaShop\Core\Product\Search\ProductSearchResult;
use PrestaShop\PrestaShop\Core\Product\Search\SortOrder;

class AdminSohoshopIndexationIndexationController extends ModuleAdminController
{

    public $module;
    public $themes;

    public function __construct()
    {
        $this->table = 'sohoshopindexation_indexation';
        $this->className = 'SohoshopIndexationIndexation';
        $this->module = 'sohoshopindexation';
        $this->lang = false;
        $this->bootstrap = true;

        $this->bulk_actions = array(
            'delete' => array(
                'text' => 'Usuń zaznaczone',
                'confirm' => 'Usunąć zaznaczone elementy?',
                'icon' => 'icon-trash'
            )
        );

        $id_lang = Configuration::get('PS_LANG_DEFAULT', null, null, Context::getContext()->shop->id);
        $id_shop = Context::getContext()->shop->id;
        $this->tpl_list_vars['icon'] = 'icon-folder-close';
        $this->tpl_list_vars['title'] = 'Indeksowane wyniki filtrów';

        $this->_select .= 'cl.`name` AS category_name';

        $this->_join .= '
			LEFT JOIN `' . _DB_PREFIX_ . 'category_lang` cl ON (cl.`id_category` = a.`id_category` AND cl.`id_lang` = ' . $id_lang . ' AND cl.`id_shop` = ' . $id_shop . ') 
		';
        $this->_where .= ' AND a.id_shop = ' . (int)$id_shop;

        $this->fields_list = array(
            'id_sohoshopindexation_indexation' => array(
                'title' => 'ID',
                'align' => 'center',
                'type' => 'text',
                'orderby' => true,
                'filter' => true,
                'search' => true,
                'class' => 'fixed-width-xs'
            ),
            'name' => array(
                'title' => 'Nazwa',
                'width' => 'auto',
                'type' => 'text',
                'orderby' => true,
                'filter' => true,
                'search' => true,
                'filter_key' => 'a!name',
            ),
            'category_name' => array(
                'title' => 'Kategoria',
                'align' => 'left',
                'type' => 'int',
                'orderby' => true,
                'filter' => true,
                'search' => true,
                'filter_key' => 'cl!name',
            ),
            'seo_url' => array(
                'title' => 'URL',
                'align' => 'left',
                'type' => 'text',
                'orderby' => true,
                'filter' => true,
                'search' => true,
            ),
            'active' => array(
                'title' => 'Aktywne',
                'active' => 'status',
                'type' => 'bool',
                'align' => 'center',
                'ajax' => true,
                'orderby' => false
            ),
            'date_add' => array(
                'title' => 'Data',
                'align' => 'text-right',
                'type' => 'datetime',
                'filter_key' => 'a!date_add'
            )
        );

        parent::__construct();
    }

    public function initToolbar()
    {
        parent::initToolbar();
    }

    public function renderList()
    {
        $this->addRowAction('edit');
        $this->addRowAction('delete');

        return parent::renderList();
    }

    public function renderView()
    {
        $this->initToolbar();
    }

    public function renderForm()
    {
        if (Tools::getValue('ajax') == 1)
            return $this->ajaxCall();

        $this->display = 'edit';
        $this->initToolbar();

        $obj = $this->loadObject(true);
        $context = Context::getContext();
        $id_shop = $context->shop->id;

        $selected_categories = array((isset($obj->id_category) && $obj->id_category) ? (int)$obj->id_category : 0);

        if (Configuration::get('PS_SSL_ENABLED') && Configuration::get('PS_SSL_ENABLED_EVERYWHERE')) {
            $prefix = Tools::getShopDomainSsl(true) . '/';
        } else {
            $prefix = Tools::getShopDomain(true) . '/';
        }

        $filters = null;
        if ((int)$obj->id_category && Module::isEnabled('ps_facetedsearch')) {

            $filters = Db::getInstance()->executeS(
                'SELECT type, id_value, filter_show_limit, filter_type ' .
                'FROM ' . _DB_PREFIX_ . 'layered_category ' .
                'WHERE id_category = ' . $obj->id_category . ' ' .
                'GROUP BY `type`, id_value ORDER BY position ASC'
            );
        }

        $this->fields_form = array(
            'legend' => array(
                'title' => $this->trans('Ustawienia', array(), 'Admin.SohoshopIndexation.Module') . ' [' . Context::getContext()->shop->name . ']',
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->trans('Nazwa', array(), 'Admin.SohoshopIndexation.Module'),
                    'name' => 'name',
                    'required' => true,
                    'class' => 'fixed-width-xxl',
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->trans('Aktywne', array(), 'Admin.Global'),
                    'name' => 'active',
                    'required' => false,
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'active_on',
                            'value' => 1,
                            'label' => $this->trans('Enabled', array(), 'Admin.Global')
                        ),
                        array(
                            'id' => 'active_off',
                            'value' => 0,
                            'label' => $this->trans('Disabled', array(), 'Admin.Global')
                        )
                    )
                ),
                array(
                    'type' => 'text',
                    'label' => $this->trans('Sklep', array(), 'Admin.SohoshopIndexation.Module'),
                    'name' => 'id_shop',
                    'readonly' => true,
                ),
                array(
                    'type' => 'categories',
                    'label' => $this->trans('Kategoria', array(), 'Admin.SohoshopIndexation.Module'),
                    'name' => 'id_category',
                    'tree' => array(
                        'id' => 'categories-tree',
                        'root_category' => $context->shop->getCategory(),
                        'selected_categories' => $selected_categories,
                    )
                ),
            ),
        );

        $this->fields_value['id_shop'] = (int)Context::getContext()->shop->id;

        $filters = $this->module->getFilterBlock((int)$obj->id_category);
        if (count($filters)) {
            $urlSerializer = new URLFragmentSerializer();
            $objFilters = Tools::jsonDecode($obj->filters, true);

            $id_lang = Configuration::get('PS_LANG_DEFAULT', null, null, Context::getContext()->shop->id);
            foreach ($filters['filters'] as $filter) {
                $forurl = true;
                if ($filter['name']) {
                    $idValue = (isset($filter['id_key']) && $filter['id_key']) ? '_' . $filter['id_key'] : '';
                    $idFil = $filter['type'] . $idValue;

                    $idFilVal = isset($objFilters[$idFil]) ? $objFilters[$idFil] : null;

                    $countFilter = 0;
                    foreach ($filter['values'] as $fi) {
                        if ((int)$fi['nbr'] > 0)
                            $countFilter++;
                    }

                    if ($countFilter) {
                        $this->fields_form['input'][] = array(
                            'type' => 'html',
                            'label' => $filter['name'],
                            'name' => 'filters',
                            'html_content' => $this->displayFilter($filter, $idFilVal, $idFil, $forurl),
                        );
                    }
                }
            }

            $forurl_input_serialize = '';
            if ($objFilters) {
                $objFiltersArray = $this->module->setUrlFromFilters($objFilters, $id_lang);
                $forurl_input_serialize = str_replace(' ', '_', $urlSerializer->serialize($objFiltersArray));
            }
        }

        $this->fields_form['input'][] = array(
            'type' => 'text',
            'label' => $this->trans('Meta Title', array(), 'Admin.SohoshopIndexation.Module'),
            'name' => 'meta_title',
        );
        $this->fields_form['input'][] = array(
            'type' => 'textarea',
            'label' => $this->trans('Meta Description', array(), 'Admin.SohoshopIndexation.Module'),
            'name' => 'meta_description',
        );
        $this->fields_form['input'][] = array(
            'type' => 'text',
            'label' => $this->trans('Nagłówek H1', array(), 'Admin.SohoshopIndexation.Module'),
            'name' => 'seo_title',
        );
        $this->fields_form['input'][] = array(
            'type' => 'textarea',
            'label' => $this->trans('Opis', array(), 'Admin.SohoshopIndexation.Module'),
            'name' => 'seo_description',
            'autoload_rte' => true,
        );

        $imageType = $obj->id . '.jpg';
        $image = _PS_IMG_DIR_ . 'sohoshopindexation/' . $imageType;
        $image_url = ImageManager::thumbnail($image, 'sohoshopindexation_' . (int)$obj->id . '.' . $imageType, 310, $imageType, true, true);
        $image_size = file_exists($image) ? filesize($image) / 1000 : false;

        $this->fields_form['input'][] = array(
            'type' => 'file',
            'label' => $this->trans('Zdjęcie', array(), 'Admin.SohoshopIndexation.Module'),
            'name' => 'image',
            'display_image' => true,
            'image' => $image_url ? $image_url : false,
            'size' => $image_size,
        );

        if ($obj->id_category) {
            $this->fields_form['input'][] = array(
                'type' => 'html',
                'label' => $this->trans('Adres URL', array(), 'Admin.SohoshopIndexation.Module'),
                'name' => 'seo_url',
                'html_content' => $this->displayURL($obj->seo_url, $obj->seo_redirect, $obj->id_category, $forurl_input_serialize),
            );
        }

        $this->fields_form['submit'] = array(
            'title' => 'Save',
        );


        $this->fields_form['buttons'] = array(
            'save-and-stay' => array(
                'title' => $this->trans('Zapisz i zostań', array(), 'Admin.SohoshopIndexation.Module'),
                'name' => 'submitAdd' . $this->table . 'AndEditIndexation',
                'type' => 'submit',
                'class' => 'btn btn-default pull-right',
                'icon' => 'process-icon-save'
            )
        );

        Media::addJsDef(array('module_id_category' => (int)$obj->id_category));

        return parent::renderForm();
    }

    public function ajaxCall()
    {
        $result = (array(
            'test' => 'abc',
        ));

        $this->ajaxDie(Tools::jsonEncode($result));
    }

    private function displayFilter($filter, $idFilterValues = null, $idFil = null, $forurl = false)
    {
        $context = Context::getContext();
        $context->smarty->assign('filter', $filter);
        $context->smarty->assign('filter_values', $idFilterValues);
        $context->smarty->assign('filter_id', $idFil);
        $context->smarty->assign('forurl', $forurl);

        $context->smarty->assign('filter_form', $this->displayFilterForm($filter, $idFilterValues, $idFil));

        return $context->smarty->fetch(_PS_MODULE_DIR_ . 'sohoshopindexation/views/templates/admin/filter.tpl');
    }

    private function displayURL($url, $seo_redirect, $id_category, $serializefilters)
    {
        $context = Context::getContext();
        //$categoryLink = $serializefilters != '' ? $context->link->getCategoryLink($id_category).'_'.$serializefilters : $context->link->getCategoryLink($id_category);
        $context->smarty->assign('category_link', $context->link->getCategoryLink($id_category));
        $context->smarty->assign('seo_link', $url);
        $context->smarty->assign('seo_redirect', $seo_redirect);
        $context->smarty->assign('serializefilters', $serializefilters);

        return $context->smarty->fetch(_PS_MODULE_DIR_ . 'sohoshopindexation/views/templates/admin/url.tpl');
    }

    public function displayFilterForm($filter, $idFilterValues, $idFil)
    {
        $context = Context::getContext();
        $id_lang = Configuration::get('PS_LANG_DEFAULT', null, null, $context->shop->id);
        $content = array();

        switch ($filter['type']) {
            case 'id_feature':
                $context->smarty->assign(array(
                    'values' => $filter['values'],
                    'type' => $filter['type'],
                    'value' => $filter['id_key'],
                ));
                $content[$idFil] = $context->smarty->fetch(_PS_MODULE_DIR_ . $this->module->name . '/views/templates/admin/content_feature.tpl');
                break;
            case 'id_attribute_group':
                $this->context->smarty->assign(array(
                    'values' => $filter['values'],
                    'type' => $filter['type'],
                    'value' => $filter['id_key'],
                ));
                $content[$idFil] = $context->smarty->fetch(_PS_MODULE_DIR_ . $this->module->name . '/views/templates/admin/content_attribute.tpl');
                break;
            case 'manufacturer':
                $this->context->smarty->assign(array(
                    'values' => $filter['values'],
                    'type' => $filter['type'],
                    'id_value' => null,
                ));
                $content[$idFil] = $context->smarty->fetch(_PS_MODULE_DIR_ . $this->module->name . '/views/templates/admin/content_manufacturer.tpl');
                break;
            /**case 'price':
             * $this->context->smarty->assign(array(
             * 'type' => $filter['type'],
             * ));
             * $content[$idFil] = $context->smarty->fetch(_PS_MODULE_DIR_.$this->module->name.'/views/templates/admin/content_price.tpl');
             * break;*/
        }

        return $content;
    }

    public function processAdd()
    {
        $object = parent::processAdd();

        if (Tools::isSubmit('submitAdd' . $this->table . 'AndEditIndexation') && $object) {
            $this->redirect_after = Context::getContext()->link->getAdminLink('AdminSohoshopIndexationIndexation') . '&id_sohoshopindexation_indexation=' . $object->id . '&updatesohoshopindexation_indexation';
        }

        return $object;
    }

    public function processUpdate()
    {
        $object = parent::processUpdate();
        $context = Context::getContext();
        $id_lang = Configuration::get('PS_LANG_DEFAULT', null, null, $context->shop->id);

        $filters = Tools::getValue('filter');

        if (count($filters)) {
            $filtersJson = array();
            foreach ($filters as $key => $fil) {
                if (isset($fil['values']) && $fil['active'])
                    $filtersJson[$key] = $fil;
            }

            $object->filters = Tools::jsonEncode($filtersJson, JSON_FORCE_OBJECT | JSON_UNESCAPED_UNICODE);

            $urlSerializer = new URLFragmentSerializer();
            $objFilters = Tools::jsonDecode($object->filters, true);
            $forurl_input_serialize = '';
            if ($objFilters) {
                $objFiltersArray = $this->module->setUrlFromFilters($objFilters, $id_lang);
                $forurl_input_serialize = str_replace(' ', '_', $urlSerializer->serialize($objFiltersArray));
            }

            if (!Tools::getValue('category_link_default'))
                $object->seo_redirect = '';
            $object->seo_url = $context->link->getCategoryLink($object->id_category) . '_' . $forurl_input_serialize;
            $object->category_url = $context->link->getCategoryLink($object->id_category);

            $object->update();
        }

        if (Tools::isSubmit('submitAdd' . $this->table . 'AndEditIndexation')) {
            $this->redirect_after = Context::getContext()->link->getAdminLink('AdminSohoshopIndexationIndexation') . '&id_sohoshopindexation_indexation=' . $object->id . '&updatesohoshopindexation_indexation';
        }

        return $object;
    }

    public function setMedia($isNewTheme = false)
    {
        parent::setMedia($isNewTheme);
        $this->addJS(_MODULE_DIR_ . 'sohoshopindexation/views/js/admin_filter.js?ver=' . time());
        $this->addCSS(_MODULE_DIR_ . 'sohoshopindexation/views/css/admin_filter.css', 'all', 0);
    }

    protected function postImage($id)
    {
        return $this->uploadImage($id, 'image', 'sohoshopindexation/');
    }

}
