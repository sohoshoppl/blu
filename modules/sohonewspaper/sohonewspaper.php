<?php
/*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA

*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_CAN_LOAD_FILES_')) {
    exit;
}

use PrestaShop\PrestaShop\Core\Module\WidgetInterface;

include_once _PS_MODULE_DIR_.'sohonewspaper/newpaperClass.php';

class Sohonewspaper extends Module implements WidgetInterface
{
    private $templateFile;

    public function __construct()
    {
        $this->name = 'sohonewspaper';
        $this->author = 'Aleksander sohoshop.pl';
        $this->version = '3.0.1';

        $this->bootstrap = true;
        parent::__construct();

        $this->displayName = $this->trans('sohonewspaper', array(), 'Modules.Sohonewspaper.Admin');
        $this->description = $this->trans('sohonewspaper on page', array(), 'Modules.Sohonewspaper.Admin');

        $this->ps_versions_compliancy = array('min' => '1.7.2.0', 'max' => _PS_VERSION_);
        $this->templateFile = 'module:sohonewspaper/views/templates/hook/top.tpl';
    }

    public function install()
    {
        return parent::install()
            && $this->installDB()
            && Configuration::updateValue('sohonewspaper_NBBLOCKS', 5)
            && $this->registerHook('displayTop')
            && $this->registerHook('actionUpdateLangAfter')
            && $this->registerHook('actionFrontControllerSetMedia')
        ;
    }

    public function installDB()
    {
        $return = true;
        $return &= Db::getInstance()->execute('
            CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'sohonewspaper` (
                `id_sohonewspaper` INT UNSIGNED NOT NULL AUTO_INCREMENT,
                `id_shop` int(10) unsigned NOT NULL ,
                `file_name` VARCHAR(100) NOT NULL,
                PRIMARY KEY (`id_sohonewspaper`)
            ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 ;');

        $return &= Db::getInstance()->execute('
            CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'sohonewspaper_lang` (
                `id_sohonewspaper` INT UNSIGNED NOT NULL AUTO_INCREMENT,
                `id_lang` int(10) unsigned NOT NULL ,
                `text` VARCHAR(300) NOT NULL,
                `url` VARCHAR(300) NOT NULL,
                PRIMARY KEY (`id_sohonewspaper`, `id_lang`)
            ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 ;');

        return $return;
    }

    public function uninstall()
    {
        return Configuration::deleteByName('sohonewspaper_NBBLOCKS') &&
            $this->uninstallDB() &&
            parent::uninstall();
    }

    public function uninstallDB()
    {
        return Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'sohonewspaper`') && Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'sohonewspaper_lang`');
    }

    public function addToDB()
    {
        if (isset($_POST['nbblocks'])) {
            for ($i = 1; $i <= (int)$_POST['nbblocks']; $i++) {
                $filename = explode('.', $_FILES['info'.$i.'_file']['name']);
                if (isset($_FILES['info'.$i.'_file']) && isset($_FILES['info'.$i.'_file']['tmp_name']) && !empty($_FILES['info'.$i.'_file']['tmp_name'])) {
                    if ($error = ImageManager::validateUpload($_FILES['info'.$i.'_file'])) {
                        return false;
                    } elseif (!($tmpName = tempnam(_PS_TMP_IMG_DIR_, 'PS')) || !move_uploaded_file($_FILES['info'.$i.'_file']['tmp_name'], $tmpName)) {
                        return false;
                    } elseif (!ImageManager::resize($tmpName, dirname(__FILE__).'/img/'.$filename[0].'.jpg')) {
                        return false;
                    }
                    unlink($tmpName);
                }
                Db::getInstance()->execute('INSERT INTO `'._DB_PREFIX_.'sohonewspaper` (`filename`,`text`)
                                            VALUES ("'.((isset($filename[0]) && $filename[0] != '') ? pSQL($filename[0]) : '').
                    '", "'.((isset($_POST['info'.$i.'_text']) && $_POST['info'.$i.'_text'] != '') ? pSQL($_POST['info'.$i.'_text']) : '').'", "'.((isset($_POST['info'.$i.'_url']) && $_POST['info'.$i.'_url'] != '') ? pSQL($_POST['info'.$i.'_url']) : '').'")');
            }
            return true;
        } else {
            return false;
        }
    }

    public function removeFromDB()
    {
        $dir = opendir(dirname(__FILE__).'/img');
        while (false !== ($file = readdir($dir))) {
            $path = dirname(__FILE__).'/img/'.$file;
            if ($file != '..' && $file != '.' && !is_dir($file)) {
                unlink($path);
            }
        }
        closedir($dir);

        return Db::getInstance()->execute('DELETE FROM `'._DB_PREFIX_.'sohonewspaper`');
    }

    public function hookActionUpdateLangAfter($params)
    {
        if (!empty($params['lang']) && $params['lang'] instanceOf Language) {
            include_once _PS_MODULE_DIR_ . $this->name . '/lang/SohonewspaperLang.php';

            Language::updateMultilangFromClass(_DB_PREFIX_ . 'sohonewspaper_lang', 'SohonewspaperLang', $params['lang']);
        }
    }

    public function getContent()
    {
        $html = '';
        $id_sohonewspaper = (int)Tools::getValue('id_sohonewspaper');

        if (Tools::isSubmit('savesohonewspaper')) {
            if ($id_sohonewspaper = Tools::getValue('id_sohonewspaper')) {
                $sohonewspaper = new newpaperClass((int)$id_sohonewspaper);
            } else {
                $sohonewspaper = new newpaperClass();
            }

            $sohonewspaper->copyFromPost();
            $sohonewspaper->id_shop = $this->context->shop->id;

            if ($sohonewspaper->validateFields(false) && $sohonewspaper->validateFieldsLang(false)) {
                $sohonewspaper->save();

                if (isset($_FILES['image']) && isset($_FILES['image']['tmp_name']) && !empty($_FILES['image']['tmp_name'])) {
                    if ($error = ImageManager::validateUpload($_FILES['image'])) {
                        return false;
                    } elseif (!($tmpName = tempnam(_PS_TMP_IMG_DIR_, 'PS')) || !move_uploaded_file($_FILES['image']['tmp_name'], $tmpName)) {
                        return false;
                    } elseif (!ImageManager::resize($tmpName, dirname(__FILE__).'/img/sohonewspaper-'.(int)$sohonewspaper->id.'-'.(int)$sohonewspaper->id_shop.'.jpg')) {
                        return false;
                    }

                    unlink($tmpName);
                    $sohonewspaper->file_name = 'sohonewspaper-'.(int)$sohonewspaper->id.'-'.(int)$sohonewspaper->id_shop.'.jpg';
                    $sohonewspaper->save();
                }
                $this->_clearCache('*');
            } else {
                $html .= '<div class="conf error">'.$this->trans('An error occurred while attempting to save.', array(), 'Admin.Notifications.Error').'</div>';
            }
        }

        if (Tools::isSubmit('updatesohonewspaper') || Tools::isSubmit('addsohonewspaper')) {
            $helper = $this->initForm();
            foreach (Language::getLanguages(false) as $lang) {
                if ($id_sohonewspaper) {
                    $sohonewspaper = new newpaperClass((int)$id_sohonewspaper);
                    $helper->fields_value['text'][(int)$lang['id_lang']] = $sohonewspaper->text[(int)$lang['id_lang']];
                    $helper->fields_value['url'][(int)$lang['id_lang']] = $sohonewspaper->url[(int)$lang['id_lang']];
                    $image = dirname(__FILE__).DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.$sohonewspaper->file_name;
                    $this->fields_form[0]['form']['input'][0]['image'] = '<img src="'.$this->getImageURL($sohonewspaper->file_name).'" />';
                } else {
                    $helper->fields_value['text'][(int)$lang['id_lang']] = Tools::getValue('text_'.(int)$lang['id_lang'], '');
                    $helper->fields_value['url'][(int)$lang['id_lang']] = Tools::getValue('url_'.(int)$lang['id_lang'], '');
                }
            }
            if ($id_sohonewspaper = Tools::getValue('id_sohonewspaper')) {
                $this->fields_form[0]['form']['input'][] = array('type' => 'hidden', 'name' => 'id_sohonewspaper');
                $helper->fields_value['id_sohonewspaper'] = (int)$id_sohonewspaper;
            }

            return $html.$helper->generateForm($this->fields_form);
        } elseif (Tools::isSubmit('deletesohonewspaper')) {
            $sohonewspaper = new newpaperClass((int)$id_sohonewspaper);
            if (file_exists(dirname(__FILE__).'/img/'.$sohonewspaper->file_name)) {
                unlink(dirname(__FILE__).'/img/'.$sohonewspaper->file_name);
            }
            $sohonewspaper->delete();
            $this->_clearCache('*');
            Tools::redirectAdmin(AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'));
        } else {
            $content = $this->getListContent((int)Configuration::get('PS_LANG_DEFAULT'));
            $helper = $this->initList();
            $helper->listTotal = count($content);
            return $html.$helper->generateList($content, $this->fields_list);
        }

        if (isset($_POST['submitModule'])) {
            Configuration::updateValue('sohonewspaper_NBBLOCKS', ((isset($_POST['nbblocks']) && $_POST['nbblocks'] != '') ? (int)$_POST['nbblocks'] : ''));
            if ($this->removeFromDB() && $this->addToDB()) {
                $this->_clearCache('top.tpl');
                $output = '<div class="conf confirm">'.$this->trans('The block configuration has been updated.', array(), 'Modules.Sohonewspaper.Admin').'</div>';
            } else {
                $output = '<div class="conf error"><img src="../img/admin/disabled.gif"/>'.$this->trans('An error occurred while attempting to save.', array(), 'Admin.Notifications.Error').'</div>';
            }
        }
    }

    protected function getListContent($id_lang)
    {
        return  Db::getInstance()->executeS('
            SELECT r.`id_sohonewspaper`, r.`id_shop`, r.`file_name`, rl.`text` , rl.`url`
            FROM `'._DB_PREFIX_.'sohonewspaper` r
            LEFT JOIN `'._DB_PREFIX_.'sohonewspaper_lang` rl ON (r.`id_sohonewspaper` = rl.`id_sohonewspaper`)
            WHERE `id_lang` = '.(int)$id_lang.' '.Shop::addSqlRestrictionOnLang());
    }

    protected function getListCatalog($id_lang)
    {
        return  Db::getInstance()->executeS('
        SELECT r.`id_sohonewspapercatalog`, r.`id_shop`, r.`file_name`, rl.`title`
        FROM `'._DB_PREFIX_.'sohonewspapercatalog` r
        LEFT JOIN `'._DB_PREFIX_.'sohonewspapercatalog_lang` rl ON (r.`id_sohonewspapercatalog` = rl.`id_sohonewspapercatalog`)
        WHERE `id_lang` = '.(int)$id_lang.' '.Shop::addSqlRestrictionOnLang());
    }

    protected function getListPapers($id_lang)
    {
        return  Db::getInstance()->executeS('
        SELECT r.`id_sohonewspapergazetki`, r.`id_shop`, r.`file_name`, rl.`title`
        FROM `'._DB_PREFIX_.'sohonewspapergazetki` r
        LEFT JOIN `'._DB_PREFIX_.'sohonewspapergazetki_lang` rl ON (r.`id_sohonewspapergazetki` = rl.`id_sohonewspapergazetki`)
        WHERE `id_lang` = '.(int)$id_lang.' '.Shop::addSqlRestrictionOnLang());
    }

    protected function initForm()
    {
        $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');

        $this->fields_form[0]['form'] = array(
            'legend' => array(
                'title' => $this->trans('New sohonewspaper block', array(), 'Modules.Sohonewspaper.Admin'),
            ),
            'input' => array(
                array(
                    'type' => 'file',
                    'label' => $this->trans('Image', array(), 'Admin.Global'),
                    'name' => 'image',
                    'value' => true,
                    'display_image' => true,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->trans('url', array(), 'Admin.Global'),
                    'lang' => true,
                    'name' => 'url',
                ),
                array(
                    'type' => 'textarea',
                    'label' => $this->trans('Text', array(), 'Admin.Global'),
                    'lang' => true,
                    'name' => 'text',
                    'cols' => 40,
                    'rows' => 10,
                    'class' => 'rte',
                    'autoload_rte' => true,
                )
            ),
            'submit' => array(
                'title' => $this->trans('Save', array(), 'Admin.Actions'),
            )
        );

        $helper = new HelperForm();
        $helper->module = $this;
        $helper->name_controller = 'sohonewspaper';
        $helper->identifier = $this->identifier;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        foreach (Language::getLanguages(false) as $lang) {
            $helper->languages[] = array(
                'id_lang' => $lang['id_lang'],
                'iso_code' => $lang['iso_code'],
                'name' => $lang['name'],
                'is_default' => ($default_lang == $lang['id_lang'] ? 1 : 0)
            );
        }

        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;
        $helper->toolbar_scroll = true;
        $helper->title = $this->displayName;
        $helper->submit_action = 'savesohonewspaper';
        $helper->toolbar_btn =  array(
            'save' =>
            array(
                'desc' => $this->trans('Save', array(), 'Admin.Actions'),
                'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'),
            ),
            'back' =>
            array(
                'href' => AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'),
                'desc' => $this->trans('Back to list', array(), 'Admin.Actions'),
            )
        );
        return $helper;
    }

    protected function initList()
    {
        $this->fields_list = array(
            'id_sohonewspaper' => array(
                'title' => $this->trans('ID', array(), 'Admin.Global'),
                'width' => 120,
                'type' => 'text',
                'search' => false,
                'orderby' => false
            ),
            'text' => array(
                'title' => $this->trans('Text', array(), 'Admin.Global'),
                'width' => 140,
                'type' => 'text',
                'search' => false,
                'orderby' => false
            ),
        );

        if (Shop::isFeatureActive()) {
            $this->fields_list['id_shop'] = array(
                'title' => $this->trans('ID Shop', array(), 'Modules.Sohonewspaper.Admin'),
                'align' => 'center',
                'width' => 25,
                'type' => 'int'
            );
        }

        $helper = new HelperList();
        $helper->shopLinkType = '';
        $helper->simple_header = false;
        $helper->identifier = 'id_sohonewspaper';
        $helper->actions = array('edit', 'delete');
        $helper->show_toolbar = true;
        $helper->imageType = 'jpg';
        $helper->toolbar_btn['new'] =  array(
            'href' => AdminController::$currentIndex.'&configure='.$this->name.'&add'.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'),
            'desc' => $this->trans('Add new', array(), 'Admin.Actions')
        );

        // $helper->title = $this->displayName;
        $helper->title = 'sohonewspaper banners';
        $helper->table = $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        return $helper;
    }

    protected function _clearCache($template, $cacheId = null, $compileId = null)
    {
        parent::_clearCache($this->templateFile);
    }

    public function renderWidget($hookName = null, array $configuration = [])
    {
        if (!$this->isCached($this->templateFile, $this->getCacheId('sohonewspaper'))) {
            $this->smarty->assign($this->getWidgetVariables($hookName, $configuration));
        }

        return $this->fetch($this->templateFile, $this->getCacheId('sohonewspaper'));
    }

    public function getWidgetVariables($hookName = null, array $configuration = [])
    {
        $elements = $this->getListContent($this->context->language->id);

        foreach ($elements as &$element) {
            $element['image'] = $this->getImageURL($element['file_name']);
        }

        return array(
            'elements' => $elements,
        );
    }

    private function getImageURL($image)
    {
        return $this->context->link->getMediaLink(__PS_BASE_URI__.'modules/'.$this->name.'/img/'.$image);
    }

    public function hookDisplayTop($params) {

        $lang = $this->context->language->id;
        $elements = $this->getListContent($lang);
        foreach ($elements as &$element) {
            $element['image'] = $this->getImageURL($element['file_name']);
        }

        $catalogs= $this->getListCatalog($lang);
        $papers= $this->getListPapers($lang);

        foreach($catalogs as $key=>$value) {
            $catalogs[$key]['link']=$this->context->link->getModuleLink('sohonewspaper','single',array('category'=>'sohonewspapercatalog','id'=>$value['id_sohonewspapercatalog'] ));
        }

        foreach($papers as $key2=>$value2) {
            $papers[$key2]['link']=$this->context->link->getModuleLink('sohonewspaper','single',array('category'=>'sohonewspapergazetki','id'=>$value2['id_sohonewspapergazetki'] ));
        }

        $this->context->smarty->assign(
            array(
              'banners' => $elements,              
              'catalogs' => $catalogs,              
              'papers' => $papers,              
        ));

        return $this->display(__FILE__, '/views/templates/hook/top.tpl');

    }

    public function hookActionFrontControllerSetMedia()
    {
        echo 
        $this->context->controller->registerStylesheet(
            'sohonewspaper-top',
            $this->_path.'views/css/top.css',
            [
                'media' => 'all',
                'priority' => 1000,
            ]
        );
    }
}
