{extends 'page.tpl'}
{block name='content'}
<section id="main">
    <div class="container sohonewspaper">     
        <h1 class="sohonewspaper-header">{l s='Gazetki, Katalogi, Okazje' mod='sohonewspaper'}</h1>
        <div class="sohonewspaper-papers"> 
            <p class="pappers-header">{l s='Gazetki' mod='sohonewspaper'}</p>
            <ul class="papers-list-container">
                {foreach from=$modulePapers item=paper}
                    <li class="papers-item col-lg-4 col-md-6 col-xs-12">
                    <a href="{$paper['link']}" class="papers-item-href">
                        <img src="modules/sohonewspapergazetki/img/{$paper['file_name']}">
                        <button class="btn newspaper_btn">
                            {l s='Zobacz gazetkę' mod='sohonewspaper'}
                            <svg class="newspaper_arrow" xmlns="http://www.w3.org/2000/svg" width="14.262" height="9.192" viewBox="0 0 12.262 7.192">
                                <defs></defs>
                                <g transform="translate(0.53 0.53)">
                                    <path d="M-317.515-44.61l-5.6,5.6-5.6-5.6" transform="translate(328.717 44.61)"></path>
                                </g>
                            </svg>
                        </button>
                    </a>
                    </li>
                {/foreach}
            </ul>
        </div>  
        <div class="sohonewspaper-catalogs"> 
            <p class="pappers-header">{l s='Katalogi' mod='sohonewspaper'}</p>
            <ul class="catalogs-list-container">
                {foreach from=$moduleCatalogs item=catalog}
                    <li class="papers-item col-lg-4 col-md-6 col-xs-12">
                        <a href="{$paper['link']}" class="papers-item-href">
                            <img src="modules/sohonewspapercatalog/img/{$paper['file_name']}">
                            <button class="btn newspaper_btn">
                                {l s='Zobacz gazetkę' mod='sohonewspaper'}
                                <svg class="newspaper_arrow" xmlns="http://www.w3.org/2000/svg" width="14.262" height="9.192" viewBox="0 0 12.262 7.192">
                                    <defs></defs>
                                    <g transform="translate(0.53 0.53)">
                                        <path d="M-317.515-44.61l-5.6,5.6-5.6-5.6" transform="translate(328.717 44.61)"></path>
                                    </g>
                                </svg>
                            </button>
                        </a>
                    </li>

                {/foreach}
            </ul>
        </div>  
         <div class="sohonewspaper-banners">
            <p class="pappers-header">
                {l s='Okazje' mod='sohonewspaper'}
            </p>
            <ul>
            {foreach from=$moduleBanners item=element}
                <li class="pappers-footer-item" style="background-image: url({$element.image});">
                    <a href="{$element.url}" class="pappers-footer-url">
                        <div class="pappers-footer-content">
                            <div class="pappers-footer-text">{$element.text nofilter}</div>
                            <button class="pappers-footer-btn btn">
                                {l s='Odkryj teraz' mod='sohonewspaper'}
                                <svg class="newspaper_arrow" xmlns="http://www.w3.org/2000/svg" width="14.262" height="9.192" viewBox="0 0 12.262 7.192">
                                    <defs></defs>
                                    <g transform="translate(0.53 0.53)">
                                        <path d="M-317.515-44.61l-5.6,5.6-5.6-5.6" transform="translate(328.717 44.61)"></path>
                                    </g>
                                </svg>
                            </button>
                        </div>
                    </a>
                </li>
            {/foreach}
            </ul>
        </div>
    </div>
</section>
{/block}