{extends 'page.tpl'}
{block name='content'}
    {if !empty($single_content)}
        <section id="main" class="soho-single-newspaper">
            <h1 class="header-single-newspaper">{$single_content[0]['title']}</h1>
            <div class="header-single-newspaper-img newspaper-img-container">
                <img src="{$single_content[0]['file_name']}"/>
                {if !empty($imageMarkers)}
                    {foreach name=markers from=$imageMarkers item=marker}
                        {if $single_content[0]['file_name']|strstr:$marker.img_src AND  $marker.productObj->id}
                            <div class="marker" style="left:{$marker.pos_left}%;top:{$marker.pos_top}%">
                                <img class="marker-icon" src="{$urls.img_url}product_marker.svg">
                        <div class="marker_content clearfix {if $marker.pos_left > 50} right_marker_content {else} left_marker_content{/if}  {if $marker.pos_left < 60 &&  $marker.pos_left > 40 } centered_marker_content {/if}">
                                 <div class="marker-inner">
                                        <img src="{$urls.base_url}{$marker.productImages[0].id_image}-home_default/{$marker.productObj->link_rewrite[$language.id]}.jpg">
                                        <div class="right-box">
                                            <h3>{$marker.productObj->name[$language.id]}</h3>
                                            {assign var=subtitle value=(SohoshopProductAdditionalDescriptions::getProductAdditionalDescriptions($marker.productObj->id))}
                                            {assign var="manufacturer_name" value=(Manufacturer::getNameById($marker.productObj->id_manufacturer))}
                                            <div class="product-subtitle">{$subtitle.description_1}</div>
                                            <div class="product-manufacturer">{$manufacturer_name}</div>
                                            <a class="more-button" href="{url entity=product id=$marker.productObj->id}">
                                                <button class="btn btn-primary btn-inspire">{l s='przejdź do produktu' d='Modules.Sohoshopinspiracje.Shop'}</button>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        {/if}
                    {/foreach}
                {/if}
                {*----*}
            </div>
            <div class="content-single-newspaper-img">
                {foreach from=$single_content[0]['file_name_list'] item=image}
                    <div class="col-md-6 col-xs-12 newspaper-img-container">
                        <img src="{$image}">
                        {if !empty($imageMarkers)}
                            {foreach name=markers from=$imageMarkers item=marker}
                                {if $image|strstr:$marker.img_src AND $marker.productObj->id}
                                    <div class="marker" style="left:{$marker.pos_left}%;top:{$marker.pos_top}%">
                                        <img class="marker-icon" src="{$urls.img_url}product_marker.svg">
                                        <div class="marker_content clearfix {if $marker.pos_left > 50} right_marker_content {else} left_marker_content{/if}  {if $marker.pos_left < 60 &&  $marker.pos_left > 40 } centered_marker_content {/if}">
                                            <div class="marker-inner">
                                                <img src="{$urls.base_url}{$marker.productImages[0].id_image}-home_default/{$marker.productObj->link_rewrite[$language.id]}.jpg">
                                                <div class="right-box">
                                                    <h3>{$marker.productObj->name[$language.id]}</h3>
                                                    {assign var=subtitle value=(SohoshopProductAdditionalDescriptions::getProductAdditionalDescriptions($marker.productObj->id))}
                                                    {assign var="manufacturer_name" value=(Manufacturer::getNameById($marker.productObj->id_manufacturer))}
                                                    <div class="product-subtitle">{$subtitle.description_1}</div>
                                                    <div class="product-manufacturer">{$manufacturer_name}</div>
                                                    <a class="more-button"
                                                    href="{url entity=product id=$marker.productObj->id}">
                                                        <button class="btn btn-primary btn-inspire">{l s='przejdź do produktu' d='Modules.Sohoshopinspiracje.Shop'}</button>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                {/if}
                            {/foreach}
                        {/if}
                    </div>
                {/foreach}
            </div>
        </section>
    {/if}
{/block}