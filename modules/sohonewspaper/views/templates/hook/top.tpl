<div id="pappers-top" class="float-right hidden-sm-down">
    <button type="button" data-toggle="collapse" data-target="#collapsePappersTop" aria-expanded="false"
            aria-controls="collapsePappersTop" class="collapsePappersTop-btn">
        {l s='Gazetki, Katalogi, Okazje' mod='sohonewspaper'}
        {literal}
            <svg xmlns="http://www.w3.org/2000/svg" width="12.262" height="7.192" viewBox="0 0 12.262 7.192">
                <defs></defs>
                <g transform="translate(0.53 0.53)">
                    <path d="M-317.515-44.61l-5.6,5.6-5.6-5.6" transform="translate(328.717 44.61)"/>
                </g>
            </svg>
        {/literal}
    </button>
    <span class="overlay"></span>
    <div class="collapse papaers-top-content" id="collapsePappersTop">
        <div class="card card-body">
            <div class="col-lg-6 col-xs-12 paper">
                <p class="pappers-header">{l s='Katalogi' mod='sohonewspaper'}</p>
                <div class="pappers-container">
                {foreach from=$catalogs item=catalog name=catalogs}
                    {if $smarty.foreach.catalogs.iteration < 3}
                        <div class="col-md-6">
                            <img src="{$urls.base_url}modules/sohonewspapercatalog/img/{$catalog['file_name']}">
                            <a href="{$catalog['link']}" class="top-sohonewspaper-link"> 
                                {l s='Zobacz katalog' mod='sohonewspaper'}
                            </a>
                        </div>
                    {/if}
                {/foreach}
                </div>
            </div>
            <div class="col-lg-6 col-xs-12 catalog">
                <p class="pappers-header">{l s='Gazetki' mod='sohonewspaper'}</p>
                <div class="pappers-container">
                {foreach from=$papers item=paper name=papers}
                    {if $smarty.foreach.papers.iteration < 3}
                        <div class="col-md-6">
                            <img src="{$urls.base_url}modules/sohonewspapergazetki/img/{$paper['file_name']}">
                            <a href="{$paper['link']}" class="top-sohonewspaper-link">
                                {l s='Zobacz gazetkę' mod='sohonewspaper'}
                            </a>
                        </div>
                     {/if}
                {/foreach}
                </div>
            </div>
        </div>
        {if $banners}
            <div class="pappers-footer">   
                <p class="pappers-footer-header">{l s='Gazetki' mod='sohonewspaper'}</p>
                <ul class="pappers-footer-list">
                {foreach from=$banners item=element name=banners}
                    {if $smarty.foreach.banners.iteration < 3}
                        <li class="pappers-footer-item" style="background-image: url({$element.image});">
                            <a href="{$element.url}" class="pappers-footer-url">
                                <div class="pappers-footer-text">
                                    {$element.text nofilter}
                                    <button class="pappers-footer-btn btn">
                                    {l s='Odkryj teraz' mod='sohonewspaper'}
                                    <svg class="newspaper_arrow" xmlns="http://www.w3.org/2000/svg" width="14.262" height="9.192" viewBox="0 0 12.262 7.192">
                                            <defs></defs>
                                            <g transform="translate(0.53 0.53)">
                                                <path d="M-317.515-44.61l-5.6,5.6-5.6-5.6" transform="translate(328.717 44.61)"></path>
                                            </g>
                                        </svg>
                                    </button>
                                </div>
                            </a>
                        </li>
                     {/if}
                {/foreach}
                </ul>
            </div>    
        {/if}
    </div>
</div>