<?php

class SohonewspaperSingleModuleFrontController extends ModuleFrontController {
    public function initContent() {
        parent::initContent();

        $category = Tools::getValue('category');
        $id = Tools::getValue('id');
        $lang = $this->context->language->id;
        $content_data = $this->getData($category,$id,$lang);

        /* markery*/
        include_once _PS_MODULE_DIR_ . 'sohonewspapergazetki/sohonewspapergazetki.php';
        include_once _PS_MODULE_DIR_ . 'sohoshopProductMultipleDescriptions/classes/SohoshopProductAdditionalDescriptions.php'; // <- tutaj są pobierane dodatkowe dane produktu
        $imageMarkers = Sohonewspapergazetki::getGezetkaImageMarkers($id, true);
        /*-------------------*/

        $this->context->smarty->assign(
            array(
                'imageMarkers' => $imageMarkers,
                'single_content' => $content_data,
            ));

        $this->setTemplate("module:sohonewspaper/views/templates/front/single.tpl");
    }
    public function setMedia()
    {
        $this->registerStylesheet(
            "front-controller-sohonewspaper",
            'modules/' . $this->module->name . '/views/css/single.css',
            [
                'priority' => 999
            ]
        );
        return parent::setMedia();
    }
    public function getData($category,$id,$lang)
    {

        $arr = [];
        $sqlPost = 'SELECT `file_name`,`file_name_list` FROM `ps_'.$category.'` WHERE `id_'.$category.'`='.$id;
        $arr=Db::getInstance()->executeS($sqlPost);

        if($arr){
            $sqlPost2 = 'SELECT `title` FROM `ps_'.$category.'_lang` WHERE `id_'.$category.'`='.$id;
            $arr2=Db::getInstance()->executeS($sqlPost2);

            $arr[0]['title']=$arr2[0]['title'];

            $images = explode(",", $arr[0]['file_name_list']);

            foreach($images as $key=>$image) {
                $images[$key] = $this->getImageURL($image);
            }
            $arr[0]['file_name_list'] = $images;
            $arr[0]['file_name']=$this->getImageURL($arr[0]['file_name']);


        }
        return $arr;

    }

    private function getImageURL($image)
    {
        return $this->context->link->getMediaLink(__PS_BASE_URI__.'modules/'.Tools::getValue('category').'/img/'.$image);
    }


    public function getBreadcrumbLinks( ){

        $moduleBreadcrumb =  $this->l('Gazetka');
        $breadcrumb = parent::getBreadcrumbLinks();
            $breadcrumb['links'][] = [
            'title' => $this->getTranslator()->trans($moduleBreadcrumb, [], 'Breadcrumb'),
            'url' => $this->context->link->getModuleLink('sohonewspaper', 'paper')
        ];

        return $breadcrumb;
    }
}