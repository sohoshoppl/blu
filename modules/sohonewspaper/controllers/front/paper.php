<?php

class SohonewspaperPaperModuleFrontController extends ModuleFrontController
{
    public function initContent()
    {
       parent::initContent();


    //    $this->context->link->getModuleLink('sohonewspaper','single',array('id'=>$data->id));

       $lang = $this->context->language->id;
       $moduleBanners = $this->getListBanners($lang);
       foreach ($moduleBanners as &$element) {
            $element['image'] = $this->getImageURL($element['file_name']);
        }
        $getPapers = $this->getPapers($lang);
        $getCatalogs = $this->getCatalogs($lang);

       $this->context->smarty->assign(
            array(
              'moduleBanners' => $moduleBanners,
              'modulePapers' => $getPapers,
              'moduleCatalogs' => $getCatalogs,
              
        ));

        $this->setTemplate("module:sohonewspaper/views/templates/front/sohonewspaper.tpl");
    }
    private function getImageURL($image)
    {
        return $this->context->link->getMediaLink(__PS_BASE_URI__.'modules/'.$this->module->name.'/img/'.$image);
    }
    public function setMedia()
    {
        
        $this->registerStylesheet(
            "front-controller-sohonewspaper",
            'modules/' . $this->module->name . '/views/css/sohonewspaper.css',
            [
                'priority' => 999
            ]
        );
        return parent::setMedia();
    }

    public function getListBanners($id_lang)
    {
        return  Db::getInstance()->executeS('
            SELECT r.`id_sohonewspaper`, r.`id_shop`, r.`file_name`, rl.`text` , rl.`url`
            FROM `'._DB_PREFIX_.'sohonewspaper` r
            LEFT JOIN `'._DB_PREFIX_.'sohonewspaper_lang` rl ON (r.`id_sohonewspaper` = rl.`id_sohonewspaper`)
            WHERE `id_lang` = '.(int)$id_lang.' '.Shop::addSqlRestrictionOnLang());
    }

    public function getPapers($lang)
    {
        $sqlPost = 'SELECT * FROM `ps_sohonewspapergazetki_lang` WHERE `id_lang`='.$lang.'';
        $arr=Db::getInstance()->executeS($sqlPost);

        $sqlPost2 = 'SELECT * FROM `ps_sohonewspapergazetki`';
        $arr2=Db::getInstance()->executeS($sqlPost2);

        foreach($arr as $key=>$value) {
            $arr[$key]['file_name_list']= $arr2[$key]['file_name_list'];
            $arr[$key]['file_name']= $arr2[$key]['file_name'];
        }
        foreach($arr as $key2=>$value2) {
            $arr[$key2]['link']=$this->context->link->getModuleLink('sohonewspaper','single',array('category'=>'sohonewspapergazetki','id'=>$value2['id_sohonewspapergazetki'] ));
        }

        return $arr;
    }

    public function getCatalogs($lang)
    {
        $sqlPost = 'SELECT * FROM `ps_sohonewspapercatalog_lang` WHERE `id_lang`='.$lang.'';
        $arr=Db::getInstance()->executeS($sqlPost);

        $sqlPost2 = 'SELECT * FROM `ps_sohonewspapercatalog`';
        $arr2=Db::getInstance()->executeS($sqlPost2);

        foreach($arr as $key=>$value) {
            $arr[$key]['file_name_list']= $arr2[$key]['file_name_list'];
            $arr[$key]['file_name']= $arr2[$key]['file_name'];
        }
        foreach($arr as $key2=>$value2) {
            $arr[$key2]['link']=$this->context->link->getModuleLink('sohonewspaper','single',array('category'=>'sohonewspapercatalog','id'=>$value2['id_sohonewspapercatalog'] ));
        }
        return $arr;
    }

    public function getBreadcrumbLinks( ){

        $moduleBreadcrumb =  $this->l('Gazetka');
        $breadcrumb = parent::getBreadcrumbLinks();
            $breadcrumb['links'][] = [
            'title' => $this->getTranslator()->trans($moduleBreadcrumb, [], 'Breadcrumb'),
            'url' => $this->context->link->getModuleLink('sohonewspaper', 'paper')
        ];

        return $breadcrumb;
    }
}