{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{extends 'customer/page.tpl'}

{block name='page_content'}
	<div class="account-container  row">
        <div class="-left-col">
			{*include file='customer/links-menu.tpl'*}
			<div class="panel-links">
				<!-- account links -->
				{include file='customer/links-account.tpl' active=1 active_item='mywishlist'}
				{include file='module:sohoshopbuticzanka/views/templates/front/links-buticzanka.tpl'}
				{if isset($loginAsSeller) && $loginAsSeller}
				{include file='module:sohomarketplace/views/templates/front/links-seller.tpl'}
				{/if}
				{*include file='customer/links-close.tpl'*}
			</div>
		</div>
		<div class="-right-col">
			<div class="container-customer">
				<div class="row">
					<div class="hidden-sm-down col-md-2"></div>
					<div class="col-sm-12 col-md-8">
						<div class="row">
							{block name='page_title'}
							<div class="col-md-4 col-xs-12">
								<h1 >{l s='Listy zakupowe' d='Shop.Theme.Customeraccount'}</h1>
							</div>
							{/block}
							{if $id_customer|intval neq 0}
							<div class="col-md-8 col-xs-12 wishlist-add-top">
								<div class="wishlist-add-container row">
								<a href="#" id="make_new_wishlist" class="btn btn-secondary col-md-4 col-xl-3 float-right">{l s='Nowa lista życzeń' mod='blockwishlist'}</a>
									<form method="post" class="std col-md-8 col-xl-9" id="form_wishlist">
										<fieldset>
											<div class="row">
												<div class="col-xs-8 col-sm-8">
													<input type="hidden" name="token" value="{$token|escape:'html':'UTF-8'}" />
													<input placeholder="{l s='Nazwa' mod='blockwishlist'}" type="text" id="name" name="name" class="form-control" value="{if isset($smarty.post.name) and $errors|@count > 0}{$smarty.post.name|escape:'html':'UTF-8'}{/if}" />
												</div>
												<div class="col-xs-4 col-sm-4">
													<input type="submit" name="submitWishlist" id="submitWishlist" value="{l s='Save' mod='blockwishlist'}" class="btn btn-primary form-control-submit float-xs-left" />
												</div>
										</fieldset>
									</form>
									
								</div>
							</div>
							{/if}
						</div>
						{block name='page_content_top'}
						  {block name='customer_notifications'}
							{include file='_partials/notifications.tpl'}
						  {/block}
						{/block}
					</div>
					<div class="hidden-sm-down col-md-2"></div>
					<div class="col-xs-12">
						<div id="mywishlist">
							{if $id_customer|intval neq 0}
								{if $wishlists}
								<div id="block-history" class="block-center">
									<table class="table-customer-account">
										<thead>
											<tr>
												<th class="first_item">{l s='Nazwa' mod='blockwishlist'}</th>
												<th class="item mywishlist_second center display-none">{l s='Publiczna' mod='blockwishlist'}</th>
												<th class="item mywishlist_second right">{l s='Wartość' mod='blockwishlist'}</th>
												<th class="item mywishlist_first">&nbsp</th>
												<th class="item mywishlist_second display-none">{l s='Produkty' mod='blockwishlist'}</th>
												<th class="last_item mywishlist_first center">{l s='Dodaj do koszyka' mod='blockwishlist'}</th>
												<th class="last_item mywishlist_first center">&nbsp</th>
											</tr>
										</thead>
										<tbody>
										{section name=i loop=$wishlists}
											<tr id="wishlist_{$wishlists[i].id_wishlist|intval}">
												<td class="col-xs-6 wishlist-title" >
													<a href="javascript:;" onclick="javascript:WishlistManage('block-order-detail', '{$wishlists[i].id_wishlist|intval}');" >{$wishlists[i].name|truncate:30:'...'|escape:'html':'UTF-8'}</a>
												</td>
												<td class="wishlist_default center display-none">
													{if isset($wishlists[i].default) && $wishlists[i].default == 1}
														<p class="is_wish_list_default">
															<i class="icon icon-check-square"></i>
														</p>
													{else}
														<a href="#" onclick="javascript:event.preventDefault();(WishlistDefault('wishlist_{$wishlists[i].id_wishlist|intval}', '{$wishlists[i].id_wishlist|intval}'));">
															<i class="icon icon-square"></i>
														</a>
													{/if}
												</td>
												<td class="right col-xs-6">
												   <strong>{Tools::displayPrice($wishlists[i].products_total_value)}</strong>
												</td>
												<td class="bold center display-none">
													{assign var=n value=0}
													{foreach from=$nbProducts item=nb name=i}
														{if $nb.id_wishlist eq $wishlists[i].id_wishlist}
															{assign var=n value=$nb.nbProducts|intval}
														{/if}
													{/foreach}
													{if $n}
														{$n|intval}
													{else}
														0
													{/if}
												</td>
												<td class="more-btn">
													<a class="btn btn-secondary btn-blue-empty more" href="javascript:;" onclick="javascript:WishlistManage(this, '{$wishlists[i].id_wishlist|intval}');">{l s='View' mod='blockwishlist'}</a>
													<a class="btn btn-secondary btn-blue-empty less" href="javascript:;" onclick="javascript:WishlistClose(this, '{$wishlists[i].id_wishlist|intval}');">{l s='Ukryj' mod='blockwishlist'}</a>
												</td>
												<td class="center col-xs-6 mywishlist-add-delete">
													{if $wishlists[i].count|intval}
														<a class="reorder" href="#" onclick="WishlistAddWishlistCart({$wishlists[i].id_wishlist|intval})"><i class="fas fa-shopping-bag shopping-cart"></i></a>
													{else}
														<a class="reorder disable" href="#"><i class="fas fa-shopping-bag shopping-cart"></i></a>
													{/if}
												</td>
												<td class="wishlist_delete center col-xs-6 mywishlist-add-delete">
													<a href="javascript:;"onclick="return (WishlistDelete('wishlist_{$wishlists[i].id_wishlist|intval}', '{$wishlists[i].id_wishlist|intval}', '{l s='Do you really want to delete this wishlist ?' mod='blockwishlist' js=1}'));"><i class="far fa-trash-alt"></i></a>
												</td>
											</tr>
											<tr id="block-order-detail_{$wishlists[i].id_wishlist|intval}" class="block-order-detail">

											</tr>
										{/section}
										</tbody>
									</table>
								</div>
								<div id="block-order-detail">&nbsp;</div>
								{/if}
							{/if}
						</div>						
					</div>
					
					<div class="clearfix"></div>
					<div class="hidden-sm-down col-md-2"></div>
					<div class="col-md-8">
						{*if $id_customer|intval neq 0}
						<footer class="form-footer clearfix">
							<a href="#" id="make_new_wishlist" class="btn btn-secondary">{l s='Nowa lista życzeń' mod='blockwishlist'}</a>
							<form method="post" class="std" id="form_wishlist">
								<fieldset>
									<div class="row">
										<div class="col-xs-12 col-sm-8">
											<input type="hidden" name="token" value="{$token|escape:'html':'UTF-8'}" />
											<input placeholder="{l s='Nazwa' mod='blockwishlist'}" type="text" id="name" name="name" class="form-control" value="{if isset($smarty.post.name) and $errors|@count > 0}{$smarty.post.name|escape:'html':'UTF-8'}{/if}" />
										</div>
										<div class="col-xs-12 col-sm-4">
											<input type="submit" name="submitWishlist" id="submitWishlist" value="{l s='Save' mod='blockwishlist'}" class="btn btn-primary form-control-submit float-xs-left" />
										</div>
								</fieldset>
							</form>
						</footer>
						{/if*}
					</div>
					<div class="hidden-sm-down col-md-2"></div>
				</div>
			</div>
		</div>
	</div>
{/block}

{block name='page_footer'}
  {block name='my_account_links'}
  {/block}
{/block}


