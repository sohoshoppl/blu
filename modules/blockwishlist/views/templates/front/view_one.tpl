{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{extends 'customer/page.tpl'}

{block name='head' append}
	<meta property="og:type" content="product.group">
	<meta property="og:url" content="{$urls.current_url}">
	<meta property="og:title" content="{l s='Lista zakupowa' mod='blockwishlist'}">
	<meta property="og:site_name" content="{$shop.name}">
	<meta property="og:description" content="{l s='Polecane produkty z listy zakupowej' mod='blockwishlist'}">
	<meta property="og:image" content="{$urls.shop_domain_url}{$shop.logo}">
{/block}

{block name='page_content'}
<div id="view_wishlist">
    <h1>
        {l s='Lista zakupowa' mod='blockwishlist'}
    </h1>
    {if $products|count}
    <div class="products">
		<div class="row">
		{foreach from=$products item="product"}
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
			{block name='product_miniature'}
				{include file='catalog/_partials/miniatures/product.tpl' product=$product}
			{/block}
			</div>
		{/foreach}
		</div>
	</div>
	{else}
		<article class="alert alert-info" role="alert">
			{l s='Dla tej listy zakupowej nie przypisano jeszcze produktów.' mod='blockwishlist'}
		</article>
	{/if}
</div> <!-- #view_wishlist -->
{/block}

{block name='page_footer'}
  {block name='my_account_links'}
  {/block}
{/block}