{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{assign var="wl_count" value=0}
{if $wishlist_products}
        
        {foreach from=$wishlist_products item="w_product"}
                {assign var="wl_count" value=$wl_count + $w_product.quantity }
        {/foreach}

{/if}
<div id="wishlist-top" class="float-right ">
	<a class="header-icon {if $wl_count > 0}in{/if}" href="{url entity='module' name='blockwishlist' controller='mywishlist'}" title="{l s='Ulubione' mod='blockwishlist'}"><i class="fas fa-heart"><span class="wl-count-header">{if $wl_count > 0}{$wl_count}{/if}</span></i> <span class="header-icon-txt">{l s='Ulubione' mod='blockwishlist'}</span></a>
</div>
{strip}
    <script>
        static_token = prestashop.static_token;
        wishlistProductsIds={$wishlist_products|json_encode nofilter};
        loggin_required = '{l s='Musisz być zalogowany aby zarządzać Ulubionymi'  mod='blockwishlist'}';
        added_to_wishlist = '{l s='Produkt został dodany Ulubionych' mod='blockwishlist'}';
        mywishlist_url = '{$link->getModuleLink('blockwishlist', 'mywishlist', array(), true)|escape:'quotes':'UTF-8'}';
        baseDir = '{$urls.base_url}';
{if $customer.is_logged}
	isLoggedWishlist=true;
        isLogged = true;
{else}
	isLoggedWishlist=false;
        isLogged = false;
{/if}        
    </script>    

{/strip}
