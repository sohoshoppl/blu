{*
* 2015
*
* NOTICE OF LICENSE
*
*  @author Sosho.pl <biuro@sosho.pl>
*  @copyright  2014 Sosho.pl
*}
<div id="soshohomecategories" class="swiper-container">
    <div class="swiper-wrapper">
    {foreach from=$headcategories item=subcat2 name=subcat2}
        <div class="hc_block col-col-md-6 col-lg-4 swiper-slide">
            <a class="image" href="{$link->getCategoryLink($subcat2.id_category)|escape:'html':'UTF-8'}">
                <img alt="{$subcat2.name|lower|ucfirst}" title="{$subcat2.name|lower|ucfirst}"
                     class="img-responsive" src="/img/c/{$subcat2.id_category}-category_default.jpg"
            </a>
            <div class="hc_block_title">
                <h2>
                    <a href="{$link->getCategoryLink($subcat2.id_category)|escape:'html':'UTF-8'}">{$subcat2.name}</a>
                </h2>
            </div>
        </div>
    {/foreach}
    </div>
    <div class="swiper-scrollbar"></div>
</div>