<?php


if (!defined('_CAN_LOAD_FILES_'))
    exit;

class SohoshopHomeCategories extends Module
{
    public $Products;

    public function __construct()
    {
        $this->name = 'sohoshophomecategories';
        $this->tab = 'front_office_features';
        $this->version = '1.0';
        $this->author = 'sosho.pl';

        $this->bootstrap = true;
        parent::__construct();

        $this->displayName = $this->l('Sosho - home categories');
        $this->description = $this->l('Sosho - kategorie z miniaturkami na głównej');
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }

    public function install()
    {
        if (
            !parent::install() ||
            !$this->registerHook('displayHome') ||
            !Configuration::updateValue('SOHOSHOPHOMECATEGORIES_LIST', '')
        )
            return false;
        return true;
    }

    public function uninstall()
    {

        if (
            !parent::uninstall() ||
            !Configuration::deleteByName('SOHOSHOPHOMECATEGORIES_LIST')
        )
            return false;
        return true;
    }

    public function psversion()
    {
        $version = _PS_VERSION_;
        $exp = $explode = explode(".", $version);

        return $exp[1];
    }

    public function getContent()
    {

        $html = '';

        if (Tools::isSubmit('settings')) {
            Configuration::updateValue('SOHOSHOPHOMECATEGORIES_LIST', Tools::getValue('sohoshophomecategories_list'));
            $html .= $this->displayConfirmation($this->l('Zapisano ustawienia dla modułu'));
        }

        $html .= $this->renderForm();

        return $html;
    }

    private function renderForm()
    {
        $fields_form[] = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Ustawienia'),
                    'icon' => 'icon-cogs'
                ),
                'input' => array(
                    array(
                        'type' => 'text',
                        'label' => $this->l('Kategorie na stronie głównej'),
                        'name' => 'sohoshophomecategories_list',
                        'desc' => $this->l('ID kategori, oddzielonych przecinkiem')
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Zapisz ustawienia'),
                    'name' => $this->l('settings')
                )
            ),
        );

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $this->fields_form = array();

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false) . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        return $helper->generateForm($fields_form);
    }

    public function getConfigFieldsValues()
    {
        return array(
            'sohoshophomecategories_list' => Tools::getValue('sohoshophomecategories_list', Configuration::get('SOHOSHOPHOMECATEGORIES_LIST'))
        );
    }

    private function getSelectedCategories()
    {
        $selectedCategories = Configuration::get('SOHOSHOPHOMECATEGORIES_LIST');

        if($selectedCategories){
            return Db::getInstance()->executeS('
			SELECT `id_category`, `name`
			FROM `' . _DB_PREFIX_ . 'category_lang`
			WHERE `id_category` IN (' .$selectedCategories . ')
			AND `id_lang` = ' . (int)$this->context->language->id . '
			ORDER BY FIELD(`id_category`, ' . $selectedCategories . ')
		');
        }

    }

    public function hookdisplayHome($params)
    {
        if (isset($this->context->controller->php_self) && $this->context->controller->php_self == 'index') {
            $categories = $this->getSelectedCategories();
            $this->context->smarty->assign(array(
                'thumbsize' => Image::getSize('category_home'),
                'headcategories' => $categories,
            ));

            return $this->display(__FILE__, 'sohoshophomecategories.tpl');
        }
    }

}

?>