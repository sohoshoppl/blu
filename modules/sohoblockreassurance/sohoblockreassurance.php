<?php
/*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA

*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_CAN_LOAD_FILES_')) {
    exit;
}

use PrestaShop\PrestaShop\Core\Module\WidgetInterface;

include_once _PS_MODULE_DIR_.'sohoblockreassurance/sohoreassuranceClass.php';

class Sohoblockreassurance extends Module implements WidgetInterface
{
    private $templateFile;

    public function __construct()
    {
        $this->name = 'sohoblockreassurance';
        $this->author = 'soho_shop';
        $this->version = '3.0.1';

        $this->bootstrap = true;
        parent::__construct();

        $this->displayName = $this->trans('sohoblockreassurance', array(), 'Modules.Sohoblockreassurance.Admin');
        $this->description = $this->trans('Adds an information block aimed at offering helpful information to reassure customers that your store is trustworthy.', array(), 'Modules.Sohoblockreassurance.Admin');

        $this->ps_versions_compliancy = array('min' => '1.7.2.0', 'max' => _PS_VERSION_);

        $this->templateFile = 'module:sohoblockreassurance/views/templates/hook/sohoblockreassurance.tpl';
    }

    public function install()
    {
        return parent::install()
            && $this->installDB()
            && Configuration::updateValue('SOHOBLOCKREASSURANCE_NBBLOCKS', 5)
            && $this->installFixtures()
            && $this->registerHook('displayOrderConfirmation2')
            && $this->registerHook('actionUpdateLangAfter')
        ;
    }

    public function installDB()
    {
        $return = true;
        $return &= Db::getInstance()->execute('
            CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'sohoreassurance` (
                `id_sohoreassurance` INT UNSIGNED NOT NULL AUTO_INCREMENT,
                `id_shop` int(10) unsigned NOT NULL ,
                `id_category` int(10) unsigned NOT NULL ,
                `file_name` VARCHAR(100) NOT NULL,
                PRIMARY KEY (`id_sohoreassurance`)
            ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 ;');

        $return &= Db::getInstance()->execute('
            CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'sohoreassurance_lang` (
                `id_sohoreassurance` INT UNSIGNED NOT NULL AUTO_INCREMENT,
                `id_lang` int(10) unsigned NOT NULL ,
                `text` text NOT NULL,
                PRIMARY KEY (`id_sohoreassurance`, `id_lang`)
            ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 ;');

        return $return;
    }

    public function uninstall()
    {
        return Configuration::deleteByName('SOHOBLOCKREASSURANCE_NBBLOCKS') &&
            $this->uninstallDB() &&
            parent::uninstall();
    }

    public function uninstallDB()
    {
        return Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'sohoreassurance`') && Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'sohoreassurance_lang`');
    }

    public function addToDB()
    {
        if (isset($_POST['nbblocks'])) {
            for ($i = 1; $i <= (int)$_POST['nbblocks']; $i++) {
                $filename = explode('.', $_FILES['info'.$i.'_file']['name']);
                if (isset($_FILES['info'.$i.'_file']) && isset($_FILES['info'.$i.'_file']['tmp_name']) && !empty($_FILES['info'.$i.'_file']['tmp_name'])) {
                    if ($error = ImageManager::validateUpload($_FILES['info'.$i.'_file'])) {
                        return false;
                    } elseif (!($tmpName = tempnam(_PS_TMP_IMG_DIR_, 'PS')) || !move_uploaded_file($_FILES['info'.$i.'_file']['tmp_name'], $tmpName)) {
                        return false;
                    } elseif (!ImageManager::resize($tmpName, dirname(__FILE__).'/img/'.$filename[0].'.jpg')) {
                        return false;
                    }
                    unlink($tmpName);
                }
                Db::getInstance()->execute('INSERT INTO `'._DB_PREFIX_.'sohoreassurance` (`filename`,`text`, `id_category`)
                                            VALUES ("'.((isset($filename[0]) && $filename[0] != '') ? pSQL($filename[0]) : '').
                    '", "'.((isset($_POST['info'.$i.'_text']) && $_POST['info'.$i.'_text'] != '') ? pSQL($_POST['info'.$i.'_text']) : '').((isset($_POST['info'.$i.'_id_category']) && $_POST['info'.$i.'_id_category'] != '') ? pSQL($_POST['info'.$i.'_id_category']) : '').'")');
            }
            return true;
        } else {
            return false;
        }
    }

    public function removeFromDB()
    {
        $dir = opendir(dirname(__FILE__).'/img');
        while (false !== ($file = readdir($dir))) {
            $path = dirname(__FILE__).'/img/'.$file;
            if ($file != '..' && $file != '.' && !is_dir($file)) {
                unlink($path);
            }
        }
        closedir($dir);

        return Db::getInstance()->execute('DELETE FROM `'._DB_PREFIX_.'sohoreassurance`');
    }

    public function hookActionUpdateLangAfter($params)
    {
        if (!empty($params['lang']) && $params['lang'] instanceOf Language) {
            include_once _PS_MODULE_DIR_ . $this->name . '/lang/SohoReassuranceLang.php';

            Language::updateMultilangFromClass(_DB_PREFIX_ . 'sohoreassurance_lang', 'SohoReassuranceLang', $params['lang']);
        }
    }

    public function getContent()
    {
        $html = '';
        $id_sohoreassurance = (int)Tools::getValue('id_sohoreassurance');
        if (Tools::isSubmit('savesohoblockreassurance')) {
            if ($id_sohoreassurance = Tools::getValue('id_sohoreassurance')) {
                $reassurance = new sohoreassuranceClass((int)$id_sohoreassurance);
            } else {
                $reassurance = new sohoreassuranceClass();
            }

            $reassurance->copyFromPost();
            $reassurance->id_shop = $this->context->shop->id;

            if ($reassurance->validateFields(false) && $reassurance->validateFieldsLang(false)) {
                $reassurance->save();

                if (isset($_FILES['image']) && isset($_FILES['image']['tmp_name']) && !empty($_FILES['image']['tmp_name'])) {
                    if ($error = ImageManager::validateUpload($_FILES['image'])) {
                        return false;
                    } elseif (!($tmpName = tempnam(_PS_TMP_IMG_DIR_, 'PS')) || !move_uploaded_file($_FILES['image']['tmp_name'], $tmpName)) {
                        return false;
                    } elseif (!ImageManager::resize($tmpName, dirname(__FILE__).'/img/reassurance-'.(int)$reassurance->id.'-'.(int)$reassurance->id_shop.'.jpg')) {
                        return false;
                    }

                    unlink($tmpName);
                    $reassurance->file_name = 'reassurance-'.(int)$reassurance->id.'-'.(int)$reassurance->id_shop.'.jpg';
                    $reassurance->save();
                }
                $this->_clearCache('*');
            } else {
                $html .= '<div class="conf error">'.$this->trans('An error occurred while attempting to save.', array(), 'Admin.Notifications.Error').'</div>';
            }
        }

        if (Tools::isSubmit('updatesohoblockreassurance') || Tools::isSubmit('addsohoblockreassurance')) {
            $helper = $this->initForm();
            foreach (Language::getLanguages(false) as $lang) {
                if ($id_sohoreassurance) {
                    $reassurance = new sohoreassuranceClass((int)$id_sohoreassurance);
                    $helper->fields_value['text'][(int)$lang['id_lang']] = $reassurance->text[(int)$lang['id_lang']];
                    $helper->fields_value['id_category'] = $reassurance->id_category;

                    $image = dirname(__FILE__).DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.$reassurance->file_name;
                    $this->fields_form[0]['form']['input'][0]['image'] = '<img src="'.$this->getImageURL($reassurance->file_name).'" />';
                } else {
                    $helper->fields_value['text'][(int)$lang['id_lang']] = Tools::getValue('text_'.(int)$lang['id_lang'], '');
                }
            }
            if ($id_sohoreassurance = Tools::getValue('id_sohoreassurance')) {
                $this->fields_form[0]['form']['input'][] = array('type' => 'hidden', 'name' => 'id_sohoreassurance');
                $helper->fields_value['id_sohoreassurance'] = (int)$id_sohoreassurance;
            }

            return $html.$helper->generateForm($this->fields_form);
        } elseif (Tools::isSubmit('deletesohoblockreassurance')) {
            $reassurance = new sohoreassuranceClass((int)$id_sohoreassurance);
            if (file_exists(dirname(__FILE__).'/img/'.$reassurance->file_name)) {
                unlink(dirname(__FILE__).'/img/'.$reassurance->file_name);
            }
            $reassurance->delete();
            $this->_clearCache('*');
            Tools::redirectAdmin(AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'));
        } else {
            $content = $this->getListContent((int)Configuration::get('PS_LANG_DEFAULT'));
            $helper = $this->initList();
            $helper->listTotal = count($content);
            return $html.$helper->generateList($content, $this->fields_list);
        }

        if (isset($_POST['submitModule'])) {
            Configuration::updateValue('SOHOBLOCKREASSURANCE_NBBLOCKS', ((isset($_POST['nbblocks']) && $_POST['nbblocks'] != '') ? (int)$_POST['nbblocks'] : ''));
            if ($this->removeFromDB() && $this->addToDB()) {
                $this->_clearCache('sohoblockreassurance.tpl');
                $output = '<div class="conf confirm">'.$this->trans('The block configuration has been updated.', array(), 'Modules.Sohoblockreassurance.Admin').'</div>';
            } else {
                $output = '<div class="conf error"><img src="../img/admin/disabled.gif"/>'.$this->trans('An error occurred while attempting to save.', array(), 'Admin.Notifications.Error').'</div>';
            }
        }
    }

    protected function getListContent($id_lang)
    { 
        return  Db::getInstance()->executeS('
            SELECT r.`id_sohoreassurance`, r.`id_category` , r.`id_shop`, r.`file_name`, rl.`text`
            FROM `'._DB_PREFIX_.'sohoreassurance` r
            LEFT JOIN `'._DB_PREFIX_.'sohoreassurance_lang` rl ON (r.`id_sohoreassurance` = rl.`id_sohoreassurance`)
            WHERE `id_lang` = '.(int)$id_lang.' '.Shop::addSqlRestrictionOnLang());
    }

    protected function initForm()
    {
        $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');

        $this->fields_form[0]['form'] = array(
            'legend' => array(
                'title' => $this->trans('New reassurance block', array(), 'Modules.Sohoblockreassurance.Admin'),
            ),
            'input' => array(
                array(
                    'type' => 'file',
                    'label' => $this->trans('Image', array(), 'Admin.Global'),
                    'name' => 'image',
                    'value' => true,
                    'display_image' => true,
                ),
                array(
                    'type' => 'textarea',
                    'label' => $this->trans('Text', array(), 'Admin.Global'),
                    'lang' => true,
                    'name' => 'text',
                    'class' => 'rte',
                    'autoload_rte' => true,
                    'cols' => 40,
                    'rows' => 10
                ),
                array(
                    'type' => 'text',
                    'label' => $this->trans('Category Id', array(), 'Modules.Sohoblockreassurance.Admin'),
                    'name' => 'id_category',
                    'class' => 'fixed-width-xs',
                    'desc' => $this->trans('Set the Category Id here', array(), 'Modules.Sohoblockreassurance.Admin'),
                ),
            ),
            'submit' => array(
                'title' => $this->trans('Save', array(), 'Admin.Actions'),
            )
        );

        $helper = new HelperForm();
        $helper->module = $this;
        $helper->name_controller = 'sohoblockreassurance';
        $helper->identifier = $this->identifier;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        foreach (Language::getLanguages(false) as $lang) {
            $helper->languages[] = array(
                'id_lang' => $lang['id_lang'],
                'iso_code' => $lang['iso_code'],
                'name' => $lang['name'],
                'is_default' => ($default_lang == $lang['id_lang'] ? 1 : 0)
            );
        }

        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;
        $helper->toolbar_scroll = true;
        $helper->title = $this->displayName;
        $helper->submit_action = 'savesohoblockreassurance';
        $helper->toolbar_btn =  array(
            'save' =>
            array(
                'desc' => $this->trans('Save', array(), 'Admin.Actions'),
                'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'),
            ),
            'back' =>
            array(
                'href' => AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'),
                'desc' => $this->trans('Back to list', array(), 'Admin.Actions'),
            )
        );
        return $helper;
    }

    protected function initList()
    {
        $this->fields_list = array(
            'id_sohoreassurance' => array(
                'title' => $this->trans('ID', array(), 'Admin.Global'),
                'width' => 120,
                'type' => 'text',
                'search' => false,
                'orderby' => false
            ),
            'text' => array(
                'title' => $this->trans('Text', array(), 'Admin.Global'),
                'width' => 140,
                'type' => 'text',
                'search' => false,
                'orderby' => false
            ),
        );

        if (Shop::isFeatureActive()) {
            $this->fields_list['id_shop'] = array(
                'title' => $this->trans('ID Shop', array(), 'Modules.Sohoblockreassurance.Admin'),
                'align' => 'center',
                'width' => 25,
                'type' => 'int'
            );
        }

        $helper = new HelperList();
        $helper->shopLinkType = '';
        $helper->simple_header = false;
        $helper->identifier = 'id_sohoreassurance';
        $helper->actions = array('edit', 'delete');
        $helper->show_toolbar = true;
        $helper->imageType = 'jpg';
        $helper->toolbar_btn['new'] =  array(
            'href' => AdminController::$currentIndex.'&configure='.$this->name.'&add'.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'),
            'desc' => $this->trans('Add new', array(), 'Admin.Actions')
        );

        $helper->title = $this->displayName;
        $helper->table = $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        return $helper;
    }

    protected function _clearCache($template, $cacheId = null, $compileId = null)
    {
        parent::_clearCache($this->templateFile);
    }

    public function renderWidget($hookName = null, array $configuration = [])
    {
        if (!$this->isCached($this->templateFile, $this->getCacheId('sohoblockreassurance'))) {
            $this->smarty->assign($this->getWidgetVariables($hookName, $configuration));
        }

        return $this->fetch($this->templateFile, $this->getCacheId('sohoblockreassurance'));
    }

    public function getWidgetVariables($hookName = null, array $configuration = [])
    {
        $elements = $this->getListContent($this->context->language->id);
        foreach ($elements as &$element) {
            $element['image'] = $this->getImageURL($element['file_name']);
            $element['id_category'] = $element['id_category'];
        }
        return array(
            'elements' => $elements,
        );
    }

    public function installFixtures()
    {
        $return = true;
        $tab_texts = array(
            array('text' => $this->trans('Security policy (edit with Customer reassurance module)', array(), 'Modules.Sohoblockreassurance.Shop'), 'file_name' => 'ic_verified_user_black_36dp_1x.png'),
            array('text' => $this->trans('Delivery policy (edit with Customer reassurance module)', array(), 'Modules.Sohoblockreassurance.Shop'), 'file_name' => 'ic_local_shipping_black_36dp_1x.png'),
            array('text' => $this->trans('Return policy (edit with Customer reassurance module)', array(), 'Modules.Sohoblockreassurance.Shop'), 'file_name' => 'ic_swap_horiz_black_36dp_1x.png'),
        );

        foreach ($tab_texts as $tab) {
            $reassurance = new sohoreassuranceClass();
            foreach (Language::getLanguages(false) as $lang) {
                $reassurance->text[$lang['id_lang']] = $tab['text'];
            }
            $reassurance->file_name = $tab['file_name'];
            $reassurance->id_shop = $this->context->shop->id;
            $return &= $reassurance->save();
        }
        return $return;
    }

    private function getImageURL($image)
    {
        return $this->context->link->getMediaLink(__PS_BASE_URI__.'modules/'.$this->name.'/img/'.$image);
    }
}
