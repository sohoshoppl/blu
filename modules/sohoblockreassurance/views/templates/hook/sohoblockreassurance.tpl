{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{if $elements}
  {foreach from=$elements item=element}
    {if in_array($element.id_category, Product::getProductCategories($product->id|intval))}
      <div id="block-reassurance-2">
          <p class="block-reassurance-header">{l s='Zaplanuj swój remont' mod='blockreassurance'}</p>
          <p class="block-reassurance-semiheader">{l s='Kupując ten produkt weź pod uwagę:' mod='blockreassurance'}</p> 
        <ul class="block-reassurance-items">
          {foreach from=$elements item=element}
            {if in_array($element.id_category, Product::getProductCategories($product->id|intval))}
              <li class="col-md-4 block-reassurance-item">
                <div class="block-reassurance-img">
                  <img class="reassurance-img" src="{$element.image}" alt="{$element.text|strip_tags}" /> 
                </div>
                <div class="block-reassurance-text">
                  {$element.text nofilter}
                </div>
              </li>
            {/if}
          {/foreach}
        </ul>
      </div>
      {break}
    {/if}
  {/foreach}
{/if}
