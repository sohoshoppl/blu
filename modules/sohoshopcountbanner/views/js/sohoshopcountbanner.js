$(document).ready(function () {

    if (typeof dateTo !== 'undefined') {
        var toDate = new Date(dateTo.replace(' ', 'T')).getTime();

        var x = window.setInterval(function () {
            var now = new Date().getTime();
            var distance = toDate - now;

            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));

            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);

            document.querySelector("#sohoshopcountbanner .days .value").innerHTML = (days > 0 ? days + "d " : "0");
            document.querySelector("#sohoshopcountbanner .hours .value").innerHTML = (hours > 0 ? (hours < 10 ? "0" : "") + hours : "00");
            document.querySelector("#sohoshopcountbanner .mins .value").innerHTML = (minutes > 0 ? (minutes < 10 ? "0" : "") + minutes : "00");
            document.querySelector("#sohoshopcountbanner .secs .value").innerHTML = (seconds < 10 ? "0" : "") + seconds;

            if (distance < 0) {
                clearInterval(x);
                document.getElementById("couter").innerHTML = " EXPIRED ";
            }
        }, 1000);

        window.onload = function () {
            document.querySelector('#sohoshopcountbanner').classList.add('active');
        }
    }
})