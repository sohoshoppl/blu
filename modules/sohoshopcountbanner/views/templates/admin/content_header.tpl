{*
* SohoshopSynch module
 *
 * @author    SohoSHOP
 * @copyright Copyright (c) 2017 SohoSHOP
 * @license   http://sohoshop.pl
 *
 * http://sohoshop.pl
*}

<fieldset id="content_header" class="panel">
	<a target="_blank" href="{$smarty.const._DPDPOLAND_CONTENT_HEADER_URL_|escape:'htmlall':'UTF-8'}" title="{$module_display_name|escape:'htmlall':'UTF-8'}">
		<img style="float: left; margin: 0 30px 0 0;" src="{$module_dir|escape:'htmlall':'UTF-8'}logo.png" alt="Sohoshop" />
	</a>
	<p style="font-size: 34px !important;font-weight: 600;color: #4a5666;">SOHOShop.pl</p>
	<p>{l s='Jesteśmy' mod='sohoshopsynch'} <strong>{l s='certyfikowanym partnerem PrestaShop.' mod='sohoshopsynch'}</strong> {l s='Dlatego gwarantujemy dostarczyć platformę z wizerunkową, technologiczną i użytkową przewagą nad konkurencją i pomożemy wyróżnić się na rynku!' mod='sohoshopsynch'}</p>
	<p>{l s='Doświadczenie SOHOshop w zakresie oprogramowania PrestaShop pozwala nam realizować projekty wyróżniające się na tle europejskich realizacji opartych na tej platformie handlowej.' mod='sohoshopsynch'}</p>
	<p>{l s='Po więcej informacji zapraszamy do kontaktu na' mod='sohoshopsynch'} <a style="color:#b83b5f" href="mailto:biuro@sohoshop.pl">biuro@sohoshop.pl</a></p>
</fieldset>