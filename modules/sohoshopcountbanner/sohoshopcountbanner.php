<?php
/**
 * SohoshopCountbanner module
 *
 * @author    SohoSHOP
 * @copyright Copyright (c) 2018 SohoSHOP
 * @license   http://SohoSHOP.pl
 *
 * http://SohoSHOP.pl
 */
 
if (!defined('_PS_VERSION_')) {
    exit;
}

require_once (dirname(__FILE__) . '/classes/SohoshopCountbannerDB.php');

class SohoshopCountbanner extends Module
{
	const INSTALL_SQL_FILE = 'install.sql';
	
	protected $_html = '';
	
	public function __construct()
    {
        $this->name = 'sohoshopcountbanner';
        $this->version = '1.0.0';
        $this->author = 'SohoSHOP.pl';
		$this->module_dir = dirname(__FILE__).'/';
		$this->module_dir_images = '/modules/'.$this->name.'/images/';
		$this->need_upgrade = true;
        $this->secure_key = Tools::encrypt($this->name);
		$this->bootstrap = true;
		
        parent::__construct();

        $this->displayName = $this->trans('SohoSHOP Count Banner', array(), 'Modules.SohoshopCountbanner.Admin');
        $this->description = $this->trans('Umieść banner z informacją o promocjach lub kodem rabatowym z odliczaniem.', array(), 'Modules.SohoshopCountbanner.Admin');

        $this->templateFile = 'module:sohoshopcountbanner/views/templates/hook/sohoshopcountbanner.tpl';
    }
	
	public function install()
    {		
		if (!file_exists(dirname(__FILE__).'/'.self::INSTALL_SQL_FILE))
			return (false);
		else if (!$sql = file_get_contents(dirname(__FILE__).'/'.self::INSTALL_SQL_FILE))
			return (false);
		$sql = str_replace(array('PREFIX_', 'ENGINE_TYPE'), array(_DB_PREFIX_, _MYSQL_ENGINE_), $sql);
		$sql = preg_split("/;\s*[\r\n]+/", $sql);
		foreach ($sql as $query)
			if ($query)
				if (!Db::getInstance()->execute(trim($query)))
					return false;
		
        if (
			!parent::install() ||
			!$this->registerHook('displayBanner') ||
			!$this->registerHook('header') ||
			!Configuration::updateValue('SOHOSHOPCOUNTBANNER_DISPLAY', 0) ||
			!Configuration::updateValue('SOHOSHOPCOUNTBANNER_COUNTER', 1)
		)
			return false;
			
		return true;
    }
	
	public function uninstall()
	{
		if (
			!parent::uninstall() ||
			!$this->deleteTables() ||
			!Configuration::deleteByName('SOHOSHOPCOUNTBANNER_DISPLAY') ||
			!Configuration::deleteByName('SOHOSHOPCOUNTBANNER_COUNTER')
		)
			return false;

		return true;
	}
	
	private function deleteTables()
	{
		return Db::getInstance()->execute(
			'DROP TABLE IF EXISTS
			`'._DB_PREFIX_.'sohoshopcountbanner`'
		);
	}
	
	public function getContent()
	{
		$this->_html .= '';
		
		$module_url = Tools::getProtocol().$_SERVER['HTTP_HOST'].$this->getPathUri();
		
		if (Tools::isSubmit('btnSubmit'))
		{
            Configuration::updateValue('SOHOSHOPCOUNTBANNER_DISPLAY', Tools::getValue('sohoshopcountbanner_display'));
            Configuration::updateValue('SOHOSHOPCOUNTBANNER_COUNTER', Tools::getValue('sohoshopcountbanner_counter'));
			
			$languages = Language::getLanguages(false);
			$id_shop = $this->context->shop->id;
			
			foreach ($languages as $lang)
			{
				$bannerid = SohoshopCountbannerDB::getBanners($id_shop, (int)$lang['id_lang']);
				if ($bannerid['id_sohoshopcountbanner'] > 0)
				{
					$banner = new SohoshopCountbannerDB($bannerid['id_sohoshopcountbanner']);
					$banner->content = Tools::getValue('sohoshopcountbanner_content_' . $lang['id_lang']);
					$banner->timefrom = Tools::getValue('sohoshopcountbanner_timefrom');
					$banner->timeto = Tools::getValue('sohoshopcountbanner_timeto');
					$banner->update();
				}
				else
				{
					$banner = new SohoshopCountbannerDB();
					$banner->content = Tools::getValue('sohoshopcountbanner_content_' . $lang['id_lang']);
					$banner->timefrom = Tools::getValue('sohoshopcountbanner_timefrom');
					$banner->timeto = Tools::getValue('sohoshopcountbanner_timeto');
					$banner->id_lang = (int)$lang['id_lang'];
					$banner->id_shop = $id_shop;
					$banner->add();
				}
			}
			
			$this->_html .= $this->displayConfirmation($this->trans('Settings updated.', array(), 'Admin.Notifications.Success'));
        }
		
		$this->_html .= $this->getContentHeader();
		$this->_html .= $this->renderForm();
	
		return $this->_html;  
	}
	
	private function getContentHeader()
	{
		$this->context->smarty->assign(array(
			'module_display_name' => $this->displayName,
			'module_dir' => $this->module_dir_images
		));

		return $this->context->smarty->fetch($this->module_dir.'views/templates/admin/content_header.tpl');
	}
	
	public function renderForm()
    {
        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->trans('Ustawienia', array(), 'Modules.SohoshopCountbanner.Admin'),
                    'icon' => 'icon-cogs'
                ),
                'input' => array(
					array(
						'type' => 'switch',
						'label' => $this->trans('Włącz baner', array(), 'Modules.SohoshopCountbanner.Admin'),
						'name' => 'sohoshopcountbanner_display',
						'values' => array(
							array(
								'id' => 'active_on',
								'value' => 1,
								'label' => $this->trans('Enabled', array(), 'Admin.Global')
							),
							array(
								'id' => 'active_off',
								'value' => 0,
								'label' => $this->trans('Disabled', array(), 'Admin.Global')
							)
						)
					),
					array(
						'type' => 'switch',
						'label' => $this->trans('Włącz licznik', array(), 'Modules.SohoshopCountbanner.Admin'),
						'name' => 'sohoshopcountbanner_counter',
						'values' => array(
							array(
								'id' => 'active_on',
								'value' => 1,
								'label' => $this->trans('Enabled', array(), 'Admin.Global')
							),
							array(
								'id' => 'active_off',
								'value' => 0,
								'label' => $this->trans('Disabled', array(), 'Admin.Global')
							)
						)
					),
					array(
						'type' => 'textarea',
						'label' => $this->trans('Text banera', array(), 'Modules.SohoshopCountbanner.Admin'),
						'name' => 'sohoshopcountbanner_content',
						'autoload_rte' => true,
                        'lang' => true,
						'rows' => 5,
						'cols' => 40,
					),
					array(
						'type' => 'datetime',
						'label' => $this->l('Wyświetlany od'),
						'desc' => $this->l('Ustaw dzień, w którym baner będzie pokazywał informacje i rozpocznie się odliczanie'),
						'name' => 'sohoshopcountbanner_timefrom',
					),
					array(
						'type' => 'datetime',
						'label' => $this->l('Wyświretlany do'),
						'desc' => $this->l('Ustaw czas zakończenia odliczania'),
						'name' => 'sohoshopcountbanner_timeto',
					)
                ),
                'submit' => array(
                    'title' => $this->trans('Save', array(), 'Admin.Actions'),
                )
            ),
        );

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? : 0;
        $this->fields_form = array();
        $helper->id = (int)Tools::getValue('id_carrier');
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'btnSubmit';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='
            .$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        return $helper->generateForm(array($fields_form));
    }
	
	public function getConfigFieldsValues()
    {
		$fields = array();
		
		$fields['sohoshopcountbanner_display'] = Tools::getValue('sohoshopcountbanner_display', Configuration::get('SOHOSHOPCOUNTBANNER_DISPLAY'));
		$fields['sohoshopcountbanner_counter'] = Tools::getValue('sohoshopcountbanner_counter', Configuration::get('SOHOSHOPCOUNTBANNER_COUNTER'));
		
        $languages = Language::getLanguages(false);
		$id_shop = $this->context->shop->id;
		
        foreach ($languages as $lang)
		{			
			$bannerid = SohoshopCountbannerDB::getBanners($id_shop, (int)$lang['id_lang']);
			if ($bannerid['content']){
				$fields['sohoshopcountbanner_content'][$lang['id_lang']] = $bannerid['content'];
				$fields['sohoshopcountbanner_timefrom'] = $bannerid['timefrom'];
				$fields['sohoshopcountbanner_timeto'] = $bannerid['timeto'];
			}	
			else{
				$fields['sohoshopcountbanner_content'][$lang['id_lang']] = '';
			}		
        }

        return $fields;
    }
	
	public function hookDisplayBanner($params)
    {
		$id_shop = $this->context->shop->id;
		$id_lang = $this->context->language->id;
		$bannerid = SohoshopCountbannerDB::getBanners($id_shop, $id_lang);
		$timefrom = $bannerid['timefrom'];
		$timeto = $bannerid['timeto'];
		
		$now = new DateTime();
		$ymdNow = $now->format('Y-m-d H:i:s');

		if (Configuration::get('SOHOSHOPCOUNTBANNER_DISPLAY') && $timeto > $ymdNow && $timefrom <= $ymdNow)
		{
			if ((int)Configuration::get('SOHOSHOPCOUNTBANNER_COUNTER'))
			{
				$count_from = new DateTime($timefrom);
				$count_to = new DateTime($timeto);
				$cutoff = $count_to->diff($count_from);
				$timestamp = ($cutoff->d) * 24 * 60 * 60;
				$timestamp += ($cutoff->h) * 60 * 60;
				$timestamp += ($cutoff->m)*60;
				$timestamp += ($cutoff->s);
				
				$this->context->smarty->assign(array(
					'count_content' => $bannerid['content'],
					'count_logged' => $this->context->customer->isLogged(),
					'count_from' =>$count_from->format('Y-m-d H:i:s'),
					'count_to' => $count_to->format('Y-m-d H:i:s'),
					'count_cutoff' => $timestamp * 1000,
					'count_logout_url' => Context::getContext()->link->getPageLink('index', true, null, 'mylogout'),
					'display_counter' => 1,
				));
			}
			else
			{
				$this->context->smarty->assign(array(
					'count_content' => $bannerid['content'],
					'count_logged' => $this->context->customer->isLogged(),
					'count_logout_url' => Context::getContext()->link->getPageLink('index', true, null, 'mylogout'),
					'display_counter' => 0,
				));
			}

			return $this->fetch($this->templateFile);
		}
    }
	
	
	public function hookHeader()
    {
		$id_shop = $this->context->shop->id;
		$id_lang = $this->context->language->id;
		$bannerid = SohoshopCountbannerDB::getBanners($id_shop, $id_lang);
		$timefrom = $bannerid['timefrom'];
		$timeto = $bannerid['timeto'];
		
		$now = new DateTime();
		$ymdNow = $now->format('Y-m-d H:i:s');

        if (Configuration::get('SOHOSHOPCOUNTBANNER_DISPLAY') && $timeto > $ymdNow && $timefrom <= $ymdNow) {
            $this->context->controller->registerStylesheet('sohoshopcountbanner', 'modules/'.$this->name.'/views/css/sohoshopcountbanner.css', ['media' => 'all', 'priority' => 150]);
            $this->context->controller->registerJavascript('sohoshopcountbanner', 'modules/'.$this->name.'/views/js/sohoshopcountbanner.js', ['position' => 'bottom', 'priority' => 1001]);
        }
    }
}