<?php
/**
 * SohoshopMerchant module
 *
 * @author    SohoSHOP
 * @copyright Copyright (c) 2018 SohoSHOP
 * @license   http://SohoSHOP.pl
 *
 * http://SohoSHOP.pl
 */
 
if (!defined('_PS_VERSION_')) {
    exit;
}

class SohoshopCountbannerDB extends ObjectModel
{
    public $id_sohoshopcountbanner;
    public $id_shop;
    public $id_lang;
    public $content;
    public $timefrom;
    public $timeto;
    
	
    public static $definition = array(
        'table' => 'sohoshopcountbanner',
        'primary' => 'id_sohoshopcountbanner',
        'fields' => array(
            'id_sohoshopcountbanner'	=> array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
            'id_shop'				=> array('type' => self::TYPE_INT),
            'id_lang'				=> array('type' => self::TYPE_INT),
            'content'				=> array('type' => self::TYPE_HTML, 'validate' => 'isCleanHtml'),
            'timefrom'				=> array('type' => self::TYPE_DATE),
            'timeto'				=> array('type' => self::TYPE_DATE),
        ),
    );
	
	public static function getBanners($id_shop, $id_lang)
	{
		$sql = '
			SELECT *
			FROM `'._DB_PREFIX_.'sohoshopcountbanner`
			WHERE `id_shop` = '.(int)$id_shop.'
				AND `id_lang` = '.(int)$id_lang.'
		';
		
        return Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($sql);
    }
}