CREATE TABLE IF NOT EXISTS `PREFIX_sohoshopcountbanner` (
  `id_sohoshopcountbanner` int(11) unsigned NOT NULL auto_increment,
  `id_shop` int(11) default 1,
  `id_lang` int(11) default 1,
  `content` text character set utf8 NOT NULL,
  `timefrom` datetime,
  `timeto` datetime,
  PRIMARY KEY  (`id_sohoshopcountbanner`)
) ENGINE=ENGINE_TYPE DEFAULT CHARSET=utf8;