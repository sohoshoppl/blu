<?php

/**
 * SohoshopCategorySlider module
 *
 * @author    SohoSHOP
 * @copyright Copyright (c) 2018 SohoSHOP
 * @license   http://sohoshop.pl
 *
 * http://sohoshop.pl
 */
if (!defined('_PS_VERSION_')) {
    exit;
}

class AdminSohoshopCategorySliderBlocksController extends ModuleAdminController {

    public $module;

    public function __construct() {
        $this->table = 'sohoshopcategoryslider_blocks';
        $this->className = 'SohoshopCategorySliderBlocks';
        $this->module = 'sohoshopcategoryslider';
        $this->lang = false;
        $this->bootstrap = true;

        $this->bulk_actions = array(
            'delete' => array(
                'text' => 'Usuń zaznaczone',
                'confirm' => 'Usunąć zaznaczone elementy?',
                'icon' => 'icon-trash'
            )
        );

   

        $id_lang = Configuration::get('PS_LANG_DEFAULT', null, null, Context::getContext()->shop->id);
        $this->_select .= 'cl.`name` AS clname';
        $this->_join .= ' LEFT JOIN `'._DB_PREFIX_.'category_lang` cl ON (cl.`id_category` = a.`id_category` AND cl.`id_lang` = '.$id_lang.' AND cl.`id_shop` = '.Context::getContext()->shop->id.')';        

        $this->tpl_list_vars['icon'] = 'icon-folder-close';
        $this->tpl_list_vars['title'] = 'Treści';

        $this->fields_list = array(
            'id_sohoshopcategoryslider_blocks' => array(
                'title' => 'ID',
                'align' => 'center',
                'type' => 'text',
                'orderby' => true,
                'filter' => true,
                'search' => true,
                'class' => 'fixed-width-xs'
            ),
            'clname' => array(
                'title' => 'Kategoria',
                'width' => 'auto',
                'type' => 'text',
                'orderby' => true,
                'filter' => true,
                'search' => true,
                'filter_key' => 'cl!name'
            ),
            'description' => array(
                'title' => 'Opis',
                'width' => 'auto',
                'type' => 'text',
                'orderby' => true,
                'filter' => true,
                'search' => true,
            ),            
            'date_upd' => array(
                'title' => 'Data',
                'align' => 'text-right',
                'type' => 'datetime',
                'filter_key' => 'a!date_upd'
            )
        );

        parent::__construct();
    }

    public function initToolbar() {
        parent::initToolbar();
    }

    public function renderList() {
        $this->addRowAction('edit');
        $this->addRowAction('delete');

        return parent::renderList();
    }

    public function renderView() {
        $this->initToolbar();
    }

    public function renderForm() {
        $this->display = 'edit';
        $this->initToolbar();

        $this->fields_form = array(
            'legend' => array(
                'title' => $this->trans('Ustawienia', array(), 'Admin.SohoshopCategorySlider.Module'),
            ),
            'input' => array(
                array(
                    'type' => 'textarea',
                    'label' => $this->trans('Opis', array(), 'Admin.SohoshopCategorySlider.Module'),
                    'name' => 'description',
                    'autoload_rte' => false,
                    'required' => true,
                    'maxlength' => 500,
                ),
                array(
                    'type' => 'html',
                    'label' => $this->trans('Kategoria', array(), 'Admin.SohoshopCategorySlider.Module'),
                    'name' => 'id_category',
                    'class' => 'fixed-width-xxl',
                    'html_content' => $this->displayCategoriesTree()
                ),
            ),
        );

        $this->fields_form['submit'] = array(
            'title' => 'Save',
        );

        return parent::renderForm();
    }

    private function displayCategoriesTree() {
        $context = Context::getContext();

        $tree_categories_helper = new HelperTreeCategories('associated-categories-tree');
        $tree_categories_helper->setRootCategory((Shop::getContext() == Shop::CONTEXT_SHOP ? Category::getRootCategory()->id_category : 2));
        $tree_categories_helper->setUseCheckBox(0)->setUseSearch(1)->setInputName('id_category');

        if ($id_sohoshopcategoryslider_blocks = Tools::getValue('id_sohoshopcategoryslider_blocks')) {
            $sql = 'SELECT `id_category` FROM `' . _DB_PREFIX_ . 'sohoshopcategoryslider_blocks`
                    WHERE `id_sohoshopcategoryslider_blocks` = ' . (int) $id_sohoshopcategoryslider_blocks;

            $result = array(Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql));

            $tree_categories_helper->setSelectedCategories($result);
        }

        $context->smarty->assign('categories_tree', $tree_categories_helper->render());

        return $context->smarty->fetch(_PS_MODULE_DIR_ . 'sohoshopcategoryslider/views/templates/admin/form_category.tpl');
    }

    public function setMedia($isNewTheme = false) {
        parent::setMedia($isNewTheme);
        $this->addJqueryPlugin(array('autocomplete'));
        Media::addJsDef(array(
            'id_lang_default' => $this->context->language->id,
            'currentToken' => Tools::getAdminTokenLite('AdminCartRules')
        ));

        $this->addJS(_MODULE_DIR_ . 'sohoshopcategoryslider/views/js/admin-sohoshopcategoryslider.js');
    }

}
