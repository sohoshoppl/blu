<?php

/**
 * SohoshopCategorySlider module
 *
 * @author    SohoSHOP
 * @copyright Copyright (c) 2018 SohoSHOP
 * @license   http://sohoshop.pl
 *
 * http://sohoshop.pl
 */
if (!defined('_PS_VERSION_')) {
    exit;
}

class AdminSohoshopCategorySliderSlidesController extends ModuleAdminController {

    public $module;

    public function __construct() {
        $this->table = 'sohoshopcategoryslider_slides';
        $this->className = 'SohoshopCategorySliderSlides';
        $this->module = 'sohoshopcategoryslider';
        $this->lang = false;
        $this->bootstrap = true;

        $this->bulk_actions = array(
            'delete' => array(
                'text' => 'Usuń zaznaczone',
                'confirm' => 'Usunąć zaznaczone elementy?',
                'icon' => 'icon-trash'
            )
        );

        $id_lang = Configuration::get('PS_LANG_DEFAULT', null, null, Context::getContext()->shop->id);
        $this->_select .= "scb.`description` AS description, CONCAT('<img height=\"90\" src=\"./../img/cat_slides/slides_',id_sohoshopcategoryslider_slides,'.jpg\">') as slide_url ";
        $this->_join .= ' LEFT JOIN `' . _DB_PREFIX_ . 'sohoshopcategoryslider_blocks` scb ON (a.`id_sohoshopcategoryslider_blocks` = scb.`id_sohoshopcategoryslider_blocks` )';

        $this->tpl_list_vars['icon'] = 'icon-folder-close';
        $this->tpl_list_vars['title'] = 'Banery';

        $this->fields_list = array(
            'id_sohoshopcategoryslider_slides' => array(
                'title' => 'ID',
                'align' => 'center',
                'type' => 'text',
                'orderby' => true,
                'filter' => true,
                'search' => true,
                'class' => 'fixed-width-xs'
            ),
            'description' => array(
                'title' => 'Slider',
                'width' => 'auto',
                'type' => 'text',
                'orderby' => true,
                'filter' => true,
                'search' => true,
                'filter_key' => 'scb!description'
            ),
            'name' => array(
                'title' => 'Nazwa',
                'align' => 'center',
                'orderby' => true,
                'filter' => true,
                'search' => true
            ),
            'slide_link' => array(
                'title' => 'Link',
                'align' => 'center',
                'orderby' => true,
                'filter' => true,
                'search' => true
            ),
            'slide_url' => array(
                'title' => 'Slajd',
                'align' => 'center',
                'callback' => 'getImageHtml'

            ),

        );

        parent::__construct();
    }

    public function initToolbar() {
        parent::initToolbar();
    }

    public function renderList() {
        $this->addRowAction('delete');

        return parent::renderList();
    }

    public function renderView() {
        $this->initToolbar();
    }

    public function renderForm() {
        $this->display = 'edit';
        $this->initToolbar();

        $obj = $this->loadObject(true);

        @unlink(_PS_TMP_IMG_DIR_ . $this->table . '_' . (int) $obj->id . '.' . $this->imageType);
        $image_name = _PS_IMG_DIR_ . 'cat_slides/slides_' . $obj->id . '.' . $this->imageType;

        $this->fields_form = array(
            'legend' => array(
                'title' => $this->trans('Ustawienia', array(), 'Admin.SohoshopCategorySlider.Module'),
            ),
            'input' => array(
                array(
                    'type' => 'select',
                    'label' => $this->trans('Slidery', array(), 'Admin.SohoshopCategorySlider.Module'),
                    'name' => 'id_sohoshopcategoryslider_blocks',
                    'class' => 'fixed-width-xxl',
                    'options' => array(
                        'query' => SohoshopCategorySliderBlocks::getBlocks(),
                        'id' => 'id_sohoshopcategoryslider_blocks',
                        'name' => 'description',
                    )
                ),
             array(
                    'type' => 'text',
                    'label' => $this->trans('Link', array(), 'Admin.SohoshopCategorySlider.Module'),
                    'name' => 'slide_link'
                ),   
             array(
                    'type' => 'text',
                    'label' => $this->trans('Nazwa', array(), 'Admin.SohoshopCategorySlider.Module'),
                    'name' => 'name'
                ),                   
                array(
                    'type' => 'file',
                    'label' => $this->trans('Slajd', array(), 'Admin.Catalog.Feature'),
                    'name' => 'thumb',
                    'ajax' => false,
                    'multiple' => false,
                    'image' => ImageManager::thumbnail($image_name, $this->table . '_' . (int) $obj->id . '.' . $this->imageType, 300),
                    'hint' => $this->trans('The category thumbnail appears in the menu as a small image representing the category, if the theme allows it.', array(), 'Admin.Catalog.Help'),
                ),
            ),
        );

        $this->fields_form['submit'] = array(
            'title' => 'Save',
        );

        return parent::renderForm();
    }

    protected function postImage($id) {
        if (($id_sohoshopcategoryslider_blocks = (int) Tools::getValue('id_sohoshopcategoryslider_slides')) && isset($_FILES) && count($_FILES)) {
            $name = 'thumb';
            if ($_FILES[$name]['name'] != null) {
                $tmpfile = tempnam(_PS_TMP_IMG_DIR_, 'cat_slides');
                move_uploaded_file($_FILES[$name]['tmp_name'], $tmpfile);
                $res = ImageManager::resize($tmpfile, _PS_IMG_DIR_ . 'cat_slides/slides_' . $id_sohoshopcategoryslider_blocks . '.' . $this->imageType, 310, 295);
                if (!$res) {
                    $this->errors = $this->trans('An error occurred while uploading thumbnail image.', array(), 'Admin.Catalog.Notification');
                }
                if (count($this->errors)) {
                    $ret = false;
                }
                unlink($tmpfile);
                $ret = true;
            }
        }
        return $ret;
    }

    public function setMedia($isNewTheme = false) {
        parent::setMedia($isNewTheme );
        $this->addJqueryPlugin(array('autocomplete'));
        Media::addJsDef(array(
            'id_lang_default' => $this->context->language->id,
            'currentToken' => Tools::getAdminTokenLite('AdminCartRules')
        ));
        $this->addJS(_MODULE_DIR_ . 'sohoshopcategoryslider/views/js/admin_slides.js');
    }

    public function getImageHtml($params){
        return html_entity_decode($params);
    }

}
