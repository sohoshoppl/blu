<?php

/**
 * SohoshopSections module
 *
 * @author    SohoSHOP
 * @copyright Copyright (c) 2018 SohoSHOP
 * @license   http://SohoSHOP.pl
 *
 * http://SohoSHOP.pl
 */
if (!defined('_PS_VERSION_')) {
    exit;
}

class SohoshopCategorySliderBlocks extends ObjectModel {

    public $id_sohoshopcategoryslider_blocks;
    public $description;
    public $id_category;
    public $date_add;
    public $date_upd;
    public static $definition = array(
        'table' => 'sohoshopcategoryslider_blocks',
        'primary' => 'id_sohoshopcategoryslider_blocks',
        'fields' => array(
            'id_sohoshopcategoryslider_blocks' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
            'description' => array('type' => self::TYPE_STRING, 'validate' => 'isString', 'size' => 500),
            'id_category' => array('type' => self::TYPE_INT),
            'date_add' => array('type' => self::TYPE_DATE),
            'date_upd' => array('type' => self::TYPE_DATE),
        ),
    );
    protected $webserviceParameters = array(
        'objectsNodeName' => 'sohoshopcategoryslider_blocks',
        'fields' => array(
            'id_sohoshopcategoryslider_blocks' => array('xlink_resource' => 'id_sohoshopcategoryslider_blocks'),
            'description' => array(),
            'id_category' => array('xlink_resource' => 'categories'),
            'date_add' => array(),
            'date_upd' => array(),
        ),
    );

    public function delete() {
        $return = parent::delete();

        Db::getInstance()->execute('DELETE FROM `' . _DB_PREFIX_ . 'sohoshopcategoryslider_slides` WHERE `id_sohoshopcategoryslider_blocks` = ' . (int) $this->id_sohoshopcategoryslider_blocks);

        return $return;
    }

    public static function getBlocks($id_category = 0) {
        if( (int)$id_category > 0){
            $sql = 'SELECT a.*,cl.`name` AS clname FROM `' . _DB_PREFIX_ . 'sohoshopcategoryslider_blocks` as a'
                    . ' LEFT JOIN `'._DB_PREFIX_.'category_lang` cl ON (cl.`id_category` = a.`id_category` AND cl.`id_lang` = 1 AND cl.`id_shop` = '.Context::getContext()->shop->id.')'
                    . ' WHERE a.id_category  ='.(int)$id_category;
            $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);    
        }else{
            $sql = 'SELECT a.*,cl.`name` AS clname FROM `' . _DB_PREFIX_ . 'sohoshopcategoryslider_blocks` as a'
            . ' LEFT JOIN `'._DB_PREFIX_.'category_lang` cl ON (cl.`id_category` = a.`id_category` AND cl.`id_lang` = 1 AND cl.`id_shop` = '.Context::getContext()->shop->id.')';
            $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);    
        }
        return $result;
    }

    public static function getBlockValue($id_sohoshopcategoryslider_blocks, $valueName) {
        $sql = 'SELECT `' . $valueName . '`
		FROM `' . _DB_PREFIX_ . 'sohoshopcategoryslider_blocks`
		WHERE `id_sohoshopcategoryslider_blocks` = ' . (int) $id_sohoshopcategoryslider_blocks . '
		';
        return Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);
    }

}
