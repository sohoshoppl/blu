<?php

/**
 * SohoshopSections module
 *
 * @author    SohoSHOP
 * @copyright Copyright (c) 2018 SohoSHOP
 * @license   http://SohoSHOP.pl
 *
 * http://SohoSHOP.pl
 */
if (!defined('_PS_VERSION_')) {
    exit;
}

class SohoshopCategorySliderSlides extends ObjectModel {

    public $id_sohoshopcategoryslider_slides;
    public $id_sohoshopcategoryslider_blocks;
    public $slide_url;
    public $slide_link;
    public $name;
    public $position;
    public static $definition = array(
        'table' => 'sohoshopcategoryslider_slides',
        'primary' => 'id_sohoshopcategoryslider_slides',
        'fields' => array(
            'id_sohoshopcategoryslider_slides' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
            'id_sohoshopcategoryslider_blocks' => array('type' => self::TYPE_INT),
            'slide_url' => array('type' => self::TYPE_STRING),
            'name' => array('type' => self::TYPE_STRING),
            'slide_link' => array('type' => self::TYPE_STRING,'validate' => 'isUrl', 'size' => 255),
            'position' => array('type' => self::TYPE_INT),
        ),
    );

    public static function getBlockSlides($id_sohoshopcategoryslider_blocks) {
        $sql = 'SELECT `id_sohoshopcategoryslider_slides`, `slide_link`,`name`
			FROM `' . _DB_PREFIX_ . 'sohoshopcategoryslider_slides`
			WHERE `id_sohoshopcategoryslider_blocks` = ' . (int) $id_sohoshopcategoryslider_blocks . '
			ORDER BY `id_sohoshopcategoryslider_slides` ASC';

        return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
    }

    public function delete() {
        @unlink(_PS_IMG_DIR_ . 'cat_slides/slides_' . $this->id . '.jpg');
        return parent::delete();
    }

}
