{*
* SohoshopSections module
*
* @author    SohoSHOP
* @copyright Copyright (c) 2021 SohoSHOP
* @license   http://SohoSHOP.pl
*
* http://SohoSHOP.pl
*}



{if isset($blocks) && $blocks|count}
    <div class="look-filters" id="sohoshopcategoryslider">
        {foreach from=$blocks item=block name=categorySliders}
            <article>
                <header>{$block.description}</header>
                <div class="swiper-container looksSlider-{$smarty.foreach.categorySliders.index}">
                    <div class="swiper-wrapper">
                        {foreach from=$block.slides item=slide}
                            <div class="swiper-slide">
                                <a class="image" href="{$slide.slide_link}">
                                    <img class="img-fluid"
                                         src="{$urls.img_ps_url}cat_slides/slides_{$slide.id_sohoshopcategoryslider_slides}.jpg"/>

                                </a>
                                <div class="hc_block_title">
                                    <h2>
                                        <a href="{$slide.slide_link}">{$slide.name}</a>
                                    </h2>
                                </div>
                            </div>
                        {/foreach}
                    </div>
                    <div class="swiper-button-next"></div>
                    <div class="swiper-button-prev"></div>
                </div>
            </article>
        {/foreach}
    </div>
{/if}
