{*
 * SohoshopSections module
 *
 * @author    SohoSHOP
 * @copyright Copyright (c) 2018 SohoSHOP
 * @license   http://SohoSHOP.pl
 *
 * http://SohoSHOP.pl
*}

{if trim($categories_tree) != ''}
	{$categories_tree}
{else}
	<div class="alert alert-warning">
		{l s='Categories selection is disabled because you have no categories or you are in a "all shops" context.' d='Modules.sohomarketplace.Admin'}
	</div>
{/if}