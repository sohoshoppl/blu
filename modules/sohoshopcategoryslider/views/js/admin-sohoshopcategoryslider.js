/*
 * SohoshopSections module
 *
 * @author    SohoSHOP
 * @copyright Copyright (c) 2018 SohoSHOP
 * @license   http://SohoSHOP.pl
 *
 * http://SohoSHOP.pl
*/

var current_id_condition_group = 0;
var current_id_feature_group = 0;

$(document).ready(function(){
	
	$('.id_attribute:nth-of-type(1)').show();
	$('.id_feature_value:nth-of-type(1)').show();
	current_id_condition_group = $('#id_attribute_group option:first').val();
	current_id_feature_group = $('#id_feature option:first').val();

	
	$(document).on('change', '#id_attribute_group', function(e){
		e.preventDefault();
		
		$('.id_attribute').hide();
		$('#id_attribute_' + parseInt($(this).val())).show();
		
		current_id_condition_group = parseInt($(this).val());
	});
	
	$(document).on('click', '#add_condition_attribute', function(e){
		var selected_attribute_value = $('#id_attribute_' + current_id_condition_group).val();
		var selected_attribute_text = $('#id_group_' + current_id_condition_group).text();
		
		if (!selected_attribute_value)
		{
			selected_attribute_value = $('#id_attribute_' + current_id_condition_group + ' option:first').val();
			selected_attribute_text += ': ' + $('#id_attribute_' + current_id_condition_group + ' option:first').text();
		}
		else
			selected_attribute_text += ': ' + $('#id_attribute_text_' + selected_attribute_value).text();

		$('#id_attribute_value').val(selected_attribute_value);
		$('#selected_attribute').html('<button class="delAttribute btn btn-default" type="button"><i class="icon-remove text-danger"></i></button> ' + selected_attribute_text);
	});
	
	$(document).on('click', '.delAttribute', function(e){
		e.preventDefault();
		
		$('#id_attribute_value').val(0);
		$('#selected_attribute').html('');
	});
	
	$(document).on('change', '#id_feature', function(e){
		e.preventDefault();
		
		$('.id_feature_value').hide();
		$('#id_feature_' + parseInt($(this).val())).show();
		
		current_id_feature_group = parseInt($(this).val());
	});
	
	$(document).on('click', '#add_condition_feature', function(e){
		var selected_feature_value = $('#id_feature_' + current_id_feature_group).val();
		var selected_feature_text = $('#id_feature_group_' + current_id_feature_group).text();
		
		if (!selected_feature_value)
		{
			selected_feature_value = $('#id_feature_group_' + current_id_feature_group + ' option:first').val();
			selected_feature_text += ': ' + $('#id_feature_' + current_id_feature_group + ' option:first').text();
		}
		else
			selected_feature_text += ': ' + $('#id_feature_text_' + selected_feature_value).text();

		$('#id_feature_value').val(selected_feature_value);
		$('#selected_feature').html('<button class="delFeature btn btn-default" type="button"><i class="icon-remove text-danger"></i></button> ' + selected_feature_text);
	});
	
	$(document).on('click', '.delFeature', function(e){
		e.preventDefault();
		
		$('#id_feature_value').val(0);
		$('#selected_feature').html('');
	});
});