/*
 * SohoshopSections module
 *
 * @author    SohoSHOP
 * @copyright Copyright (c) 2018 SohoSHOP
 * @license   http://SohoSHOP.pl
 *
 * http://SohoSHOP.pl
 */

$(document).ready(function () {
    $('.slides').bxSlider({
        slideWidth: 890,
        infiniteLoop: true,
        pager: false,
        nextText: '<i class="fas fa-chevron-right"></i>',
        prevText: '<i class="fas fa-chevron-left"></i>'        
    });
});