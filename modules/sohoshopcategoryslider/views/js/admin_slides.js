/*
 * SohoshopSections module
 *
 * @author    SohoSHOP
 * @copyright Copyright (c) 2018 SohoSHOP
 * @license   http://SohoSHOP.pl
 *
 * http://SohoSHOP.pl
*/

$(document).ready(function(){
	var initMyAssociationsAutocomplete = function (){
		$('#product_autocomplete_input_association').setOptions({
			extraParams: {
				excludeIds : getAssociationsIds()
			}
		});
		
		$('#product_autocomplete_input_association')
			.autocomplete('ajax_products_list.php?exclude_packs=0&excludeVirtuals=0&withcomb=1', {
				minChars: 1,
				autoFill: true,
				max: 20,
				matchContains: true,
				mustMatch:true,
				scroll:false,
				cacheLength:0,
				formatItem: function(item) {
					return item[1]+' - '+item[0];
				}
		}).result(addAssociation);
		$('#nameMyAssociations').val('');
		if (typeof sohoshopmpk_products_id_product !== 'undefined' && sohoshopmpk_products_id_product)
			$('#inputMyAssociations').val(sohoshopmpk_products_id_product);
		else
			$('#inputMyAssociations').val('-');
        $('#product_autocomplete_input_association').val('');
		$('#product_autocomplete_input_association').setOptions({
			extraParams: {
				excludeIds : getAssociationsIds()
			}
		});
	};	
    //function to exclude a product if it exists in the list
            var getAssociationsIds = function()
            {
                if ($('#inputMyAssociations').val() === undefined)
                    return '';
                return $('#inputMyAssociations').val().replace(/\-/g,',');
            }
    //function to add a new association, adds it in the hidden input and also as a visible div, with a button to delete the association any time.
            var addAssociation = function(event, data, formatted)
            {
                if (data == null)
                    return false;
                var productId = data[1];
                var productName = data[0];
				var productAttribute = data[2];

                /* delete product from select + add product line to the div, input_name, input_ids elements */
                $('#divAccessories').html('<div class="form-control-static"><button type="button" class="delAssociation btn btn-default" name="' + productId + '"><i class="icon-remove text-danger"></i></button>&nbsp;'+ productName +'</div>');
                $('#nameMyAssociations').val(productName);
                $('#inputMyAssociations').val(productId);
				$('#attrMyAssociations').val(productAttribute + '-');
                $('#product_autocomplete_input_association').val('');
                $('#product_autocomplete_input_association').setOptions({
                    extraParams: {excludeIds : getAssociationsIds()}
                });
            };
    //the function to delete an associations, delete it from both the hidden inputs and the visible div list.
            var delAssociations = function(id)
            {
                $('#divAccessories').html('');
                var input = getE('inputMyAssociations');
                var name = getE('nameMyAssociations');
				var attr = getE('attrMyAssociations');

                // Reset all hidden fields
                input.value = '';
                name.value = '';
				attr.value = '';
                div.innerHTML = '';
                for (i in inputCut)
                {
                    // If empty, error, next
                    if (!inputCut[i] || !nameCut[i])
                        continue ;

                    // Add to hidden fields no selected products OR add to select field selected product
                    if (inputCut[i] != id)
                    {
                        div.innerHTML += '<div class="form-control-static"><button type="button" class="delAssociation btn btn-default" name="' + inputCut[i] +'"><i class="icon-remove text-danger"></i></button>&nbsp;' + nameCut[i] + '</div>';
                    }
                    else
                        $('#selectAssociation').append('<option selected="selected" value="' + inputCut[i] + '-' + nameCut[i] + '">' + inputCut[i] + ' - ' + nameCut[i] + '</option>');
                }

                $('#product_autocomplete_input_association').setOptions({
                    extraParams: {excludeIds : getAssociationsIds()}
                });
            };

    initMyAssociationsAutocomplete();
	$('#divAccessories').delegate('.delAssociation', 'click', function(){
		delAssociations($(this).attr('name'));
	});
});