<?php

/**
 * SohoshopCategorySlider module
 *
 * @author    SohoSHOP
 * @copyright Copyright (c) 2018 SohoSHOP
 * @license   http://SohoSHOP.pl
 *
 * http://SohoSHOP.pl
 */
if (!defined('_PS_VERSION_')) {
    exit;
}

require_once (dirname(__FILE__) . '/classes/SohoshopCategorySliderBlocks.php');
require_once (dirname(__FILE__) . '/classes/SohoshopCategorySliderSlides.php');

use PrestaShop\PrestaShop\Adapter\Image\ImageRetriever;
use PrestaShop\PrestaShop\Adapter\Product\PriceFormatter;
use PrestaShop\PrestaShop\Core\Product\ProductListingPresenter;
use PrestaShop\PrestaShop\Adapter\Product\ProductColorsRetriever;

class SohoshopCategorySlider extends Module {

    const MODULE_NAME = 'sohoshopcategoryslider';
    const TAB_SOHOSHOP = 'Sohoshop';
    const NAME_TAB_SOHOSHOP = 'Sohoshop';
    const TAB_MODULE = 'AdminSohoshopCategorySlider';

    public $parents;

    public function __construct() {
        $this->name = self::MODULE_NAME;
        $this->tab = 'sohoshopcategoryslider';
        $this->version = '1.0.0';
        $this->author = 'SohoSHOP.pl';
        $this->module_dir = dirname(__FILE__) . '/';
        $this->module_dir_images = '/modules/' . $this->name . '/images/';
        $this->need_upgrade = true;
        $this->secure_key = Tools::encrypt($this->name);
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->trans('SohoshopCategorySlider', array(), 'Modules.SohoshopCategorySlider.Admin');
        $this->description = $this->trans('Slidery na stronie kategorii.', array(), 'Modules.SohoshopCategorySlider.Admin');

        $shop_domain = Tools::getCurrentUrlProtocolPrefix() . Tools::getHttpHost();
        $url_modules = $shop_domain . __PS_BASE_URI__ . 'modules/';
        $physic_path_modules = realpath(_PS_ROOT_DIR_ . '/modules') . '/';
        $this->module_path = $url_modules . $this->name . '/';
        $this->module_path_physic = $physic_path_modules . $this->name . '/';

        $this->tabs[] = array(
            'tab_parent_class' => self::TAB_SOHOSHOP,
            'tab_parent_name' => self::NAME_TAB_SOHOSHOP,
            'class_name' => self::TAB_MODULE . 'Blocks',
            'tab_name' => $this->trans('Slidery', array(), 'Modules.SohoshopCategorySlider.Admin'),
        );
      
        $this->tabs[] = array(
            'tab_parent_class' => self::TAB_SOHOSHOP,
            'tab_parent_name' => self::NAME_TAB_SOHOSHOP,
            'class_name' => self::TAB_MODULE . 'Slides',
            'tab_name' => $this->trans('Slajdy sliderów', array(), 'Modules.SohoshopCategorySlider.Admin'),
        );
               
    }

    public function install() {
        $installed = true;
        foreach ($this->tabs as $t) {
            if (!$this->installSohoshopTabs($t['class_name'], $t['tab_name'], $t['tab_parent_class'], $t['tab_parent_name'])) {
                $installed = false;
            }
        }

        if (!$installed) {
            return false;
        }

        $this->installDbTables();

        if (
                !parent::install() ||
                !$this->registerHook('displayCategorySlider') ||
                !$this->registerHook('header')
        )
            return false;

        return true;
    }

    public function installSohoshopTabs($class_name, $tab_name, $tab_parent_class, $tab_parent_name = '') {
        $id_parent = Tab::getIdFromClassName($tab_parent_class);
        $langs = Language::getLanguages(false);

        if (!$id_parent) {
            $parent = new Tab();
            $parent->class_name = $tab_parent_class;
            $parent->module = $this->name;
            $parent->id_parent = 0;

            foreach ($langs as $l) {
                $parent->name[(int) $l['id_lang']] = $tab_parent_name;
            }

            if (!$parent->save()) {
                return false;
            }

            $id_parent = $parent->id;
        }

        if (!Tab::getIdFromClassName($class_name) || $class_name == $tab_parent_class) {
            $tab = new Tab();
            $tab->class_name = $class_name;
            $tab->module = $this->name;
            if ($this->parents[$tab_parent_class])
                $tab->id_parent = $this->parents[$tab_parent_class];
            else
                $tab->id_parent = (int) $id_parent;

            foreach ($langs as $l) {
                $tab->name[(int) $l['id_lang']] = $tab_name;
            }

            if (!$tab->save()) {
                return false;
            }

            if (!$this->parents[$tab_parent_class])
                $this->parents[$tab_parent_class] = (int) $id_parent;
        }

        return true;
    }

    public function installDbTables() {

        Db::getInstance()->execute('
			CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'sohoshopcategoryslider_blocks` (
			`id_sohoshopcategoryslider_blocks` INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
			`description` VARCHAR(500) NOT NULL,
			`id_category` INT NOT NULL,
			`date_add` DATETIME NOT NULL,
			`date_upd` DATETIME NOT NULL,
			INDEX (`id_sohoshopcategoryslider_blocks`)
			) ENGINE = ' . _MYSQL_ENGINE_ . ' CHARACTER SET utf8 COLLATE utf8_general_ci;
		');
              
        Db::getInstance()->execute('
			CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'sohoshopcategoryslider_slides` (
			`id_sohoshopcategoryslider_slides` INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
			`id_sohoshopcategoryslider_blocks` INT NOT NULL,
			`slide_url` INT NOT NULL,
                        `slide_link` VARCHAR(255),
                        `name` VARCHAR(255),
			`position` INT NOT NULL,
			INDEX (`id_sohoshopcategoryslider_slides`)
			) ENGINE = ' . _MYSQL_ENGINE_ . ' CHARACTER SET utf8 COLLATE utf8_general_ci;
		');
    }

    public function uninstall() {
        $this->uninstallSohoshopTabs(self::TAB_MODULE . 'Blocks');
        $this->uninstallSohoshopTabs(self::TAB_MODULE . 'Slides');

        $this->uninstallDbTables();

        return parent::uninstall();
    }

    public function uninstallSohoshopTabs($class_name) {
        $id_tab = Tab::getIdFromClassName($class_name);

        if ($id_tab) {
            $tab = new Tab((int) $id_tab);
            $id_parent = $tab->id_parent;
            $tab->delete();
        }
    }

    public function uninstallDbTables() {
        Db::getInstance()->execute('DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'sohoshopcategoryslider_blocks`');
        Db::getInstance()->execute('DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'sohoshopcategoryslider_slides`');

    }

    public function getContent() {
        $this->_html .= '';

        if (Tools::isSubmit('btnSubmit')) {

            $this->_html .= $this->displayConfirmation($this->trans('Settings updated.', array(), 'Admin.Notifications.Success'));
        }

        $this->_html .= $this->getContentHeader();
        $this->_html .= $this->renderForm();

        return $this->_html;
    }

    private function getContentHeader() {
        $this->context->smarty->assign(array(
            'module_display_name' => $this->displayName,
            'module_dir' => $this->module_dir_images
        ));

        return $this->context->smarty->fetch($this->module_dir . 'views/templates/admin/content_header.tpl');
    }

    public function renderForm() {
        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->trans('Ustawienia ogólne', array(), 'Modules.SohoshopCategorySlider.Admin'),
                    'icon' => 'icon-cogs'
                ),
                'submit' => array(
                    'title' => $this->trans('Save', array(), 'Admin.Actions'),
                )
            ),
        );

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int) Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ?: 0;
        $this->fields_form = array();
        $helper->id = (int) Tools::getValue('id_carrier');
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'btnSubmit';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false) . '&configure='
                . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        return $helper->generateForm(array($fields_form));
    }

    public function getConfigFieldsValues() {

    }

    public function hookDisplayCategorySlider($params) {
        $id_category = (int) Tools::getValue('id_category');
        $getBlocks = SohoshopCategorySliderBlocks::getBlocks($id_category);

        foreach ($getBlocks as $block) {
            $slides = SohoshopCategorySliderSlides::getBlockSlides($block['id_sohoshopcategoryslider_blocks']);
                $blocks[] = array(
                    'slides' => $slides,
                    'description' => $block['description']
                );
        }

        if(isset($blocks)){
            $this->context->smarty->assign('blocks', $blocks);

            return $this->fetch('module:sohoshopcategoryslider/views/templates/hook/sohoshopcategoryslider.tpl');

        }
    }

}
