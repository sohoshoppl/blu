var pm_featureItemsCount = 0;
var pm_prototypeHtml = document.createElement('div');
var pm_prototypeSelect = document.createElement('div');

function pmTransformSelect() {
    var pm_featureSelectList = document.querySelectorAll('div#features-content select[id^="form_step1_features_"][id$="_value"]:not(:disabled)');
    var pm_featureNewItemsCount = pm_featureSelectList.length;
    if (pm_featureNewItemsCount != pm_featureItemsCount) {
        // Process new features
        pm_featureItemsCount = pm_featureNewItemsCount;

        if (pm_featureSelectList.length == 0) {
            document.querySelector('div.feature-collection').setAttribute('data-prototype', pm_prototypeHtml.innerHTML);
        }
    } else {
        // Hide opened "select2" items
        if (pm_featureNewItemsCount > 0) {
            Array.prototype.forEach.call(document.querySelectorAll('div#features-content select[multiple].select2-hidden-accessible'), function(featuresSelect) {
                featuresSelect.parentNode.querySelector('.select2-container').style.display = 'none';
            });
        }
        return;
    }

    let pm_featureIdList = [];
    Array.prototype.forEach.call(pm_featureSelectList, function ($select) {
        idFeatureElement = document.querySelector('[id="' + $select.getAttribute('id').replace('_value', '_feature') + '"]');
        // Skip if we cannot find the select
        if (idFeatureElement == null) {
            return;
        }
        pm_featureIdList.push(idFeatureElement.value);
    });

    // Remove old disabled state
    Array.prototype.forEach.call(pm_prototypeSelect.querySelectorAll('option:disabled'), function (option) {
        if (!pm_featureIdList.includes(option.value)) {
            option.removeAttribute('disabled');
            // Update prototype
            document.querySelector('div.feature-collection').setAttribute('data-prototype', pm_prototypeHtml.innerHTML);
        }
    });

    // At least one feature to process
    if (pm_featureSelectList.length) {
        Array.prototype.forEach.call(pm_featureSelectList, function($select) {
            $select.setAttribute('multiple', 'multiple');
            doTransformSelect = false;
            if ($select.pmTransformSelectDone != true) {
                doTransformSelect = true;
            }
            $select.pmTransformSelectDone = true;

            idFeatureElementSelector = '#' + $select.getAttribute('id').replace('_value', '_feature');

            idFeatureElement = document.querySelector('[id="'+ $select.getAttribute('id').replace('_value', '_feature') +'"]');
            // Skip if we cannot find the select
            if (idFeatureElement == null) {
                return;
            }

            id_feature = idFeatureElement.value;
            featureName = idFeatureElement.querySelector('option[value="'+ id_feature +'"]').innerHTML;

            if (id_feature > 0) {
                idFeatureElement.parentNode.parentNode.classList.add('hide');
                idFeatureElement.parentNode.parentNode.style.display = 'none';
                // Disable already used features
                pm_prototypeSelect.querySelector('option[value="' + id_feature + '"]').setAttribute('disabled', 'disabled');
            }

            // Hide all select2
            Array.prototype.forEach.call($select.parentNode.querySelectorAll('.select2-container'), function(select2Container) {
                select2Container.classList.add('hide');
                select2Container.style.display = 'none';
            });

            // Edit label name
            if (doTransformSelect) {
                // Disable id_feature <select> to avoid undefined value exception
                idFeatureElement.setAttribute('disabled', 'disabled');

                labelElement = $select.parentNode.querySelector('label');
                labelElement.innerHTML = '<h2>' + featureName + '</h2><p>' + labelElement.innerHTML + '</p>';

                // Edit select name
                $select.setAttribute('name', 'pm_multiplefeatures_feature_' + id_feature + '_value[]');

                // Change container classes
                $select.parentNode.parentNode.setAttribute('class', 'col-xs-12 col-12');

                // Rename custom label input
                Array.prototype.forEach.call($select.parentNode.parentNode.parentNode.querySelectorAll('input[id*="custom_value"]'), function(inputElement) {
                    idSplit = inputElement.getAttribute('id').split('_');
                    idLang = idSplit[idSplit.length - 1];
                    inputElement.setAttribute('name', 'pm_multiplefeatures_feature_' + id_feature + '_custom_value_' + idLang);
                });

                // Remove feature = 0 or undefined values (when there is no translation)
                Array.prototype.forEach.call($select.querySelectorAll('option[value="0"], option[value=""]'), function(inputElement) {
                    inputElement.parentNode.removeChild(inputElement);
                });

                // Set selected to option, and reorder them (add to the end for each selected feature)
                if (typeof(pm_FeatureList[id_feature]) != 'undefined' && pm_FeatureList[id_feature].length > 0) {
                    var allOptions = $select.querySelectorAll('option');
                    var optionsCount = allOptions.length;

                    Array.prototype.forEach.call(pm_FeatureList[id_feature], function(idFeatureValue) {
                        currentOption = $select.querySelector('option[value="' + idFeatureValue + '"]');

                        if (currentOption != null) {
                            currentOption.setAttribute('selected', 'selected');
                            if (optionsCount > 1) {
                                currentOption.parentNode.appendChild(currentOption);
                            }
                        }
                    });
                }
            }

            $($select).pmConnectedList({
                availableListTitle: pm_FeatureAvailableListTitle,
                availableListSearchTitle: pm_FeatureAvailableListSearchTitle,
                searchInputPlaceHolder: pm_FeatureSearchInputPlaceHolder,
                selectedListTitle: pm_FeatureSelectedListTitle,
                addAllButtonLabel: '<i class="material-icons">add</i> ' + pm_FeatureAddAllButtonLabel,
                removeAllButtonLabel: '<i class="material-icons">delete</i> ' + pm_FeatureRemoveAllButtonLabel,
                addAllButtonClasses: 'btn btn-success',
                removeAllButtonClasses: 'btn btn-danger',
                removeAllCallback: function($selectSource) {
                    $selectSource.parentNode.parentNode.parentNode.remove($selectSource);
                },
            });
        });
        document.querySelector('div.feature-collection').setAttribute('data-prototype', pm_prototypeHtml.innerHTML);
    }
}

$(document).ready(function() {
    // Init select
    pm_prototypeHtml.innerHTML = document.querySelector('div.feature-collection').getAttribute('data-prototype');
    Array.prototype.forEach.call(pm_prototypeHtml.querySelectorAll('option'), function(prototypeOption) {
        prototypeOption.removeAttribute('disabled');
    });
    pm_prototypeSelect = pm_prototypeHtml.querySelector('select.feature-selector');

    if (typeof(pm_FeatureList) != 'undefined') {
        for (var id_feature in pm_FeatureList) {
            if (pm_FeatureList[id_feature].length > 1) {
                // We have to remove extra features group entries
                var foundOne = false;

                Array.prototype.forEach.call(document.querySelectorAll('div#features-content select.feature-selector'), function(featureSelector) {
                    if (featureSelector.value == id_feature) {
                        if (!foundOne) {
                            foundOne = true;
                        } else {
                            elementToRemove = featureSelector.parentNode.parentNode.parentNode;
                            elementToRemove.parentNode.removeChild(elementToRemove);
                        }
                    }
                });
            }
        }
    }
    // Then, watch for changes
    setInterval('pmTransformSelect();', 250);
});
