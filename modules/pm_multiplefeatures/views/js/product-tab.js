function pmTransformSelect() {
    var pm_featureSelectList = document.querySelectorAll('div#product-tab-content-Features select[id^="feature_"][id$="_value"], div#product-features select[id^="feature_"][id$="_value"], div#step5 select[id^="feature_"][id$="_value"]');

    // At least one feature
    if (pm_featureSelectList.length) {
        Array.prototype.forEach.call(pm_featureSelectList, function($select) {
            $select.setAttribute('multiple', 'multiple');
            if ($select.getAttribute('name').indexOf("[]") == -1) {
                $select.setAttribute('name', $select.getAttribute('name') + '[]');
            }
            id_feature = parseInt($select.getAttribute('id').replace('feature_', '').replace('_value', ''));

            // Remove feature = 0 or undefined values (when there is no translation)
            Array.prototype.forEach.call($select.querySelectorAll('option[value="0"], option[value=""]'), function(inputElement) {
                inputElement.parentNode.removeChild(inputElement);
            });

            // Set selected to option, and reorder them (add to the end for each selected feature)
            if (typeof(pm_FeatureList[id_feature]) != 'undefined' && pm_FeatureList[id_feature].length > 0) {
                var allOptions = $select.querySelectorAll('option');
                var optionsCount = allOptions.length;

                Array.prototype.forEach.call(pm_FeatureList[id_feature], function(idFeatureValue) {
                    currentOption = $select.querySelector('option[value="' + idFeatureValue + '"]');
                    if (currentOption != null) {
                        currentOption.setAttribute('selected', 'selected');
                        if (optionsCount > 1) {
                            currentOption.parentNode.appendChild(currentOption);
                        }
                    }
                });
            }

            $($select).pmConnectedList({
                availableListTitle: pm_FeatureAvailableListTitle,
                availableListSearchTitle: pm_FeatureAvailableListSearchTitle,
                searchInputPlaceHolder: pm_FeatureSearchInputPlaceHolder,
                selectedListTitle: pm_FeatureSelectedListTitle,
                addAllButtonLabel: pm_FeatureAddAllButtonLabel,
                removeAllButtonLabel: pm_FeatureRemoveAllButtonLabel,
                addAllButtonClasses: 'btn btn-default',
                removeAllButtonClasses: 'btn btn-default',
            });
        });
    }
}