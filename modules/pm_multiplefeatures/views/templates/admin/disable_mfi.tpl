<fieldset class="panel">
    <p class="alert alert-warning">
        <span>{$translatedMessage|escape:'htmlall':'UTF-8'}</span>
    </p>

    {if version_compare($smarty.const._PS_VERSION_, '1.7.0.0', '>=')}
        {$displaySupport nofilter}{* HTML *}
    {else}
        {$displaySupport}{* HTML *}
    {/if}
</fieldset>
