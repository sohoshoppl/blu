<section id="sohoshop_storeask">
    <div class="sohoshop_storeaskContainer">
        <div class="sectionTitle text-center">
            <h3 class="title-storeask">{l s='Zrób z nami remont lub zaprojektuj swoje wnętrze' d='Modules.Sohoshopstoreask.Shop'}</h3>
            <p class="describe-storeask">{l s='Nie masz pomysłu na swoje wnętrze. Skontaktuj się z nami, wspólnie zaprojektujemy Twoją domową przestrzeń.' d='Modules.Sohoshopstoreask.Shop'}</p>
            <p class="text-storeask">{l s='Nasi doradcy są do Twojej dyspozycji' d='Modules.Sohoshopstoreask.Shop'}</p>
        </div>
        <form method="post">
            {* <input type="hidden" name="product" value="{$product.id}"/> *}
            {if $notifications}
                <div class="notification {if $notifications.nw_error}notification-error{else}notification-success{/if}">
                    <ul>
                        {foreach $notifications.messages as $notif}
                            <li>{$notif|escape:'htmlall':'UTF-8'}</li>
                        {/foreach}
                    </ul>
                </div>
            {/if}

            {if !$notifications || $notifications.nw_error}
                <div class="row">
                    <div class="col-lg-6 select">
                        <select name="shopTheme" class="form-control form-control-select">
                            <option value="" disabled selected>{l s='Wybierz zakres inwestycji' d='Modules.Sohoshopstoreask.Shop'}</option>
                            <option>
                                {l s='Inwestycja 1' d='Modules.Sohoshopstoreask.Shop'}
                            </option>
                            <option>
                                {l s='Inwestycja 2' d='Modules.Sohoshopstoreask.Shop'}
                            </option>
                            <option>
                                {l s='Inwestycja 3' d='Modules.Sohoshopstoreask.Shop'}
                            </option>
                            <option>
                                {l s='Inwestycja 4' d='Modules.Sohoshopstoreask.Shop'}
                            </option>
                        </select>
                    </div>
                    <div class="col-lg-6 select">
                        <select name="shopContacts" class="form-control form-control-select">
                            <option value="" disabled selected>{l s='Wybierz salon' d='Modules.Sohoshopstoreask.Shop'}</option>
                            {foreach from=$contact.stores item=contact_elt}
                                <option value="{$contact_elt.id_store|escape:'htmlall':'UTF-8'}">{$contact_elt.name}</option>
                            {/foreach}
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <input type="text" class="form-control" name="name"
                            placeholder="{l s='Twoje imię i nazwisko *' d='Modules.Sohoshopstoreask.Shop'}">
                    </div>
                    <div class="col-lg-6">
                        <input class="form-control" type="text" name="phone"
                            value="{$contact.email|escape:'htmlall':'UTF-8'}"
                            placeholder="{l s='Twój numer telefonu *' d='Modules.Sohoshopstoreask.Shop'}"/>
                    </div>

                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <input class="form-control" type="text" name="from"
                            value="{$contact.email|escape:'htmlall':'UTF-8'}"
                            placeholder="{l s='Twój adres e-mail *' d='Modules.Sohoshopstoreask.Shop'}"/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                    <textarea class="form-control" rows="6"
                              name="message"
                              placeholder="{l s='Treść' d='Modules.Sohoshopstoreask.Shop'}">{if $contact.message}{$contact.message|escape:'htmlall':'UTF-8'}{/if}</textarea>
                    </div>
                </div>
                <div>
                    <small>* {l s=' Twoje dane będziemy przetwarzać zgodnie z naszą' d='Modules.Sohoshopstoreask.Shop'}
                        <a href="{l s='#' d='Modules.Sohoshopstoreask.Shop'}" target="_blank">{l s='Polityką prywatności' d='Modules.Sohoshopstoreask.Shop'}</a></small>
                </div>
                <div class="text-center">

                    <button class="btn btn-primary" type="submit" name="submitAskStore">
                        {l s='Wyślij' d='Modules.Sohoshopstoreask.Shop'}
                        <svg class="fs-locator-search-ico" xmlns="http://www.w3.org/2000/svg" width="14.262" height="9.192" viewBox="0 0 12.262 7.192">
                            <defs></defs>
                            <g transform="translate(0.53 0.53)">
                                <path d="M-317.515-44.61l-5.6,5.6-5.6-5.6" transform="translate(328.717 44.61)"></path>
                            </g>
                        </svg>
                    </button>
                </div>
            {/if}
        </form>
    </div>
</section>
