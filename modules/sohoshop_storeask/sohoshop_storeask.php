<?php
/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

use PrestaShop\PrestaShop\Core\Module\WidgetInterface;

class Sohoshop_Storeask extends Module implements WidgetInterface
{
    /** @var Contact */
    protected $contact;

    public function __construct()
    {
        $this->name = 'sohoshop_storeask';
        $this->author = 'SohoShop.pl - Aleksander';
        $this->tab = 'front_office_features';
        $this->version = '1.0.0';
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->trans('Zapytaj o remont', [], 'Modules.Sohoshopstoreask.Admin');
        $this->description = $this->trans(
            'Adds a contact form to the "Contact us" page.',
            [],
            'Modules.Sohoshopstoreask.Admin'
        );
        $this->ps_versions_compliancy = [
            'min' => '1.7.2.0',
            'max' => _PS_VERSION_
        ];
    }

    
    /**
     * @return bool
     */
    public function install()
    {
        return parent::install() && $this->registerHook('displayStoreAsk');
    }

    /**
     * {@inheritdoc}
     */
    public function renderWidget($hookName = null, array $configuration = [])
    {
        if (!$this->active) {
            return;
        }

        $this->smarty->assign($this->getWidgetVariables($hookName, $configuration));

        return $this->display(__FILE__, 'views/templates/hook/askform.tpl');
    }

    /**
     * {@inheritdoc}
     */
    public function getWidgetVariables($hookName = null, array $configuration = [])
    {
        $notifications = false;

        if (Tools::isSubmit('submitAskStore')) {
            $this->sendMessage();

            if (!empty($this->context->controller->errors)) {
                $notifications['messages'] = $this->context->controller->errors;
                $notifications['nw_error'] = true;
            } elseif (!empty($this->context->controller->success)) {
                $notifications['messages'] = $this->context->controller->success;
                $notifications['nw_error'] = false;
            }
        }

        $this->contact['stores'] = Store::getStores($this->context->language->id);
        $this->contact['message'] = Tools::getValue('message');


        $this->contact['email'] = Tools::safeOutput(
            Tools::getValue(
                'from',
                !empty($this->context->cookie->email) && Validate::isEmail($this->context->cookie->email) ?
                    $this->context->cookie->email :
                    ''
            )
        );

        return [
            'contact' => $this->contact,
            'notifications' => $notifications,
            'id_module' => $this->id
        ];
    }

    /**
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function sendMessage()
    {
            $contactStore = Configuration::get('PS_SHOP_EMAIL'); 
            $storeName = Configuration::get('PS_SHOP_NAME');
            $message = trim(Tools::getValue('message'));
            $fromName = trim(Tools::getValue('name'));
            $phone = trim(Tools::getValue('phone'));
            $from = Tools::getValue('from');
            $theme = Tools::getValue('shopTheme');

            if (!($from = trim(Tools::getValue('from'))) || !Validate::isEmail($from)) {
                $this->context->controller->errors[] = $this->trans(
                    'Invalid email address.',
                    [],
                    'Shop.Notifications.Error'
                );
            } elseif (empty($message)) {
                $this->context->controller->errors[] = $this->trans(
                    'The message cannot be blank.',
                    [],
                    'Shop.Notifications.Error'
                );
            } elseif (!Validate::isCleanHtml($message)) {
                $this->context->controller->errors[] = $this->trans(
                    'Invalid message',
                    [],
                    'Shop.Notifications.Error'
                );
            } else {

                if (!count($this->context->controller->errors)
                    && empty($mailAlreadySend)
                ) {
                    $var_list = [
                        '{lastname}' => $fromName,
                        '{storename}' => $storeName,
                        '{phone}' => $phone,
                        '{message}' => Tools::nl2br(Tools::htmlentitiesUTF8(Tools::stripslashes($message))),
                        '{email}' => $from,
                        '{theme}' => $theme,
                    ];
                    // if (empty($contactStore) || !Mail::Send(
                    //         $this->context->language->id,
                    //         'sohoshop_storeask',
                    //         $this->trans('Message from contact form', [], 'Emails.Subject') . ' [no_sync]',
                    //         $var_list,
                    //         $contactStore,
                    //         $storeName,
                    //         null,
                    //         null,
                    //         null,
                    //         null,
                    //         dirname(__FILE__).'/mails/',
                    //         false,
                    //         null,
                    //         null,
                    //         $from
                    //     )) {
                    //     $this->context->controller->errors[] = $this->trans(
                    //         'An error occurred while sending the message. ZZZZZZZZ',
                    //         [],
                    //         'Modules.Contactform.Shop'
                    //     );
                    // }


                    if (!Mail::Send(
                        $this->context->language->id,
                        'sohoshop_storeask',
                        $this->trans('Remont', [], 'Emails.Subject'),
                        $var_list,
                        $contactStore,
                        null,
                        null,
                        null,
                        null,
                        null,
                        dirname(__FILE__).'/mails/',
                        false,
                        null,
                        null,
                        $from
                    )) {
                        $this->context->controller->errors[] = $this->trans(
                            'An error occurred while sending the message.',
                            [],
                            'Modules.Contactform.Shop'
                        );
                    }
                }

                if (!count($this->context->controller->errors)) {
                    $this->context->controller->success[] = $this->trans(
                        'Your message has been successfully sent to our team.',
                        [],
                        'Modules.Contactform.Shop'
                    );
                }
            }
    }
}
