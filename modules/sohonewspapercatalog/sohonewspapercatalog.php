<?php
/*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA

*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_CAN_LOAD_FILES_')) {
    exit;
}


include_once _PS_MODULE_DIR_.'sohonewspapercatalog/newpapercatalogClass.php';

class Sohonewspapercatalog extends Module
{
    public function __construct()
    {
        $this->name = 'sohonewspapercatalog';
        $this->author = 'Aleksander sohoshop.pl';
        $this->version = '3.0.1';

        $this->bootstrap = true;
        parent::__construct();

        $this->displayName = $this->trans('sohonewspapercatalog', array(), 'Modules.Sohonewspapercatalog.Admin');
        $this->description = $this->trans('sohonewspapercatalog on page', array(), 'Modules.Sohonewspapercatalog.Admin');

        $this->ps_versions_compliancy = array('min' => '1.7.2.0', 'max' => _PS_VERSION_);
    }

    public function install()
    {
        return parent::install()
            && $this->installDB()
            && Configuration::updateValue('sohonewspapercatalog_NBBLOCKS', 5)
            && $this->registerHook('actionUpdateLangAfter')
            && $this->registerHook('displayBackOfficeHeader');
    }

    public function installDB()
    {
        $return = true;
        $return &= Db::getInstance()->execute('
            CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'sohonewspapercatalog` (
                `id_sohonewspapercatalog` INT UNSIGNED NOT NULL AUTO_INCREMENT,
                `id_shop` int(10) unsigned NOT NULL ,
                `file_name` VARCHAR(100) NOT NULL,
                `file_name_list` VARCHAR(1000) NOT NULL,
                PRIMARY KEY (`id_sohonewspapercatalog`)
            ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;');

        $return &= Db::getInstance()->execute('
            CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'sohonewspapercatalog_lang` (
                `id_sohonewspapercatalog` INT UNSIGNED NOT NULL AUTO_INCREMENT,
                `id_lang` int(10) unsigned NOT NULL ,
                `title` VARCHAR(300) NOT NULL,
                PRIMARY KEY (`id_sohonewspapercatalog`, `id_lang`)
            ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 ;');

        return $return;
    }

    public function uninstall()
    {
        return Configuration::deleteByName('sohonewspapercatalog_NBBLOCKS') &&
            $this->uninstallDB() &&
            parent::uninstall();
    }

    public function uninstallDB()
    {
        return Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'sohonewspapercatalog`') && Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'sohonewspapercatalog_lang`');
    }

    public function addToDB()
    {
        if (isset($_POST['nbblocks'])) {
            for ($i = 1; $i <= (int)$_POST['nbblocks']; $i++) {
                $filename = explode('.', $_FILES['info'.$i.'_file']['name']);
                if (isset($_FILES['info'.$i.'_file']) && isset($_FILES['info'.$i.'_file']['tmp_name']) && !empty($_FILES['info'.$i.'_file']['tmp_name'])) {
                    if ($error = ImageManager::validateUpload($_FILES['info'.$i.'_file'])) {
                        return false;
                    } elseif (!($tmpName = tempnam(_PS_TMP_IMG_DIR_, 'PS')) || !move_uploaded_file($_FILES['info'.$i.'_file']['tmp_name'], $tmpName)) {
                        return false;
                    } elseif (!ImageManager::resize($tmpName, dirname(__FILE__).'/img/'.$filename[0].'.jpg')) {
                        return false;
                    }
                    unlink($tmpName);
                }
                Db::getInstance()->execute('INSERT INTO `'._DB_PREFIX_.'sohonewspapercatalog` (`filename`,`title`)
                                            VALUES ("'.((isset($filename[0]) && $filename[0] != '') ? pSQL($filename[0]) : '').
                    '", "'.((isset($_POST['info'.$i.'_title']) && $_POST['info'.$i.'_title'] != '') ? pSQL($_POST['info'.$i.'_title']) : '').'")');
            }
            return true;
        } else {
            return false;
        }
    }

    public function removeFromDB()
    {
        $dir = opendir(dirname(__FILE__).'/img');
        while (false !== ($file = readdir($dir))) {
            $path = dirname(__FILE__).'/img/'.$file;
            if ($file != '..' && $file != '.' && !is_dir($file)) {
                unlink($path);
            }
        }
        closedir($dir);

        return Db::getInstance()->execute('DELETE FROM `'._DB_PREFIX_.'sohonewspapercatalog`');
    }

    public function hookActionUpdateLangAfter($params)
    {
        if (!empty($params['lang']) && $params['lang'] instanceOf Language) {
            include_once _PS_MODULE_DIR_ . $this->name . '/lang/SohonewspapercatalogLang.php';

            Language::updateMultilangFromClass(_DB_PREFIX_ . 'sohonewspapercatalog_lang', 'SohonewspapercatalogLang', $params['lang']);
        }
    }

    public function getContent()
    {
        $html = '';
        $id_sohonewspapercatalog = (int)Tools::getValue('id_sohonewspapercatalog');

        if (Tools::isSubmit('savesohonewspapercatalog')) {
            if ($id_sohonewspapercatalog = Tools::getValue('id_sohonewspapercatalog')) {
                $sohonewspaper = new newpapercatalogClass((int)$id_sohonewspapercatalog);
            } else {
                $sohonewspaper = new newpapercatalogClass();
            }

            $sohonewspaper->copyFromPost();
            $sohonewspaper->id_shop = $this->context->shop->id;

            if ($sohonewspaper->validateFields(false) && $sohonewspaper->validateFieldsLang(false)) {
                $sohonewspaper->save();

                if (isset($_FILES['image']) && isset($_FILES['image']['tmp_name']) && !empty($_FILES['image']['tmp_name'])) {
                    if ($error = ImageManager::validateUpload($_FILES['image'])) {
                        return false;
                    } elseif (!($tmpName = tempnam(_PS_TMP_IMG_DIR_, 'PS')) || !move_uploaded_file($_FILES['image']['tmp_name'], $tmpName)) {
                        return false;
                    } elseif (!ImageManager::resize($tmpName, dirname(__FILE__).'/img/sohonewspaper-'.(int)$sohonewspaper->id.'-'.(int)$sohonewspaper->id_shop.'.jpg')) {
                        return false;
                    }

                    unlink($tmpName);
                    $sohonewspaper->file_name = 'sohonewspaper-'.(int)$sohonewspaper->id.'-'.(int)$sohonewspaper->id_shop.'.jpg';
                    $sohonewspaper->save();
                }
                if ( Tools::getValue('file_name_list') && !empty('file') ) {
                    $filelist = array();
                    foreach ($_FILES['file_name_list']['name'] as $key=>$name) {
                            $ext = pathinfo($_FILES["file_name_list"]["name"][$key], PATHINFO_EXTENSION);
                            $file_name = md5($name).'.'.$ext;
                            $filelist[] = $file_name;
                            move_uploaded_file($_FILES['file_name_list']['tmp_name'][$key], dirname(__FILE__).DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.$file_name);
                    }
                    
                    $filestring = implode(",", $filelist);
                    $sohonewspaper->file_name_list = $filestring;
                    $sohonewspaper->save();

                }
                $this->_clearCache('*');
            } else {
                $html .= '<div class="conf error">'.$this->trans('An error occurred while attempting to save.', array(), 'Admin.Notifications.Error').'</div>';
            }
        }

        if (Tools::isSubmit('updatesohonewspapercatalog') || Tools::isSubmit('addsohonewspapercatalog')) {
            $helper = $this->initForm();
            foreach (Language::getLanguages(false) as $lang) {
                if ($id_sohonewspapercatalog) {
                    $sohonewspaper = new newpapercatalogClass((int)$id_sohonewspapercatalog);
                    $helper->fields_value['title'][(int)$lang['id_lang']] = $sohonewspaper->title[(int)$lang['id_lang']];
                    $image = dirname(__FILE__).DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.$sohonewspaper->file_name;
                    $this->fields_form[0]['form']['input'][0]['image'] = '<img src="'.$this->getImageURL($sohonewspaper->file_name).'" />';
                } else {
                    $helper->fields_value['title'][(int)$lang['id_lang']] = Tools::getValue('title_'.(int)$lang['id_lang'], '');
                }
            }
            if ($id_sohonewspapercatalog = Tools::getValue('id_sohonewspapercatalog')) {
                $this->fields_form[0]['form']['input'][] = array('type' => 'hidden', 'name' => 'id_sohonewspapercatalog');
                $helper->fields_value['id_sohonewspapercatalog'] = (int)$id_sohonewspapercatalog;
            }

            return $html.$helper->generateForm($this->fields_form);
        } elseif (Tools::isSubmit('deletesohonewspapercatalog')) {

            
            $sohonewspaper = new newpapercatalogClass((int)$id_sohonewspapercatalog);
            if (file_exists(dirname(__FILE__).'/img/'.$sohonewspaper->file_name)) {
                unlink(dirname(__FILE__).'/img/'.$sohonewspaper->file_name);
            }
            $sohonewspaper->delete();
            $this->_clearCache('*');
            Tools::redirectAdmin(AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'));
        } else {
            $content = $this->getListContent((int)Configuration::get('PS_LANG_DEFAULT'));
            $helper = $this->initList();
            $helper->listTotal = count($content);
            return $html.$helper->generateList($content, $this->fields_list);
        }

        if (isset($_POST['submitModule'])) {
            Configuration::updateValue('sohonewspapercatalog_NBBLOCKS', ((isset($_POST['nbblocks']) && $_POST['nbblocks'] != '') ? (int)$_POST['nbblocks'] : ''));
            if ($this->removeFromDB() && $this->addToDB()) {
                $output = '<div class="conf confirm">'.$this->trans('The block configuration has been updated.', array(), 'Modules.Sohonewspapercatalog.Admin').'</div>';
            } else {
                $output = '<div class="conf error"><img src="../img/admin/disabled.gif"/>'.$this->trans('An error occurred while attempting to save.', array(), 'Admin.Notifications.Error').'</div>';
            }
        }
    }

    protected function getListContent($id_lang)
    {
        return  Db::getInstance()->executeS('
            SELECT r.`id_sohonewspapercatalog`, r.`id_shop`, r.`file_name`,r.`file_name_list`, rl.`title`
            FROM `'._DB_PREFIX_.'sohonewspapercatalog` r
            LEFT JOIN `'._DB_PREFIX_.'sohonewspapercatalog_lang` rl ON (r.`id_sohonewspapercatalog` = rl.`id_sohonewspapercatalog`)
            WHERE `id_lang` = '.(int)$id_lang.' '.Shop::addSqlRestrictionOnLang());
    }

    protected function initForm()
    {
        $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
        $imagesString = '';
        $gallerylist = $this->getGalleryLinks();
        if ($gallerylist) {
            $len = count($gallerylist);
            $idgallery=(int)Tools::getValue('id_sohonewspapercatalog');
            foreach ($gallerylist as $key=>$image) {
                
                if ($key == 0) {
                    if ($image == '' ) {
                        $imagesString=$imagesString.'<div class="container-gallery" style="display:flex;flex-wrap: wrap;"></div>';
                    } else {
                        $imagesString=$imagesString.'<div class="container-gallery" style="display:flex;flex-wrap: wrap;"><div class="col-xs-2" style="position:relative;min-width: 120px;"><img class="imgm img-thumbnail" style="width:100%" src="'._MODULE_DIR_.'sohonewspapercatalog/img/'.$image.'"></div>';
                    }
                } elseif($key == $len - 1) {
                    $imagesString=$imagesString.'<div class="col-xs-2" style="position:relative;min-width: 120px;"><img class="imgm img-thumbnail" style="width:100%" src="'._MODULE_DIR_.'sohonewspapercatalog/img/'.$image.'"></div></div>';
                } else {
                    $imagesString=$imagesString.'<div class="col-xs-2" style="position:relative;min-width: 120px;"><img class="imgm img-thumbnail" style="width:100%" src="'._MODULE_DIR_.'sohonewspapercatalog/img/'.$image.'"></div>';
                }           
            }
        }


        $this->fields_form[0]['form'] = array(
            'legend' => array(
                'title' => $this->trans('New sohonewspaper block', array(), 'Modules.Sohonewspapercatalog.Admin'),
            ),
            'input' => array(
                array(
                    'type' => 'file',
                    'label' => $this->trans('Image', array(), 'Admin.Global'),
                    'name' => 'image',
                    'value' => true,
                    'display_image' => true,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->trans('title', array(), 'Admin.Global'),
                    'lang' => true,
                    'name' => 'title',
                ),
                array(
                        'type' => 'file',
                        'label' => $this->l('Katalogi upload'),
                        'name' => 'file_name_list',
                        'multiple' => true,
                ),
                array(
                    'type' => 'html',
                    'name' => 'button_remove',
                    'html_content' => '<button class="delete-gallery" data-id_sohonewspapercatalog="'.(int)Tools::getValue('id_sohonewspapercatalog').'" data-token="'.Tools::getAdminTokenLite('AdminModules').'">Usuń galerię</button>',
                    'col' => 12,
                 ), 
                array(
                    'type' => 'html',
                    'name' => 'file_name_list',
                    'label' => $this->l('Lista katalogów'),
                    'html_content' => $imagesString,
                    'col' => 12,
                 ), 
            ),
            'submit' => array(
                'title' => $this->trans('Save', array(), 'Admin.Actions'),
            )
        );

        $helper = new HelperForm();
        $helper->module = $this;
        $helper->name_controller = 'sohonewspapercatalog';
        $helper->identifier = $this->identifier;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        foreach (Language::getLanguages(false) as $lang) {
            $helper->languages[] = array(
                'id_lang' => $lang['id_lang'],
                'iso_code' => $lang['iso_code'],
                'name' => $lang['name'],
                'is_default' => ($default_lang == $lang['id_lang'] ? 1 : 0)
            );
        }

        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;
        $helper->toolbar_scroll = true;
        $helper->title = $this->displayName;
        $helper->submit_action = 'savesohonewspapercatalog';
        $helper->toolbar_btn =  array(
            'save' =>
            array(
                'desc' => $this->trans('Save', array(), 'Admin.Actions'),
                'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'),
            ),
            'back' =>
            array(
                'href' => AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'),
                'desc' => $this->trans('Back to list', array(), 'Admin.Actions'),
            )
        );
        return $helper;
    }

    public function getGalleryLinks() {

        $id = (int)Tools::getValue('id_sohonewspapercatalog');
        $s = new DbQuery();
        $s->select('file_name_list');
        $s->from('sohonewspapercatalog');
        $s->where('id_sohonewspapercatalog = '.$id);
        $images= Db::getInstance()->executeS($s);
        if ($images) {
            $imagetable = explode(",", $images[0]['file_name_list']);
        }
        else {
            $imagetable = '';
        }
        return $imagetable;
    }

    protected function initList()
    {
        $this->fields_list = array(
            'id_sohonewspapercatalog' => array(
                'title' => $this->trans('ID', array(), 'Admin.Global'),
                'width' => 120,
                'type' => 'text',
                'search' => false,
                'orderby' => false
            ),
            'title' => array(
                'title' => $this->trans('Text', array(), 'Admin.Global'),
                'width' => 140,
                'type' => 'text',
                'search' => false,
                'orderby' => false
            ),
        );

        if (Shop::isFeatureActive()) {
            $this->fields_list['id_shop'] = array(
                'title' => $this->trans('ID Shop', array(), 'Modules.Sohonewspapercatalog.Admin'),
                'align' => 'center',
                'width' => 25,
                'type' => 'int'
            );
        }

        $helper = new HelperList();
        $helper->shopLinkType = '';
        $helper->simple_header = false;
        $helper->identifier = 'id_sohonewspapercatalog';
        $helper->actions = array('edit', 'delete');
        $helper->show_toolbar = true;
        $helper->imageType = 'jpg';
        $helper->toolbar_btn['new'] =  array(
            'href' => AdminController::$currentIndex.'&configure='.$this->name.'&add'.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'),
            'desc' => $this->trans('Add new', array(), 'Admin.Actions')
        );

        // $helper->title = $this->displayName;
        $helper->title = 'sohonewspapercatalog banners';
        $helper->table = $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        return $helper;
    }

    private function getImageURL($image)
    {
        return $this->context->link->getMediaLink(__PS_BASE_URI__.'modules/'.$this->name.'/img/'.$image);
    }
    public function hookDisplayBackOfficeHeader()
    {
        $this->context->controller->addJS($this->_path.'views/js/back.js');
    }
}
