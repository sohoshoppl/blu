<?php 

class Sohoshop_ServicesServicesModuleFrontController extends ModuleFrontController{
    public function initContent(){
        parent::initContent();
        $this->setTemplate('module:sohoshop_services/views/templates/front/services.tpl');
    }

    public function getBreadcrumbLinks(){
        $breadcrumb = parent::getBreadcrumbLinks();

        $breadcrumb['links'][] = array(
            'title' => $this->trans('Usługi', array(), 'Shop.Notifications.Success'),
            'url' => '',
        );


        return $breadcrumb;

    }
}