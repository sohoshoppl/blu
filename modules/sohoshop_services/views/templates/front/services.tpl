

{extends file="page.tpl"}

{block name='content'}
    <div class="services-mobile-title">{l s='Usługi' mod='sohoshop_services'}</div>
    <div id="services">
        <div class="banner">
            <div class="flexbox title">{l s='Usługi' mod='sohoshop_services'}</div>
            <div class="flexbox middle-text">{l s='Wybierz usługę, której szukasz i sprawdź w których salonach jest dostępna' mod='sohoshop_services'}</div>
            <div class="flexbox bottom-text">{l s='Usługi projektanta dostępne są w każdym salonie BLU.' mod='sohoshop_services'}<br>{l s='Przy zakupie łazienki projekt otrzymasz gratis.' mod='sohoshop_services'}</div>
            <div class="flexbox bottom-text-mobile">{l s='Usługi projektanta dostępne są' mod='sohoshop_services'}<br>{l s='w każdym salonie BLU.' mod='sohoshop_services'} <br>{l s='Przy zakupie łazienki projekt otrzymasz gratis.' mod='sohoshop_services'}</div>
        </div>
        <div class="background-grid">

            <div class="flexbox bottom-title">{l s='Dostępne w naszych salonach:' mod='sohoshop_services'}</div>
            <div class="flexbox bottom-title-mobile">{l s='W naszych salonach:' mod='sohoshop_services'}</div>
            <div class="grid">
                <div class="col-lg-3 col-xs-6">
                    <div class="flexbox spacing"><img src="{$urls.img_url}wnies-towar.svg"/></div>
                    <div class="flexbox">{l s='Odbierzesz towar zamówiony przez stronę, telefonicznie lub mailowo' mod='sohoshop_services'}</div>
                </div>
                <div class="col-lg-3 col-xs-6">
                    <div class="flexbox spacing"><img src="{$urls.img_url}checklist.svg"/></div>
                    <div class="flexbox">{l s='Przed odbiorem towaru obsługa sprawdzi, czy nie ma żadnych uszkodzeń' mod='sohoshop_services'}</div>
                </div>
                <div class="col-lg-3 col-xs-6">
                    <div class="flexbox spacing"><img src="{$urls.img_url}ludek.svg"/></div>
                    <div class="flexbox">{l s='Fachowa obsługa dobierze niezbędne akcesoria do Twoich zakupów' mod='sohoshop_services'}</div>
                </div>
                <div class="col-lg-3 col-xs-6">
                    <div class="flexbox spacing"><img src="{$urls.img_url}komputerowa_wizualizacja.svg"/></div>
                    <div class="flexbox">{l s='Otrzymasz komputerową wizualizację łazienki w cenie zakupu' mod='sohoshop_services'}</div>
                </div>
                <div class="col-lg-3 col-xs-6">
                    <div class="flexbox spacing"><img src="{$urls.img_url}plytka.svg"/></div>
                    <div class="flexbox">{l s='Nadrukujesz zdjęcie lub fototapetę na płytce' mod='sohoshop_services'}</div>
                </div>
                <div class="col-lg-3 col-xs-6">
                    <div class="flexbox spacing"><img src="{$urls.img_url}raty.svg"/></div>
                    <div class="flexbox">{l s='Kupisz na raty' mod='sohoshop_services'}</div>
                </div>
                <div class="col-lg-3 col-xs-6">
                    <div class="flexbox spacing"><img src="{$urls.img_url}reklamacja.svg"/></div>
                    <div class="flexbox">{l s='Zgłosisz reklamacje bez konieczności wysyłania towaru' mod='sohoshop_services'}</div>
                </div>
                <div class="col-lg-3 col-xs-6">
                    <div class="flexbox spacing"><img src="{$urls.img_url}dom.svg"/></div>
                    <div class="flexbox">{l s='Dostarczymy Ci towar prosto do domu' mod='sohoshop_services'}</div>
                </div>
                <div class="col-lg-3 col-xs-6">
                    <div class="flexbox spacing"><img src="{$urls.img_url}projekt.svg"/></div>
                    <div class="flexbox">{l s='Zaprojektujesz łazienkę z profesjonalnym projektantem' mod='sohoshop_services'}</div>
                </div>
                <div class="col-lg-3 col-xs-6">
                    <div class="flexbox spacing"><img src="{$urls.img_url}rabat.svg"/></div>
                    <div class="flexbox">{l s='Dostaniesz promocyjną cenę z gazetki' mod='sohoshop_services'}</div>
                </div>
            </div>
            
        </div>
    </div>

{/block}

