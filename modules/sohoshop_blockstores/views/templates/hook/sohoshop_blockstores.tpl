<div id="sohoshop_blockstores">
    <div class="sectionTitle text-center">{l s='Dowiedz się więcej o produkcie w najbliższym salonie' d='Modules.Sohoshopblockstores.Shop'}</div>
    {foreach name=stores from=$stores item=store}
        <div class="row">
            <div class="col-xs-12"><h3>{$store.name}</h3></div>
            <div class="col-lg-4">
                <img class="map-pin" src="{$urls.img_url}svg_icons/map_pin.svg"
                     alt="{l s='Adres sklepu' d='Modules.Sohoshopblockstores.Shop'}"
                     title="{l s='Adres sklepu' d='Modules.Sohoshopblockstores.Shop'}">
                <address>{$store.address.formatted nofilter}</address>
                {if $store.phone}
                <div>{l s='tel.' d='Modules.Sohoshopblockstores.Shop'} {$store.phone}</div>
                {/if}
                {if $store.email}
                    <a href="mailto:{$store.email}"  target="_blank">{$store.email}"</a>
                {/if}
            </div>
            <div class="col-lg-4">
                <div>{l s='Godziny otwarcia:' d='Modules.Sohoshopblockstores.Shop'}</div>
                {if isset($store.business_hours[0])}<div>{l s='Pon. - Pt.' d='Modules.Sohoshopblockstores.Shop'} {$store.business_hours[0]['hours'][0]}</div>{/if}
                <div>{if isset($store.business_hours[5])}{l s='Sob.' d='Modules.Sohoshopblockstores.Shop'} {$store.business_hours[5]['hours'][0]}{/if}{if isset($store.business_hours[6])}, {l s='Niedz' d='Shop.Theme.Catalog'} {$store.business_hours[6]['hours'][0]}{/if}</div>
            </div>
        </div>
        <hr>
    {/foreach}
    <div class="text-center"><a class="btn btn-primary" href="{url entity=stores}">{l s='Znajdź inne salony' d='Modules.Sohoshopblockstores.Shop'}</a></div>
</div>
