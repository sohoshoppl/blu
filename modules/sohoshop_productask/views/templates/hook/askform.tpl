<section class="w_salonie clearfix mt-3" id="sohoshop_blockstores">
    {assign var="stores" value=(Store::getStores($language.id))}
    <h2 class="section-title">
        {l s='Dowiedz się więcej o produkcie w najbliższym salonie' d='Shop.Theme.Catalog'}
    </h2>
    {foreach from=$stores item=store name=stores}
        {if $smarty.foreach.stores.iteration lt 3}    
            <div class="w_salonie-salony clearfix">
                <h2 class="title">{$store.name}</h2>
                <address>
                    <img src="{$urls.img_url}point-icon.svg"/>
                    {$store.postcode} {$store.city}<br>{$store.address1}<br><br>
                    tel. {$store.phone}<br> <a href="mailto:{$store.email}">{$store.email}</a>
                </address>
                <div class="w_salonie-salony-godziny">
                    {assign var="hours" value=(json_decode($store.hours))}
                    {l s='Godziny otwarcia' d='Shop.Theme.Catalog'}: {if isset($hours[0][0])}<br>Pon. - Pt. {$hours[0][0]}{/if}{if isset($hours[5][0])} <br>Sob. {$hours[5][0]}{/if}{if isset($hours[6][0])}, Niedz {$hours[6][0]}{/if}
                </div>
                <a href="#"><img class="next-arrow" src="{$urls.img_url}next-arrow.svg"></a>
            </div>
            <hr>                
        {/if}
    {/foreach}
    <div class="text-center">
        <a class="btn btn-primary" href='/salony'> {l s='Znajdź inne salony >' d='Shop.Theme.Catalog'} </a>
    </div>
</section>    

<section id="sohoshop_productask">
    <div class="sohoshop_productaskContainer">
        <div class="sectionTitle text-center">{l s='Zapytaj o produkt' d='Modules.Sohoshopproductask.Shop'}</div>
        <form method="post">
            <input type="hidden" name="product" value="{$product.id}"/>
            {if $notifications}
                <div class="notification {if $notifications.nw_error}notification-error{else}notification-success{/if}">
                    <ul>
                        {foreach $notifications.messages as $notif}
                            <li>{$notif|escape:'htmlall':'UTF-8'}</li>
                        {/foreach}
                    </ul>
                </div>
            {/if}

            {if !$notifications || $notifications.nw_error}
                <div class="row">
                    <div class="col-lg-6">
                        <input type="text" class="form-control" name="name"
                               placeholder="{l s='Twoje imię i nazwisko *' d='Modules.Sohoshopproductask.Shop'}">
                    </div>
                    <div class="col-lg-6 select">
                        <select name="shopContacts" class="form-control form-control-select">
                            <option value="" disabled selected>{l s='Wybierz salon' d='Modules.Sohoshopproductask.Shop'}</option>
                            {foreach from=$contact.stores item=contact_elt}
                                <option value="{$contact_elt.id_store|escape:'htmlall':'UTF-8'}">{$contact_elt.name}</option>
                            {/foreach}
                        </select></div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <input class="form-control" type="text" name="from"
                               value="{$contact.email|escape:'htmlall':'UTF-8'}"
                               placeholder="{l s='Twój adres e-mail *' d='Modules.Sohoshopproductask.Shop'}"/>
                    </div>
                    <div class="col-lg-6">
                        <input class="form-control" type="text" name="phone"
                               value="{$contact.email|escape:'htmlall':'UTF-8'}"
                               placeholder="{l s='Twój numer telefonu *' d='Modules.Sohoshopproductask.Shop'}"/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">

                    <textarea class="form-control" rows="6"
                              name="message"
                              placeholder="{l s='Treść' d='Modules.Sohoshopproductask.Shop'}">{if $contact.message}{$contact.message|escape:'htmlall':'UTF-8'}{/if}</textarea>
                    </div>
                </div>
                <div>
                    <small>* {l s=' Twoje dane będziemy przetwarzać zgodnie z naszą' d='Modules.Sohoshopproductask.Shop'}
                        <a href="#" target="_blank">{l s='Polityką prywatności' d='Modules.Sohoshopproductask.Shop'}</a></small>
                </div>
                <div class="text-center">

                    <button class="btn btn-primary" type="submit" name="submitAskProduct">
                        {l s='Wyślij' d='Modules.Sohoshopproductask.Shop'}
                    </button>
                </div>
            {/if}
        </form>
    </div>
</section>
