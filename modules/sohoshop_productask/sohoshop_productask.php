<?php
/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

use PrestaShop\PrestaShop\Core\Module\WidgetInterface;

class Sohoshop_Productask extends Module implements WidgetInterface
{
    /** @var Contact */
    protected $contact;

    public function __construct()
    {
        $this->name = 'sohoshop_productask';
        $this->author = 'SohoShop.pl - marcinJ';
        $this->tab = 'front_office_features';
        $this->version = '1.0.0';
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->trans('Zapytaj o produkt', [], 'Modules.Sohoshopproductask.Admin');
        $this->description = $this->trans(
            'Adds a contact form to the "Contact us" page.',
            [],
            'Modules.Sohoshopproductask.Admin'
        );
        $this->ps_versions_compliancy = [
            'min' => '1.7.2.0',
            'max' => _PS_VERSION_
        ];
    }

    /**
     * @return bool
     */
    public function install()
    {
        return parent::install() && $this->registerHook('displayFooterProduct');
    }

    /**
     * {@inheritdoc}
     */
    public function renderWidget($hookName = null, array $configuration = [])
    {
        if (!$this->active) {
            return;
        }
        $this->smarty->assign($this->getWidgetVariables($hookName, $configuration));

        return $this->display(__FILE__, 'views/templates/hook/askform.tpl');
    }

    /**
     * {@inheritdoc}
     */
    public function getWidgetVariables($hookName = null, array $configuration = [])
    {
        $notifications = false;

        if (Tools::isSubmit('submitAskProduct')) {
            $this->sendMessage();

            if (!empty($this->context->controller->errors)) {
                $notifications['messages'] = $this->context->controller->errors;
                $notifications['nw_error'] = true;
            } elseif (!empty($this->context->controller->success)) {
                $notifications['messages'] = $this->context->controller->success;
                $notifications['nw_error'] = false;
            }
        }

        $this->contact['stores'] = Store::getStores($this->context->language->id);
        $this->contact['message'] = Tools::getValue('message');

        $this->contact['orders'] = [];

        $this->contact['email'] = Tools::safeOutput(
            Tools::getValue(
                'from',
                !empty($this->context->cookie->email) && Validate::isEmail($this->context->cookie->email) ?
                    $this->context->cookie->email :
                    ''
            )
        );

        return [
            'contact' => $this->contact,
            'notifications' => $notifications,
            'id_module' => $this->id
        ];
    }

    /**
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function sendMessage()
    {
        $id_product = Tools::getValue('product');
        $id_store = Tools::getValue('shopContacts');
        $contactStore = new Store($id_store);
        if ($id_product && Validate::isLoadedObject($contactStore)) {
            $message = trim(Tools::getValue('message'));
            $fromName = trim(Tools::getValue('name'));

            if (!($from = trim(Tools::getValue('from'))) || !Validate::isEmail($from)) {
                $this->context->controller->errors[] = $this->trans(
                    'Invalid email address.',
                    [],
                    'Shop.Notifications.Error'
                );
            } elseif (empty($message)) {
                $this->context->controller->errors[] = $this->trans(
                    'The message cannot be blank.',
                    [],
                    'Shop.Notifications.Error'
                );
            } elseif (!Validate::isCleanHtml($message)) {
                $this->context->controller->errors[] = $this->trans(
                    'Invalid message',
                    [],
                    'Shop.Notifications.Error'
                );
            } else {

                if (!count($this->context->controller->errors)
                    && empty($mailAlreadySend)
                ) {
                    $var_list = [
                        '{firstname}' => '',
                        '{lastname}' => $fromName,
                        '{message}' => Tools::nl2br(Tools::htmlentitiesUTF8(Tools::stripslashes($message))),
                        '{email}' => $from,
                        '{product_name}' => '',
                    ];

                    if ($id_product) {
                        $product = new Product((int)$id_product);

                        if (Validate::isLoadedObject($product) &&
                            isset($product->name[Context::getContext()->language->id])
                        ) {
                            $var_list['{product_name}'] = $product->name[Context::getContext()->language->id];
                        }
                    }

                    if (empty($contactStore->email) || !Mail::Send(
                            $this->context->language->id,
                            'sohoshop_productask',
                            $this->trans('Message from contact form', [], 'Emails.Subject') . ' [no_sync]',
                            $var_list,
                            $contactStore->email,
                            $contactStore->name,
                            null,
                            null,
                            null,
                            null,
                            dirname(__FILE__).'/mails/',
                            false,
                            null,
                            null,
                            $from
                        )) {
                        $this->context->controller->errors[] = $this->trans(
                            'An error occurred while sending the message.',
                            [],
                            'Modules.Contactform.Shop'
                        );
                    }

                    if (!Mail::Send(
                        $this->context->language->id,
                        'sohoshop_productask',
                        $this->trans('Your message has been correctly sent', [], 'Emails.Subject'),
                        $var_list,
                        $from,
                        null,
                        null,
                        null,
                        null,
                        null,
                        dirname(__FILE__).'/mails/',
                        false,
                        null,
                        null,
                        $contactStore->email
                    )) {
                        $this->context->controller->errors[] = $this->trans(
                            'An error occurred while sending the message.',
                            [],
                            'Modules.Contactform.Shop'
                        );
                    }
                }

                if (!count($this->context->controller->errors)) {
                    $this->context->controller->success[] = $this->trans(
                        'Your message has been successfully sent to our team.',
                        [],
                        'Modules.Contactform.Shop'
                    );
                }
            }
        }
    }
}
