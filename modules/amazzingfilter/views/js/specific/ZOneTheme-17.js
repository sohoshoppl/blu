/**
*  2007-2019 PrestaShop
*
*  @author    Amazzing
*  @copyright Amazzing
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/
customThemeActions.updateContentAfter = function (jsonData) {
	var $gridOptions =  $('#product_display_control').find('a'),
        e = window.localStorage || window.sessionStorage;
    if (typeof e == 'object') {
        e.productListView ? $gridOptions.filter('[data-view="'+e.productListView+'"]').click() :
        $gridOptions.filter('.selected').click();
    }
}
/* since 2.8.7 */
