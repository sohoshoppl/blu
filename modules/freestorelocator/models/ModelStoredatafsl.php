<?php
/**
* 2007-2017 Evol.digital
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web
*
* DISCLAIMER
*
*
*  @author Jerome Aldigier
*  @copyright  2007-2017 Jérôme Aldigier
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  @version  Release: 1.0
*  International Registered Trademark & Property of Jerome Aldigier
*/

class ModelStoredatafsl extends ObjectModel
{
    public $id_storedatafsl;
    public $id_store;
    public $active;
    public $display;
    public $create_page;
    public $max_orders;
    public $htmldesc;
    public $htmldesc2;
    public $web_link;
    public $facebook_link;
    public $insta_link;
    public $linkedin_link;

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'storedatafsl',
        'primary' => 'id_storedatafsl',
        'multilang' => true,
        'multilang_shop' => false,
        'fields' => array(
            'id_store'     => array('type' => self::TYPE_INT,'required' => true, 'validate' => 'isUnsignedId'),
            'active'    => array('type' => self::TYPE_INT,  'validate' => 'isBool'),
            'display'     => array('type' => self::TYPE_INT,  'validate' => 'isBool'),
            'create_page'    => array('type' => self::TYPE_INT,  'validate' => 'isBool'),
            'htmldesc'    => array('type' => self::TYPE_HTML , 'lang' => true),
            'htmldesc2'    => array('type' => self::TYPE_HTML , 'lang' => true),
            'web_link'    => array('type' => self::TYPE_STRING , 'lang' => true),
            'facebook_link'    => array('type' => self::TYPE_STRING, 'lang' => true),
            'insta_link'    => array('type' => self::TYPE_STRING, 'lang' => true),
            'linkedin_link'    => array('type' => self::TYPE_STRING, 'lang' => true),
        )
    );
}
