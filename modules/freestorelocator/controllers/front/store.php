<?php
/**
* 2007-2018 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    Jérôme Aldigier <jerome@evol.digital>
*  @copyright 2007-2019 evol.digital
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class FreestorelocatorStoreModuleFrontController extends ModuleFrontController
{
    private $templatePath;
    public function __construct()
    {
        parent::__construct();
        $this->context = Context::getContext();
        $this->templatePath = _PS_MODULE_DIR_.'/freestorelocator/views/templates/front/';
    }


    public function setMedia()
    {
        parent::setMedia();
        $this->path = __PS_BASE_URI__.'modules/freestorelocator/';
        $this->addJS($this->path . 'views/js/leaflet.js');
        $this->addJS($this->path . 'views/js/leaflet-provider.js');
        $this->addCSS($this->path . 'views/css/leaflet.css');
        $this->addCSS($this->path . 'views/css/front.css');
    }

    public function init()
    {
        parent::init();
        $this->id_store =  Tools::getValue('id_store');


        //Données enrichies
        $stores_data = Db::getInstance()->executeS('
        SELECT * 
        FROM ' . _DB_PREFIX_ . 'storedatafsl s
        ' . Shop::addSqlAssociation('storedatafsl', 's') . '
        LEFT JOIN ' . _DB_PREFIX_ . 'storedatafsl_lang sl ON (
        sl.id_storedatafsl = s.id_storedatafsl
        AND sl.id_lang = ' .(int)$this->context->language->id . '
        )
        WHERE s.active = 1 AND s.id_store='.(int)$this->id_store);

        
        if (isset($stores_data) && isset($stores_data[0])) {
            if ($stores_data[0]['create_page'] == 0) {
                Tools::redirect(_PS_BASE_URL_SSL_);
            }
        } else {
            Tools::redirect(_PS_BASE_URL_SSL_);
        }


        $store = Db::getInstance()->executeS('select s.*,ps.* from '._DB_PREFIX_.'store s, '._DB_PREFIX_.'store_lang ps where ps.id_store = s.id_store and ps.id_lang = '.(int)$this->context->language->id.' and s.id_store ='.(int)$this->id_store.'');
        $store = $store[0];
        $this->context->smarty->assign(array('meta_title' => $store['name']));
        $this->context->smarty->assign(array('meta_description' => "Page de la boutique ".$store['name'].' à '. $store['city']));
        $this->context->smarty->assign(array('meta_keywords' => $store['name'].', '. $store['city']));
    }

    public function getGalleryLinks() {
        
        $id = Tools::getValue('id_store');
        $s = new DbQuery();
        $s->select('file_name');
        $s->from('storedatafsl_gallery');
        $s->where('id_storedatafsl = '.$id);
        $images= Db::getInstance()->executeS($s);

        return $images;
    }

    public function initContent()
    {
        parent::initContent();
        $this->id_store =  Tools::getValue('id_store');
        $store = Db::getInstance()->executeS('select s.*,ps.* from '._DB_PREFIX_.'store s, '._DB_PREFIX_.'store_lang ps where ps.id_store = s.id_store and ps.id_lang = '.(int)$this->context->language->id.' and s.id_store ='.(int)$this->id_store.'');
        $store = $store[0];
        $addresses_formated = array();
        $hours = $this->renderStoreWorkingHours($store);
        $microdata_hours =  $this->renderMicrodataStoreWorkingHours($store);


        //Données enrichies
        $stores_data = Db::getInstance()->executeS('
        SELECT * 
        FROM ' . _DB_PREFIX_ . 'storedatafsl s
        ' . Shop::addSqlAssociation('storedatafsl', 's') . '
        LEFT JOIN ' . _DB_PREFIX_ . 'storedatafsl_lang sl ON (
        sl.id_storedatafsl = s.id_storedatafsl
        AND sl.id_lang = ' .(int)$this->context->language->id . '
        )
        WHERE s.active = 1 AND s.id_store='.(int)$store['id_store']);

        if (isset($stores_data) && isset($stores_data[0])) {
            $store['web_link'] = $stores_data[0]['web_link'];
            $store['facebook_link'] = $stores_data[0]['facebook_link'];
            $store['insta_link'] = $stores_data[0]['insta_link'];
            $store['html_desc'] = html_entity_decode($stores_data[0]['htmldesc']);
            $store['html_desc2'] = html_entity_decode($stores_data[0]['htmldesc2']);
        } else {
            $store['web_link'] = "";
            $store['facebook_link'] = "";
            $store['insta_link'] = "";
            $store['html_desc'] = "";
            $store['html_desc2'] = "";
        }



        $store['has_picture'] = file_exists(_PS_STORE_IMG_DIR_.(int)$store['id_store'].'.jpg');
        $address = new Address();
        $address->country = Country::getNameById($this->context->language->id, $store['id_country']);
        $address->address1 = $store['address1'];
        $address->address2 = $store['address2'];
        $address->postcode = $store['postcode'];
        $address->city = $store['city'];
        $addresses_formated = AddressFormat::getFormattedLayoutData($address);

        $store['has_picture'] = file_exists(_PS_STORE_IMG_DIR_.(int)$store['id_store'].'.jpg');


        $lat =  (float)Configuration::get('FREESTORELOCATOR_LATITUDE');
        $lon = (float)Configuration::get('FREESTORELOCATOR_LONGITUDE');
        

        $urlback = $this->context->link->getPageLink("module-freestorelocator-page");
        $gallery = $this->getGalleryLinks();
        $projekty = $this->renderProjekty($this->id_store);
        $this->context->smarty->assign(array(
            'darkmode' => Configuration::get('FREESTORELOCATOR_DARK_MODE'),
            'store' => $store,
            'addresses_formated' => $addresses_formated,
            'days_datas' => $hours,
            'id_country' =>$store['id_country'],
            'urlback' => $urlback,
            'gallery' => $gallery,
            'projekty' => $projekty,
        ));

        return $this->setTemplate('module:freestorelocator/views/templates/front/storepage.tpl');
    }


    public function renderStoreWorkingHours($store)
    {
        $days= array();
        $days[1] = 'Monday';
        $days[2] = 'Tuesday';
        $days[3] = 'Wednesday';
        $days[4] = 'Thursday';
        $days[5] = 'Friday';
        $days[6] = 'Saturday';
        $days[7] = 'Sunday';

        $days_datas = array();
        $hours = array();
        $hours = json_decode($store['hours']);
        if (!empty($hours)) {
            for ($i = 1; $i < 8; $i++) {
                if (isset($hours[(int)$i - 1])) {
                    $hours_datas = array();
                    $hours_datas['hours'] = $hours[(int)$i - 1][0];
                    $hours_datas['day'] = $days[$i];
                    $days_datas[] = $hours_datas;
                }
            }
            
            return $days_datas;
        }
        return false;
    }

    public function renderMicrodataStoreWorkingHours($store)
    {
        $days= array();

        $days[1] = 'Mo';
        $days[2] = 'Tu';
        $days[3] = 'We';
        $days[4] = 'Th';
        $days[5] = 'Fr';
        $days[6] = 'Sa';
        $days[7] = 'Su';

        $days_datas = array();
        $hours = array();
        $hours = json_decode($store['hours']);
        if (!empty($hours)) {
            for ($i = 1; $i < 8; $i++) {
                if (isset($hours[(int)$i - 1])) {
                    $hours_datas = array();
                    if ($hours[(int)$i - 1] != "closed" && $hours[(int)$i - 1] != "fermé" && $hours[(int)$i - 1] != "Fermé") {
                        $hours_datas['hours'] = $hours[(int)$i - 1][0];
                        $hours_datas['hours'] = str_replace('h', ':', $hours_datas['hours']);
                        $hours_datas['day'] = $days[$i];
                        $days_datas[] = $hours_datas;
                    }
                }
            }
            return $days_datas;
        }
        return false;
    }

    public function getBreadcrumbLinks()
    {
        $moduleBreadcrumb =  $this->module->l('freestorelocator-breadcrumbs-store', 'freestorelocator');

        $breadcrumb = parent::getBreadcrumbLinks();
        $breadcrumb['links'][] = [
            'title' => $this->getTranslator()->trans($moduleBreadcrumb, [], 'Breadcrumb'),
            'url' => $this->context->link->getModuleLink('freestorelocator', 'store')
         ];
     
         return $breadcrumb;
    }

    public function renderProjekty($id_store)
    {

        $ybc_blogModule = Module::getInstanceByName('ybc_blog');

        $galleries = $ybc_blogModule->getGalleriesWithFilter(' AND g.salon=' . (int)$id_store . ' AND g.enabled=1  AND g.is_featured=1', 'g.sort_order asc, g.id_gallery asc,', 0, 30);
        if ($galleries)
            foreach ($galleries as &$gallery) {
                if ($gallery['thumb'])
                    $gallery['thumb'] = $this->context->link->getMediaLink(_PS_YBC_BLOG_IMG_ . 'gallery/thumb/' . $gallery['thumb']);
                else
                    $gallery['thumb'] = $this->context->link->getMediaLink(_PS_YBC_BLOG_IMG_ . 'gallery/' . $gallery['image']);
                if ($gallery['image']) {
                    $gallery['image'] = $this->context->link->getMediaLink(_PS_YBC_BLOG_IMG_ . 'gallery/' . $gallery['image']);
                }

            }

        return $galleries;

    }


}
