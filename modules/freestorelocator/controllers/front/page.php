<?php
/**
* 2007-2018 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    Jérôme Aldigier <jerome@evol.digital>
*  @copyright 2007-2019 evol.digital
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class FreestorelocatorPageModuleFrontController extends ModuleFrontController
{
    public function __construct()
    {
        parent::__construct();
        $this->context = Context::getContext();
    }


    public function setMedia()
    {
        parent::setMedia();
        $this->path = __PS_BASE_URI__.'modules/freestorelocator/';
        $this->addJS($this->path . 'views/js/leaflet.js');
        $this->addJS($this->path . 'views/js/leaflet-provider.js');
        $this->addCSS($this->path . 'views/css/leaflet.css');
        $this->addCSS($this->path . 'views/css/front.css');
    }

    public function initContent()
    {
        parent::initContent();

        $stores = Db::getInstance()->executeS('
            SELECT s.id_store AS `id`, s.*, sl.*
            FROM ' . _DB_PREFIX_ . 'store s
            ' . Shop::addSqlAssociation('store', 's') . '
            LEFT JOIN ' . _DB_PREFIX_ . 'store_lang sl ON (
            sl.id_store = s.id_store
            AND sl.id_lang = ' .(int)$this->context->language->id . '
            )
            WHERE s.active = 1');

        $addresses_formated = array();

        foreach ($stores as &$store) {

             //Données enrichies
            $stores_data = Db::getInstance()->executeS('
            SELECT * 
            FROM ' . _DB_PREFIX_ . 'storedatafsl s
            ' . Shop::addSqlAssociation('storedatafsl', 's') . '
            LEFT JOIN ' . _DB_PREFIX_ . 'storedatafsl_lang sl ON (
            sl.id_storedatafsl = s.id_storedatafsl
            AND sl.id_lang = ' .(int)$this->context->language->id . '
            )
            WHERE s.active = 1 AND s.id_store='.(int)$store['id_store']);

            
            if (isset($stores_data) && isset($stores_data[0])) {
                $store['web_link'] = $stores_data[0]['web_link'];
                $store['facebook_link'] = $stores_data[0]['facebook_link'];
                $store['insta_link'] = $stores_data[0]['insta_link'];

                if ($stores_data[0]['create_page'] == 0) {
                    $store['page'] = 0;
                } else {
                    $store['page'] = 1;
                }

                if ($stores_data[0]['active'] == 0) {
                    $store['display'] = 0;
                } else {
                    $store['display'] = 1;
                }
            } else {
                $store['web_link'] = "";
                $store['facebook_link'] = "";
                $store['insta_link'] = "";
                $store['display'] = 0;
            }
            
            $address = new Address();
            $address->country = Country::getNameById($this->context->language->id, $store['id_country']);
            $address->address1 = $store['address1'];
            $address->address2 = $store['address2'];
            $address->postcode = $store['postcode'];
            $address->city = $store['city'];

            $addresses_formated[$store['id_store']] = AddressFormat::getFormattedLayoutData($address);
            $hours =  $this->convertHours($store);
            $hours2 = $this->convertHours2($store['hours']);
            //     var_dump($hours);exit;
            $store['hours2'] =  array($hours2);
            $store['hours'] =  html_entity_decode($hours);
            
            

            //  var_dump($store['hours']);exit;
            $store['has_picture'] = file_exists(_PS_STORE_IMG_DIR_.(int)$store['id_store'].'.jpg');
            $store['link'] =  $this->context->link->getStorePageLink($store['id_store']);
        }


        if (Configuration::get('FREESTORELOCATOR_SEARCH') == "1") {
            $city = Tools::getValue('city');
            $zip = Tools::getValue('zip');

            if (isset($city)) {
                if (Tools::strtolower($city)== "paris") {
                    $lat =(float)"48.856614";
                    $lon =(float)"2.3522219000000177";
                } elseif (Tools::strlen($city)>3) {
                    $return = $this->getWebPage("https://nominatim.openstreetmap.org/search?city=". $city ."&format=json");
              
                    if (isset($return['content'])) {
                        $return_json = Tools::jsonDecode($return['content'], true);

                        $lat =(float)$return_json[0]['boundingbox'][0];
                        $lon =(float)$return_json[0]['boundingbox'][2];
                    }
                }
            }

            if (isset($zip)) {
                if (Tools::strlen($zip)>3) {
                    $return = $this->getWebPage("https://nominatim.openstreetmap.org/search?postalcode=". $zip ."&country=pl&format=json");
              
                    if (isset($return['content'])) {
                        $return_json = Tools::jsonDecode($return['content'], true);

                        $lat =(float)$return_json[0]['boundingbox'][0];
                        $lon =(float)$return_json[0]['boundingbox'][2];
                        $city ="";
                    }
                }
            }
        } else {
            $city ="";
            $zip ="";
        }
        //https://nominatim.openstreetmap.org/search?postalcode=63100&format=json

        if (!isset($lon) && !isset($lat)) {
            $lat =  (float)Configuration::get('FREESTORELOCATOR_LATITUDE');
            $lon = (float)Configuration::get('FREESTORELOCATOR_LONGITUDE');
        }




        $this->context->smarty->assign(array(
            'darkmode' => Configuration::get('FREESTORELOCATOR_DARK_MODE'),
            'lat' => $lat,
            'long' => $lon ,
            'zoom' => (int)Configuration::get('FREESTORELOCATOR_ZOOM_LEVEL'),
            'simplifiedStoresDiplay' => true,
            'stores' => $stores,
            'addresses_formated' => $addresses_formated,
            'city' => $city,
            'zip' => $zip,
            'search' => Configuration::get('FREESTORELOCATOR_SEARCH'),
        ));


        $this->setTemplate('module:freestorelocator/views/templates/front/page.tpl');
    }


   


    public function convertHours($store)
    {
        $days= array();

        $days[1] = 'Mo';
        $days[2] = 'Tu';
        $days[3] = 'We';
        $days[4] = 'Th';
        $days[5] = 'Fr';
        $days[6] = 'Sa';
        $days[7] = 'Su';

        $days_datas = array();
        $hours = array();
        $hours = json_decode($store['hours']);

        $hours_datas = array();
        if (!empty($hours)) {
            for ($i = 1; $i < 8; $i++) {
                if (isset($hours[(int)$i - 1])) {
                    if ($hours[(int)$i - 1] != "closed" && $hours[(int)$i - 1] != "fermé" && $hours[(int)$i - 1] != "Fermé") {
                        $twotimeslots = false;
                        $h = $hours[(int)$i - 1][0];


                        $harray = explode("et", $h);

                        if (is_array($harray) && count($harray)>1) {
                            $twotimeslots = true;
                        }

                        if (!$twotimeslots) {
                            $harray = explode("&", $h);

                            if (is_array($harray) && count($harray)>1) {
                                $twotimeslots = true;
                            }
                        }

                        if (is_array($harray) && count($harray)>1) {
                            $twotimeslots = true;
                        }

                        if (!$twotimeslots) {
                            $harray = explode("/", $h);

                            if (is_array($harray) && count($harray)>1) {
                                $twotimeslots = true;
                            }
                        }

                        if (!$twotimeslots) {
                            $harray[0] =$h;
                        }
                            


                        foreach ($harray as $key => $value) {
                            //on supprime les espaces
                            $value = str_replace(' ', '', $value);

                            //on remplace les séparateur par des espaces
                            $value = str_replace('-', ' ', $value);
                            $value = str_replace('h', ':', $value);

                            $hht = explode(" ", $value);

                            if (is_array($hht) && count($hht)==2) {
                                $ho = explode(":", $hht[0]);
                                if (is_array($ho) && count($ho)>1) {
                                    if (Tools::strlen($hht[0]) == 5) {
                                        $hht[0].=':00';
                                    }
                                    if (Tools::strlen($hht[0]) == 3) {
                                        $hht[0].='00:00';
                                    }
                                } else {
                                    if (Tools::strlen($hht[0]) == 2) {
                                        $hht[0].=':00:00';
                                    }
                                }

                                $ho2 = explode(":", $hht[1]);
                                if (is_array($ho2) && count($ho2)>1) {
                                    if (Tools::strlen($hht[1]) == 5) {
                                        $hht[1].=':00';
                                    }
                                    if (Tools::strlen($hht[1]) == 3) {
                                        $hht[1].='00:00';
                                    }
                                } else {
                                    if (Tools::strlen($hht[1]) == 2) {
                                        $hht[1].=':00:00';
                                    }
                                }

                                if (Tools::strlen($hht[1]) < 8) {
                                    $hht[1]='';
                                }
                                if (Tools::strlen($hht[0]) < 8) {
                                    $hht[0]='';
                                }

                      
                                $hours_datas[$days[$i]][$key.'-']['from'] = $hht[0];
                                $hours_datas[$days[$i]][$key.'-']['to'] = $hht[1];
                            }
                        }
                    }
                }
            }


            return  html_entity_decode(Tools::jsonEncode($hours_datas));
            exit;
        }
        return false;
    }


    public function convertHours2($store)
    {
        $days= array();

        $days[1] = 'Mo';
        $days[2] = 'Tu';
        $days[3] = 'We';
        $days[4] = 'Th';
        $days[5] = 'Fr';
        $days[6] = 'Sa';
        $days[7] = 'Su';

        $days_datas = array();
        $hours = array();
        $hours = json_decode($store);

        $hours_datas = array();
        if (!empty($hours)) {
            for ($i = 1; $i < 8; $i++) {
                if (isset($hours[(int)$i - 1])) {
                    if ($hours[(int)$i - 1] != "closed" && $hours[(int)$i - 1] != "fermé" && $hours[(int)$i - 1] != "Fermé") {
                        $twotimeslots = false;
                        $h = $hours[(int)$i - 1][0];


                        $harray = explode("et", $h);

                        if (is_array($harray) && count($harray)>1) {
                            $twotimeslots = true;
                        }

                        if (!$twotimeslots) {
                            $harray = explode("&", $h);

                            if (is_array($harray) && count($harray)>1) {
                                $twotimeslots = true;
                            }
                        }

                        if (is_array($harray) && count($harray)>1) {
                            $twotimeslots = true;
                        }

                        if (!$twotimeslots) {
                            $harray = explode("/", $h);

                            if (is_array($harray) && count($harray)>1) {
                                $twotimeslots = true;
                            }
                        }

                        if (!$twotimeslots) {
                            $harray[0] =$h;
                        }
                            


                        foreach ($harray as $key => $value) {
                            //on supprime les espaces
                            $value = str_replace(' ', '', $value);

                            //on remplace les séparateur par des espaces
                            $value = str_replace('-', ' ', $value);
                            $value = str_replace('h', ':', $value);

                            $hht = explode(" ", $value);

                            if (is_array($hht) && count($hht)==2) {
                                $ho = explode(":", $hht[0]);
                                if (is_array($ho) && count($ho)>1) {
                                    if (Tools::strlen($hht[0]) == 5) {
                                    }
                                    if (Tools::strlen($hht[0]) == 3) {
                                        $hht[0].='00';
                                    }
                                } else {
                                    if (Tools::strlen($hht[0]) == 2) {
                                        $hht[0].=':00';
                                    }
                                }

                                $ho2 = explode(":", $hht[1]);
                                if (is_array($ho2) && count($ho2)>1) {
                                    if (Tools::strlen($hht[1]) == 5) {
                                    }
                                    if (Tools::strlen($hht[1]) == 3) {
                                        $hht[1].='00';
                                    }
                                } else {
                                    if (Tools::strlen($hht[1]) == 2) {
                                        $hht[1].=':00';
                                    }
                                }

                                if (Tools::strlen($hht[1]) < 5) {
                                    $hht[1]='';
                                }
                                if (Tools::strlen($hht[0]) < 5) {
                                    $hht[0]='';
                                }

                      
                                $hours_datas[$days[$i]]['from'] = $hht[0];
                                $hours_datas[$days[$i]]['to'] = $hht[1];
                            }
                        }

                    }
                }
                $days_datas[] = $hours_datas;
            }


            return  $hours_datas;
            exit;
        }
        return false;
    }

    /**
 * Get a web file (HTML, XHTML, XML, image, etc.) from a URL.  Return an
 * array containing the HTTP server response header fields and content.
 */
    public function getWebPage($url)
    {
        $options = array(
            CURLOPT_RETURNTRANSFER => true,     // return web page
            CURLOPT_HEADER         => false,    // don't return headers
            CURLOPT_FOLLOWLOCATION => true,     // follow redirects
            CURLOPT_ENCODING       => "",       // handle all encodings
            CURLOPT_USERAGENT      => "spider", // who am i
            CURLOPT_AUTOREFERER    => true,     // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
            CURLOPT_TIMEOUT        => 120,      // timeout on response
            CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
            CURLOPT_SSL_VERIFYPEER => false     // Disabled SSL Cert checks
        );

        $ch      = curl_init($url);
        curl_setopt_array($ch, $options);
        $content = curl_exec($ch);
        $err     = curl_errno($ch);
        $errmsg  = curl_error($ch);
        $header  = curl_getinfo($ch);
        curl_close($ch);

        $header['errno']   = $err;
        $header['errmsg']  = $errmsg;
        $header['content'] = $content;
        return $header;
    }

    public function getBreadcrumbLinks()
    {
        $moduleBreadcrumb =  $this->module->l('Salony', 'freestorelocator');

        $breadcrumb = parent::getBreadcrumbLinks();
        $breadcrumb['links'][] = [
            'title' => $this->getTranslator()->trans($moduleBreadcrumb, [], 'Breadcrumb'),
            'url' => $this->context->link->getModuleLink('freestorelocator', 'page')
         ];
     
         return $breadcrumb;
    }
}
