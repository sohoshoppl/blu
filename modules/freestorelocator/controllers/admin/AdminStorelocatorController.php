<?php
/**
* 2007-2017 Evol.digital
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web
*
* DISCLAIMER
*
*
*  @author Jerome Aldigier
*  @copyright  2007-2017 Jérôme Aldigier
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  @version  Release: 1.0
*  International Registered Trademark & Property of Jerome Aldigier
*/

require_once(_PS_MODULE_DIR_ . '/freestorelocator/models/ModelStoredatafsl.php');

class AdminStorelocatorController extends ModuleAdminController
{

    /*
     * Constructeur pour l'instanciation du module
     */
    public function __construct()
    {
        $this->module = 'freestorelocator';
        $this->table        = 'storedatafsl';
        $this->className    = 'ModelStoredatafsl';
        $this->bootstrap    = true;
        $this->allow_export = true;
        $this->lang         = true;
        $this->hours        = "";
        parent::__construct();
        $this->fieldImageSettings = array('name' => 'picture', 'dir' => $this->module->name);

        $this->initList();
    }

    // public function initContent()
    //     {
    //         $this->show_toolbar = true;
    //         $this->display = 'view';
    //         parent::initContent();
    //         $this->setTemplate('storelocator.tpl');
    //     }

    private function initList()
    {
        /* GET ALL THE PRESTASHOP STORES */
        $s = new DbQuery();
        $s->select('id_store, name');
        $s->from('store_lang', 'l');
        $s->groupBy('id_store');
        $s->where('id_lang = '.(int)$this->context->language->id);
        $r= Db::getInstance()->executeS($s);
        

        foreach ($r as $key => $store) {
            $exist = false;
            $s = new DbQuery();
            $s->select('id_store');
            $s->from('storedatafsl', 's');
            $s->where('id_store = '.(int)$store['id_store']);
            $p= Db::getInstance()->executeS($s);

            if (isset($p) && is_array($p) && count($p)>0) {
                $exist = true;
            } else {
                Db::getInstance()->insert(
                    'storedatafsl',
                    array('id_store' => (int) $store['id_store'],
                    'active' => 0)
                 );
            }
        }


        $this->_select = 's.name';
        $this->_join = 'LEFT JOIN `'._DB_PREFIX_.'store_lang` s ON (s.`id_store` = a.`id_store`)';
        $this->_where =' AND s.id_lang = '.(int)$this->context->language->id;
        $this->_group = 'GROUP BY a.id_storedatafsl';

       

        $this->addRowAction('edit');
        //$this->addRowAction('delete');
        $this->_defaultOrderBy = 'id_storedatafsl';

        $this->fields_list = array(
            'name' => array(
                'title'  => $this->l('Store name'),
                'class' => 'fixed-width-xs'
            ),
            'active' => array(
                'title' => $this->l('Status'),
                'active' => 'status',
                'type' => 'bool',
                'align' => 'text-center',
                'class' => 'fixed-width-sm',
                'orderby' => false
            )
        );
    }

    public function setMedia($isNewTheme = false)
    {
        parent::setMedia($isNewTheme);
    }
    public function renderForm()
    {
        if (!($storedatafsl = $this->loadObject(true))) {
            return;
        }
        $gallerylist = $this->getGalleryLinks();
        $imagesString = '';
        $len = count($gallerylist);
        $idgallery=(int)Tools::getValue('id_'.$this->table);
        foreach ($gallerylist as $key=>$image) {
            
            if ($key == 0) {
                $imagesString=$imagesString.'<div class="container-gallery" style="display:flex;flex-wrap: wrap;"><div class="col-xs-2" style="position:relative;min-width: 120px;"><img class="imgm img-thumbnail" style="width:100%" src="'._MODULE_DIR_.'freestorelocator/controllers/admin/img/'.$image["file_name"].'"><button style="position: absolute;           top: 0;right: 15px;font-size: 35px;color: #f00;box-shadow: unset;text-shadow: unset;" type="button" class="close" aria-label="Close" data-galleryid='.$idgallery.' data-token='.Tools::getAdminTokenLite('AdminStorelocator').' data-src='.$image["file_name"].'><span aria-hidden="true">&times;</span></button></div>';
            } elseif($key == $len - 1) {
                $imagesString=$imagesString.'<div class="col-xs-2" style="position:relative;min-width: 120px;"><img class="imgm img-thumbnail" style="width:100%" src="'._MODULE_DIR_.'freestorelocator/controllers/admin/img/'.$image["file_name"].'"><button style="position: absolute;           top: 0;right: 15px;font-size: 35px;color: #f00;box-shadow: unset;text-shadow: unset;" type="button" class="close" aria-label="Close" data-token='.Tools::getAdminTokenLite('AdminStorelocator').' data-galleryid='.$idgallery.' data-src='.$image["file_name"].'><span aria-hidden="true">&times;</span></button></div></div>';
            } else {
                $imagesString=$imagesString.'<div class="col-xs-2" style="position:relative;min-width: 120px;"><img class="imgm img-thumbnail" style="width:100%" src="'._MODULE_DIR_.'freestorelocator/controllers/admin/img/'.$image["file_name"].'"><button data-src='.$image["file_name"].' data-galleryid='.$idgallery.' data-token='.Tools::getAdminTokenLite('AdminStorelocator').' style="position: absolute;           top: 0;right: 15px;font-size: 35px;color: #f00;box-shadow: unset;text-shadow: unset;" type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
            }
            

            
        }
        $this->fields_form = array(
            'legend' => array(
                'title' => $this->l('storedatafsl'),
                'icon' => 'icon-cogs'
            ),
            'input' => array(
                array(
                    'type'     => 'text',
                    'label'    => $this->l('Nom de la boutique'),
                    'name'     => 'name',
                    'lang'     => false,
                    'hint'     => $this->l('255 characters max'),
                    'required' => true,
                ),
                array(
                    'type'     => 'text',
                    'label'    => $this->l('Store ID'),
                    'name'     => 'id_store',
                    'lang'     => false,
                    'hint'     => $this->l('Store ID'),
                    'required' => true,
                ),
                array(
                    'type'     => 'switch',
                    'label'    => $this->l('Display the store on the Store Locator page'),
                    'name'     => 'active',
                    'hint'     => $this->l(''),
                    'required' => false,
                    'is_bool'  => true,
                    'values'   => array(
                        array(
                            'id' => 'active_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'active_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type'     => 'switch',
                    'label'    => $this->l('Create a page on this Website for this shop'),
                    'name'     => 'create_page',
                    'hint'     => $this->l(''),
                    'required' => false,
                    'is_bool'  => true,
                    'values'   => array(
                        array(
                            'id' => 'active_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'active_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => 'textarea',
                    'label' => $this->l('Description HTML'),
                    'name' => 'htmldesc',
                    'autoload_rte' => true,
                    'cols' => 12,
                    'rows' => 50,
                    'lang' => true,
                ),
                array(
                    'type' => 'textarea',
                    'label' => $this->l('Description HTML SEO'),
                    'name' => 'htmldesc2',
                    'autoload_rte' => true,
                    'cols' => 12,
                    'rows' => 50,
                    'lang' => true,
                ),
                array(
                        'type' => 'text',
                        'label' => $this->l('Banner text bg'),
                        'name' => 'web_link',
                        'lang' => true,
                    ),
                array(
                        'type' => 'text',
                        'label' => $this->l('Banner text small'),
                        'name' => 'facebook_link',
                        'lang' => true,
                    ),
                array(
                        'type' => 'text',
                        'label' => $this->l('Instagram link'),
                        'name' => 'insta_link',
                        'lang' => true,
                    ),
                array(
                        'type' => 'file',
                        'label' => $this->l('Gallery upload'),
                        'name' => 'gallery_field',
                        'multiple' => true,
                ),
                array(
                    'type' => 'html',
                    'name' => 'gallery_field',
                    'label' => $this->l('Lista zdjęc galleri'),
                    'html_content' => $imagesString,
                    'col' => 12,
                 ),       
            ),
            'submit' => array(
                'title' => $this->l('Save'),
                'class' => 'btn btn-default pull-right'
            )
        );

        if (!($storedatafsl = $this->loadObject(true))) {
            return;
        }

    
       

        return parent::renderForm();
    }


    public function postProcess()
    {
        if (Tools::getValue('submitAdd'.$this->table)) {
            $this->validateRules();



            if (!count($this->errors)) {
                $id = (int)Tools::getValue('id_'.$this->table);

                if ( Tools::getValue('gallery_field') && !empty('file') ) {
                    foreach ($_FILES['gallery_field']['name'] as $key=>$name) {
                        if ($_FILES['gallery_field']['size'][$key] < 4000000) {
                            $ext = pathinfo($_FILES["gallery_field"]["name"][$key], PATHINFO_EXTENSION);
                            $file_name = md5($name).'.'.$ext;
                            Db::getInstance()->insert('storedatafsl_gallery', array(
                                'id_storedatafsl'	=> (int)$id,
                                'file_name'		=> pSQL($file_name),
                            ));
                            move_uploaded_file($_FILES['gallery_field']['tmp_name'][$key], dirname(__FILE__).DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.$file_name);

                        } else {
                            return $this->displayError($this->trans('Files are not image type or are to big.', array(), 'Admin.Notifications.Success'));
                        }
                    }
                }
                // Object update
                if (isset($id) && !empty($id)) {
                    try {
                        $current_storedatafsl = new Modelstoredatafsl($id);
                        if (!Validate::isLoadedObject($current_storedatafsl)) {
                            throw new PrestaShopException('Cannot load object');
                        }

                        $this->copyFromPost($current_storedatafsl, $this->table);
                        $this->updateAssoShop($current_storedatafsl->id);
                    } catch (PrestaShopException $e) {
                        $this->errors[] = $e->getMessage();
                    }
                } else {
                    $storedatafsl = new Modelstoredatafsl();

                    $this->copyFromPost($storedatafsl, $this->table);
                    if ($storedatafsl->add()) {
                        $this->updateAssoShop($storedatafsl->id);
                        Tools::redirectAdmin(self::$currentIndex.'&id_'.$this->table.'='.$storedatafsl->id.'&conf=3&token='.$this->token);
                    } else {
                        $this->errors[] =   $this->trans('An error occurred while creating an object..', array(), 'Admin.Notifications.Error');
                    }
                }
            }
            parent::postProcess();
        } else {
            parent::postProcess();
        }
    }
    public function getGalleryLinks() {

        $id = (int)Tools::getValue('id_'.$this->table);
        $s = new DbQuery();
        $s->select('file_name');
        $s->from('storedatafsl_gallery');
        $s->where('id_storedatafsl = '.$id);
        $images= Db::getInstance()->executeS($s);

        return $images;
    }
    public function getFieldsValue($obj)
    {
        foreach ($this->fields_form as $fieldset) {
            if (isset($fieldset['form']['input'])) {
                foreach ($fieldset['form']['input'] as $input) {
                    if (!isset($this->fields_value[$input['name']])) {
                        $field_value = $this->getFieldValue($obj, $input['name']);
                        if ($field_value === false && isset($input['default_value'])) {
                            $field_value = $input['default_value'];
                        }
                        $this->fields_value[$input['name']] = $field_value;
                    }
                }
            }
        }


        /* get store name */
        $s = new DbQuery();
        $s->select('id_store, name');
        $s->from('store_lang', 'l');
        $s->where('id_lang = '.(int)$this->context->language->id .' and id_store = '.(int)$this->fields_value['id_store']);
        $r= Db::getInstance()->executeS($s);
        if (isset($r) && is_array($r) &&  count($r)>0) {
            $this->fields_value['name'] = $r[0]['name'];
        } else {
            $this->fields_value['name'] = '';
        }
        return $this->fields_value;
    }
}
