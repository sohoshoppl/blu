<!-- Modal -->
<script>
    id_selectedShop = {$modal_id_salon};
</script>
<div class="modal fade" id="wokolicyModal" tabindex="-1" aria-labelledby="wokolicyModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog" style="max-width: 900px; max-height: 550px;">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"
                    id="wokolicyModalLabel">{l s='Wybierz salon w którym dokonasz rezerwacji produktu' d='Modules.Sohoshopwokolicy.Shop'}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <svg class="close_cross" xmlns="http://www.w3.org/2000/svg" width="20.673" height="20.673" viewBox="0 0 20.673 20.673">
                        <defs></defs>
                        <g transform="translate(-556.872 -526.47)">
                        <path class="a" d="M3778.4,5611l19.612,19.612" transform="translate(-3221 -5084)"/><path class="a" d="M0,0,19.612,19.612" transform="translate(557.402 546.612) rotate(-90)"/>
                        </g>
                    </svg>
                </button>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class="col-md-6">
                        <div class="label">{l s='Twój najbliższy salon:' d='Modules.Sohoshopwokolicy.Shop'}</div>
                        <div id="sohoshop_wokolicy-nearestShop">
                            {if isset($choosedSalon)}
                                <h1>{$choosedSalon->name[$language.id]}</h1>
                                <div class="shop_adress_modal_wrapper">
                                    <img class="pin" src="/themes/sohoshop_blu/assets/img/point-icon.svg" alt="Najbliższy salon" >
                                    <address>
                                        {$choosedSalon->postcode} {$choosedSalon->city}<br>
                                        {$choosedSalon->address1[$language.id]}<br><br>
                                        tel. {$choosedSalon->phone}<br>
                                        <a href="mailto:{$choosedSalon->email}">{$choosedSalon->email}</a>
                                    </address>
                                    <button class="btn btn-primary" onclick="chooseSalon({$modal_id_salon});">
                                        ZAREZERWUJ W SALONIE
                                    </button>
                                </div>
                            {else}
                            <button class="btn btn-primary"
                                    onclick="navigator.geolocation.getCurrentPosition(findNearest, findNearestError);"
                                    type="button">{l s='Ustal moją lokalizację' d='Modules.Sohoshopwokolicy.Shop'}</button>
                            {/if}
                        </div>
                        <div id="sohoshop_wokolicy-findShop">
                            <div class="label">{l s='Znajdź inny salon:' d='Modules.Sohoshopwokolicy.Shop'}</div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <input class="form-control" name="findShopQuery" type="text" value="" required
                                           placeholder="{l s='Wpisz miasto lub kod' d='Modules.Sohoshopwokolicy.Shop'}">
                                </div>
                                <div class="col-lg-12 ">
                                    <button type="button" class="btn btn-primary" onclick="findShop(this);">
                                        {l s='POKAŻ NAJBLIŻSZE' d='Modules.Sohoshopwokolicy.Shop'}
                                        <span class="hidden-sm-down">{l s='SALONY' d='Modules.Sohoshopwokolicy.Shop'}</span>
                                        <svg class="arrow-right" xmlns="http://www.w3.org/2000/svg" width="14.262" height="9.192" viewBox="0 0 12.262 7.192">
                                            <defs></defs>
                                            <g transform="translate(0.53 0.53)">
                                                <path d="M-317.515-44.61l-5.6,5.6-5.6-5.6" transform="translate(328.717 44.61)"></path>
                                            </g>
                                        </svg>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6" id="wokolicyModal-map">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>