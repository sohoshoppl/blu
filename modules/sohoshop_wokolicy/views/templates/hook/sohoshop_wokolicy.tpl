<div id="sohoshop_choosedSalon">
    <div>

        {if isset($choosedSalon)}
        {if $hookName eq 'displayOrderConfirmation' }
            <p class="sectionDesc">{l s='Salon realizujący rezerwację:' d='Modules.Sohoshopwokolicy.Shop'}</p>
        {/if}
            <h1>{$choosedSalon->name[$language.id]}</h1>
            <img class="pin" src="/themes/sohoshop_blu/assets/img/point-icon.svg"
                 alt="Najbliższy salon">
            <address class="row">
                <div class="col-md-6">
                    {$choosedSalon->postcode} {$choosedSalon->city}<br>
                    {$choosedSalon->address1[$language.id]}<br>
                </div>
                <div class="col-md-6">
                    tel. {$choosedSalon->phone}<br>
                    <a href="mailto:{$choosedSalon->email}">{$choosedSalon->email}</a>
                </div>
            </address>
        {/if}
    </div>
    {if $hookName neq 'displayOrderConfirmation' }
    <div>
        <button class="btn btn-primary" onclick="showWokolicyModal();">
            {l s='WYBIERZ INNY SALON BLU >' d='Modules.Sohoshopwokolicy.Shop'}
        </button>
    </div>
    {/if}
</div>
<script>
    {assign var="stores" value=(Store::getStores($language.id))}

    var villesordered = Array ({foreach from=$stores item=store}
        Array (
            "{$store.name}",
            {
                lat: {$store.latitude},
                lon: {$store.longitude},
                pop: `<section>
				        <h2>{$store.name|escape:'htmlall':'UTF-8'} sdfdfsfsfd</h2>
						<img class="pin" src="/themes/sohoshop_blu/assets/img/point-icon.svg"alt="Najbliższy salon">
                        <address>
                            {$store.postcode} {$store.city|escape:'htmlall':'UTF-8'}<br />
                            {$store.address1|escape:'htmlall':'UTF-8'}
                            {if $store.address2}<br />{$store.address2|escape:'htmlall':'UTF-8'}{/if}
                        </address>
                        {if $store.phone}
                            <img class="pin" src="/themes/sohoshop_blu/assets/img/svg_icons/phone.svg" alt="Najbliższy salon">
                            <div>{l s='tel.' js=0}{$store.phone|escape:'htmlall':'UTF-8'}</div>
                        {/if}
                        {if $store.email}
                            <img class="pin" src="/themes/sohoshop_blu/assets/img/svg_icons/store-mail.svg" alt="Najbliższy salon">
                            <div>{$store.email|escape:'htmlall':'UTF-8'}</div>
                        {/if}
                        {if isset($store['hours2'])}
                        <p class="store-item-hours">
                            {l s='Godziny otwarcia:' mod='freestorelocator'} <br>
                            {foreach from=$store['hours2'] item=open name=time}
                                {l s='Pon. - Pt.' mod='freestorelocator'} {$open['Mo']['from']}-{$open['Mo']['to']} <br>
                                {l s='Sob.' mod='freestorelocator'} {$open['Sa']['from']}-{$open['Sa']['to']},
                                {if isset($open['Su'])} {l s='Niedz' mod='freestorelocator'} {$open['Su']['from']}-{$open['Su']['to']}{/if}
                            {/foreach}
                        </p>
                        {/if}
				</section>`
            }
        ),{/foreach} );

</script>
