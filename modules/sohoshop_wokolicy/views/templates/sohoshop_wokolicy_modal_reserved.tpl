<div class="modal fade" id="reservedModal" tabindex="-1" aria-labelledby="reservedModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog" style="max-width: 900px; max-height: 550px">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"
                    id="reservedModalLabel"><i class="material-icons rtl-no-flip">&#xE876;</i>{l s='Świetny wybór, Twoja rezerwacja to:' d='Modules.Sohoshopwokolicy.Shop'}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <svg class="close_cross" xmlns="http://www.w3.org/2000/svg" width="20.673" height="20.673" viewBox="0 0 20.673 20.673">
                        <defs></defs>
                        <g transform="translate(-556.872 -526.47)">
                        <path class="a" d="M3778.4,5611l19.612,19.612" transform="translate(-3221 -5084)"/><path class="a" d="M0,0,19.612,19.612" transform="translate(557.402 546.612) rotate(-90)"/>
                        </g>
                    </svg>
                </button>
            </div>
            <div class="modal-body">
                <div class="product clearfix">
                    <img src="{$img_url}">
                    <div>
                        <div class="product-name">{$product.name}</div>
                        {assign var=subtitle value=(SohoshopProductAdditionalDescriptions::getProductAdditionalDescriptions($product.id_product))}

                        <div class="product-subtitle">{$subtitle.description_1}</div>
                        {if isset($product.attributes_small)}<div class="product-variant">{$product.attributes_small}</div>{/if}
                        <div class="product-qty">{$product.quantity} {l s='szt.' d='Modules.Sohoshopwokolicy.Shop'}</div>
                    </div>
                </div>
                <div class="salon">
                    <div class="label">{l s='Wybrany salon:' d='Modules.Sohoshopwokolicy.Shop'}</div>
                              <h1>{$choosedSalon->name[$language.id]}</h1>
                    <div>
                        <img class="pin" src="/themes/sohoshop_blu/assets/img/point-icon.svg"
                             alt="Najbliższy salon">
                        <address>
                            {$choosedSalon->postcode} {$choosedSalon->city}<br>
                            {$choosedSalon->address1[$language.id]}<br>
                        </address>
                    </div>
                </div>
                <div class="cart-detailed-actions">
                    <a href="{url entity=cart}" class="next">{l s='PRZEJDŹ DO TWOICH REZERWACJI' d='Modules.Sohoshopwokolicy.Shop'}</a>
                    <button data-dismiss="modal" aria-label="Close" class="back" >{l s='Kontynuuj przeglądanie' d='Modules.Sohoshopwokolicy.Shop'}</button>

                </div>
            </div>
        </div>
    </div>