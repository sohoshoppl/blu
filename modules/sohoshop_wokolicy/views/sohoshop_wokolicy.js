var id_selectedShop = 0;
var wokolicyModalmap = null;
var showModalProductAdded = false;

$(function () {
    prestashop.on(
        'updateCart',
        function (event) {
            let body_id = $('body').attr('id');
            if (event.reason.linkAction == 'add-to-cart' && (body_id == 'product')) {
                showModalProductAdded = true;
                showWokolicyModal();
            } else {
                idlastAddedProduct = 0;
                idlastAddedProductAttriubute = 0;
                showModalProductAdded = false;
            }
        });
});

function showWokolicyModal() {
    $('#wokolicyModal').remove();
    $.get(prestashop.urls.base_url + 'module/sohoshop_wokolicy/ajax?action=renderModal', function (data) {
        $('body').append(data.modal);
        wokolicyModalmap = L.map('wokolicyModal-map').setView([51.815, 19.072], 6);
        L.tileLayer("https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png").addTo(wokolicyModalmap);

        let icon = L.divIcon({className: 'my-div-icon'});

        for (st in villesordered) { 
            let marker = L.marker([
                    villesordered[st][1].lat,
                    villesordered[st][1].lon
                ],
                {
                    icon: icon
                }
            ).addTo(wokolicyModalmap).bindPopup(villesordered[st][1].pop);
        }

        //$('#blockcart-modal').modal('hide');
        $('#wokolicyModal').modal('show');
        wokolicyModalmap.whenReady(() => {
            setTimeout(() => {
                wokolicyModalmap.invalidateSize();
            }, 0);
        });
    });
}

function findNearest(pos) {
    let chooser = '';
    $.get(prestashop.urls.base_url + 'module/sohoshop_wokolicy/ajax?action=findNearest&lat=' + pos.coords.latitude + '&long=' + pos.coords.longitude, function (data) {
        if (data.code == 'ok') {
            let icon = L.divIcon({className: 'my-div-icon'});
            let marker = L.marker([data.result[0].store.latitude, data.result[0].store.longitude], {icon: icon}).addTo(wokolicyModalmap).bindPopup('test');
            wokolicyModalmap.flyTo([data.result[0].store.latitude, data.result[0].store.longitude], 7, {
                animate: true,
                duration: 0.8
            });

            $('#sohoshop_wokolicy-nearestShop').html(
                `<h1>` + data.result[0].store.name + `</h1>
                 <div>
                    <img class="pin" src="/themes/sohoshop_blu/assets/img/point-icon.svg" alt="Najbliższy salon" >
                    <address>
                        ` + data.result[0].store.postcode + ' ' + data.result[0].store.city + `<br>
                        ` + data.result[0].store.address1 + `<br><br>
                        tel. ` + data.result[0].store.phone + `<br>
                        <a href="mailto:` + data.result[0].store.email + `">` + data.result[0].store.email + `</a>
                    </address>
                    <button class="btn btn-primary" onclick="chooseSalon(` + data.result[0].store.id_store + `);">
                        ZAREZERWUJ W SALONIE
                    </button>                    
                 </div>`
            );


        }
    });

}

function findShop(e) {
    let query = $(e).parents('.modal-body').find('input[name="findShopQuery"]').val();
    $.get('https://nominatim.openstreetmap.org/search?format=json&countrycodes=pl&q=' + query, function (data) {
        findNearest({
            coords: {latitude: data[0].lat, longitude: data[0].lon}
        })
    });
}

function choiseShop(e) {
    wokolicyModalmap.closePopup();
    let query = $(e).data('salonpostcode');
    $.get('https://nominatim.openstreetmap.org/search?format=json&countrycodes=pl&q=' + query, function (data) {
        findNearest({
            coords: {latitude: data[0].lat, longitude: data[0].lon}
        })
    });
}


function chooseSalon(id_salon) {

    $.post(prestashop.urls.base_url + 'module/sohoshop_wokolicy/ajax?action=chooseSalon', {id_salon: id_salon}, function (result) {
        $('#wokolicyModal').modal('hide');
        $('#chooseShopHeaderPopup').modal('hide');
        if (showModalProductAdded) {
            $.get(prestashop.urls.base_url + 'module/sohoshop_wokolicy/ajax?action=renderModalReserved', function (data) {
                $('body').append(data.modal);
                $('#reservedModal').modal('show');
            });
        }
    });


}

function findNearestError(err) {
    $('#sohoshop_wokolicy-nearestShop').prepend(`
            <div class="alert alert-danger" role="alert">
                Przeglądarka nie była w stanie określić Twojej lokalizacji. Proszę użyć poniższej wyszukiwarki.
            </div>
        `);
}