<?php

class Sohoshop_WokolicyAjaxModuleFrontController extends ModuleFrontController
{
    /**
     * @var bool
     */
    public $ssl = true;

    /**
     * @return void
     * @see FrontController::initContent()
     *
     */
    public function initContent()
    {
        parent::initContent();

        $action = Tools::getValue('action');
        switch ($action) {
            case "renderModal" :
                $modal = $this->module->renderModal();
                ob_end_clean();
                header('Content-Type: application/json');
                die(json_encode([
                    'modal' => $modal,
                ]));
                break;
            case "renderModalReserved" :
                $modal = $this->module->renderModalReserved();
                ob_end_clean();
                header('Content-Type: application/json');
                die(json_encode([
                    'modal' => $modal,
                ]));
                break;
            case "findNearest":
                $res = $this->module->nearest();
                ob_end_clean();
                header('Content-Type: application/json');
                die(json_encode($res));
                break;
            case "chooseSalon":
                if($id_Salon = Tools::getValue('id_salon',false)){
                    $data[$this->context->cookie->id_cart] = $id_Salon;
                    $this->context->cookie->choosedSalon = json_encode($data);
                    die('OK');
                }
                die('ERROR');
                break;
        }


    }
}
