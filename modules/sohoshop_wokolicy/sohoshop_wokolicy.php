<?php
/**
 * 2007-2020 PrestaShop.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2020 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

use PrestaShop\PrestaShop\Core\Module\WidgetInterface;

class Sohoshop_Wokolicy extends Module implements WidgetInterface
{


    public function __construct()
    {
        $this->name = 'sohoshop_wokolicy';
        $this->need_instance = 0;

        $this->bootstrap = true;
        parent::__construct();

        $this->displayName = $this->trans('Sohoshop - w okolicy', array(), 'Modules.Sohoshopblockstores.Admin');
        $this->description = $this->trans('Pozwala na wyszukiwanie sklepów w okolicy', array(), 'Modules.Sohoshopblockstores.Admin');
        $this->confirmUninstall = $this->trans('Are you sure that you want to delete all of your contacts?', array(), 'Modules.Sohoshopblockstores.Admin');
        $this->ps_versions_compliancy = array('min' => '1.7.1.0', 'max' => _PS_VERSION_);
        $this->controllers = ['ajax'];


        $this->version = '1.0.0';
        $this->author = 'SohoShop.pl';
        $this->error = false;
        $this->valid = false;

    }

    public function install()
    {
        $res = true;
        $res &= parent::install();
        $res &= $this->registerHook('actionFrontControllerSetMedia');
        return $res;

    }

    public function uninstall()
    {
        return parent::uninstall();
    }


    public function renderWidget($hookName = null, array $configuration = [])
    {
        $this->smarty->assign('hookName', $hookName);
        $id_cart = 0;
        if($hookName == 'displayOrderConfirmation'){
            $id_cart = $configuration['order']->id_cart;
        }elseif (!empty($this->context->cart->id)) {
            $id_cart = $this->context->cart->id;
        }

        $id_salon = 0;
        $choosedSalon = json_decode($this->context->cookie->choosedSalon, true);
        if ($id_cart) {
            $id_salon = (isset($choosedSalon[$id_cart]) ? (int)$choosedSalon[$id_cart] : 0);
            if ($id_salon) {
                $choosedSalon = new Store($id_salon);
                $this->smarty->assign('choosedSalon', $choosedSalon);
            }
        }
        return $this->fetch('module:' . $this->name . '/views/templates/hook/' . $this->name . '.tpl');

    }

    public function getWidgetVariables($hookName = null, array $configuration = [])
    {
    }


    public function hookActionFrontControllerSetMedia($params)
    {

        $this->context->controller->registerStylesheet(
            $this->name,
            'modules/' . $this->name . '/views/' . $this->name . '.css',
            [
                'media' => 'all',
                'priority' => 2000,
            ]
        );

        $this->context->controller->registerJavascript(
            $this->name,
            'modules/' . $this->name . '/views/' . $this->name . '.js',
            [
                'priority' => 2000,
            ]
        );
    }

    public function renderModal()
    {
        $choosedSalonJSON = json_decode($this->context->cookie->choosedSalon, true);
        $id_salon = (isset($choosedSalonJSON[$this->context->cart->id]) ? (int)$choosedSalonJSON[$this->context->cart->id] : 0);

        if ((int)$id_salon > 0) {
            $choosedSalon = new Store($id_salon);
            $this->smarty->assign('choosedSalon', $choosedSalon);
            $this->smarty->assign('modal_id_salon', $id_salon);
        }else{
            $this->smarty->assign('modal_id_salon', 0);
        }

        return $this->fetch('module:' . $this->name . '/views/templates/' . $this->name . '_modal.tpl');
    }

    public function nearest()
    {
        $return = [];
        $long = Tools::getValue('long', 0);
        $lat = Tools::getValue('lat', 0);
        $nearestStores = [];
        if ($lat != 0 && $long != 0) {
            $fromQuery = $long . ',' . $lat . ';';
            $toQuery = '';
            $stores = Store::getStores($this->context->language->id);
            foreach ($stores as $store) {
                $toQuery = $store['longitude'] . ',' . $store['latitude'];
                //dump('https://routing.openstreetmap.de/routed-foot/route/v1/foot/'.$fromQuery.$toQuery.'?generate_hints=false&overview=false');
                $curlSession = curl_init();
                curl_setopt($curlSession, CURLOPT_URL, 'https://routing.openstreetmap.de/routed-foot/route/v1/foot/' . $fromQuery . $toQuery);
                curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curlSession, CURLOPT_SSL_VERIFYPEER, false);

                $jsonData = json_decode(curl_exec($curlSession));
                curl_close($curlSession);

                if ($jsonData->code == 'Ok') {
                    $distance = round((int)$jsonData->routes[0]->distance / 1000, 0);
                    $nearestStores[] = array('distance' => $distance, 'store' => $store);
                }
            }
        }

        if (!empty($nearestStores)) {
            usort($nearestStores, function ($item1, $item2) {
                return $item1['distance'] <=> $item2['distance'];
            });
            $return['code'] = 'ok';
            $return['result'] = $nearestStores;
        } else {
            $return['code'] = 'error';
        }
        return $return;
    }

    public function renderModalReserved()
    {
        require_once(_PS_ROOT_DIR_ . '/modules/sohoshopProductMultipleDescriptions/classes/SohoshopProductAdditionalDescriptions.php');

        $choosedSalonJSON = json_decode($this->context->cookie->choosedSalon, true);
        $id_salon = (isset($choosedSalonJSON[$this->context->cart->id]) ? (int)$choosedSalonJSON[$this->context->cart->id] : 0);

        if ((int)$id_salon > 0) {
            $choosedSalon = new Store($id_salon);
            $product = $this->context->cart->getLastProduct();
            $img_url = $this->context->link->getImageLink($product['link_rewrite'], $product['id_image'], 'cart_default');

            $this->smarty->assign('img_url', $img_url);
            $this->smarty->assign('product', $product);
            $this->smarty->assign('choosedSalon', $choosedSalon);
        }else{
            $this->smarty->assign('modal_id_salon', 0);
        }
        return $this->fetch('module:' . $this->name . '/views/templates/' . $this->name . '_modal_reserved.tpl');
    }


}
