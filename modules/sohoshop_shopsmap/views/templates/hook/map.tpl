<section id="sohoshop_shopsmap">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/openlayers/openlayers.github.io@master/en/v6.5.0/css/ol.css" type="text/css">
    <style>
        .map {
            height: 400px;
            width: 100%;
        }
    </style>
    <script src="https://cdn.jsdelivr.net/gh/openlayers/openlayers.github.io@master/en/v6.5.0/build/ol.js"></script>

    <body>
    <h2>My Map</h2>
    <div id="map" class="map"></div>
    <script>
        var map = new ol.Map({
            target: "map",
            layers: [
                new ol.layer.Tile({
                    source: new ol.source.OSM()
                })
            ],
            view: new ol.View({
                center: ol.proj.fromLonLat([15.506186, 51.935619]),
                zoom: 7
            })
        });

        function getCoordinates() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(alertPosition);
            } else {
                alert('twoja przeglądarka nie wspiera geolokacji...');
            }
        }

        function alertPosition(position)
        {
            alert('szer. geogr.: ' + position.coords.latitude +
                ', długość geogr.: ' + position.coords.longitude);
        }

        getCoordinates();
    </script>

</section>
