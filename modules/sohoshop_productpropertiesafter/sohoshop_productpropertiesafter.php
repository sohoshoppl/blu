<?php
/**
 * 2007-2020 PrestaShop.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2020 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */

if (!defined('_PS_VERSION_')) {
    exit;
}


class Sohoshop_Productpropertiesafter extends Module
{

    public function __construct()
    {
        $this->name = 'sohoshop_productpropertiesafter';
        $this->need_instance = 0;

        $this->bootstrap = true;
        parent::__construct();

        $this->displayName = $this->trans('Sohoshop -  product properties after', array(), 'Modules.Sohoshopblockstores.Admin');
        $this->description = $this->trans('Moduł do modyfikowania danych produktu dostęnych na listingach', array(), 'Modules.Sohoshopblockstores.Admin');
        $this->ps_versions_compliancy = array('min' => '1.7.1.0', 'max' => _PS_VERSION_);


        $this->version = '1.0.0';
        $this->author = 'SohoShop.pl';
        $this->error = false;
        $this->valid = false;

    }

    public function install()
    {
        if (!parent::install() || !$this->registerHook('actionGetProductPropertiesAfter')) {
            return false;
        }
        return true;

    }

    public function uninstall()
    {
        return parent::uninstall();
    }

    public function hookActionGetProductPropertiesAfter($params)
    {
        //jeżeli jest zainstalowany moduł ybc_blog i jesteśmy na stronie kategorii.
        if (Module::isEnabled('ybc_blog') && $params['context']->controller->php_self == 'category') {
            //sprawdzamy, czy dany produkt i kategoria mają powiązane posty bloga
            $sql = 'SELECT p.id_post,pl.thumb FROM `' . _DB_PREFIX_ . 'ybc_blog_post` p 
            INNER JOIN `' . _DB_PREFIX_ . 'ybc_blog_post_shop` ps ON ( p.id_post = ps.id_post AND ps.id_shop = ' . (int)$params['product']['id_shop'] . ')
            LEFT JOIN `' . _DB_PREFIX_ . 'ybc_blog_post_lang` pl ON (p.id_post=pl.id_post)             
            LEFT JOIN `' . _DB_PREFIX_ . 'ybc_blog_post_related_categories` rpc ON (p.id_post = rpc.id_post) 
            WHERE products = ' . (int)$params['product']['id_product'] . ' AND rpc.`id_category` = ' . (int)$params['context']->controller->getCategory()->id;
            $post = dB::getInstance()->getRow($sql);
            if ($post) {
                $params['product']['ybc_blog_thumb'] = $post['thumb'];
                //$ybc_blog_module = Module::getInstanceByName('ybc_blog');
                //$params['product']['ybc_blog_link'] = $ybc_blog_module->getLink('blog', array('id_post' => $post['id_post']));
            }
        }
    }
}
