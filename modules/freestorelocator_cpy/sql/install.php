<?php
/**
* 2007-2018 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    Jérôme Aldigier <jerome@evol.digital>
*  @copyright 2007-2018 evol.digital
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

$sql = array();

$sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'freestorelocator` (
    `id_freestorelocator` int(11) NOT NULL AUTO_INCREMENT,
    PRIMARY KEY  (`id_freestorelocator`)
) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;';


$sql[] = "CREATE TABLE `" . _DB_PREFIX_ . "storedatafsl` (
  `id_storedatafsl` int UNSIGNED NOT NULL,
  `id_store` int NOT NULL,
  `active` tinyint UNSIGNED NOT NULL,
  `display` tinyint UNSIGNED NOT NULL,
  `create_page` tinyint UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `" . _DB_PREFIX_ . "storedatafsl_lang` (
  `id_storedatafsl` int UNSIGNED NOT NULL,
  `htmldesc` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `web_link` varchar(255) NOT NULL,
  `facebook_link` varchar(255) NOT NULL,
  `insta_link` varchar(255) NOT NULL,
  `linkedin_link` varchar(255) NOT NULL,
  `id_shop` int UNSIGNED NOT NULL,
  `id_lang` int UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `" . _DB_PREFIX_ . "storedatafsl_shop` (
  `id_storedatafsl` int UNSIGNED NOT NULL,
  `id_shop` int UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `" . _DB_PREFIX_ . "storedatafsl`
  ADD PRIMARY KEY (`id_storedatafsl`);

ALTER TABLE `" . _DB_PREFIX_ . "storedatafsl_shop`
  ADD PRIMARY KEY (`id_storedatafsl`,`id_shop`);

ALTER TABLE `" . _DB_PREFIX_ . "storedatafsl`
  MODIFY `id_storedatafsl` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

ALTER TABLE `" . _DB_PREFIX_ . "storedatafsl_shop`
  MODIFY `id_storedatafsl` int UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;";


foreach ($sql as $query) {
    if (Db::getInstance()->execute($query) == false) {
        return false;
    }
}
