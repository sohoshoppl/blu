{*
* 2007-20121 Evol.digital
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web
*
* DISCLAIMER
*
* {$urls.img_url}
*  @author Jerome Aldigier
*  @copyright  2007-2021 Evol.digital
*  @version  Release: 1.0
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of Jerome Aldigier
*}
{extends file=$layout}
{block name='content'}
<div id="page-container">
	<div class="page-headingh2">
		<div class="col-lg-6 col-xs-12 headingh2-adress">
			<h1 class="page-heading">
				{$store.name|escape:'htmlall':'UTF-8'} 
			</h1>
			<div class="store-adress-heading-wrapper">
				<div class="store-adress-heading adress">
					<img src="{$urls.img_url}svg_icons/store-map-pin.svg">
					{$store.address1|escape:'htmlall':'UTF-8'} {$store.address2|escape:'htmlall':'UTF-8'} <br>{$store.postcode|escape:'htmlall':'UTF-8'}, {$store.city|escape:'htmlall':'UTF-8'}
				</div>
				<div class="store-adress-heading mail">
					<img src="{$urls.img_url}svg_icons/store-mail.svg">
					{if $store.email|count_characters >0 }
						<a  href="mailto:{$store.email|escape:'htmlall':'UTF-8'}">
							{$store.email|escape:'htmlall':'UTF-8'}
						</a>
					{/if}
				</div>
				<div class="store-adress-heading phone-store">
					<img src="{$urls.img_url}svg_icons/phone.svg">
					{if $store.phone|count_characters >0 }
						<a href="tel:{$store.phone|escape:'htmlall':'UTF-8'|replace:'.':''}">
							tel. {$store.phone|escape:'htmlall':'UTF-8'}
						</a> 
					{/if}
				</div>
				<div class="store-adress-heading storehours">
					{l s='Godziny otwarcia:' mod='freestorelocator'} <br>
					{foreach from=$days_datas  item=one_day name=storeid}
						{if $smarty.foreach.storeid.index == 1}
							{l s='Pon. - Pt.' mod='freestorelocator'} {$one_day.hours|escape:'htmlall':'UTF-8'}<br />
						{/if}
						{if $smarty.foreach.storeid.index == 5}
							{l s='Sob.' mod='freestorelocator'} {$one_day.hours|escape:'htmlall':'UTF-8'}<br />
						{/if}
						{if $smarty.foreach.storeid.index == 6}
							{l s='Niedz' mod='freestorelocator'} {$one_day.hours|escape:'htmlall':'UTF-8'}
						{/if}
					{/foreach}
				</div>
			</div>

			<div class="stores-footer">
				<button class="stores-button">
				{l s='Sprawdź dojazd samochodem' mod='freestorelocator'}
					<svg class="fs-locator-search-ico" xmlns="http://www.w3.org/2000/svg" width="14.262" height="9.192" viewBox="0 0 12.262 7.192">
						<defs></defs>
						<g transform="translate(0.53 0.53)">
							<path d="M-317.515-44.61l-5.6,5.6-5.6-5.6" transform="translate(328.717 44.61)"></path>
						</g>
					</svg>
				</button>
			</div>
		</div>
		<div class="col-lg-6 col-xs-12">
				<div id="map" style="width:100%">
				</div>
		</div>

	</div>
	<div class="page-headingh2" >
	<div >

	<div class='social-bar'>
		{if ($store.web_link|count_characters) >1 }
			<a href='{$store.web_link|escape:'htmlall':'UTF-8'} class='link' target='_blank'>
				<i class='fa fa-globe' aria-hidden='true'>www</i></i>
			</a>
		{/if}
		{if ($store.facebook_link|count_characters) >1 }
			<a href='{$store.facebook_link|escape:'htmlall':'UTF-8'}' class='facebook' target='_blank'>
				<i class='fa fa-facebook' aria-hidden='true'></i>
			</a>
		{/if}
		{if ($store.insta_link|count_characters) >1 }
			<a href='{$store.insta_link|escape:'htmlall':'UTF-8'}' target='_blank' class='instagram'>
				<i class='fa fa-insta' aria-hidden='true'></i>
			</a>
		{/if}
</div>
	</div>
</div>


<div class="pageheading-3">
		{if $store.has_picture}<img style="width:100%" src='/img/st/{$store.id_store|escape:'htmlall':'UTF-8'}.jpg' alt='' />{/if}
</div>

<div class="pageheading-3">
	{$store.html_desc|cleanHtml nofilter}
</div>

 </div>

</div>

<div id="store-page-describe">
		<h3 class="store-describe-header">{l s='Umywalki SEO' mod='freestorelocator'}</h3>
		<p>
		{l s='Dobra umywalka do łazienki powinna być trwała i łatwa w utrzymaniu czystości. W IKEA znajdziesz wiele modeli umywalek, które możesz dopasować do niemal każdego wnętrza. Na wszystkie umywalki ceramiczne oraz wykonane z kruszonego marmuru (w tym umywalki nablatowe) oferujemy 10 lat bezpłatnej gwarancji. ' mod='freestorelocator'}
		</p>
		<p>
			<b>{l s='Umywalki łazienkowe ' mod='freestorelocator'}</b><br>
			{l s='Warto zadbać, by umywalka łazienkowa była dopasowana do metrażu pomieszczenia. Umywalka z szafką podumywalkową to sprytne rozwiązania dla właścicieli niewielkich łazienek. Dzięki nim zyskasz mnóstwo miejsca do przechowywania. Umywalka podwójna lepiej sprawdzi się w przestronnych łazienkach.' mod='freestorelocator'}
		</p>

		<p>
			<b>{l s='Umywalka do łazienki' mod='freestorelocator'}</b><br>
			{l s='Umywalki łazienkowe wytwarzane z solidnych materiałów posłużą ci przez wiele lat. W IKEA znajdziesz wiele modeli do wyboru, które można wykorzystać w każdej łazience – nawet tej niewielkiej. Umywalka mała, wcale nie musi oznaczać umywalkę niepraktyczną. Jeśli zdecydujesz się na umywalkę nablatową, możesz wykorzystać blat wokół niej jako miejsce na kosmetyki i akcesoria łazienkowe. ' mod='freestorelocator'}
		</p>
		<p>
			{l s='Warto zadbać, by umywalka łazienkowa była dopasowana do metrażu pomieszczenia. Umywalka z szafką podumywalkową to sprytne rozwiązania dla właścicieli niewielkich łazienek. Dzięki nim zyskasz mnóstwo miejsca do przechowywania. Umywalka podwójna lepiej sprawdzi się w przestronnych łazienkach. Umywalki łazienkowe wytwarzane z solidnych materiałów posłużą ci przez wiele lat. W IKEA znajdziesz wiele modeli do wyboru, które można wykorzystać w każdej łazience – nawet tej niewielkiej. Umywalka mała, wcale nie musi oznaczać umywalkę niepraktyczną. Jeśli zdecydujesz się na umywalkę nablatową, możesz wykorzystać blat wokół niej jako miejsce na kosmetyki i akcesoria łazienkowe.' mod='freestorelocator'}
		</p>
</div>


<!-- micro data -->
{literal}
	<script type="application/ld+json">
	{
		"@context": "http://schema.org",
		"@type": "LocalBusiness",
		{/literal}"name": "{$store.name|escape:'htmlall':'UTF-8'} ",{literal}
		"address": {
			"@type": "PostalAddress",
			{/literal}"streetAddress": "{$store.address1|escape:'htmlall':'UTF-8'} {$store.address2|escape:'htmlall':'UTF-8'} ",{literal}
			{/literal}"addressLocality": "{$store.city|escape:'htmlall':'UTF-8'}",{literal}
			{/literal}"postalCode": "{$store.postcode|escape:'htmlall':'UTF-8'}"{literal}
		},
		{/literal}{if isset($store.phone) && ($store.phone|count_characters >5)}"telephone": "{$store.phone|escape:'htmlall':'UTF-8'}",{/if}{literal}
		{/literal} {if isset($microdatahours[0])}
		"openingHours": [
		    {if isset($microdatahours[0])}"{$microdatahours[0].day|escape:'htmlall':'UTF-8'} {$microdatahours[0].hours|escape:'htmlall':'UTF-8'}"{if isset($microdatahours[1])},{/if}{/if}
		    {if isset($microdatahours[1])}"{$microdatahours[1].day|escape:'htmlall':'UTF-8'} {$microdatahours[1].hours|escape:'htmlall':'UTF-8'}"{if isset($microdatahours[2])},{/if}{/if}
		    {if isset($microdatahours[2])}"{$microdatahours[2].day|escape:'htmlall':'UTF-8'} {$microdatahours[2].hours|escape:'htmlall':'UTF-8'}"{if isset($microdatahours[3])},{/if}{/if}
		    {if isset($microdatahours[3])}"{$microdatahours[3].day|escape:'htmlall':'UTF-8'} {$microdatahours[3].hours|escape:'htmlall':'UTF-8'}"{if isset($microdatahours[4])},{/if}{/if}
		    {if isset($microdatahours[4])}"{$microdatahours[4].day|escape:'htmlall':'UTF-8'} {$microdatahours[4].hours|escape:'htmlall':'UTF-8'}"{if isset($microdatahours[5])},{/if}{/if}
		    {if isset($microdatahours[5])}"{$microdatahours[5].day|escape:'htmlall':'UTF-8'} {$microdatahours[5].hours|escape:'htmlall':'UTF-8'}"{if isset($microdatahours[6])},{/if}{/if}
		    {if isset($microdatahours[6])}"{$microdatahours[6].day|escape:'htmlall':'UTF-8'} {$microdatahours[6].hours|escape:'htmlall':'UTF-8'}"{/if}		    
		],  {/if} {literal}
		"geo": {
			"@type": "GeoCoordinates",
			{/literal}"latitude": "{$store.latitude|escape:'htmlall':'UTF-8'}",{literal}
			{/literal}"longitude": "{$store.longitude|escape:'htmlall':'UTF-8'}"{literal}
		},
		"priceRange":"$"
	}
	</script>
{/literal}

  <!-- end microdata -->




<script type="text/javascript">
			// On initialise la latitude et la longitude de Paris (centre de la carte)

	
var url_map ="";
			 {if $darkmode ==1}
			 	{literal}
			 	url_map = "http://{s}.tile.openstreetmap.de/tiles/osmde/{z}/{x}/{y}.png"; // Blue
			 	{/literal}
			 {elseif $darkmode ==2}
			   {literal}
 				url_map = "http://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png"; // Grise	
 				{/literal}
			 {elseif $darkmode ==3}
			   {literal}
 				url_map = "http://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png";//Classique
 				{/literal}
			 {elseif $darkmode ==4}
			   {literal}
 				url_map = "http://{s}.tile.opentopomap.org/{z}/{x}/{y}.png"; // Carto
 				{/literal}
			 {elseif $darkmode ==5}
			   {literal}
 				url_map = "http://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png"; // Green
 				{/literal}
			 {elseif $darkmode ==6}
			   {literal}
 				url_map = "https://stamen-tiles-{s}.a.ssl.fastly.net/watercolor/{z}/{x}/{y}.jpg";//draw
 				{/literal}
			 {/if}
			{literal}
			


			var macarte = null;
            var zoom= 8;
			
			function initMap() {

				{/literal}
		     	var lat = {$store.latitude|escape:'htmlall':'UTF-8'};
			    var lon = {$store.longitude|escape:'htmlall':'UTF-8'};	
				{literal}
                macarte = L.map('map').setView([lat, lon],  zoom);
                macarte.scrollWheelZoom.disable();

 

                L.tileLayer(url_map, {
                    attribution: 'données © <a href="//osm.org/copyright">OpenStreetMap</a>/ODbL - rendu <a href="//openstreetmap.fr">OSM France</a>',
                    minZoom: 1,
                    maxZoom: 20
                }).addTo(macarte);

       
				var marker = L.marker([lat,lon]);			
				marker.addTo(macarte);
				var icon = marker.options.icon;
				icon.options.iconSize = [50, 50];
				marker.setIcon(icon);

            }
			setTimeout(function(){ initMap();  }, 2000);

		</script>
{/literal}
{/block}
