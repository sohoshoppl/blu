{*
* 2007-2021 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2021 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<div class="panel">
	<h3><i class="icon icon-credit-card"></i> {l s='Store locator' mod='freestorelocator'}</h3>
	<p>
		<strong>{l s='Congratulation, the module is correctly installed !' mod='freestorelocator'}</strong><br />
		{l s='You can now access to the new page:' mod='freestorelocator'} <a target="_blank" href="{$base_dir|escape:'htmlall':'UTF-8'}module/freestorelocator/page"> {$base_dir|escape:'htmlall':'UTF-8'}module/freestorelocator/page</a><br />
	</p>
	<br />
</div>

<div class="alert bg-info">
	<div class="row modules-addons-buttons">
		<a class="btn btn-default btn-primary _blank" href="index.php?controller=AdminStorelocator&token={$token}" target="_blank"><i class="icon-pencil"></i>  
			{l s='Configure les données enrichies de vos boutiques' mod='freestorelocator'}</a>
	</div>
</div>


