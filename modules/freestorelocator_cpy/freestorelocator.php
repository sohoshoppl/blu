<?php
/**
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

use PrestaShop\PrestaShop\Core\Module\WidgetInterface;

class Freestorelocator extends Module implements WidgetInterface
{
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'freestorelocator';
        $this->tab = 'front_office_features';
        $this->version = '4.1.2';
        $this->author = 'Evol.digital';
        $this->need_instance = 0;
        $this->module_key = '47fc5a2435a59103a1d661d1137170ed';
        $this->bootstrap = true;
        parent::__construct();
        $this->displayName = $this->l('Freestorelocator - Pages et carte des magasins ');
        $this->description = $this->l("Module permetant d'affichager une carte de magasins et une page par magasin");
        $this->ps_versions_compliancy = array('min' => '1.5', 'max' => _PS_VERSION_);

        $this->path = __PS_BASE_URI__.'modules/freestorelocator/';
    }

    public function install()
    {
        Configuration::updateValue('FREESTORELOCATOR_DARK_MODE', false);
        Configuration::updateValue('FREESTORELOCATOR_ZOOM_LEVEL', "6");
        Configuration::updateValue('FREESTORELOCATOR_LATITUDE', "46.486105");
        Configuration::updateValue('FREESTORELOCATOR_LONGITUDE', "3.070828");
        Configuration::updateValue('FREESTORELOCATOR_SEARCH', false);
        $this->installTable();
        return parent::install() && $this->registerHook('displayHome') && $this->registerHook('displayHeader') ;
    }

    public function uninstall()
    {
        Configuration::deleteByName('FREESTORELOCATOR_DARK_MODE');
        Configuration::deleteByName('FREESTORELOCATOR_ZOOM_LEVEL');
        Configuration::deleteByName('FREESTORELOCATOR_LATITUDE');
        Configuration::deleteByName('FREESTORELOCATOR_LONGITUDE');
        Configuration::deleteByName('FREESTORELOCATOR_SEARCH');


        $sql = array();
        $sql[] = 'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'storedatafsl`;';
        $sql[] = 'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'storedatafsl_lang`;';
        $sql[] = 'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'storedatafsl_shop`;';

        foreach ($sql as $query) {
            if (Db::getInstance()->execute($query) == false) {
                return false;
            }
        }


        return parent::uninstall();
    }

    public function installTable()
    {
        $sql = array();
        $sql[] = "CREATE TABLE `" . _DB_PREFIX_ . "storedatafsl` (
              `id_storedatafsl` int UNSIGNED NOT NULL,
              `id_store` int NOT NULL,
              `active` tinyint UNSIGNED NOT NULL,
              `display` tinyint UNSIGNED NOT NULL,
              `create_page` tinyint UNSIGNED NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;


            CREATE TABLE `" . _DB_PREFIX_ . "storedatafsl_lang` (
              `id_storedatafsl` int UNSIGNED NOT NULL,
              `htmldesc` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
              `web_link` varchar(255) NOT NULL,
              `facebook_link` varchar(255) NOT NULL,
              `insta_link` varchar(255) NOT NULL,
              `linkedin_link` varchar(255) NOT NULL,
              `id_shop` int UNSIGNED NOT NULL,
              `id_lang` int UNSIGNED NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

            CREATE TABLE `" . _DB_PREFIX_ . "storedatafsl_shop` (
              `id_storedatafsl` int UNSIGNED NOT NULL,
              `id_shop` int UNSIGNED NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

            ALTER TABLE `" . _DB_PREFIX_ . "storedatafsl`
              ADD PRIMARY KEY (`id_storedatafsl`);

            ALTER TABLE `" . _DB_PREFIX_ . "storedatafsl_shop`
              ADD PRIMARY KEY (`id_storedatafsl`,`id_shop`);

            ALTER TABLE `" . _DB_PREFIX_ . "storedatafsl`
              MODIFY `id_storedatafsl` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

            ALTER TABLE `" . _DB_PREFIX_ . "storedatafsl_shop`
              MODIFY `id_storedatafsl` int UNSIGNED NOT NULL AUTO_INCREMENT;
            COMMIT;";

        foreach ($sql as $query) {
            if (Db::getInstance()->execute($query) == false) {
                return false;
            }
        }
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        /**
         * If values have been submitted in the form, process.
         */
        $output='';
        $error=false;
        if (((bool)Tools::isSubmit('submitFreestorelocatorModule')) == true) {
            $long = Tools::getValue('FREESTORELOCATOR_LONGITUDE');

            if (!is_numeric($long)) {
                $output .= $this->displayError($this->l('The field : "Longitude center point" must be a floating point number'));
                $error=true;
            } else {
                if ($long == "" ||  !is_float($long +0)) {
                    $output .= $this->displayError($this->l('The field : "Longitude center point" must not be empty, it must be a floating point number'));
                    $error=true;
                }
            }

            
            $lat = Tools::getValue('FREESTORELOCATOR_LATITUDE');


            if (!is_numeric($lat)) {
                $output .= $this->displayError($this->l('The field : "Latitude center point" must be a floating point number'));
                $error=true;
            } else {
                if ($lat == "" || !is_float($lat +0)) {
                    $output .= $this->displayError($this->l('The field : "Latitude center point" must not be empty, it must be a floating point number'));
                    $error=true;
                }
            }
          

            $zoom = Tools::getValue('FREESTORELOCATOR_ZOOM_LEVEL');
            if ($zoom == "" ||  !ctype_digit($zoom) || (int)$zoom < 1 || (int)$zoom >19) {
                $output .= $this->displayError($this->l('The field : "Zoom level" must not be empty, it must be a numeric number between 1 and 19'));
                $error=true;
            }


            /* upload the marker */

            if (count($_FILES)>0) {
                if (null !== $_FILES['image'] && null !== $_FILES['image']['tmp_name'] && !empty($_FILES['image']['tmp_name'])) {
                    if ($error = ImageManager::validateUpload($_FILES['image'])) {
                        $output .= $this->displayError($this->l('Can\'t upload the file : Marker'));
                        $error=true;
                    } elseif (!($tmpName = tempnam(_PS_TMP_IMG_DIR_, 'PS')) || !move_uploaded_file($_FILES['image']['tmp_name'], $tmpName)) {
                        $output .= $this->displayError($this->l('Can\'t upload the file: Marker'));
                        $error=true;
                    } elseif (!ImageManager::resize($tmpName, dirname(__FILE__).'/views/img/marker-hover.png')) {
                        $output .= $this->displayError($this->l('Can\'t upload the file: Marker'));
                        $error=true;
                    }

                    if ($_FILES['image']['type'] != 'image/png') {
                        $output .= $this->displayError($this->l('The image extension must be .png '));
                        $error=true;
                    }
                    if (isset($tmpName)) {
                        unlink($tmpName);
                    }
                }

                if (null !== $_FILES['image2'] && null !== $_FILES['image2']['tmp_name'] && !empty($_FILES['image2']['tmp_name'])) {
                    if ($error = ImageManager::validateUpload($_FILES['image2'])) {
                        $output .= $this->displayError($this->l('Can\'t upload the file: Marker hover'));
                        $error=true;
                    } elseif (!($tmpName = tempnam(_PS_TMP_IMG_DIR_, 'PS')) || !move_uploaded_file($_FILES['image2']['tmp_name'], $tmpName)) {
                        $output .= $this->displayError($this->l('Can\'t upload the file: Marker hover'));
                        $error=true;
                    } elseif (!ImageManager::resize($tmpName, dirname(__FILE__).'/views/img/marker.png')) {
                        $output .= $this->displayError($this->l('Can\'t upload the file: Marker hover'));
                        $error=true;
                    }

                    if ($_FILES['image2']['type'] != 'image/png') {
                        $output .= $this->displayError($this->l('The image (Marker hover) extension must be .png '));
                        $error=true;
                    }
                    if (isset($tmpName)) {
                        unlink($tmpName);
                    }
                }
            }
                
         
            if (!$error) {
                $output .= $this->displayConfirmation($this->l('The information has been updated'));
                $this->postProcess();
            }
        }
        $this->context->smarty->assign('token', Tools::getAdminTokenLite('AdminStorelocator'));

      
        $base_dir = Tools::getShopDomainSsl(true, true) . __PS_BASE_URI__;
        $this->context->smarty->assign('base_dir', $base_dir);
        $this->context->smarty->assign('module_dir', $this->_path);

        $output .= '<span class="module_confirmation conf confirm alert alert-warning" style="display: block;">'.$this->l('You need to activate Prestashop\'s simplified url (Url rewriting) mode to use the module').'</span>;﻿﻿';

        $output .= $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');

        return $output.$this->renderForm();
    }


    public function hookDisplayHeader()
    {
        $this->context->controller->addJS($this->path . 'views/js/leaflet.js');
        $this->context->controller->addJS($this->path . 'views/js/leaflet-provider.js');
        $this->context->controller->addCSS($this->path . 'views/css/leaflet.css');
        $this->context->controller->addCSS($this->path . 'views/css/front.css');
    }

    public function hookDisplayHome()
    {
        return $this->getHP();
    }


    public function renderWidget($hookName = null, array $configuration = [])
    {
        return $this->getHP();
    }

    public function getWidgetVariables($hookName = null, array $configuration = [])
    {
        return false;
    }


    public function getHP()
    {
        $stores = Db::getInstance()->executeS('
            SELECT s.id_store AS `id`, s.*, sl.*
            FROM ' . _DB_PREFIX_ . 'store s
            ' . Shop::addSqlAssociation('store', 's') . '
            LEFT JOIN ' . _DB_PREFIX_ . 'store_lang sl ON (
            sl.id_store = s.id_store
            AND sl.id_lang = ' .(int)$this->context->language->id . '
            )
            WHERE s.active = 1');

        $addresses_formated = array();

        foreach ($stores as &$store) {

            //Données enrichies
            $stores_data = Db::getInstance()->executeS('
            SELECT * 
            FROM ' . _DB_PREFIX_ . 'storedatafsl s
            ' . Shop::addSqlAssociation('storedatafsl', 's') . '
            LEFT JOIN ' . _DB_PREFIX_ . 'storedatafsl_lang sl ON (
            sl.id_storedatafsl = s.id_storedatafsl
            AND sl.id_lang = ' .(int)$this->context->language->id . '
            )
            WHERE s.active = 1 AND s.id_store='.(int)$store['id_store']);

          
            if (isset($stores_data) && isset($stores_data[0])) {
                $store['web_link'] = $stores_data[0]['web_link'];
                $store['facebook_link'] = $stores_data[0]['facebook_link'];
                $store['insta_link'] = $stores_data[0]['insta_link'];

                if ($stores_data[0]['create_page'] == 0) {
                    $store['page'] = 0;
                } else {
                    $store['page'] = 1;
                }

                if ($stores_data[0]['active'] == 0) {
                    $store['display'] = 0;
                } else {
                    $store['display'] = 1;
                }
            } else {
                $store['web_link'] = "";
                $store['facebook_link'] = "";
                $store['insta_link'] = "";
                $store['display'] = 0;
            }

            $address = new Address();
            $address->country = Country::getNameById($this->context->language->id, $store['id_country']);
            $address->address1 = $store['address1'];
            $address->address2 = $store['address2'];
            $address->postcode = $store['postcode'];
            $address->city = $store['city'];

            $addresses_formated[$store['id_store']] = AddressFormat::getFormattedLayoutData($address);
            $hours =  $this->convertHours($store);
            $store['hours'] =  html_entity_decode($hours);
            $store['has_picture'] = file_exists(_PS_STORE_IMG_DIR_.(int)$store['id_store'].'.jpg');
            $store['link'] =  $this->context->link->getStorePageLink($store['id_store']);
        }


        if (Configuration::get('FREESTORELOCATOR_SEARCH') == "1") {
            $city = Tools::getValue('city');
            $zip = Tools::getValue('zip');

            if (isset($city)) {
                if (Tools::strtolower($city)== "paris") {
                    $lat =(float)"48.856614";
                    $lon =(float)"2.3522219000000177";
                } elseif (Tools::strlen($city)>3) {
                    $return = $this->getWebPage("https://nominatim.openstreetmap.org/search?city=". $city ."&format=json");
              
                    if (isset($return['content'])) {
                        $return_json = Tools::jsonDecode($return['content'], true);

                        $lat =(float)$return_json[0]['boundingbox'][0];
                        $lon =(float)$return_json[0]['boundingbox'][2];
                    }
                }
            }

            if (isset($zip)) {
                if (Tools::strlen($zip)>3) {
                    $return = $this->getWebPage("https://nominatim.openstreetmap.org/search?postalcode=". $zip ."&country=fr&format=json");
              
                    if (isset($return['content'])) {
                        $return_json = Tools::jsonDecode($return['content'], true);

                        $lat =(float)$return_json[0]['boundingbox'][0];
                        $lon =(float)$return_json[0]['boundingbox'][2];
                        $city ="";
                    }
                }
            }
        } else {
            $city ="";
            $zip ="";
        }


        if (!isset($lon) && !isset($lat)) {
            $lat =  (float)Configuration::get('FREESTORELOCATOR_LATITUDE');
            $lon = (float)Configuration::get('FREESTORELOCATOR_LONGITUDE');
        }




        $this->context->smarty->assign(array(
            'darkmode' => Configuration::get('FREESTORELOCATOR_DARK_MODE'),
            'lat' => $lat,
            'long' => $lon ,
            'zoom' => (int)Configuration::get('FREESTORELOCATOR_ZOOM_LEVEL'),
            'simplifiedStoresDiplay' => true,
            'stores' => $stores,
            'addresses_formated' => $addresses_formated,
            'city' => $city,
            'zip' => $zip,
            'search' => Configuration::get('FREESTORELOCATOR_SEARCH'),
        ));

        $template = $this->context->smarty->fetch(_PS_MODULE_DIR_.'freestorelocator/views/templates/front/pageblock.tpl');
        return $template;
        ;
    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     */
    protected function renderForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitFreestorelocatorModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(), /* Add values for your inputs */
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($this->getConfigForm()));
    }

    /**
     * Create the structure of your form.
     */
    protected function getConfigForm()
    {
        $image = '../modules'.DIRECTORY_SEPARATOR.$this->name.DIRECTORY_SEPARATOR.'views/img/marker-hover.png';
        if (file_exists($image)) {
            $image_url = '<img src="'.$image."?t=".time().'" alt="" style="width:75px" class="imgm img-thumbnail" />';
            $image_size = file_exists($image) ? filesize($image) / 1000 : false;
        }

        $image2 = '../modules'.DIRECTORY_SEPARATOR.$this->name.DIRECTORY_SEPARATOR.'views/img/marker.png';
        if (file_exists($image)) {
            $image_url2 = '<img src="'.$image2."?t=".time().'" alt="" style="width:75px" class="imgm img-thumbnail" />';
            $image_size2= file_exists($image2) ? filesize($image2) / 1000 : false;
        }

        $c = array(
           array('id' => 2, 'name' => 'Grey'),
           array('id' => 3, 'name' => 'Classic'),
           array('id' => 4, 'name' => 'Map'),
           array('id' => 5, 'name' => 'Green'),
           array('id' => 6, 'name' => 'Draw'));
         
        return array(
            'form' => array(
                'legend' => array(
                'title' => $this->l('Settings'),
                'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'type' => 'select',
                        'label' => $this->l('Display theme'),
                        'name' => 'FREESTORELOCATOR_DARK_MODE',
                            'options' => array(
                                'query' => $c,
                                'id' => 'id',
                                'name' => 'name'
                        ),
                        'required' => true,
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'desc' => $this->l('Default zoom of the map'),
                        'name' => 'FREESTORELOCATOR_ZOOM_LEVEL',
                        'label' => $this->l('Default zoom'),
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'desc' => $this->l('Default latitude of the center of the map'),
                        'name' => 'FREESTORELOCATOR_LATITUDE',
                        'label' => $this->l('Latitude center point'),
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'desc' => $this->l('Default Longitude of the center of the map'),
                        'name' => 'FREESTORELOCATOR_LONGITUDE',
                        'label' => $this->l('Longitude center point'),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Search by zipcode and city'),
                        'name' => 'FREESTORELOCATOR_SEARCH',
                        'is_bool' => true,
                        'desc' => $this->l('Display a search form and the three closest stores'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                    array(
                    'type' => 'file',
                    'label' => "Image Marker",
                    'desc' => $this->l('You can upload an image for the marker (100x100)'),
                    'name' => 'image',
                    'value' => true,
                    'required' => true,
                    'display_image' => true,
                    'image' => $image_url ? $image_url : false,
                    'size' => $image_size,
                    ),
                    array(
                    'type' => 'file',
                    'label' => "Image Marker Hover",
                    'desc' => $this->l('You can upload an image for the marker on hover event (100x100)'),
                    'name' => 'image2',
                    'value' => true,
                    'required' => true,
                    'display_image' => true,
                    'image' => $image_url2 ? $image_url2 : false,
                    'size' => $image_size2,
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );
    }

    /**
     * Set values for the inputs.
     */
    protected function getConfigFormValues()
    {
        return array(
            'FREESTORELOCATOR_DARK_MODE' => Configuration::get('FREESTORELOCATOR_DARK_MODE'),
            'FREESTORELOCATOR_ZOOM_LEVEL' => Configuration::get('FREESTORELOCATOR_ZOOM_LEVEL'),
            'FREESTORELOCATOR_LATITUDE' => Configuration::get('FREESTORELOCATOR_LATITUDE'),
            'FREESTORELOCATOR_LONGITUDE' => Configuration::get('FREESTORELOCATOR_LONGITUDE'),
            'FREESTORELOCATOR_SEARCH' => Configuration::get('FREESTORELOCATOR_SEARCH'),
        );
    }

    /**
     * Save form data.
     */
    protected function postProcess()
    {
        $form_values = $this->getConfigFormValues();

        foreach (array_keys($form_values) as $key) {
            Configuration::updateValue($key, Tools::getValue($key));
        }
    }

    public function convertHours($store)
    {
        $days= array();

        $days[1] = 'Mo';
        $days[2] = 'Tu';
        $days[3] = 'We';
        $days[4] = 'Th';
        $days[5] = 'Fr';
        $days[6] = 'Sa';
        $days[7] = 'Su';

        $days_datas = array();
        $hours = array();
        $hours = json_decode($store['hours']);

        $hours_datas = array();
        if (!empty($hours)) {
            for ($i = 1; $i < 8; $i++) {
                if (isset($hours[(int)$i - 1])) {
                    if ($hours[(int)$i - 1] != "closed" && $hours[(int)$i - 1] != "fermé" && $hours[(int)$i - 1] != "Fermé") {
                        $twotimeslots = false;
                        $h = $hours[(int)$i - 1][0];


                        $harray = explode("et", $h);

                        if (is_array($harray) && count($harray)>1) {
                            $twotimeslots = true;
                        }

                        if (!$twotimeslots) {
                            $harray = explode("&", $h);

                            if (is_array($harray) && count($harray)>1) {
                                $twotimeslots = true;
                            }
                        }

                        if (is_array($harray) && count($harray)>1) {
                            $twotimeslots = true;
                        }

                        if (!$twotimeslots) {
                            $harray = explode("/", $h);

                            if (is_array($harray) && count($harray)>1) {
                                $twotimeslots = true;
                            }
                        }

                        if (!$twotimeslots) {
                            $harray[0] =$h;
                        }
                            


                        foreach ($harray as $key => $value) {
                            //on supprime les espaces
                            $value = str_replace(' ', '', $value);

                            //on remplace les séparateur par des espaces
                            $value = str_replace('-', ' ', $value);
                            $value = str_replace('h', ':', $value);

                            $hht = explode(" ", $value);

                            if (is_array($hht) && count($hht)==2) {
                                $ho = explode(":", $hht[0]);
                                if (is_array($ho) && count($ho)>1) {
                                    if (Tools::strlen($hht[0]) == 5) {
                                        $hht[0].=':00';
                                    }
                                    if (Tools::strlen($hht[0]) == 3) {
                                        $hht[0].='00:00';
                                    }
                                } else {
                                    if (Tools::strlen($hht[0]) == 2) {
                                        $hht[0].=':00:00';
                                    }
                                }

                                $ho2 = explode(":", $hht[1]);
                                if (is_array($ho2) && count($ho2)>1) {
                                    if (Tools::strlen($hht[1]) == 5) {
                                        $hht[1].=':00';
                                    }
                                    if (Tools::strlen($hht[1]) == 3) {
                                        $hht[1].='00:00';
                                    }
                                } else {
                                    if (Tools::strlen($hht[1]) == 2) {
                                        $hht[1].=':00:00';
                                    }
                                }

                                if (Tools::strlen($hht[1]) < 8) {
                                    $hht[1]='';
                                }
                                if (Tools::strlen($hht[0]) < 8) {
                                    $hht[0]='';
                                }

                      
                                $hours_datas[$days[$i]][$key.'-']['from'] = $hht[0];
                                $hours_datas[$days[$i]][$key.'-']['to'] = $hht[1];
                            }
                        }
                    }
                }
            }


            return  html_entity_decode(Tools::jsonEncode($hours_datas));
            exit;
        }
        return false;
    }

    /**
 * Get a web file (HTML, XHTML, XML, image, etc.) from a URL.  Return an
 * array containing the HTTP server response header fields and content.
 */
    public function getWebPage($url)
    {
        $options = array(
            CURLOPT_RETURNTRANSFER => true,     // return web page
            CURLOPT_HEADER         => false,    // don't return headers
            CURLOPT_FOLLOWLOCATION => true,     // follow redirects
            CURLOPT_ENCODING       => "",       // handle all encodings
            CURLOPT_USERAGENT      => "spider", // who am i
            CURLOPT_AUTOREFERER    => true,     // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
            CURLOPT_TIMEOUT        => 120,      // timeout on response
            CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
            CURLOPT_SSL_VERIFYPEER => false     // Disabled SSL Cert checks
        );

        $ch      = curl_init($url);
        curl_setopt_array($ch, $options);
        $content = curl_exec($ch);
        $err     = curl_errno($ch);
        $errmsg  = curl_error($ch);
        $header  = curl_getinfo($ch);
        curl_close($ch);

        $header['errno']   = $err;
        $header['errmsg']  = $errmsg;
        $header['content'] = $content;
        return $header;
    }
}
