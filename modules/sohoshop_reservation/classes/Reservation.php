<?php
 /* @author    SohoSHOP
 * @copyright Copyright (c) 2019 SohoSHOP
 * @license   http://SohoSHOP.pl
 *
 * http://SohoSHOP.pl
 */
class Reservation extends ObjectModel
{
    public $id_reservation;
    public $id_cart;
    public $id_store;
    public $date_add;
    public $date_upd;

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'reservation',
        'primary' => 'id_reservation',
        'fields' => array(
            'id_cart' =>    array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
            'id_store' =>    array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
            'date_add' => array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
            'date_upd' => array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
            
        ),
    );
}
