{extends file=$layout}

{block name='content'}
    <section id="main">
        <div class="cart-grid row">
            <div class="cart-grid-body col-xs-12">
                <div class="cart-container">
                    <header>
                        <h1 class="h1">{l s='Twoje dane' d='Shop.Theme.Checkout'}</h1>
                        <ul class="hidden-sm-down">
                            <li>
                                <span class="reservationStep current">1</span>{l s='Twoje rezerwacje' d='Shop.Theme.Checkout'}
                            </li>
                            <li><span class="reservationStep current">2</span>{l s='Twoje dane' d='Shop.Theme.Checkout'}
                            </li>
                            <li>
                                <span class="reservationStep">3</span>{l s='Potwierdzenie' d='Shop.Theme.Checkout'}
                            </li>
                        </ul>
                    </header>
                    <div id="forms">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="cart-detailed-totals-header text-center">{l s='Mam już konto/logowanie' d='Shop.Theme.Checkout'}</div>
                                <form method="post"
                                      action="{url entity='module' name='sohoshop_reservation' controller='reserve'}">
                                    <input type="hidden" name="submitLogin" value="1">
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <input name="email" required type="email" class="form-control" value=""
                                                   placeholder="{l s='Twój adres e-mail' d='Shop.Theme.Checkout'}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <input name="password" required type="password" class="form-control"
                                                   value="" placeholder="{l s='Hasło' d='Shop.Theme.Checkout'}">
                                        </div>
                                    </div>
                                    <input class="btn btn-primary" type="submit" name="submit"
                                           value="{l s='Zaloguj się' d='Shop.Theme.Checkout'}">
                                    <div class="forgot-password">
                                        <a href="{url entity='password'}" rel="nofollow">
                                            {l s='Nie pamiętasz hasła?' d='Shop.Theme.Checkout'}
                                        </a>
                                    </div>
                                    <button type="button"
                                            class="btn btn-primary"><i
                                                class="fab fa-facebook-f"></i> {l s='Zaloguj się przez facebook' d='Shop.Theme.Checkout'}
                                    </button>
                                </form>
                            </div>
                            <div class="col-md-6">
                                <div class="cart-detailed-totals-header text-center">{l s='Rezerwacja bez logowania' d='Shop.Theme.Checkout'}</div>
                                <form method="post"
                                      action="{url entity='module' name='sohoshop_reservation' controller='reserve'}">
                                    <div id="company_active">
                                        <div class="form-group row">
                                            <legend class="text-center">{l s='Zamawiam jako:' d='Shop.Theme.Checkout'}</legend>
                                            <div class="col-xs-6">
                                                <input checked="checked" id="company_no" type="radio"
                                                       name="company_active" value="0">
                                                <label for="company_no">{l s='Osoba prywatne' d='Shop.Theme.Checkout'}</label>
                                            </div>
                                            <div class="col-xs-6">
                                                <input id="company_yes" type="radio" name="company_active" value="1">
                                                <label for="company_yes">{l s='Firma' d='Shop.Theme.Checkout'}</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row" id="company">
                                        <div class="col-sm-12">
                                            <input type="text" class="form-control" name="company" required
                                                   pattern={literal}"^\D+$"{/literal}                                                   placeholder="{l s='Nazwa firmy *' d='Shop.Theme.Checkout'}">
                                        </div>
                                    </div>
                                    <div id="firstname" class="form-group row">
                                        <div class="col-sm-12">
                                            <input type="text" class="form-control" name="firstname" required
                                                   pattern={literal}"^\D+$"{/literal}
                                                   placeholder="{l s='Imię *' d='Shop.Theme.Checkout'}">
                                        </div>
                                    </div>
                                    <div id="lastname" class="form-group row">
                                        <div class="col-sm-12">
                                            <input type="text" class="form-control" name="lastname" required
                                                   pattern={literal}"^\D+$"{/literal}
                                                   placeholder="{l s='Nazwisko *' d='Shop.Theme.Checkout'}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <input required class="form-control" name="address1" type="text" value=""
                                                   data-validation="address" data-validation-regexp={literal}"[a-zA-Z].*[0-9]|[0-9].*[a-zA-Z]$"{/literal}
                                                   placeholder="{l s='Adres *' d='Shop.Theme.Checkout'}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-6">
                                            <input required class="form-control" name="city" type="text" value=""
                                                   placeholder="{l s='Miasto *' d='Shop.Theme.Checkout'}">
                                        </div>
                                        <div class="col-sm-6">
                                            <input required class="form-control" name="postcode" type="text" value=""
                                                   data-validation="custom" data-validation-regexp={literal}"^[0-9]{2}\-[0-9]{3}$"{/literal}
                                                   placeholder="{l s='Kod pocztowy *' d='Shop.Theme.Checkout'}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <input required class="form-control" name="phone" type="text" value=""
                                                   pattern={literal}"^[1-9]\d{8}$"{/literal}
                                                   placeholder="{l s='Telefon *' d='Shop.Theme.Checkout'}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <input required class="form-control" name="email" type="email" value=""
                                                   placeholder="{l s='E-mail *' d='Shop.Theme.Checkout'}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <textarea class="form-control" rows="5" id="delivery_message"
                                                      name="delivery_message" {l s='Komentarz do zamówienia' d='Shop.Theme.Checkout'}></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <span class="custom-checkbox">
                                                <label>
                                                    <input name="createAccount" type="checkbox" value="1">
                                                    <span><i class="material-icons rtl-no-flip checkbox-checked">&#xE5CA;</i></span>
                                                    {l s='Chcę założyć konto w Blu.pl' d='Shop.Theme.Checkout'}
                                                </label>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <span class="custom-checkbox">
                                                <label>
                                                    <input name="terms" type="checkbox" value="1" required>
                                                    <span><i class="material-icons rtl-no-flip checkbox-checked">&#xE5CA;</i></span>
                                                    {l s='Przeczytałem i akceptuję Regulamin sklepu' d='Shop.Theme.Checkout'}
                                                    <small>{l s='Twoje dane będziemy przetwarzać zgodnie z naszą' d='Shop.Theme.Checkout'} <a
                                                                href="{url entity=cms id=2}">{l s='Polityką prywatności' d='Shop.Theme.Checkout'}</a></small>
                                                </label>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <span class="custom-checkbox">
                                                <label>
                                                    <input name="createAccount" type="checkbox" value="1">
                                                    <span><i class="material-icons rtl-no-flip checkbox-checked">&#xE5CA;</i></span>
                                                    {l s='Akceptuję powyższe zgody' d='Shop.Theme.Checkout'}
                                                </label>
                                            </span>
                                        </div>
                                    </div>
                                    <button type="submit" name="submitAddress"
                                            class="btn btn-primary">{l s='Rezerwuję' d='Shop.Theme.Checkout'}</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
{/block}
