$(function () {
    initFormValidator();
    company_personChooser();
    $('#company_active input[name="company_active"]').change(company_personChooser);
});

function company_personChooser(){
    let company_preson = $('#company_active input[name="company_active"]:checked').val();
    if(company_preson == '0'){
        $('#forms #company').hide().find('input').prop('required',false);
        $('#forms #firstname').show().find('input').prop('required',true);
        $('#forms #lastname').show().find('input').prop('required',true);
    }else{
        $('#forms #company').show().find('input').prop('required',true);
        $('#forms #firstname').hide().find('input').prop('required',false);
        $('#forms #lastname').hide().find('input').prop('required',false);
    }


}

function initFormValidator() {


    if (prestashop.language.iso_code == "pl") {
        $('input[name="postcode"]').mask("00-000", {placeholder: "XX-XXX"});
        $('input[name="vat_number"]').mask("0000000000", {placeholder: "XXXXXXXXXX"});
        $('input[name="phone"]').mask("000000000", {placeholder: "XXXXXXXXX"});
    }
    $.validate({
        validateOnBlur: true,
    });
}