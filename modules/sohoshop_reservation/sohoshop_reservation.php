<?php
/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 */

use PrestaShop\PrestaShop\Core\Payment\PaymentOption;

if (!defined('_PS_VERSION_')) {
    exit;
}

class Sohoshop_Reservation extends PaymentModule
{
    private $_html = '';
    private $_postErrors = [];

    public $checkName;
    public $address;
    public $extra_mail_vars;

    public function __construct()
    {
        $this->name = 'sohoshop_reservation';
        $this->tab = 'payments_gateways';
        $this->version = '1.0.0';
        $this->author = 'SohoShop.pl';
        $this->controllers = ['reserve'];

        $this->currencies = true;
        $this->currencies_mode = 'checkbox';

        $this->bootstrap = true;
        parent::__construct();

        $this->displayName = $this->trans('SohoShop - rezerwacje', [], 'Modules.Sohoshopreservation.Admin');
        $this->description = $this->trans('Skrócony process składania zamówienia, zaprojoektowany jako rezerwacje.', [], 'Modules.Sohoshopreservation.Admin');
        $this->ps_versions_compliancy = ['min' => '1.7.1.0', 'max' => _PS_VERSION_];

        if ((!isset($this->checkName) || !isset($this->address) || empty($this->checkName) || empty($this->address))) {
            $this->warning = $this->trans('The "Payee" and "Address" fields must be configured before using this module.', [], 'Modules.Sohoshopreservation.Admin');
        }
        if (!count(Currency::checkPaymentCurrencies($this->id))) {
            $this->warning = $this->trans('No currency has been set for this module.', [], 'Modules.Sohoshopreservation.Admin');
        }

        $this->extra_mail_vars = [
            '{check_name}' => Configuration::get('CHEQUE_NAME'),
            '{check_address}' => Configuration::get('CHEQUE_ADDRESS'),
            '{check_address_html}' => Tools::nl2br(Configuration::get('CHEQUE_ADDRESS')),
        ];
    }

    public function install()
    {
        $sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'reservation` (
    `id_reservation` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `id_cart` int(10) unsigned NOT NULL,
    `id_store` int(10) unsigned NOT NULL,
    `date_add` datetime NOT NULL,
    `date_upd` datetime NOT NULL,
    PRIMARY KEY (`id_reservation`),
    KEY `id_cart` (`id_cart`),
    KEY `id_store` (`id_store`)
) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8';

        foreach ($sql as $sq) {
            if (!Db::getInstance()->Execute($sq)) {
                return false;
            }
        }

        return parent::install()
            && $this->registerHook('displayAdminOrder')
            && $this->registerHook('actionFrontControllerSetMedia');
    }

    public function uninstall()
    {
        return 1
            && parent::uninstall();
    }

    private function _displayCheck()
    {
        return $this->display(__FILE__, './views/templates/hook/infos.tpl');
    }


    public function hookPaymentOptions($params)
    {
        if (!$this->active) {
            return;
        }

        $this->smarty->assign(
            $this->getTemplateVars()
        );

        $newOption = new PaymentOption();
        $newOption->setModuleName($this->name)
            ->setCallToActionText($this->trans('Pay by Check', [], 'Modules.Sohoshopreservation.Admin'))
            ->setAction($this->context->link->getModuleLink($this->name, 'validation', [], true))
            ->setAdditionalInformation($this->fetch('module:ps_checkpayment/views/templates/front/payment_infos.tpl'));

        return [$newOption];
    }

    public function hookPaymentReturn($params)
    {
        if (!$this->active) {
            return;
        }

        $state = $params['order']->getCurrentState();
        $rest_to_paid = $params['order']->getOrdersTotalPaid() - $params['order']->getTotalPaid();
        if (in_array($state, [Configuration::get('PS_OS_CHEQUE'), Configuration::get('PS_OS_OUTOFSTOCK'), Configuration::get('PS_OS_OUTOFSTOCK_UNPAID')])) {
            $this->smarty->assign([
                'total_to_pay' => Tools::displayPrice(
                    $rest_to_paid,
                    new Currency($params['order']->id_currency),
                    false
                ),
                'shop_name' => $this->context->shop->name,
                'checkName' => $this->checkName,
                'checkAddress' => Tools::nl2br($this->address),
                'status' => 'ok',
                'id_order' => $params['order']->id,
            ]);
            if (isset($params['order']->reference) && !empty($params['order']->reference)) {
                $this->smarty->assign('reference', $params['order']->reference);
            }
        } else {
            $this->smarty->assign('status', 'failed');
        }

        return $this->fetch('module:ps_checkpayment/views/templates/hook/payment_return.tpl');
    }

    public function checkCurrency($cart)
    {
        $currency_order = new Currency((int)($cart->id_currency));
        $currencies_module = $this->getCurrency((int)$cart->id_currency);

        if (is_array($currencies_module)) {
            foreach ($currencies_module as $currency_module) {
                if ($currency_order->id == $currency_module['id_currency']) {
                    return true;
                }
            }
        }

        return false;
    }

    public function getTemplateVars()
    {
        $cart = $this->context->cart;
        $total = $this->trans(
            '%amount% (tax incl.)',
            [
                '%amount%' => Tools::displayPrice($cart->getOrderTotal(true, Cart::BOTH)),
            ],
            'Modules.Sohoshopreservation.Admin'
        );

        $checkOrder = Configuration::get('CHEQUE_NAME');
        if (!$checkOrder) {
            $checkOrder = '___________';
        }

        $checkAddress = Tools::nl2br(Configuration::get('CHEQUE_ADDRESS'));
        if (!$checkAddress) {
            $checkAddress = '___________';
        }

        return [
            'checkTotal' => $total,
            'checkOrder' => $checkOrder,
            'checkAddress' => $checkAddress,
        ];
    }

    public function hookActionFrontControllerSetMedia($params)
    {

        $this->context->controller->registerStylesheet(
            $this->name,
            'modules/' . $this->name . '/views/' . $this->name . '.css',
            [
                'media' => 'all',
                'priority' => 2000,
            ]
        );


    }

    public function hookDisplayAdminOrder($params)
    {
        $order = new Order($params['id_order']);
        $sql = 'SELECT id_store FROM `'._DB_PREFIX_.'reservation` WHERE `id_cart` = '.(int)$order->id_cart;
        if($id_store = Db::getInstance()->getValue($sql)){
            $store = new Store($id_store);
            $this->smarty->assign('store', $store);
            $html = '
<div class="card">
  <div class="card-header">
    '.$this->trans('Wybrany salon', [], 'Modules.Sohoshopreservation.Admin').'
  </div>
  <div class="card-body">
        <address class="row">
            <div class="col-md-6">
                '.$store->postcode.' '.$store->city.'<br>
                '.$store->address1[$this->context->language->id].'<br>
            </div>
            <div class="col-md-6">
                tel. '.$store->phone.'<br>
                <a href="mailto:{$choosedSalon->email}">'.$store->email.'</a>
            </div>
        </address>
  </div>
</div>        
        ';
            return $html;
        }

    }
}
