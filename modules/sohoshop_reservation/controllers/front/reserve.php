<?php
/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 */

/**
 * @since 1.5.0
 */
class Sohoshop_ReservationReserveModuleFrontController extends ModuleFrontController
{
    public $ssl = true;

    /**
     * @see FrontController::initContent()
     */
    public function initContent()
    {
        parent::initContent();

        $this->context->smarty->assign([]);

        $this->setTemplate('module:' . $this->module->name . '/views/templates/front/reserve.tpl');

    }

    public function postProcess()
    {
        if (Tools::isSubmit('submitAddress')) {
            $email = Tools::getValue('email','');
            $id_customer = Customer::customerExists($email, true, true);
            if ($id_customer) {
                $this->errors[] = $this->trans('Adres e-mail jest już zajęty', [], 'Shop.Notifications.Error');
                return;
            }

            $customer = new Customer();
            $address = new Address();
            $company = Tools::getValue('company','');

            $customer->company = $company;
            $customer->lastname = ($company ? $company : Tools::getValue('lastname',''));
            $customer->firstname = ($company ? 'Firma' : Tools::getValue('firstname',''));

            $customer->email = $email;
            $customer->is_guest = 1;
            $customer->passwd = time();
            $customer->add();

            $address->id_customer = $customer->id;
            $address->id_country = 14; //14 -> Polska
            $address->alias = 'Adres';
            $address->company = $company;
            $address->lastname = ($company ? $company : Tools::getValue('lastname',''));
            $address->firstname = ($company ? 'Firma' : Tools::getValue('firstname',''));
            $address->address1 = Tools::getValue('address1','');
            $address->address2 = '-';
            $address->postcode = Tools::getValue('postcode','');
            $address->city = Tools::getValue('city','');;
            $address->phone_mobile = Tools::getValue('phone','00000000');;
            $address->add();
            $this->context->updateCustomer($customer);
            $this->makeReservation();
        }

        if (Tools::isSubmit('submitLogin')) {
            $login_form = $this->makeLoginForm()->fillWith(Tools::getAllValues());
            $res = $login_form->submit();
            if ($res) {
                //zalogowany
                $this->makeReservation();
            } else {
                dump($login_form->getErrors());
                die();
            }
        }
    }

    protected function makeReservation($id_address = 0)
    {

        $id_address = $this->getCustomerLastModAddressID($this->context->customer->id);
        if ($id_address) {
            $this->context->cart->id_address_delivery = $id_address;
            $this->context->cart->id_address_invoice = $id_address;
            $this->context->cart->update();

            $total = (float)$this->context->cart->getOrderTotal(true, Cart::BOTH);

            $this->module->validateOrder(
                (int)$this->context->cart->id,
                (int)Configuration::get('PS_OS_CHEQUE'),
                $total,
                $this->module->displayName,
                null,
                null,
                (int)$this->context->currency->id,
                false,
                $this->context->customer->secure_key
            );

            $choosedSalon = json_decode($this->context->cookie->choosedSalon, true);
            $id_salon = (int)$choosedSalon[$this->context->cart->id];
            require_once (_PS_ROOT_DIR_ . '/modules/'.$this->module->name.'/classes/Reservation.php');
            $reservation = new Reservation();
            $reservation->id_cart = (int)$this->context->cart->id;
            $reservation->id_store = $id_salon;
            $reservation->save();
            Tools::redirect('index.php?controller=order-confirmation&id_cart=' . (int)$this->context->cart->id . '&id_module=' . (int)$this->module->id . '&id_order=' . $this->module->currentOrder . '&key=' . $this->context->customer->secure_key);
        }
    }

    protected function getCustomerLastModAddressID($id_customer)
    {
        $sql = 'SELECT `id_address` FROM `' . _DB_PREFIX_ . 'address` WHERE `id_customer` = ' . (int)$id_customer . ' AND `active` = 1 AND `deleted` = 0 ORDER BY `date_upd` DESC';
        return Db::getInstance()->getValue($sql);
    }

    public function setMedia()
    {
        parent::setMedia();
        $this->addJqueryPlugin('mask');
        $this->registerStylesheet('form-validator-css', '/js/form-validator/theme-default.min.css', ['media' => 'all', 'priority' => 900]);
        $this->registerJavascript('form-validator-js', '/js/form-validator/jquery.form-validator.min.js', ['position' => 'bottom', 'priority' => 0]);
        $this->registerJavascript('form-validator-js-poland', '/js/form-validator/poland.js', ['position' => 'bottom', 'priority' => 0]);
        $this->registerJavascript('form-validator-js-html5', '/js/form-validator/html5.js', ['position' => 'bottom', 'priority' => 0]);
        $this->registerJavascript('form-validator-js-lang', '/js/form-validator/lang/pl.js', ['position' => 'bottom', 'priority' => 0]);
        $this->registerJavascript(
            $this->module->name,
            'modules/' . $this->module->name . '/views/' . $this->module->name . '.js',
            [
                'priority' => 2000,
            ]
        );

    }


    public function getBreadcrumbLinks()
    {
        $breadcrumb = parent::getBreadcrumbLinks();
        $breadcrumb['links'][] = [
            'title' => $this->getTranslator()->trans('Twoje rezerwacje', [], 'Shop.Theme.Global'),
            'url' => $this->context->link->getPageLink($this->php_self, true),
        ];

        return $breadcrumb;
    }

}
