<?php
class ManufacturerController extends ManufacturerControllerCore{
    
    
    protected function assignAll()
    {
        $bigLetters=array();
        $bigCount = 0;
        $smallCount = 0;
        $manufacturersVar = $this->getTemplateVarManufacturers();

        if (!empty($manufacturersVar)) {
            foreach ($manufacturersVar as $k => $manufacturer) {
                $filteredManufacturer = Hook::exec(
                    'filterManufacturerContent',
                    ['filtered_content' => $manufacturer['text']],
                    $id_module = null,
                    $array_return = false,
                    $check_exceptions = true,
                    $use_push = false,
                    $id_shop = null,
                    $chain = true
                );
                if (!empty($filteredManufacturer)) {
                    $manufacturersVar[$k]['text'] = $filteredManufacturer;
                }
            }
        }
        foreach(range('A', 'Z') as $element){
            $bigLetters[$bigCount] = $element;
            $bigCount++;
        }
        // var_dump($bigLetters);
        $this->context->smarty->assign([
            'brands' => $manufacturersVar,
            'letters' => $bigLetters
        ]);
    }
    
    protected function doProductSearch($template, $params = [], $locale = null)
    {
        if ($this->ajax) {
            ob_end_clean();
            header('Content-Type: application/json');
            $this->ajaxRender(json_encode($this->getAjaxProductSearchVariables()));

            return;
        } else {
            $variables = $this->getProductSearchVariables();
            $categories = [];
            foreach ($variables['products'] as $product) {
                $categoriesTmp = Product::getProductCategories($product->id_product);
                if (!empty($categoriesTmp)) {
                    $categories = array_merge($categories, $categoriesTmp);
                }
            }
            $categories = array_unique($categories);
            $categoriesObjs = [];
            foreach($categories as $category){
                $categoriesObjs[] = new Category($category);
            }
            
            $this->context->smarty->assign([
                'listing' => $variables,
                'categories' => $categoriesObjs
            ]);
            $this->setTemplate($template, $params, $locale);
        }
    }    
    
}
