<?php


class CartController extends CartControllerCore
{
    public function getBreadcrumbLinks()
    {
        $breadcrumb = parent::getBreadcrumbLinks();
        $breadcrumb['links'][] = [
            'title' => $this->getTranslator()->trans('Twoje rezerwacje', [], 'Shop.Theme.Global'),
            'url' => $this->context->link->getPageLink($this->php_self, true),
        ];

        return $breadcrumb;
    }
}
