<?php

class FrontController extends FrontControllerCore
{

    public function setMedia()
    {
        parent::setMedia();
        //$this->registerStylesheet('gothamFonts', 'http://fonts.cdnfonts.com/css/gotham', ['server'=>'remote','media' => 'all', 'priority' => 1]);

        //slider
        $this->registerStylesheet('swiperSlider', 'https://unpkg.com/swiper/swiper-bundle.min.css', ['server'=>'remote','media' => 'all', 'priority' => 100]);
        $this->registerJavascript('swiperSlider', 'https://unpkg.com/swiper/swiper-bundle.min.js', ['server'=>'remote', 'position' => 'bottom', 'priority' => 100]);

        //slider
    }
}
