<?php 

class FreestoreLocatorOverride extends FreestoreLocator{

    public function __construct()
    {
        $this->name = 'freestorelocator';
        $this->tab = 'front_office_features';
        $this->version = '4.1.2';
        $this->author = 'Evol.digital';
        $this->need_instance = 0;
        $this->module_key = '47fc5a2435a59103a1d661d1137170ed';
        $this->bootstrap = true;
        parent::__construct();
        $this->displayName = $this->l('Freestorelocator - Pages et carte des magasins ');
        $this->description = $this->l("Module permetant d'affichager une carte de magasins et une page par magasin");
        $this->ps_versions_compliancy = array('min' => '1.5', 'max' => _PS_VERSION_);

        $this->path = __PS_BASE_URI__.'modules/freestorelocator/';

        if(!$this->isRegisteredInHook('displayOnContactPage')){
            $this->registerHook('displayOnContactPage');
        }
    }

    public function hookDisplayOnContactPage(){
        return $this->getHP();
    }



    public function getSmartyVariables(){
        $stores = Db::getInstance()->executeS('
            SELECT s.id_store AS `id`, s.*, sl.*
            FROM ' . _DB_PREFIX_ . 'store s
            ' . Shop::addSqlAssociation('store', 's') . '
            LEFT JOIN ' . _DB_PREFIX_ . 'store_lang sl ON (
            sl.id_store = s.id_store
            AND sl.id_lang = ' .(int)$this->context->language->id . '
            )
            WHERE s.active = 1');

        $addresses_formated = array();

        foreach ($stores as &$store) {

            //Données enrichies
            $stores_data = Db::getInstance()->executeS('
            SELECT * 
            FROM ' . _DB_PREFIX_ . 'storedatafsl s
            ' . Shop::addSqlAssociation('storedatafsl', 's') . '
            LEFT JOIN ' . _DB_PREFIX_ . 'storedatafsl_lang sl ON (
            sl.id_storedatafsl = s.id_storedatafsl
            AND sl.id_lang = ' .(int)$this->context->language->id . '
            )
            WHERE s.active = 1 AND s.id_store='.(int)$store['id_store']);

          
            if (isset($stores_data) && isset($stores_data[0])) {
                $store['web_link'] = $stores_data[0]['web_link'];
                $store['facebook_link'] = $stores_data[0]['facebook_link'];
                $store['insta_link'] = $stores_data[0]['insta_link'];

                if ($stores_data[0]['create_page'] == 0) {
                    $store['page'] = 0;
                } else {
                    $store['page'] = 1;
                }

                if ($stores_data[0]['active'] == 0) {
                    $store['display'] = 0;
                } else {
                    $store['display'] = 1;
                }
            } else {
                $store['web_link'] = "";
                $store['facebook_link'] = "";
                $store['insta_link'] = "";
                $store['display'] = 0;
            }

            $address = new Address();
            $address->country = Country::getNameById($this->context->language->id, $store['id_country']);
            $address->address1 = $store['address1'];
            $address->address2 = $store['address2'];
            $address->postcode = $store['postcode'];
            $address->city = $store['city'];

            $addresses_formated[$store['id_store']] = AddressFormat::getFormattedLayoutData($address);
            $hours =  $this->convertHours($store);
            $store['hours'] =  html_entity_decode($hours);
            $store['has_picture'] = file_exists(_PS_STORE_IMG_DIR_.(int)$store['id_store'].'.jpg');
            $store['link'] =  $this->context->link->getStorePageLink($store['id_store']);
        }


        if (Configuration::get('FREESTORELOCATOR_SEARCH') == "1") {
            $city = Tools::getValue('city');
            $zip = Tools::getValue('zip');

            if (isset($city)) {
                if (Tools::strtolower($city)== "paris") {
                    $lat =(float)"48.856614";
                    $lon =(float)"2.3522219000000177";
                } elseif (Tools::strlen($city)>3) {
                    $return = $this->getWebPage("https://nominatim.openstreetmap.org/search?city=". $city ."&format=json");
              
                    if (isset($return['content'])) {
                        $return_json = Tools::jsonDecode($return['content'], true);

                        $lat =(float)$return_json[0]['boundingbox'][0];
                        $lon =(float)$return_json[0]['boundingbox'][2];
                    }
                }
            }

            if (isset($zip)) {
                if (Tools::strlen($zip)>3) {
                    $return = $this->getWebPage("https://nominatim.openstreetmap.org/search?postalcode=". $zip ."&country=fr&format=json");
              
                    if (isset($return['content'])) {
                        $return_json = Tools::jsonDecode($return['content'], true);

                        $lat =(float)$return_json[0]['boundingbox'][0];
                        $lon =(float)$return_json[0]['boundingbox'][2];
                        $city ="";
                    }
                }
            }
        } else {
            $city ="";
            $zip ="";
        }


        if (!isset($lon) && !isset($lat)) {
            $lat =  (float)Configuration::get('FREESTORELOCATOR_LATITUDE');
            $lon = (float)Configuration::get('FREESTORELOCATOR_LONGITUDE');
        }

        $stores = array(
            'darkmode' => Configuration::get('FREESTORELOCATOR_DARK_MODE'),
            'lat' => $lat,
            'long' => $lon ,
            'zoom' => (int)Configuration::get('FREESTORELOCATOR_ZOOM_LEVEL'),
            'simplifiedStoresDiplay' => true,
            'stores' => $stores,
            'addresses_formated' => $addresses_formated,
            'city' => $city,
            'zip' => $zip,
            'search' => Configuration::get('FREESTORELOCATOR_SEARCH'),
        );

        return $stores;
    }
}