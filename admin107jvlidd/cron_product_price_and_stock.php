<?php
/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */

if (!defined('_PS_ADMIN_DIR_')) {
    define('_PS_ADMIN_DIR_', __DIR__);
}
include _PS_ADMIN_DIR_ . '/../config/config.inc.php';

$secureKey = md5(_COOKIE_KEY_ . Configuration::get('PS_SHOP_NAME'));
//echo $secureKey;

if (isset($_GET['secure_key'])) {

    if (!empty($secureKey) && $secureKey === $_GET['secure_key']) {
        PrestaShopLogger::addLog('Pobranie danych z SOAP - Start');
        $eanFilter = '';

        $eansList = Tools::getValue('eans', false);
        if ($eansList) {
            $eanFilter = explode(',', $eansList);
        }

        flush();
        ob_flush();
        echo "Przygotowanie listy ean<br>" . PHP_EOL;

        $products = getproductsFromPrestaDB($eanFilter);

        $eans = array_map(function ($row) {
            return array(
                'id_product' => $row['id_product'],
                'EAN' => $row['ean13']
            );
        }, $products);

        flush();
        ob_flush();
        echo "Pobranie danych z SOAP<br>" . PHP_EOL;

        $soapData = getDataFromSoap($eans);
        $i = 1;
        foreach ($soapData as $soapDataRow) {
            //szukamy id_produktu na podstawie ean
            $key = array_search($soapDataRow->EAN, array_column($eans, 'EAN'));
            $product = new Product((int)$eans[$key]['id_product']);

            flush();
            ob_flush();
            echo "----------produkt:" . $product->id . '----------<br>' . PHP_EOL;

            if (Validate::isLoadedObject($product)) {

                flush();
                ob_flush();
                echo "Aktualizacja stanu magazynowego<br>" . PHP_EOL;

                StockAvailableCore::setQuantity($product->id, 0, (int)$soapDataRow->STAN);

                flush();
                ob_flush();
                echo "Aktualizacja ceny<br>" . PHP_EOL;

                $product->price = (float)$soapDataRow->CENA_KATALOGOWA_NETTO;
                $product->update();
                $i++;

                flush();
                ob_flush();
                echo "Aktualizacja promocyjnej<br>" . PHP_EOL;
                //czyścimy istniejące rabaty
                SpecificPrice::deleteByProductId($product->id);
                SpecificPriceRule::applyAllRules(array((int)$product->id));

                //sprawdzamy czy jest jakaś cena promocyjna i czy jest mniejsza od aktualnej
                $promoPrice = (float)$soapDataRow->CENA_PROMOCYJNA_NETTO;
                if ($promoPrice > 0 and $promoPrice < $product->price) {
                    $calculatedPromoPrice = $product->price - $promoPrice;
                    $spPriceData = [
                        'id_product' => $product->id,
                        'id_product_attribute' => 0,
                        'discount' => $calculatedPromoPrice,
                    ];
                    addSpecificPrice($spPriceData);
                }
            }
        }
        echo "KONIEC!<br>" . PHP_EOL;

        PrestaShopLogger::addLog('Pobranie danych z SOAP - zaktualizowano:' . $i);
    }
}

function getDataFromSoap($eans)
{
    $wsdl = 'http://refnet.refleks.pl/edi/server_prestashop.php?wsdl';

    try {
        $client = new SoapClient($wsdl, array(
            'exceptions' => true,
        ));

        $hash = 'Ex5LPjCm4ZQFHbfsR79BV';
        $response = $client->getTowaryPoEAN(json_encode($eans), $hash);

    } catch (SoapFault $e) {
        print_r($e);

    }
    $res = json_decode($response);

    if (!empty($res->result)) {
        return $res->result;
    }

    return false;
}

function getProductsFromPrestaDB($eans, $from = 0, $to = 3000)
{

    $where = "WHERE ean13 IS NOT NULL AND `ean13` != ''";
    if (!empty($eans)) {
        $where = "WHERE `ean13` IN (";
        foreach ($eans as $ean) {
            $where .= "'" . $ean . "',";
        }
        $where = trim($where, ',');
        $where .= ')';
    }
    $sql = 'SELECT id_product, ean13 FROM `ps_product` ' . $where . ' ORDER BY `ps_product`.`date_upd` ASC  LIMIT ' . $from . ' ,  ' . $to;
    return Db::getInstance()->executeS($sql);


}

/**
 * @param array $data elements id_product, id_product_attribute, discount,
 * @throws PrestaShopDatabaseException
 * @throws PrestaShopException
 */
function addSpecificPrice($data)
{
    $obj_spec_price = new SpecificPrice();
    $obj_spec_price->id_shop = Context::getContext()->shop->id;
    $obj_spec_price->id_currency = 0;
    $obj_spec_price->id_country = 0;
    $obj_spec_price->id_group = 0;
    $obj_spec_price->id_customer = 0;
    $obj_spec_price->reduction_tax = 0;
    $obj_spec_price->price = -1.00;
    $obj_spec_price->from = '0000-00-00 00:00:00';
    $obj_spec_price->to = '0000-00-00 00:00:00';
    $obj_spec_price->id_product = $data['id_product'];
    $obj_spec_price->id_product_attribute = (int)$data['id_product_attribute'];
    $obj_spec_price->reduction = $data['discount'];
    $obj_spec_price->from_quantity = 0;
    $obj_spec_price->reduction_type = 'amount';
    $obj_spec_price->add();
}